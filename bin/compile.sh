#!/bin/bash


function help {
  cat <<EOF
Usage: $0 [options]
Options:
--help
        Print this guide.
--compiler <compiler>
        Use the specified compiler. Default is gfortran, but make sure your
        installation is up to date if you use it.
--dir <directory>
        Directory for object and module files
Any other option
        Add to the list of compiler flags.
EOF
}

function die {
  echo $1
  exit
}

# [[ $# -eq 0 ]] && help && die

BASE=$PWD
[[ -z $COMPILER ]] && COMPILER=gfortran
ogl=""
FFLAGS=""

while [[ $# -gt 0 ]]; 
do
  case $1 in
    --help )
      help
      exit;;
    -c | --compiler )
      COMPILER=$2
      shift;;
    -d | --dir )
      DIR=$2
      shift;;
    -ogl )
      ogl=yes;;
    --rebuild )
      rm $COMPILER/exe/*.o $COMPILER/exe/*.mod;;
    -DOPENGL )
      FFLAGS="$FFLAGS $1"
      ogl=yes;;
    -DNOOPENGL )
      FFLAGS="$FFLAGS $1"
      ogl="";;
    * )
      FFLAGS="$FFLAGS $1";;
  esac
  shift
done

[[ $COMPILER == pgi ]] && COMPILER=pgf90
[[ -z $DIR ]] && DIR=$COMPILER/exe
here=$PWD
cd $DIR

function compile {
  name=${1%%.[fF]90}
  srcfile=$here/src/$1
  [[ -e $name.o && $name.o -nt $srcfile ]] && return
  echo $1
  $FC -c $FFLAGS $srcfile || die "Error in compilation"
}

case $COMPILER in
  nag | nagfor )
    FC=nagfor;;
  g95 )
    FC=g95;;
  gfortran )
    FC=gfortran;;
  pgi | pgf90 )
    FC=pgf90
    [[ $ogl ]] && die "Compiler $COMPILER cannot handle f03gl modules";;
  ifort )
    FC=ifort
    [[ $ogl ]] && echo "WARNING: Versions of $COMPILER before 11.1.059 cannot handle f03gl modules";;
  * )
    FFLAGS="$FFLAGS $1";;
esac

#  Always recompile orient.F90
[[ -e orient.o ]] && rm -f orient.o

#modules
[[ $ogl ]] && compile f03gl_gl.f90
[[ $ogl ]] && compile f03gl_glu.f90
[[ $ogl ]] && compile f03gl_glut.f90
compile version.f90
compile parameters.f90
compile indparams.f90
compile input.F90
compile output.f90
compile global.f90
compile utility.f90
compile timing.F90
compile indexes.f90
compile consts.f90
compile binomials.f90
compile derivs.f90
compile induction.f90
compile switches.f90
compile labels.f90
compile molecules.f90
compile sites.f90
compile interact.f90
compile names.f90
compile pairderiv.f90
compile random.f90
compile sizes.f90
compile sqrts.f90
compile types.f90
compile rotations.f90
[[ $ogl ]] && compile gl_module.F90
compile connect.f90
[[ $ogl ]] && compile marching_cubes.f90
compile nrtype.f90
compile nrutil.f90
compile gammafns.f90
compile sfns.F90
compile damping.f90
compile properties.f90
compile variables.f90
compile alphas.f90
compile orses_module.F90
compile autocor.f90
compile lj.f90
compile lattice.F90
compile monolayer.F90
compile geom.f90
compile dump.F90
compile options.f90
compile show_data.f90
compile plot.F90
compile force.F90
[[ $ogl ]] && compile display.F90
compile thermofns.f90
compile gmin.F90
compile optimize.F90
compile mdmc.F90
compile fit.F90
compile histograms.f90
compile statistics.F90
compile mc.F90
compile md.F90
compile tabulate.F90
compile virial_module.F90

# subroutines
compile analyse.F90
compile axes.f90
compile axis.f90
compile compare.f90
compile deriv_check.F90
compile displace.f90
compile edit.f90
compile extra.f90
compile ferrenberg.f90
compile fields.F90
compile fourier.f90
compile hdiag.f90
compile induce.f90
compile init.f90
compile interact.f90
compile listinds.f90
compile lists.f90
compile mdata.f90
compile position_molecules.f90
compile printq.f90
compile readpair.f90
compile recipes.f90
compile relative.f90
compile slice.F90
compile structure.f90
compile triple.f90
compile pair_es.f90
compile fieldsml.f90
compile induceml.f90
compile lattinfo.f90
compile mlinfo.f90
compile molapair.f90
compile pairlatt.f90
compile basinhop.F90
compile orient.F90
compile runorient.f90

