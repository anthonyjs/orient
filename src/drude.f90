MODULE drude


IMPLICIT NONE

PRIVATE
PUBLIC drude_alpha

DOUBLE PRECISION, ALLOCATABLE :: B(:,:), A(:,:), alpha_drude(:), mu(:,:)

CONTAINS

SUBROUTINE drude_alpha(m)

!  Generate a non-local dipole-dipole polarizability model from Drude
!  oscillator data.

!  NOTE that this routine generates polarizability tensors in Cartesian
!       (x,y,z) order, not in spherical (00,10,11c,11s) order.

!  (DRUDE) already read
!  MOLECULE name
!  site  q  [s]  (q is the Drude charge, s the spring constant, default 0.1)
!  ...
!  END

USE molecules, ONLY: molecule, mol
USE sites, ONLY: next, locate, find_site, name, nsites
USE input, ONLY: read_line, item, nitems, reada, readf, upcase, assert, die

!  Molecule index
INTEGER, INTENT(IN) :: m

DOUBLE PRECISION :: chg, spring, spring_default=0.1d0
INTEGER :: i, j, k, k1, k2, p, p1, p2, nb, ns, info
LOGICAL :: eof
CHARACTER(LEN=12) :: w
CHARACTER(LEN=6), ALLOCATABLE :: site_name(:)

allocate(alpha_drude(nsites), stat=info)
if (info > 0) call die("Can't allocate alpha_drude array",.true.)
alpha_drude = 0d0

ns = 0
data: do
  call read_line(eof)
  if (eof) call die("Unexpected end of input in drude_parser", .true.)
  do while (item < nitems)
    call reada(w)
    select case(upcase(w))
    case("END")
      exit data
    case("", "!", "NOTE")
      cycle
    !  case("MOLECULE")
    !    call reada(w)
    !    m = locate(w)
    case default
      !  Site drude parameters
      k = find_site(m,w)
      ns = ns + 1
      call readf(chg)
      call readf(spring)
      if (spring == 0d0) spring = spring_default
      alpha_drude(k) = chg**2 / spring
      print "(a,i0,a, f12.5)", "Site ", k, "  alpha ", alpha_drude(k)
    end select
  end do
end do data



allocate(B(3*ns,3*ns), A(3*ns,3*ns), site_name(ns), mu(3,0:ns), stat=info)
if (info > 0) call die("Can't allocate B and A arrays in drude_parser",.true.)
B = 0d0

k1 = mol(m)%head
p1 = 0
ns = 0
do while (k1 > 0)
  if (alpha_drude(k1) > 0d0) then
    ns = ns + 1
    site_name(ns) = name(k1)
    k2 = mol(m)%head
    p2 = 0
    do while (k2 > 0)
      if (alpha_drude(k2) > 0d0) then
        if (k1 == k2) then
          do i = 1,3
            B(p1+i,p1+i) = 1d0/alpha_drude(k1)
          end do
        else
          call thole_T(k1,k2,(alpha_drude(k1)*alpha_drude(k2))**(1d0/6d0), &
              B(p1+1:p1+3,p2+1:p2+3))
        end if
        p2 = p2+3
      end if
      k2 = next(k2)
    end do
    p1 = p1 + 3
  end if
  k1 = next(k1)
end do

nb = p1
call assert(nb==3*ns,"Dimension mismatch in drude_parser")

! !  Print matrix B
! p1 = 0
! do i=1,ns
!   p2 = 0
!   do j=1,ns
!     print "(/2a)", site_name(i), site_name(j)
!     print "(3f12.5)", (B(p1+p,p2+1:p2+3), p=1,3)
!     p2 = p2+3
!   end do
!   p1 = p1+3
! end do

!  Invert matrix B
A = 0d0
do i = 1,nb
  A(i,i) = 1d0
end do
call dposv("U",nb,nb, B,nb, A,nb, info)
select case(info)
case(0)
  !  Successful inversion
case(-8:-1)
  print "(a,i0,a)", "Argument ", -info, " has an illegal value"
  call die("Stopping")
case(1:)
  print "(a)", "Matrix is not positive definite"
  call die("Stopping")
end select

p1 = 0
do i=1,ns
  p2 = 0
  do j=1,ns
    print "(/2a)", site_name(i), site_name(j)
    print "(3f12.5)", (A(p1+p,p2+1:p2+3), p=1,3)
    p2 = p2+3
  end do
  p1 = p1+3
end do

!  Response to uniform field
print "(/a/a)", "Responses to uniform fields",                          &
    "             x           y           z"
mu = 0d0
ns = 0
k = mol(m)%head
do while (k > 0)
  if (alpha_drude(k) > 0d0) then
    ns = ns + 1
    do i = 1,3
      do p = i,nb,3
        mu(i,ns) = mu(i,ns) + A(3*(ns-1)+i,p)  !  i.e. A(3*ns+i,p)*1d0
      end do
    end do
    print "(a, 3f12.5)", site_name(ns), mu(:,ns)
    mu(:,0) = mu(:,0) + mu(:,ns)
  end if
  k = next(k)
end do
print "(/a, 3f12.5)", "Total ", mu(:,0)

deallocate(A, B, site_name, alpha_drude, mu)

END SUBROUTINE drude_alpha

SUBROUTINE thole_T(k1,k2,s,T)

USE sites, ONLY: sx

INTEGER, INTENT(IN) :: k1, k2
DOUBLE PRECISION, INTENT(IN) :: s
DOUBLE PRECISION, INTENT(OUT) :: T(3,3)

INTEGER :: i, j
DOUBLE PRECISION :: r(3), d, u, fe, ft
DOUBLE PRECISION, PARAMETER ::  a=0.572

!  Set up Thole dipole-dipole interaction tensor for sites k1 and k2.
r = sx(:,k2)-sx(:,k1)
d = sqrt(r(1)**2+r(2)**2+r(3)**2)
! print "(2i4, f12.5)", k1, k2, d
u = d/s
fe = 1d0 - exp(-a*u**3)
ft = 1d0 - (1d0 + a*u**3)*exp(-a*u**3)
! print "(2i4, 3f12.5)", k1, k2, d, ft, fe
do i=1,3
  do j=1,3
    T(i,j) = 3d0*ft*r(i)*r(j)/d**5
  end do
  T(i,i) = T(i,i) - fe/d**3
end do

END SUBROUTINE thole_T

END MODULE drude
