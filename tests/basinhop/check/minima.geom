Minimum 1, energy =   -95.029567 kJ/mol
Moments of inertia:    321.48797       999.22994       1181.3697      amu Angstrom^2
Water1 at     2.827319    2.804167   -1.304238 rotated by  158.932 deg about   0.350598 -0.844589 -0.404660
Water2 at    -1.825240   -5.158878   -0.935444 rotated by  122.081 deg about  -0.457203 -0.476466 -0.750963
Water3 at    -1.754485    3.493672    1.009633 rotated by   26.971 deg about   0.932282 -0.147584 -0.330257
Water4 at     0.752407   -1.138962    1.230049 rotated by   99.507 deg about  -0.481372  0.698496 -0.529514
Minimum 2, energy =  -116.590619 kJ/mol
Moments of inertia:    495.22968       495.22976       968.19458      amu Angstrom^2
Water1 at     2.870481    2.203032    0.387238 rotated by  109.481 deg about  -0.865348 -0.092174  0.492623
Water2 at    -2.656311   -2.353745   -0.804277 rotated by   84.900 deg about   0.365622 -0.095147  0.925887
Water3 at    -1.854252    2.674693   -1.628107 rotated by  125.969 deg about   0.954205 -0.266384 -0.136132
Water4 at     1.640081   -2.523980    2.045146 rotated by  113.944 deg about  -0.040181  0.961568  0.271610
Minimum 3, energy =   -85.452734 kJ/mol
Moments of inertia:    421.16059       619.11997       1023.7835      amu Angstrom^2
Water1 at     2.859743    2.620313    1.250481 rotated by  113.136 deg about   0.360742 -0.898930  0.248574
Water2 at    -2.859742   -2.620316   -1.250480 rotated by   82.786 deg about  -0.313732  0.833230  0.455303
Water3 at    -2.290963    2.467377    0.068980 rotated by  162.210 deg about   0.358180 -0.611116 -0.705864
Water4 at     2.290963   -2.467378   -0.068981 rotated by  105.720 deg about   0.874822  0.193973  0.443915
Minimum 4, energy =   -93.677782 kJ/mol
Moments of inertia:    441.44574       574.43879       1000.9010      amu Angstrom^2
Water1 at     2.353129    1.820455    2.527281 rotated by   78.010 deg about  -0.267726 -0.501051  0.822965
Water2 at    -2.410406   -1.825143   -2.552092 rotated by   78.440 deg about   0.287573  0.807799  0.514552
Water3 at    -2.664335    1.777463    1.213370 rotated by  170.329 deg about   0.496436 -0.373778 -0.783480
Water4 at     2.721611   -1.772783   -1.188541 rotated by  144.701 deg about   0.094878  0.663973  0.741713
Minimum 5, energy =  -100.505135 kJ/mol
Moments of inertia:    427.11398       609.69434       657.24776      amu Angstrom^2
Water1 at     2.940834    2.163736    0.851408 rotated by   66.628 deg about  -0.531598 -0.513818  0.673346
Water2 at    -1.766223   -2.521270    2.081563 rotated by  175.160 deg about   0.722853 -0.069545 -0.687493
Water3 at    -2.159014    1.932719   -0.568242 rotated by  156.316 deg about  -0.917079 -0.365567  0.159145
Water4 at     0.984404   -1.575183   -2.364726 rotated by  133.192 deg about   0.424980  0.031021  0.904671
Minimum 6, energy =   -93.063993 kJ/mol
Moments of inertia:    333.71147       977.64831       1154.3382      amu Angstrom^2
Water1 at     1.410736    3.936273    0.573606 rotated by   76.405 deg about  -0.377354 -0.322879  0.867959
Water2 at     0.083479   -0.588888   -1.795953 rotated by  164.403 deg about   0.581055  0.066380  0.811153
Water3 at    -3.324090    1.796607    1.252259 rotated by  151.166 deg about   0.322347 -0.932871  0.160764
Water4 at     1.829875   -5.143991   -0.029912 rotated by  171.874 deg about   0.401304  0.686459 -0.606407
