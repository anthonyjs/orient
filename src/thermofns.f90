MODULE thermofns

USE consts
USE parameters
USE molecules
USE options
USE sites
USE utility

PRIVATE
PUBLIC :: modes, thermofuncs, degsfreedom

INTERFACE
  SUBROUTINE hdiag (iegen,order,a,eivr,n)
  LOGICAL, INTENT(IN) :: order, iegen
  DOUBLE PRECISION :: a(:,:)
  DOUBLE PRECISION, INTENT(OUT) :: eivr(:,:)
  INTEGER, INTENT(IN) :: n
  END SUBROUTINE hdiag
END INTERFACE

DOUBLE PRECISION, ALLOCATABLE :: freq(:)

DOUBLE PRECISION, ALLOCATABLE, SAVE :: sxo(:,:),seo(:,:,:)

CONTAINS

!-----------------------------------------------------------------------

SUBROUTINE modes(ndim)

!     Computes the normal modes and frequencies
!     following Pohorille et al. JCP 87, 6070 (1987)
!     INPUT:
!     ndim   : number of degrees of freedom
!     second : second derivatives (ORIENT convention) from derivs
!     module "sites" : coordinates of sites in global coordinates
!     OUTPUT:
!     freq   : eigenfrequencies of normal modes

!     We adopt Pohorille's notation for clarity:
!     K matrix : block diagonal kinetic energy matrix (6N x 6N)
!     kblock : for each rigid body we have one 6x6 matrix which consists
!     of two blocks : left upper or "mass" diagonal submatrix
!     and right lower or inertia tensor matrix. Here kblock
!     is the 3 x 3 inertia tensor.
!     KD     : using the diagonalized kinetic energy tensor
!     we keep track of the diagonal of KD only
!     U      : eigenvector matrix (Pohorille's S)

USE autocor
USE derivs, second => deriv2
USE input, only : die, find_io
USE plot
USE types
IMPLICIT NONE

!     Note: subroutine "hdiag" returns the eigenvectors in the columns
!     of "u".

INTEGER :: ndim

DOUBLE PRECISION, PARAMETER :: thres=1.d-8   ! when frequency**2
! is below this value it is treated as zero

DOUBLE PRECISION, PARAMETER :: vsmall = 1.d-12
DOUBLE PRECISION ::  rxyz(3)
CHARACTER(LEN=1) :: imag(nhess)
CHARACTER(LEN=64) :: plotlabel

DOUBLE PRECISION ::  kblock(3,3),kd(nhess)
DOUBLE PRECISION :: urb(3,3), hessian(nhess,nhess),               &
    u(nhess,nhess), tus(nhess,nhess), product(nhess,nhess)

DOUBLE PRECISION ::  nmdisp(nhess),nmadisp(nhess),qmat(nhess,nhess)
DOUBLE PRECISION ::  moments(4),moms(5,3),dmudq(3)
DOUBLE PRECISION ::  oscistrength,h_num,frequency
LOGICAL :: degfrec_flag
DOUBLE PRECISION ::  lastfreq, sumosci, rmass, summass

INTEGER :: col, base, animation_unit=25
LOGICAL :: open

CHARACTER(LEN=32) :: vecfile, animfile, intensfile

INTEGER :: i, i1, i2, i3, imol, ineg, it1, it2, itel, j, j2,      &
    k, kk, ll, m1, m2

CHARACTER(LEN=2) :: atname

if (.not. decfrequency) return

!  Initialization

do i=1,ndim
  imag(i)=' '
  do j=1,ndim
    u(i,j)=0.d0
  end do
end do

!  Rearrange "second" matrix from 4 dimensional into 2 dimensional

it1=0
do m1=1,mols
  do kk=1,6
    it1=it1+1
    it2=0
    do m2=1,mols
      do ll=1,6
        it2=it2+1
        hessian(it1,it2)=0.5d0*(second(kk,ll,m1,m2)             &
            +second(ll,kk,m2,m1))
      end do
    end do
  end do
end do
!     print "(/A)", "Hessian (milliHartree)"
!     call matwrt(hessian(1:it1,1:it2),6)

col=0
base=0
!  Construction of the blocks of the K matrix
do imol=1,mols         ! loop over all molecules
  summass=0d0
  kblock=0.d0
  k=head(imol)
  do while (k .ne. 0)
    if (type(k) .gt. 0) then
      rmass=mass(type(k))
      do i=1,3
        rxyz(i)=sx(i,k)-sx(i,cm(imol)) ! refer to molecular c.m.
      end do
      do i=1,3
        kblock(i,i)=kblock(i,i)+                                  &
            rmass*(rxyz(1)**2+rxyz(2)**2+rxyz(3)**2)
        do j=1,3
          kblock(i,j)=kblock(i,j)-rmass*rxyz(i)*rxyz(j)
        end do
      end do
      summass=summass+rmass
    endif
    k=next(k)
  end do                 ! end loop over sites of rigid body imol
  if (summass .eq. 0d0) call die                               &
      ('No masses defined for '//name(head(imol)),.false.)

  !  Construction of U: translation coordinates.
  do i=1,3
    col=col+1
    u(base+i,col)=1d0
    kd(col)=1d0/sqrt(summass)
  end do
  base=base+3

  !  Eigenvalues and eigenvectors of inertia matrix for this molecule
  !  (not ordered)
  call hdiag(.true.,.false.,kblock,urb,3)

  !  Construction of U: rotation coordinates.
  do i=1,3
    if (kblock(i,i) .gt. vsmall) then
      col=col+1
      kd(col)=1d0/sqrt(kblock(i,i))
      do j=1,3
        u(base+j,col)=urb(j,i)
      end do
      !         else
      !     print '(A,I3,A,1p,e12.3/A)',
      !     &      'The inertia tensor of molecule',imol,
      !     &      ' has a small eigenvalue ', kblock(i,i),
      !     &      'This degree of freedom has been projected out.'
    endif
  end do
  base=base+3

end do                  ! end loop over all molecules

!      print '(A)', 'U matrix'
!      do i=1,ndim
!        print '(12f12.5)', (u(i,j),j=1,col)
!      end do
!      print '(A)', 'kd^(-1/2)'
!      print '(12f12.5)', (kd(i), i=1,col)

!     Construction of the [KD^(-1/2) U(t) H U KD^(-1/2)] matrix

do i=1,ndim
  do j=1,col
    tus(i,j)=0.d0
    do k=1,ndim
      tus(i,j)=tus(i,j) + hessian(i,k)*u(k,j)*kd(j)
    end do
  end do
end do
do i=1,col
  do j=1,col
    product(i,j)=0.d0
    do k=1,ndim
      product(i,j)=product(i,j) + kd(i)*u(k,i)*tus(k,j) ! times U transpose
    end do
  end do
end do

!     print '(A)', 'Kd(-1/2) [U^T H U]  Kd(-1/2) * 1E6'
!     do i=1,col
!       print '(9f11.4)', (1E6*product(i,j),j=1,col)
!     end do

!     Eigenvalues and eigenvectors of matrix constructed above, arranged
!     in increasing order

call hdiag(.true.,.true.,product,tus,col)

!     Find negative eigenvalues
!     Values that are negative but very small are not counted
ineg=0
do i=1,col
  if (product(i,i) .lt. -thres) ineg=ineg+1
  if (product(i,i) .lt. 0d0)  imag(i)='i'
end do

inquire (unit=66,opened=open)
if (openfile .ne. freqfile) then
  if (open) close(66)
  open=.false.
  openfile=""
endif
if (.not. open .and. freqfile .ne. "") then
  open(unit=66,file=pfile(freqfile),status='unknown')
  open=.true.
  openfile=freqfile
endif

!     Fill freq
if (.not. allocated(freq)) allocate(freq(6*nmols))
do i=1,6*nmols
  freq(i)=0.0d0
end do
do i=col,1,-1
  freq(col-i+1)=freqcv*sqrt(abs(product(i,i)))
end do

if (open) write(66,'(f20.10,a)')                                   &
    (freqcv*sqrt(abs(product(i,i))),imag(i),i=col,1,-1)
write(6,'(/A)')                                                    &
    'Frequencies of the intermolecular modes (cm-1):'
print '(/6(f11.4,a1))',                                            &
    (freqcv*sqrt(abs(product(i,i))),imag(i),i=1,col)


!     At this stage we have the matrix of eigenvectors in mass-weighted
!     space. We need to back transform into cartesian space.

!     Print out the normal mode displacements. We print out two files
!     the names of which are based on freqfile with extensions _vec.xyz
!     and _anim.xyz. The first prints the xyz and delta xyz coords for
!     use in xmol with vectors turned on. The second prints out
!     consecutive animations of each normal mode.

!     The Eigenvectors are in the matrix tus. We need to perform the
!     following transformation :-
!
!     x = U  D^-1/2  q   (where q is a column of tus)
!     -   =  =       -
!

!     q are in tus
!     -

do i1=1,col
  do i2=1,col
    tus(i1,i2)=kd(i1)*tus(i1,i2)
  end do
end do

!     D^-1/2  q are in tus
!     =       -

!      write(6,'(a)') 'After first transformation'

!      do i1=1,col
!         write(6,'(12f10.5)') (tus(i1,i2),i2=1,col)
!      end do

do i1=1,ndim
  do i2=1,col
    qmat(i1,i2)=0d0
    do i3=1,col
      qmat(i1,i2)=qmat(i1,i2)+u(i1,i3)*tus(i3,i2)
    end do
  end do
end do

!     U  D^-1/2  q are in qmat
!     =  =       -

!      write(6,'(a)') 'After second transformation'

!      do i1=1,ndim
!         write(6,'(12f10.5)') (qmat(i1,i2),i2=1,col)
!      end do

!  Set the file names

if (freqfile .eq. ' ') then
  vecfile="vec.xyz"
  animfile="mode"
  intensfile="intens.dat"
else
  vecfile=trim(freqfile)//"_vec.xyz"
  animfile=trim(freqfile)//"_mode"
  intensfile=trim(freqfile)//"_intens.dat"
endif

if (vectors_flag) then
  open(39,file=vecfile,status='UNKNOWN')
end if
if (intensities_flag) then
  open(41,file=intensfile,status='UNKNOWN')
end if

!     Store the sx and se arrays as we will need to restore them for
!     each normal mode.

if (.not. allocated(sxo)) allocate(sxo(3,nsites),seo(3,3,nsites))
do i=1,mols
  i2=cm(i)
  do while (i2.ne.0)
    sxo(1,i2)=sx(1,i2)
    sxo(2,i2)=sx(2,i2)
    sxo(3,i2)=sx(3,i2)
    do i3=1,3
      seo(1,i3,i2)=se(1,i3,sm(i2))
      seo(2,i3,i2)=se(2,i3,sm(i2))
      seo(3,i3,i2)=se(3,i3,sm(i2))
    end do
    i2=NEXT(i2)
  end do
end do

!     Loop over columns of matrix u (which are the eigenvectors of the
!     mass-weighted hessian and therefore the normal mode displacements)

degfrec_flag=.false.
lastfreq=0.0d0
sumosci=0.0d0

do i=1,col

  !     Displace the molecules according the the relevant normal mode

  do i2=1,ndim
    nmdisp(i2)=qmat(i2,i)
  end do

  !         write(6,'(a,i)') 'mode ',i

  !         write(6,'(12f10.5)') (nmdisp(i2),i2=1,ndim)

  call displace(nmdisp)

  !     Print out the xyz and delta xyz coords (This code adapted from
  !     xyzoutput subroutine)

  itel=0
  do imol=1,mols
    k=head(imol)
    do while (k .ne. 0)
      if (type(k) .gt. 0) then
        if (tlabel(type(k)) .ne. 'none') itel=itel+1
      endif
      k=next(k)
    end do
  end do

  if (vectors_flag) then

    write(39,'(I3/A,I3,A,F12.7,2a)')                               &
        itel,"Mode ",i," Freq = ",                                &
        freqcv*sqrt(abs(product(i,i))),imag(i)," cm-1"


    do imol=1,mols
      k=head(imol)
      do while (k .ne. 0)
        if (type(k) .gt. 0) then
          if (tlabel(type(k)) .ne. ' ') then
            atname=tlabel(type(k))
          else if (atnum(type(k)) .gt. 0                         &
              .and. atnum(type(k)) .le. 18) then
            atname=element(atnum(type(k)))
          else
            atname=tname(type(k))
          endif
          if (atname .ne. 'no')                                  &
              write(39,'(a,6(f12.7))')                           &
              atname,                                            &
              (bohrva*sxo(i3,k),i3=1,3),                         &
              (bohrva*(sx(i3,k)-sxo(i3,k)),i3=1,3)

        endif
        k=next(k)
      end do
    end do
  end if !vectors_flag

  !     Print out frames for animation

  if (animations_flag) then

    animation_unit=find_io(25)
    open(animation_unit,                                         &
        file=trim(animfile)//"_"//trim(stri(i))//".xyz",         &
        status='UNKNOWN')

    do j=1,nframes

      !     First restore geometry

      call mode_restore

      !     Scale nmadisp accordingly

      do j2=1,6*mols
        nmadisp(j2)=nmdisp(j2)*                                  &
            sin(2.0d0*3.1415927d0*(j-1)/nframes)

      end do

      call displace(nmadisp)

      write(unit=plotlabel,fmt='(a,i3,a,f12.7,2a,i3,a)')          &
          "Mode ",i,", Freq = ",                                  &
          freqcv*sqrt(abs(product(i,i))),imag(i)," cm-1 (Frame ", &
          j,")"

      call xyzoutput(animation_unit,plotlabel)

    end do                 ! loop over frames
    close(animation_unit)
  end if !animations_flag

  !  Calculate intensities
  if (intensities_flag) then
    if (i==1) then
      print "(/2a)", "Intensities written to file ", trim(intensfile) 
      write (41,"(8x,a,16x,a/a)") "Frequency", "Oscillator strength",  &
          "         [cm-1]          [dimensionless]   [m^2 s^-1 mol^-1]"
    end if
    h_num=1.0d-6
    do j=1,5
      !  Restore geometry

      call mode_restore

      !  Scale nmadisp accordingly
      do j2=1,6*mols
        nmadisp(j2)=nmdisp(j2)*dble(j-2)*h_num
      end do
      call displace(nmadisp)
      !  Calculate dipole moments
      call calc_moments(moments)
      do j2=1,3 !@!
        moms(j,j2)=moments(j2+1)
      end do
    end do!j
    !  Calculate first derivative, oscistrength and frequency
    oscistrength=0.0d0
    do j=1,3
      dmudq(j)=1.0d0/(12.0d0*h_num)*(- 3.0d0*moms(1,j)                 &
          -10.0d0*moms(2,j)              &
          +18.0d0*moms(3,j)              &
          - 6.0d0*moms(4,j)              &
          +       moms(5,j))
      oscistrength=oscistrength+dmudq(j)**2   ![e^2]
    end do
    oscistrength=oscistrength/3.0d0
    frequency=freqcv*sqrt(abs(product(i,i)))
    !  Check for degeneracy and sum up accordingly
    if (frequency .ne. 0.0d0) then
      if (abs(lastfreq/frequency-1.0d0).gt. 1D-8) then
        if (degfrec_flag) then
          write(unit=41,fmt='(f20.12,1X,e20.12,1X,e20.12,A)')         &
              lastfreq, sumosci, 1.598280718D18*sumosci,              &
              ' degensum'
          degfrec_flag=.false.
        end if
        lastfreq=frequency
        sumosci=oscistrength
      else
        sumosci=sumosci+oscistrength
        degfrec_flag=.true.
      end if
    end if
    !  Write to oscistrength file
    write(unit=41,fmt='(f20.12,a,1p,e20.12,1X,e20.12)')          &
        frequency, imag(i), oscistrength, 1.598280718D18*oscistrength
    !          [cm^-1]       [n.u.]           [m^2/(s mol)]
  end if!intensities_flag
  !  End calculate intensities


  !     Restore previous geometry
  call mode_restore

end do                    ! loop over normal modes

close(39)

if (degfrec_flag) then
  write(unit=41,fmt='(1p,e20.12,A,e20.12,A,e20.12,A)')           &
      lastfreq,' ',sumosci,' ',1.598280718D18*sumosci,' degensum'
end if

close(41)

deallocate(sxo,seo)

END SUBROUTINE modes

!-----------------------------------------------------------------------

!     Subroutine to restore geometry. This is used at a number of
!     points in subroutine mode.

SUBROUTINE mode_restore

IMPLICIT NONE

INTEGER :: i,j,k

do i=1,mols
  j=cm(i)
  do while (j.ne.0)
    sx(1,j)=sxo(1,j)
    sx(2,j)=sxo(2,j)
    sx(3,j)=sxo(3,j)
    do k=1,3
      se(1,k,j)=seo(1,k,sm(j))
      se(2,k,j)=seo(2,k,sm(j))
      se(3,k,j)=seo(3,k,sm(j))
    end do
    j=NEXT(j)
  end do
end do

END SUBROUTINE mode_restore

!-----------------------------------------------------------------------

SUBROUTINE thermofuncs(mineng)

IMPLICIT NONE

INTEGER :: itemp,ncomplex,i,nconstituents
DOUBLE PRECISION :: temp,denergy,dtentropy,d_0,mineng,denthalpy

if (.not. thermofuncs_flag) return

call degsfreedom(ncomplex,nconstituents)

!  Output of the input
print '(/A/,A,I4,A/)',                                             &
    'Calculate Thermodynamical Functions',                       &
    '(from ',ncomplex,' Normal Mode Frequencies):'
print '(A,f20.4,A)',                                               &
    'Pressure               : ',tf_pressure,' Pascal'
print '(A,f20.10,A,A)',                                            &
    'Minimum Energy D_e     : ',mineng*efact,' ',eunit

!  Calculate dissociation energy
d_0=mineng
do i=1,ncomplex
  d_0=d_0-0.5d0*freq(i)/EHVCMM      !cm^-1 to hartree
end do
print '(A,f20.10,A,A)', 'Dissociation Energy D_0: ',               &
    d_0*efact,' ',eunit

!  Output of temperature range
if (tf_tempgrid .gt. 1) then
  print '(A,f20.4,A)',   'Minimum Temperature    : ',               &
      tf_tempmin*EHVK,' K'
  print '(A,f20.4,A/)',  'Maximum Temperature    : ',               &
      tf_tempmax*EHVK,' K'
else
  print '(A,f20.4,A/)',  'Temperature            : ',               &
      tf_tempmin*EHVK,' K'
end if

!  File of the thermodynamic functions
open(unit=99,file=tf_file,status='unknown')

!  Temperature loop
do itemp=1,tf_tempgrid
  !  Temperature value
  if (tf_tempgrid .gt. 1) then
    temp=tf_tempmin+                                                 &
        (tf_tempmax-tf_tempmin)*dble(itemp-1)/dble(tf_tempgrid-1)
  else
    temp=tf_tempmin
  end if

  !  Calculate Thermodynamical Functions
  call calctf_denergy(temp,mineng,denergy)
  call calctf_dtentropy(temp,dtentropy)
  denthalpy=denergy-temp !  negative because of dH=Hcmplx-Hconst

  print "(/A)", "Changes in thermodynamic functions on formation"
  print "(/(a,f12.2,a))",                                           &
      "Temperature       ", temp*EHVK, " K",                        &
      "Entropy           ",                                         &
      (dtentropy*EHVKJM*1000.0d0)/(temp*EHVK), " J mol^1 K^1",  &
      "TS                ", dtentropy*EHVKJM, " kJ/mol",            &
      "Energy            ", denergy*EHVKJM, " kJ/mol",              &
      "Enthalpy          ", denthalpy*EHVKJM, " kJ/mol"

  write (99,'(9(1p,e20.12))')                                        &
      mineng*EHVKJM,                                          &
      d_0*EHVKJM,                                             &
      temp*EHVK,                                              &
      (dtentropy/(temp*EHVK))*EHVKJM*1000.0d0,                &
      dtentropy*EHVKJM*1000.0d0,                              &
      denergy*EHVKJM,                                         &
      denthalpy*EHVKJM,                                       &
      (denergy-dtentropy)*EHVKJM,                             &
      (denthalpy-dtentropy)*EHVKJM

end do !itemp
close(99)

END SUBROUTINE thermofuncs

!-----------------------------------------------------------------------

SUBROUTINE calctf_denergy(temp,d_e,denergy)

IMPLICIT NONE

DOUBLE PRECISION :: temp,d_e,denergy,hny
INTEGER :: i,ncomplex,nconstituents

call degsfreedom(ncomplex,nconstituents)

!  Add 1/2kT per degree of freedom of the complex
denergy=dble(ncomplex)*temp/2.0d0

!  Add intermolecular vibrational contribution of the complex
do i=1,ncomplex
  ! if (freq(i)<10d0) then
  !   print "(a,i0,a,f6.2,a)", "Free energy calculation: vibration mode ", &
  !       i, " ignored (frequency ", freq(1), " cm-1 )"
  ! else
    hny=freq(i)/EHVCMM      !cm^-1 to hartree
    denergy=denergy+(hny/2.0d0+hny/(exp(hny/temp)-1.0d0))
  ! end if
end do

!  Subtract dissociation energy of the complex
denergy=denergy-d_e

!  Subtract degrees of freedom of the constituents
denergy=denergy-dble(nconstituents)*temp/2.0d0

END SUBROUTINE calctf_denergy

!-----------------------------------------------------------------------

SUBROUTINE calctf_dtentropy(temp,dtentropy)

IMPLICIT NONE

DOUBLE PRECISION :: temp,dtentropy,totalmass,fact
DOUBLE PRECISION :: hny,MIlin
INTEGER :: ncomplex,i,nconstituents

call degsfreedom(ncomplex,nconstituents)

!  Total mass of the complex
totalmass=0.0d0
do i=1,mols
  totalmass=totalmass+MMASS(i)
end do

!  TRANSLATIONAL
fact=2.0d0*PI*temp*JVEH*AMUVKG/PLANCK**2  ! 1/(m^2 amu)

!  Add contribution of the complex
dtentropy=2.5d0+                                                   &
    log(temp*JVEH/tf_pressure*((totalmass*fact)**(1.5d0)))

!  Subtract contribution of the constituents
do i=1,mols
  dtentropy=dtentropy-(2.5d0+                                        &
      log(temp*JVEH/tf_pressure*((mmass(i)*fact)**(1.5d0))))
end do

!  ROTATIONAL
fact=8.0d0*PI**2*temp*JVEH*AMUVKG*BOHRVM**2/PLANCK**2 ! 1/(amu a0^2)

!  Add contribution of the complex
if (min(MI(1,0),MI(2,0),MI(3,0)) .gt. 1.0d-4) then
  !  Non-linear
  dtentropy=dtentropy+1.5d0+log(sqrt(pi)/dble(tf_sigma)             &
      *sqrt(fact*MI(1,0))*sqrt(fact*MI(2,0))*sqrt(fact*MI(3,0)))
else
  !  Linear
  MIlin=max(MI(1,0),MI(2,0),MI(3,0))
  dtentropy=dtentropy+1.0d0+log(1.0d0/dble(tf_sigma)*fact*MIlin)
end if

!  Subtract contribution of the constituents
do i=1,mols
  if (i .gt. natoms) then
    if (i .le. (natoms+nlin)) then
      !  Linear
      MIlin=max(MI(1,i),MI(2,i),MI(3,i))
      dtentropy=dtentropy-                                            &
          (1.0d0+log(1.0d0/dble(tf_sigma)*fact*MIlin))
    else
      !  Non-linear
      dtentropy=dtentropy-(1.5d0+log(sqrt(pi)/dble(tf_sigma)          &
          *sqrt(fact*MI(1,i))*sqrt(fact*MI(2,i))*sqrt(fact*MI(3,i))))
    end if
  end if
end do

!  VIBRATIONAL
!  Add contribution of the complex
do i=1,ncomplex
  hny=freq(i)/EHVCMM      !cm^-1 to hartree
  dtentropy=dtentropy+hny/(temp*(exp(hny/temp)-1.0d0))              &
      -log(1.0d0-exp(-hny/temp))
end do

!  Normalisation
dtentropy=temp*dtentropy

END SUBROUTINE calctf_dtentropy

!-----------------------------------------------------------------------

SUBROUTINE degsfreedom(ncomplex,nconstituents)

USE parameters
USE molecules
USE properties, ONLY : inertia
IMPLICIT NONE

INTEGER :: ncomplex,nconstituents

!  Degrees of freedom for the constituents
nconstituents=3*natoms+5*nlin+6*(mols-natoms-nlin)

!  Degrees of freedom for a nonlinear complex
ncomplex=3*natoms+5*nlin+6*(mols-natoms-nlin)-6

!  Calculate moments of inertia for the entire system
call inertia (0)

!  Correction for a linear complex
if (min(MI(1,0),MI(2,0),MI(3,0)) .lt. 1.0d-4) then
  ncomplex=ncomplex+1
end if

END SUBROUTINE degsfreedom

END MODULE thermofns
