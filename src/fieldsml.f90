SUBROUTINE fieldsml(m1,k1,m2,k2,order)

!  Calculate fields for induction energy, and their derivatives, if required.
!  Calculates fields at k1 (in the molecule or adlayer) and k2 (possibly
!  in the crystal), unlike Ola's version, which calculated fields at k1 only.
!  (But currently the crystal sites are unpolarizable.)

!  The fields are to be used for calculating the induction energy, so the
!  induced moments are not included in the sources.

!  Note: fields must be zeroed elsewhere, outside the loop over site pairs,
!  because this routine just adds on the contribution at k1 due to k2 and
!  vice versa.

USE indparams
USE consts
USE damping
USE induction
USE interact
USE molecules
USE pairderiv
USE Sfns, ONLY : t1s, t2s
USE sites
USE sizes
USE switches
USE types, ONLY : indform, indampf, pairindex

IMPLICIT NONE

INTEGER, INTENT(IN) :: k1, k2, m1, m2, order

INTEGER :: jp, k, ix, n, t1, t2, pk1, pk2, z1, z2
DOUBLE PRECISION :: vr(7), fk, qq, g0, g1(12), g2(12,12),               &
    rkd, rkd1, rkd2, f0, f1, f2
LOGICAL :: newd

pk1 = ps(k1)
pk2 = ps(k2)
if (pk1 == 0 .and. pk2 == 0) return

vr(1) = 1d0/r
vr(2) = vr(1)**2

!  Damped or undamped r dependence for T functions
!  using Tang-Toennies or other damping functions.
t1=type(k1)
t2=type(k2)
if (t1 .gt. 0 .and. t2 .gt. 0) then
  pair=pairindex(t1,t2)
else
  pair=0
endif

z1 = pz(k1)
z2 = pz(k2)
!  Loop over electrostatic interactions
k=1
fk=1d0
newd=.true.
do n=1,nesmax
  ix=list(n)
  t1=t1s(ix)
  t2=t2s(ix)
  if (t1 .le. z1 .or. t2 .le. z2) then
    do while (k .lt. power(n))
      k=k+1
      vr(k)=vr(k-1)*vr(1)
    end do
    fk=k
    !  Evaluate induction damping function f0 and its derivatives f1 and f2
    call damp(indform(pair),indampf(0:,pair),k,alpha0,newd, r, f0,f1,f2)
    newd=.false.
    !  rkd is the damped R^-k and rkd1 and rkd2 are its derivatives w.r.t. R
    !  g0 is the damped T function and g1 and g2 are its derivatives w.r.t.
    !  external variables
    rkd=vr(k)*f0
    g0=fac(n)*rkd*s0(n)
    if (order .ge. 1) then
      rkd1=vr(k)*f1-fk*vr(1)*rkd
      g1(1:12)=fac(n)*(rkd*d1s(1:12,n)+s0(n)*rkd1*d1(16,1:12))
      if (order .ge. 2) then
        rkd2=vr(k)*(f2-2d0*fk*f1*vr(1))+fk*(fk+1d0)*vr(2)*rkd
        do jp=1,12
          g2(1:12,jp)=fac(n)*(rkd*d2s(1:12,jp,n)                        &
              +rkd1*(d1s(1:12,n)*d1(16,jp)+d1s(jp,n)*d1(16,1:12)        &
              +s0(n)*d2(16,1:12,jp))                                    &
              +rkd2*s0(n)*d1(16,1:12)*d1(16,jp))
        end do
      endif
    endif

    !  Cases to consider:
    !  (i)    m2=0.   k2 is a site in the lattice and we need only the
    !                 fields for m1.
    !  (ii)   m1=m2.  In this case m2 actually a periodic image of m1, and
    !                 the translational derivatives are all zero.
    !  (iii)  m1<>m2. 

    !  Fields at k1, if polarizable.
    if (pk1 .gt. 0) then
      qq=q2(n)
      if (t1 .le. z1 .and. qq .ne. 0d0) then
        v0(t1,pk1)=v0(t1,pk1)+qq*g0
        if (order .ge. 1) then
          if (m2 .eq. 0) then
            v1(1:6,m1,t1,pk1)=v1(1:6,m1,t1,pk1)+qq*g1(1:6)
          else if (m1 .eq. m2) then
            v1(4:6,m1,t1,pk1)=v1(4:6,m1,t1,pk1)+qq*g1(4:6)
            v1(4:6,m2,t1,pk1)=v1(4:6,m2,t1,pk1)+qq*g1(10:12)
          else
            v1(1:6,m1,t1,pk1)=v1(1:6,m1,t1,pk1)+qq*g1(1:6)
            v1(1:6,m2,t1,pk1)=v1(1:6,m2,t1,pk1)+qq*g1(7:12)
          endif
          if (order .ge. 2) then
            if (m2 .eq. 0) then
              v2(1:6,1:6,m1,m1,t1,pk1)=v2(1:6,1:6,m1,m1,t1,pk1)         &
                  +qq*g2(1:6,1:6)
            else if (m1 .eq. m2) then
              v2(4:6,4:6,m1,m1,t1,pk1)=v2(4:6,4:6,m1,m1,t1,pk1)         &
                  +qq*g2(4:6,4:6)
              v2(4:6,4:6,m1,m2,t1,pk1)=v2(4:6,4:6,m1,m2,t1,pk1)         &
                  +qq*g2(4:6,10:12)
              v2(4:6,4:6,m2,m1,t1,pk1)=v2(4:6,4:6,m2,m1,t1,pk1)         &
                  +qq*g2(10:12,4:6)
              v2(4:6,4:6,m2,m2,t1,pk1)=v2(4:6,4:6,m2,m2,t1,pk1)         &
                  +qq*g2(10:12,10:12)
            else
              v2(1:6,1:6,m1,m1,t1,pk1)=v2(1:6,1:6,m1,m1,t1,pk1)         &
                  +qq*g2(1:6,1:6)
              v2(1:6,1:6,m1,m2,t1,pk1)=v2(1:6,1:6,m1,m2,t1,pk1)         &
                  +qq*g2(1:6,7:12)
              v2(1:6,1:6,m2,m1,t1,pk1)=v2(1:6,1:6,m2,m1,t1,pk1)         &
                  +qq*g2(7:12,1:6)
              v2(1:6,1:6,m2,m2,t1,pk1)=v2(1:6,1:6,m2,m2,t1,pk1)         &
                  +qq*g2(7:12,7:12)
            endif
          endif
        endif
      endif
    endif

    !  Fields at k2, if polarizable. Note that if m2 = m1, we have a
    !  a monolayer, and m2 is a periodic image of the same
    !  molecule. Otherwise we need the fields at k2 due to k1, since the loop
    !  (in forcex) runs over distinct pairs of molecules.
    if (m1 .ne. m2 .and. pk2 .gt. 0) then
      qq=q1(n)
      if (t2 .le. z2 .and. qq .ne. 0d0) then
        v0(t2,pk2)=v0(t2,pk2)+qq*g0
        if (order .ge. 1) then
          v1(1:6,m1,t2,pk2)=v1(1:6,m1,t2,pk2)+qq*g1(1:6)
          if (m2 .gt. 0) then
            v1(1:6,m2,t2,pk2)=v1(1:6,m2,t2,pk2)+qq*g1(7:12)
          endif
          if (order .ge. 2) then
            v2(1:6,1:6,m1,m1,t2,pk2)=v2(1:6,1:6,m1,m1,t2,pk2)           &
                +qq*g2(1:6,1:6)
            if (m2 .gt. 0) then
              v2(1:6,1:6,m1,m2,t2,pk2)=v2(1:6,1:6,m1,m2,t2,pk2)         &
                  +qq*g2(1:6,7:12)
              v2(1:6,1:6,m2,m1,t2,pk2)=v2(1:6,1:6,m2,m1,t2,pk2)         &
                  +qq*g2(7:12,1:6)
              v2(1:6,1:6,m2,m2,t2,pk2)=v2(1:6,1:6,m2,m2,t2,pk2)         &
                  +qq*g2(7:12,7:12)
            endif
          endif
        endif
      endif
    endif
    
  endif
end do    ! end of loop over electrostatic functions

END SUBROUTINE fieldsml
