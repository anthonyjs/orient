#! /bin/bash
if [[ ! -a out ]]; then
  echo "No output file"
  exit 1
fi

tail -n 32 out | head -n 30 > out.tail
../stripcr.pl out.tail && rm out.tail.bak
diff -q -B -b check/out.tail out.tail && echo "File out checked"
