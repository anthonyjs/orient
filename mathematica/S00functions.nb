(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 7.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[     15746,        591]
NotebookOptionsPosition[     13442,        503]
NotebookOutlinePosition[     13779,        518]
CellTagsIndexPosition[     13736,        515]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"s110", "=", "czz"}]], "Input",
 CellChangeTimes->{{3.447161763970909*^9, 3.447161780945047*^9}, 
   3.447162041843263*^9}],

Cell[BoxData["czz"], "Output",
 CellChangeTimes->{3.447161782485405*^9, 3.447162043902341*^9, 
  3.44722939946962*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s101", "=", "raz"}]], "Input",
 CellChangeTimes->{{3.447161784035073*^9, 3.447161791004551*^9}, {
  3.447162378522415*^9, 3.447162386313849*^9}}],

Cell[BoxData["raz"], "Output",
 CellChangeTimes->{{3.44716238161793*^9, 3.447162387205916*^9}, 
   3.447229400077711*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s011", "=", "rbz"}]], "Input",
 CellChangeTimes->{{3.447162391953555*^9, 3.447162397107939*^9}}],

Cell[BoxData["rbz"], "Output",
 CellChangeTimes->{3.447162415906001*^9, 3.447229400215171*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s022", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"3", 
      RowBox[{"rbz", "^", "2"}]}], "-", "1"}], ")"}], "/", "2"}]}]], "Input",
 CellChangeTimes->{{3.447241874581419*^9, 3.447241917196292*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "1"}], "+", 
    RowBox[{"3", " ", 
     SuperscriptBox["rbz", "2"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447241922217808*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s202", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"3", 
      RowBox[{"raz", "^", "2"}]}], "-", "1"}], ")"}], "/", "2"}]}]], "Input",
 CellChangeTimes->{{3.447241936201681*^9, 3.447241957106383*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "1"}], "+", 
    RowBox[{"3", " ", 
     SuperscriptBox["raz", "2"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.44724195821035*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s112", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"3", "raz", " ", "rbz"}], "+", "czz"}], ")"}], "/", 
   "2"}]}]], "Input",
 CellChangeTimes->{{3.447157558449095*^9, 3.44715756813661*^9}, {
  3.447157608873509*^9, 3.447157646539654*^9}, {3.447170409743062*^9, 
  3.447170412730831*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{"czz", "+", 
    RowBox[{"3", " ", "raz", " ", "rbz"}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447157647994492*^9, 3.447162574735697*^9, 
  3.44717041407055*^9, 3.447229400245586*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s121", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"3", "rbz", " ", "czz"}], "+", "raz"}], ")"}], "/", 
   "2"}]}]], "Input",
 CellChangeTimes->{{3.447162459477653*^9, 3.447162498563586*^9}, {
  3.447166649989085*^9, 3.447166651034912*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{"raz", "+", 
    RowBox[{"3", " ", "czz", " ", "rbz"}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447166652297235*^9, 3.447170429471291*^9, 
  3.447229400277626*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s211", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"3", "raz", " ", "czz"}], "+", "rbz"}], ")"}], "/", 
   "2"}]}]], "Input",
 CellChangeTimes->{{3.447164796957349*^9, 3.447164831752912*^9}, {
  3.447166674432483*^9, 3.447166676966377*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"3", " ", "czz", " ", "raz"}], "+", "rbz"}], ")"}]}]], "Output",
 CellChangeTimes->{3.447164832712382*^9, 3.447166679104779*^9, 
  3.447170436469681*^9, 3.447229400459613*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s222", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"3", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"s112", " ", "s110"}], " ", "+", 
      RowBox[{"s022", "/", "3"}], "+", 
      RowBox[{"s202", "/", "3"}]}], ")"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.447241991418979*^9, 3.44724206451355*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "2"}], "+", 
    RowBox[{"3", " ", 
     SuperscriptBox["czz", "2"]}], "+", 
    RowBox[{"3", " ", 
     SuperscriptBox["raz", "2"]}], "+", 
    RowBox[{"9", " ", "czz", " ", "raz", " ", "rbz"}], "+", 
    RowBox[{"3", " ", 
     SuperscriptBox["rbz", "2"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447242065618049*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s033", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"5", 
      RowBox[{"rbz", "^", "3"}]}], "-", 
     RowBox[{"3", "rbz"}]}], ")"}], "/", "2"}]}]], "Input",
 CellChangeTimes->{{3.447242407939205*^9, 3.447242448344145*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "3"}], " ", "rbz"}], "+", 
    RowBox[{"5", " ", 
     SuperscriptBox["rbz", "3"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447242459581589*^9, 3.447242619583018*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s303", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"5", 
      RowBox[{"raz", "^", "3"}]}], "-", 
     RowBox[{"3", "raz"}]}], ")"}], "/", "2"}]}]], "Input",
 CellChangeTimes->{{3.447242461865435*^9, 3.447242474980561*^9}, {
  3.447242607678552*^9, 3.447242609754733*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "3"}], " ", "raz"}], "+", 
    RowBox[{"5", " ", 
     SuperscriptBox["raz", "3"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447242486208521*^9, 3.447242624861377*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s123", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"5", "raz", " ", 
      RowBox[{"rbz", "^", "2"}]}], "-", "raz", "+", 
     RowBox[{"2", "rbz", " ", "czz"}]}], ")"}], "/", "2"}]}]], "Input",
 CellChangeTimes->{{3.447229295203528*^9, 3.447229376839211*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "raz"}], "+", 
    RowBox[{"2", " ", "czz", " ", "rbz"}], "+", 
    RowBox[{"5", " ", "raz", " ", 
     SuperscriptBox["rbz", "2"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447229400641756*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s213", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"5", 
      RowBox[{"raz", "^", "2"}], "rbz"}], "-", "rbz", "+", 
     RowBox[{"2", "raz", " ", "czz"}]}], ")"}], "/", "2"}]}]], "Input",
 CellChangeTimes->{{3.447236712002521*^9, 3.447236789080646*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"2", " ", "czz", " ", "raz"}], "-", "rbz", "+", 
    RowBox[{"5", " ", 
     SuperscriptBox["raz", "2"], " ", "rbz"}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447236790408279*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s132", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"5", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"s011", " ", "s121"}], " ", "-", 
       RowBox[{"s110", "/", "3"}], "+", 
       RowBox[{"s112", "/", "15"}]}], ")"}], "/", "3"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.447164888204072*^9, 3.44716497863922*^9}, {
  3.447170640077074*^9, 3.447170669854313*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"raz", " ", "rbz"}], "+", 
  RowBox[{
   FractionBox["1", "2"], " ", "czz", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     RowBox[{"5", " ", 
      SuperscriptBox["rbz", "2"]}]}], ")"}]}]}]], "Output",
 CellChangeTimes->{{3.447164934661449*^9, 3.447164950962315*^9}, 
   3.447164995093835*^9, 3.447166707953369*^9, 3.447170483120538*^9, {
   3.44717065286058*^9, 3.447170677673125*^9}, 3.447229400723083*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s312", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"5", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"s101", " ", "s211"}], "-", 
       RowBox[{"s110", "/", "3"}], "+", 
       RowBox[{"s112", "/", "15"}]}], ")"}], "/", "3"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.447170806606353*^9, 3.447170901607192*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   FractionBox["1", "2"], " ", "czz", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     RowBox[{"5", " ", 
      SuperscriptBox["raz", "2"]}]}], ")"}]}], "+", 
  RowBox[{"raz", " ", "rbz"}]}]], "Output",
 CellChangeTimes->{{3.447170890692111*^9, 3.447170902417391*^9}, 
   3.447229400925407*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s143", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"7", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"s011", " ", "s132"}], "-", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"2", "/", "5"}], ")"}], "s121"}], "+", 
       RowBox[{"s123", "/", "35"}]}], ")"}], "/", "4"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.447229125115274*^9, 3.447229184816255*^9}, {
  3.447229270852337*^9, 3.447229288697763*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "8"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"3", " ", "raz", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       RowBox[{"5", " ", 
        SuperscriptBox["rbz", "2"]}]}], ")"}]}], "+", 
    RowBox[{"5", " ", "czz", " ", "rbz", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "3"}], "+", 
       RowBox[{"7", " ", 
        SuperscriptBox["rbz", "2"]}]}], ")"}]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.44722940151396*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s413", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"7", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"s101", " ", "s312"}], " ", "-", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"2", "/", "5"}], ")"}], "s211"}], "+", 
       RowBox[{"s213", "/", "35"}]}], ")"}], "/", "4"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.447236634110216*^9, 3.447236690703632*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "8"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"5", " ", "czz", " ", "raz", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "3"}], "+", 
       RowBox[{"7", " ", 
        SuperscriptBox["raz", "2"]}]}], ")"}]}], "+", 
    RowBox[{"3", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       RowBox[{"5", " ", 
        SuperscriptBox["raz", "2"]}]}], ")"}], " ", "rbz"}]}], 
   ")"}]}]], "Output",
 CellChangeTimes->{3.447236799904311*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s233", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"15", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"s123", " ", "s110"}], "+", 
       RowBox[{"s033", "/", "3"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"2", "/", "5"}], ")"}], "s213"}]}], ")"}], "/", "4"}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.447241587863823*^9, 3.447241668478703*^9}, {
  3.447242335398422*^9, 3.447242382033919*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "8"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"30", " ", 
     SuperscriptBox["czz", "2"], " ", "rbz"}], "+", 
    RowBox[{"3", " ", "czz", " ", "raz", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       RowBox[{"25", " ", 
        SuperscriptBox["rbz", "2"]}]}], ")"}]}], "+", 
    RowBox[{"rbz", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "21"}], "+", 
       RowBox[{"30", " ", 
        SuperscriptBox["raz", "2"]}], "+", 
       RowBox[{"25", " ", 
        SuperscriptBox["rbz", "2"]}]}], ")"}]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447241674019854*^9, 3.447242493745808*^9, 
  3.447242642843561*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s323", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{"15", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"s213", " ", "s110"}], "+", 
       RowBox[{"s303", "/", "3"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"2", "/", "5"}], ")"}], "s123"}]}], ")"}], "/", "4"}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.44724251589335*^9, 3.447242554683902*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "8"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"25", " ", 
     SuperscriptBox["raz", "3"]}], "-", 
    RowBox[{"3", " ", "czz", " ", "rbz"}], "+", 
    RowBox[{"75", " ", "czz", " ", 
     SuperscriptBox["raz", "2"], " ", "rbz"}], "+", 
    RowBox[{"3", " ", "raz", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "7"}], "+", 
       RowBox[{"10", " ", 
        SuperscriptBox["czz", "2"]}], "+", 
       RowBox[{"10", " ", 
        SuperscriptBox["rbz", "2"]}]}], ")"}]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.447242647500849*^9}]
}, Open  ]]
},
WindowSize->{640, 750},
WindowMargins->{{24, Automatic}, {41, Automatic}},
FrontEndVersion->"7.0 for Linux x86 (32-bit) (February 25, 2009)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[567, 22, 146, 3, 32, "Input"],
Cell[716, 27, 118, 2, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[871, 34, 169, 3, 32, "Input"],
Cell[1043, 39, 121, 2, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1201, 46, 120, 2, 32, "Input"],
Cell[1324, 50, 94, 1, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1455, 56, 240, 7, 32, "Input"],
Cell[1698, 65, 234, 8, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1969, 78, 240, 7, 32, "Input"],
Cell[2212, 87, 233, 8, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2482, 100, 328, 9, 32, "Input"],
Cell[2813, 111, 269, 7, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3119, 123, 280, 8, 32, "Input"],
Cell[3402, 133, 248, 7, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3687, 145, 280, 8, 32, "Input"],
Cell[3970, 155, 270, 7, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4277, 167, 330, 9, 32, "Input"],
Cell[4610, 178, 423, 13, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5070, 196, 263, 8, 32, "Input"],
Cell[5336, 206, 284, 9, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5657, 220, 312, 9, 32, "Input"],
Cell[5972, 231, 284, 9, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6293, 245, 299, 8, 32, "Input"],
Cell[6595, 255, 297, 9, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6929, 269, 294, 8, 32, "Input"],
Cell[7226, 279, 277, 8, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7540, 292, 410, 11, 32, "Input"],
Cell[7953, 305, 467, 12, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8457, 322, 357, 10, 32, "Input"],
Cell[8817, 334, 352, 11, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9206, 350, 460, 13, 32, "Input"],
Cell[9669, 365, 517, 17, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10223, 387, 416, 12, 32, "Input"],
Cell[10642, 401, 522, 18, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11201, 424, 464, 14, 32, "Input"],
Cell[11668, 440, 706, 22, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12411, 467, 414, 13, 32, "Input"],
Cell[12828, 482, 598, 18, 47, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
