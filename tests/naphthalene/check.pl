#!/usr/bin/perl

while (<>) {
  $e=$1 if /^Total energy: +(-\d+\.\d\d)/;
  $g=$1 if /^Gradient norm .* (\d+\.\d\d\d)/;
}

if ($e<-1139 && $g == 0.0) {
  print "File out checked\n";
  exit 0
} else {
  print "Output check failed. Compare file out with check/out\n";
  exit 1
}
