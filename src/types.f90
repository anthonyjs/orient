MODULE types

USE input, ONLY : die
PRIVATE

!  Type parameters:

!  NTYPES is the maximum number of site types.
!  RADIUS is the default van der Waals radius for the site type.
!  ATNUM is the atomic number for the type.
!  MASS is the atomic mass for the type, in a.m.u.

!  Pair parameters:

!  PAIRS maximum number of pair interaction types.
!  NPAIR is the number actually defined

!  PAIRINDEX is the index used to maintain the list of interaction
!  types. Note that it is not symmetric. pairindex(t1,t2) is an integer
!  index into into the various arrays that specify properties of the
!  interactions between types t1 and t2. pairindex(t2,t1) is different from
!  pairindex(t1,t2) because the S-functions are different (but related:
!  e.g. S(t1,t2)(00,10,1) has the same coefficient as S(t2,t1)(10,00,1).)

!  DAMPF is the damping scale factor or other damping parameters. The
!  argument of the damping function (Tang-Toennies) is DAMPF(0)*ALPHA*R
!  or DAMPF(0)*R.
!  DFORM specifies the form of the dispersion damping. Possible values
!  include
!    0  no damping;
!    1  Tang-Toennies, argument DAMPF*R;
!    2  Tang-Toennies, argument DAMPF*ALPHA0*R,
!  where in the last case ALPHA0 is the anisotropic alpha value for
!  the interaction.
!  INDFORM specifies induction damping as for DFORM.
!  INDAMPF gives damping scale factors or other parameters for induction
!  damping. Recommended Tang-Toennies values are 0.725 times the corresponding
!  value for dispersion damping.

!  BMunit(0,p) is the prefactor for the Born-Mayer repulsion expression for
!  pair p. BMunit(1:2,p) are possible coefficients in a polynomial in R to
!  multiply the Born-Mayer expression. That is, the whole expression is
!  BMunit(0,p)*(1+c1*R+c2*R^2)exp[-alpha(R-rho)].
!  c1 and c2 are not orientation-dependent. BMunit(0,0) is the default
!  prefactor, 1 millihartree.
!  PREFACTOR_ORDER is the order of the prefactor polynomial. Default 0,
!  modified automatically if higher-order coefficients are specified for
!  a particular pair interaction. The default prefactor always has order 0.

!  Anisotropic repulsion-dispersion parameters:

!  ALPH contains the coefficients for the S-function expansion of the hardness
!  parameter in the exponential repulsion.
!  RHO contains the coefficients for the rho parameter.
!  C(..,n) contains the coefficients for the dispersion coefficient C_n.
!  For a given pair, with pair index IP, the first coefficient is at sptr(IP),
!  nexts(IP) points to the next, and so on until the nexts value is zero.
!  They do not occur in order in the coefficient array, because they have
!  been sorted into order of increasing S-function index. If there is a
!  non-zero coefficient for any of alph, rho or the C_n, there will be a
!  coefficient for all the others, even if it is zero. The first coefficient
!  always relates to ix=1, i.e. to the isotropic term.
!  ixptr points to the last entry in the list.

!  In fact the C array contains all the coefficients. ALPH is a pointer to
!  C(:,4), and RHO is a pointer to C(:5). PARAM is a pointer to C -- that
!  is PARAM and C are synonyms. PARAM is used in the fit module to handle
!  alpha, rho and the C coefficients together.

INTEGER, PUBLIC :: NTYPES, MAXPAIRS, MAXIX

INTEGER, PUBLIC, SAVE :: ntype=0, npair=0, ixptr=0, prefactor_order=0
INTEGER, PUBLIC, ALLOCATABLE :: atnum(:), sptr(:), ixs(:), nexts(:),   &
    dform(:), indform(:), pairindex(:,:)

DOUBLE PRECISION, PUBLIC :: cdisp
DOUBLE PRECISION, PUBLIC, ALLOCATABLE :: MASS(:), radius(:),           &
    dampf(:,:), BMunit(:,:), indampf(:,:)

DOUBLE PRECISION, PUBLIC, ALLOCATABLE, TARGET :: C(:,:)
DOUBLE PRECISION, PUBLIC, POINTER, DIMENSION(:) :: alph, rho
DOUBLE PRECISION, PUBLIC, POINTER, DIMENSION(:,:) :: param
        
!     EQUIVALENCE (param(1,1), alph(1)), (param(1,2), rho(1)),          &
!         (param(1,3),C(1,6))

!  TNAME is the type name; TLABEL is the label that is used in the xyz file.
CHARACTER*20, ALLOCATABLE, PUBLIC :: tname(:)
CHARACTER*4, ALLOCATABLE, PUBLIC :: tlabel(:)
!  TCOLOUR is used only in the DISPLAY module
REAL, ALLOCATABLE, PUBLIC :: tcolour(:,:)


INTEGER, PRIVATE :: i
DOUBLE PRECISION, PARAMETER :: vdwdef=2.5d0
!  Bondi van der Waals radii, J Phys Chem (1964) 68, 441
!  These values are in bohr.
DOUBLE PRECISION, PARAMETER, PUBLIC :: vdw_radius(0:82) = (/ 0.00d0, &
    2.268d0, 2.646d0,                   & !  H, He
    3.440d0, vdwdef,  vdwdef,  3.213d0, & !  Li, Be, B, C
    2.929d0, 2.872d0, 2.778d0, 2.910d0, & !  N, O, F, Ne
    4.290d0, 3.270d0, vdwdef,  3.968d0, & !  Na, Mg, Al, Si
    3.402d0, 3.402d0, 3.307d0, 3.553d0, & !  P, S, Cl, Ar
    5.197d0, vdwdef,  vdwdef,  vdwdef,  & !  K, Ca, Sc, Ti
    vdwdef,  vdwdef,  vdwdef,  vdwdef,  & !  V, Cr, Mn, Fe
    vdwdef,  3.080d0, 2.646d0, 2.627d0, & !  Co, Ni, Cu, Zn
    3.534d0, vdwdef,  3.496d0, 3.590d0, & !  Ga, Ge, As, Se
    3.496d0, 3.817d0, vdwdef,  vdwdef,  & !  Br, Kr, Rb, Sr
    vdwdef,  vdwdef,  vdwdef,  vdwdef,  & !  Y, Zr, Nb, Mo
    vdwdef,  vdwdef,  vdwdef,  3.080d0, & !  Tc, Ru, Rh, Pd
    3.250d0, 2.986d0, 3.647d0, 4.100d0, & !  Ag, Cd, In, Sn
    vdwdef,  3.893d0, 3.742d0, 4.082d0, & !  Sb, Te, I, Xe
    (vdwdef,i=55,72),                   & !  Cs, Ba, La-Hf
    vdwdef,  vdwdef,  vdwdef,  vdwdef,  & !  Ta, W, Re, Os
    vdwdef,  3.250d0, 3.137d0, 2.929d0, & !  Ir, Pt, Au, Hg
    3.704d0, 3.817d0 /)                   !  Tl, Pb

DOUBLE PRECISION, PARAMETER, PUBLIC :: atmass(0:36) = (/0.00d0,        &
    1.007825d+00, 4.00260d+00, 7.01600d+00,                            &
    9.01218d+00, 11.00931d+00, 12.00000d+00,                           &
    14.00307d+00, 15.99491d+00, 18.99840d+00,                          &
    19.99244d+00, 22.9898d+00,  23.98504d+00,                          &
    26.98153d+00, 27.97693d+00, 30.97376d+00,                          &
    31.97207d+00, 34.96885d+00, 39.948d+00,                            &
    38.96371d+00, 39.96259d+00, 44.95592d+00,                          &
    47.90000d+00, 50.944d+00,   51.9405d+00,                           &
    54.9381d+00,  55.9349d+00,  58.93320d+00,                          &
    57.9353d+00,  62.9298d+00,  63.9291d+00,                           &
    68.9257d+00,  73.9219d+00,  74.9216d+00,                           &
    79.9165d+00,  78.9183d+00,  83.8d+00 /)

!  Covalent radii from Cordero et al., Dalton Trans. (2008) 21, 2832.
!  (Cambridge Structural Database). Values are in pm.
DOUBLE PRECISION, PUBLIC, PARAMETER :: covalent_radius(0:54) =         &
    [ 0d0,  32d0,  28d0,                                               &
    128d0,  96d0,  84d0,  76d0,  71d0,  66d0,  57d0,  58d0,            &
    166d0, 141d0, 121d0, 111d0, 107d0, 105d0, 102d0, 106d0,            &
    203d0, 176d0, 170d0, 160d0, 153d0, 139d0, 139d0, 132d0, 126d0,     &
    124d0, 132d0, 122d0, 122d0, 120d0, 119d0, 120d0, 120d0, 116d0,     &
    220d0, 195d0, 190d0, 175d0, 164d0, 154d0, 147d0, 146d0, 142d0,     &
    139d0, 145d0, 144d0, 142d0, 139d0, 139d0, 138d0, 139d0, 140d0 ]

REAL, PUBLIC, PARAMETER :: atom_colour(3,0:18) = reshape((/ 0.0,0.0,0.0, &
    0.75,0.75,0.75, 1.000,0.753,0.796, 0.698,0.133,0.133,   & ! H, He, Li
    0.0,0.0,0.0, 0.0,1.0,0.0, 0.439,0.502,0.56,             & ! B, Be, C
    0.0,0.0,0.933, 1.0,0.0,0.0, 0.855,0.647,0.125,          & ! N, O, F
    0.0,0.0,0.0, 1.000,0.925,0.545, 0.0,0.0,0.0,            & ! Ne, Na, Mg
    0.133,0.545,0.133, 0.855,0.647,0.125, 1.000,0.647,0.00, & ! Al, Si, P
    1.0,1.0,0.0, 0.0,1.0,0.0, 0.0,0.0,0.545                 & ! S, Cl, Ar
    /),(/3,19/))

CHARACTER(LEN=2), PARAMETER, PUBLIC :: element(0:36) = ["  ", "H ", "He", &
    "Li", "Be", "B ", "C ", "N ", "O ", "F ", "Ne",                     &
    "Na", "Mg", "Al", "Si", "P ", "S ", "Cl", "Ar",                     &
    "K ", "Ca", "Sc", "Ti", "V ", "Cr", "Mn", "Fe", "Co", "Ni", "Cu",   &
    "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr"]

PUBLIC init_types, readtypes, gettype, show_types

CONTAINS

SUBROUTINE init_types
IMPLICIT NONE
INTEGER :: ok

allocate (atnum(0:ntypes), radius(0:ntypes), mass(0:ntypes),           &
    tname(0:ntypes), tlabel(ntypes), pairindex(0:ntypes,0:ntypes),     &
    tcolour(3,0:ntypes), stat=ok)
if (ok .gt. 0) then
  print "(A)", "Can't allocate type variables"
  stop
endif

tname=""; tlabel=""
radius=0d0
mass=0d0
atnum=0
pairindex=0
tname(0)='undefined'

allocate (sptr(maxpairs), dform(0:maxpairs), indform(0:maxpairs),      &
    Bmunit(0:2,0:maxpairs), dampf(0:10,0:maxpairs),                        &
    indampf(0:10,0:maxpairs), stat=ok)
if (ok .gt. 0) then
  print "(A)", "Can't allocate pair variables"
  stop
endif

sptr=0
dform=0; indform=0
dampf=0d0; indampf=0d0
BMunit=0d0
dampf(0,0)=1d0
BMunit(0,0)=1D-3
indampf(0,0)=1d0

allocate (ixs(0:maxix), nexts(0:maxix), C(maxix,4:12),stat=ok)
if (ok .gt. 0) then
  print "(A)", "Can't allocate space for pair parameters"
  stop
endif

ixs=0; nexts=0; C=0d0

alph => C(:,4)
rho => C(:,5)
param => C(:,4:12)

END SUBROUTINE init_types

SUBROUTINE readtypes

USE consts
USE input
IMPLICIT NONE
CHARACTER ww*20, w*4
INTEGER i, k, Z
DOUBLE PRECISION zz
LOGICAL m_unset, r_unset, end

do
  call read_line(end)
  if (end) call die('Unexpected end of file',.false.)
  call readu(ww)
  if (ww .eq. "END") exit
  if (ww .eq. "!") cycle
  call reread(-1)
  call reada(ww)
  k=0
  do i=1,ntype
    if (ww .eq. tname(i)) k=i
  end do
  if (k .eq. 0) then
    ntype=ntype+1
    k=ntype
    radius(k)=0
    r_unset=.true.
    atnum(k)=0
    mass(k)=0d0
    m_unset=.true.
    tname(k)=ww
    tlabel(k)=" "
    tcolour(:,k)=0.0
  endif
  do while (item .lt. nitems)
    call readu(ww)
    call reada(w)
    if (w .ne. '=' .and. w .ne. ' ') call reread(-1)
    select case(ww)
    case("RADIUS")
      call readf(radius(k))
      if (item .lt. nitems) then
        call readu(ww)
        if (ww .eq. 'A') then
          radius(k)=radius(k)/bohrva
        else if (ww .ne. 'B') then
          call reread(-1)
        endif
      else
        radius(k)=radius(k)/rfact
      endif
      r_unset=.false.
   case("Z")
      call readf(zz)
      Z = int(zz)
      atnum(k)=Z
      if (Z > 0) then
        if (m_unset .and. Z<=82) mass(k)=atmass(Z)
        !  Blank labels will be replaced by the standard element symbol at
        !  plot time.
        !  if (tlabel(k) .eq. ' ') tlabel(k)='none'
        if (r_unset .and. Z<=36) radius(k)=vdw_radius(Z)
        if (all(tcolour(:,k)==0.0) .and. Z<=18) tcolour(:,k)=atom_colour(:,Z)
      end if
    case("MASS")
      call readf(mass(k))
      m_unset=.false.
    case("LABEL")
      call reada(tlabel(k))
    case("NOLABEL")
      tlabel(k)='none'
    case("COLOUR","COLOR")
      call readu(ww)
      call read_colour(ww,tcolour(:,k))
    case default
      call die('Keyword not recognised',.true.)
    end select
  end do
end do

END SUBROUTINE readtypes

INTEGER FUNCTION newtype(ww)

USE input
IMPLICIT NONE
CHARACTER(LEN=*), INTENT(IN) :: ww
INTEGER :: k

ntype = ntype+1
if (ntype > NTYPES) call die ("Too many types",.true.)
k = ntype
radius(k) = 0
atnum(k) = 0
mass(k) = 0d0
tname(k) = ww
tlabel(k) = " "
newtype = k

END FUNCTION newtype

INTEGER FUNCTION gettype(ww,exists)

!  Find the index number of the type with name ww (case-sensitive)
!  If not found, define a new type with the name given, unless
!  the exists argument is present and true, in which case the
!  type must have been defined previously.

IMPLICIT NONE
CHARACTER(LEN=*), INTENT(IN) :: ww
LOGICAL, INTENT(IN), OPTIONAL :: exists
INTEGER :: i, k

k = 0
do i = 1,ntype
  if (ww == tname(i)) k = i
end do
if (k == 0) then
  if (present(exists)) then
    if (exists) then
      !  The type is required to have been defined already
      print "(3a/a)", "Type ", trim(ww), " has not been defined.",      &
          "This is probably an error."
      call die ("Stopping",.true.)
    end if
  end if
  ntype = ntype+1
  if (ntype .gt. NTYPES) call die ("Too many types",.true.)
  print "(/3a)", "*** Warning: type ", trim(ww), " was not defined"
  k = ntype
  tname(k) = ww
  radius(k) = 0d0
  tlabel(k) = "none"
  mass(k) = 0d0
  atnum(k) = 0
endif
gettype = k

END FUNCTION gettype

SUBROUTINE show_types

INTEGER :: i

print "(A)", "Type Name                  Label  Z     radius    mass"
do i = 1,ntype
  print "(I3,2x,a,2x,a,i4,2x,2F10.5)",                            &
      i, tname(i), tlabel(i), atnum(i), radius(i), mass(i)
end do

END SUBROUTINE show_types

END MODULE types
