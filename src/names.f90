MODULE names

INTEGER labels
PARAMETER (labels=81)
!  label is the array of angular momentum indices: 00, 10, 11c, 11s, etc.
CHARACTER*4 label(labels)
CHARACTER*79 title(2)

END MODULE names
