MODULE orses_module

USE molecules, ONLY : nmols
USE input, ONLY : die
#ifdef TEST_ORSES
USE utility
#endif

IMPLICIT NONE

!     PARAMETER (MXATMS=NMOLS*2)

INTEGER :: natoms, istatus, ntrans, inr, ivec, istcrt,           &
    nmol, nlin, nzeros, nrot, ntot
DOUBLE PRECISION :: stepmax

PRIVATE
PUBLIC :: orses

CONTAINS

SUBROUTINE ORSES(NATOMSO,NMOLO,NLINO,INRO,stpmax,IVECO,                &
    VNEW,HNEW,Q,ISTATUSO,STEP,THRESH,CFLAG,PLIN,                       &
    AXLIN,PUSHOFF,CONSTRAIN,NTOTO,nextra)

USE switches
IMPLICIT NONE

DOUBLE PRECISION :: vnew(:), hnew(:,:), q(:), step(:),            &
    axlin(:), pushoff, thresh, stpmax
LOGICAL :: cflag, constrain
INTEGER :: plin(0:), natomso,nmolo,nlino,inro,iveco,ntoto,           &
    istatuso,nextra
       

!  Eigenvector-following routine for orient: D. J. Wales, November 1993.
!  This code may not be modified without the permission of the author.

!  The variables are expected in the order:
!  (1) atomic centres of mass
!  (2) molecular centres of mass
!  (3) molecular orientations
!  and this must also be true of the first and second derivatives. The
!  shifts of course come out in the same order.

!  NATOMS= number of atoms as opposed to rigid molecules
!  NMOL  = number of rigid molecules (including linear)
!  NLIN  = number of linear molecules
!  INR   = type of calculation
!     INR=0 - minimise
!     INR=1 - Newton-Raphson
!     INR=2 -transition state
!     INR=3 - minimise but don't do any reorientations (reaction paths)
!             and use pseudo-third derivative correction.
!     INR=4 - ts search but and use pseudo-third derivative correction.
!  stepmax is the maximum step size in bohr or radian,
!  IVEC tells which eigenmode to follow
!     0 lowest eigenvalue, irrespective of overlap
!     1 lowest eigenvalue on the first cycle, then determined by overlap
!     2 next lowest eigenvalue, etc.
!  VNEW is the gradient
!  HNEW is the Hessian
!  Q    contains centre of mass positions
!  ISTATUS counts the number of steps
!  STEP    contains the EF step
!  THRESH is the convergence criterion for the RMS force (in a.u.).
!  1D-7 is a reasonable value.
!  CFLAG is set .TRUE. on exit if the process has converged, otherwise
!  .FALSE.

!  ISTCRT = 0 uses full step length in Cartesian space
!  as the scaling criterion, while any other value uses the maximum
!  individual internal coordinate increment.

DOUBLE PRECISION, ALLOCATABLE, SAVE :: VEC(:)
DOUBLE PRECISION :: CGRAD(ntoto), PMAT(ntoto,ntoto),              &
    B(ntoto,6+2*NMOLS), S(6+2*NMOLS,6+2*NMOLS),                  &
    SINV(6+2*NMOLS,6+2*NMOLS)
CHARACTER(LEN=60) :: message
INTEGER :: i
DOUBLE PRECISION :: rms

#ifdef TEST_ORSES
print "(A)", "Orses: Hessian on entry:"
call matwrt(hnew(1:ntoto,1:ntoto))
#endif

!  Vec has to be allocatable (not automatic) so that it can be saved
!  between calls.
if (allocated(vec)) then
  if (ntoto .gt. size(vec,1)) then
    deallocate(vec)
    allocate(vec(ntoto))
  endif
else
  allocate(vec(ntoto))
endif


ISTATUS=ISTATUSO

IF (ISTATUS.EQ.1) THEN
  NMOL=NMOLO
  NLIN=NLINO
  INR=INRO
  IVEC=IVECO
  NTOT=NTOTO
  !  MXATMS must be at least NATOMS+2*NMOL because NATOMS+NMOL <= NMOLS
  !         IF (MXATMS.LT.NATOMS+2*NMOL) THEN
  !            PRINT*,'Increase MXATMS to at least ',NATOMS+2*NMOL
  !            PRINT*,'(i.e. NHESS to at least ',3*(NATOMS+2*NMOL),')'
  !            STOP
  !         ENDIF
  
  NATOMS=NATOMSO
  NTRANS=3*(NATOMS+NMOL)
  NROT=3*NMOL
  IF ((.NOT.CONSTRAIN).AND.(NTOT.NE.NTRANS+NROT)) THEN
    PRINT "(2a)", "Orses: ",                                    &
        "failed consistency check for number of variables:'"
    write (message,"(a,i3,a,i3)")                               &
        'NTOT =',NTOT, ' should equal NTRANS+NROT =',NTRANS+NROT
    call die(message,.false.)
  ENDIF

ENDIF

stepmax=stpmax
rms=100d0
IF (CONSTRAIN .AND. PLIN(0) .EQ. 0) THEN
  NZEROS=NEXTRA
ELSE
  IF (CONSTRAIN) THEN
    CALL BUILDC(PMAT,Q,AXLIN,PLIN,B,S,SINV)
  ELSE
    CALL BUILDO(PMAT,Q,AXLIN,PLIN,B,S,SINV)
  ENDIF
  NZEROS=NZEROS+NEXTRA
  CALL PROJG(PMAT, CGRAD, VNEW, 0, RMS)
  VNEW=CGRAD
  CALL PROJH(PMAT, HNEW)
ENDIF
!     print *, rms
CALL EFOL(VEC,VNEW,HNEW,STEP,Q,THRESH,CFLAG,RMS,PUSHOFF)
if (print .ge. 1 .and. .not. constrain) CALL ADM(Q)
if (print .ge. 1) print "(A/(1P,5E15.6))", "Step",                 &
    (step(i), i=1,ntoto)

END SUBROUTINE orses

!-----------------------------------------------------------------------

SUBROUTINE EFOL(VEC,VNEW,HNEW,STEP,Q,THRESH,CFLAG,RMS,PUSHOFF)

!   Eigenvector following optimization algorithm.

USE switches
USE utility, ONLY : stri
IMPLICIT NONE

DOUBLE PRECISION :: VEC(:), VNEW(:), HNEW(:,:), STEP(:), Q(:)
LOGICAL :: CFLAG
DOUBLE PRECISION :: THRESH, RMS, PUSHOFF

LOGICAL DONE, TSTEST, MINTEST, NRTEST, ZT(ntot), AWAY
LOGICAL :: verbose=.false.
DOUBLE PRECISION SCRATCH(ntot),FOB(ntot), DIAG(ntot), PRMS,      &
    TEMP2(ntot), ZIP(ntot), AV(6), PSTEP(ntot), TEMP1(6),         &
    work(3*ntot), b(ntot,ntot)

DOUBLE PRECISION :: lp,lp1,lp2, small, z0, svh, sum, third,      &
    scale, stpmag, xalpha
INTEGER :: i, j, j1, ineg, iassign, ntest(ntot), info, im,       &
    imode, imode2, ncycle
CHARACTER(LEN=25) :: string
SAVE

! Initialize some stuff.

!     print "(A)", "EFOL: Hessian on entry:"
!     call matwrt(hnew(1:ntot,1:ntot))

#ifdef TEST_ORSES
print "(a)", "Entering EFOL"
#endif
IMODE=0
NCYCLE=ISTATUS
MINTEST=.FALSE.
TSTEST=.FALSE.
NRTEST=.FALSE.
if (verbose) then
  string="Optimization cycle "//stri(ncycle)
  write (*,"(/a,i4)", advance="no") trim(string)
  select case(inr)
  case(0)
    print "(a)", ".  Updating structure with eigenvector-following step."
  case(1)
    print "(a)", ".  Updating structure with Newton-Raphson step."
  case(2)
    print "(a)", ".  Updating structure with eigenvector-following step."
  case(3)
    print "(a)", ".  Updating structure with eigenvector-following step -- no rotations."
  case(4)
    print "(a)", ".  Updating structure with eigenvector-following step -- no rotations."
  case default
    PRINT "(a)", 'INR value not recognised,  set to 0'
    print "(a)", ".  Updating structure with eigenvector-following step."
  end select
end if
select case(inr)
case(0,3)
  MINTEST=.TRUE.
case(1)
  NRTEST=.TRUE.
case(2,4)
  TSTEST=.TRUE.
case default
  INR=0
  MINTEST=.TRUE.
end select
b=0d0
do i=1,ntot
  b(i,i)=1d0
end do
!     print "(A)", "Orses: Hessian before diagonalization:"
!     call matwrt(hnew(1:ntot,1:ntot))
call dsygv(1,"V","U",ntot,hnew,ntot,b,ntot,diag,work,3*ntot,info)
!  Sort eigenvalues into descending order.
call eigsrt(diag,hnew,ntot)

IF ((MOD(ISTATUS,1).EQ.0) .and. (print .ge. 1)) THEN
  print "(A/(5G14.5))", 'Hessian matrix eigenvalues: ',           &
      (DIAG(I),I=1,NTOT)
ENDIF
#ifdef TEST_ORSES
print "(A/(5G14.5))", 'Hessian matrix eigenvalues: ', DIAG
#endif
!     WRITE(*,175)
!175  FORMAT ('The eigenvectors of the Hessian matrix: ')
!     CALL HESSOUT(HNEW,NTOT,NTOT,NTOT,1)

!  Find eigenvalue demanded by value of ivec (first pass only)
!  and count number of negative eigenvalues in hessian (all passes).
!  We can now take advantage of the above sort to simplify this.

DO J=1,NTOT
  TEMP2(J)=DABS(DIAG(J))
  NTEST(J)=J
  ZT(J)=.TRUE.
ENDDO
CALL SORT(NTOT,TEMP2,NTEST)

!  Determine the zero eigenvalues.

DO J1=1,NZEROS
  ZT(NTEST(J1))=.FALSE.
ENDDO
INEG=0
IASSIGN=0
DONE=.FALSE.
DO J=NTOT,1,-1
  IF (DIAG(J).LT.0.0D0.AND.(ZT(J))) INEG=INEG+1
  IF (ZT(J)) THEN
    IASSIGN=IASSIGN+1
    IF ((IASSIGN.GE.ABS(IVEC)).AND.(.NOT.(DONE))) THEN
      IMODE=J
      DONE=.TRUE.
    ENDIF
  ENDIF
ENDDO

!  On later passes, determine overlap between Hessian eigenvectors
!  and VEC (saved from previous step).

SMALL=1.0D6
IF (NCYCLE.GT.1.AND.TSTEST.AND.(IVEC.NE.0)) THEN
  Z0=0.0D0
#ifdef TEST_ORSES
  print "(A)", "Hessian eigenvector overlaps"
#endif
  DO I=1,NTOT
    SVH=dot_product(VEC(1:ntot),HNEW(1:ntot,I))
#ifdef TEST_ORSES
    print "(6F12.6)", vec(1:ntot), hnew(1:ntot,i)
    print "(i3,f16.10)", i, svh
#endif
    IF (DABS(SVH).GT.Z0) THEN
      Z0=DABS(SVH)
      IM=I
    ENDIF
    IF ((DIAG(I).LT.SMALL).AND.(ZT(I))) THEN
      SMALL=DIAG(I)
    ENDIF
  ENDDO
  WRITE(*,200) IM,Z0,SMALL
200 FORMAT('Eigenvector ',I3,' has largest overlap with last ',     &
      'mode followed.'/'Magnitude of overlap is ',F8.5,'.',          &
      ' Smallest eigenvalue is ',F15.7)
210 IF (ZT(IM)) IMODE=IM
ENDIF
IMODE2=IMODE

DO I = 1, NTOT
  FOB(I)=0.0D0
  DO J = 1, NTOT
    FOB(I)=FOB(I)+VNEW(J)*HNEW(J,I)
  ENDDO
ENDDO
IF ((MOD(ISTATUS,1).EQ.0) .and. (print .ge. 1)) THEN
  print "(A/(5G14.5))", "Gradients along Hessian eigenvectors:",   &
      (FOB(I),I=1,NTOT)
ENDIF
if (print .ge. 1) then
  WRITE(*,270)INEG
270 FORMAT('There are ',I3,' negative eigenvalues.')
endif
IF (TSTEST) THEN
  WRITE(*,310) IMODE,DIAG(IMODE)
310 FORMAT('Eigenvector following is turned on.'/'Mode '            &
      ,I3,' will be searched uphill. Eigenvalue=',F20.10)
  ! Save eigenvector being followed for use on next step
  VEC(1:ntot)=HNEW(1:ntot,IMODE)
ENDIF

! Calculate step:

SUM=0.0D0
DO J1=1,NTOT
  SUM=SUM+DABS(DIAG(J1))
ENDDO
SUM=SUM/MAX(NTOT-NZEROS,1)
#ifdef TEST_ORSES
PRINT*,'Average Hessian eigenvalue (modulus)=',SUM
TEMP=-1.0D0
#endif

!  Take a step away from a stationary point along the appropriate
!  Hessian eigenvector. This enables us to start from converged minima.
!  Distinguish the case where we want to take a very small step away
!  from a transition state from others where we want a big displacement
!  to get unstuck. Note that here NCYCLE is one more than in OPTIM!

AWAY=.FALSE.
IF (RMS.LT.THRESH) THEN
  IF ((TSTEST).AND.(INEG.NE.1)) THEN
    IF ((NCYCLE.EQ.1).AND.(INEG.EQ.0)) THEN
      IF (IVEC.EQ.0) IVEC=1
      IF (IVEC.GT.0) PRINT*,                                    &
          'Stepping away from minimum along mode ',IMODE,' + direction'
      IF (IVEC.LT.0) PRINT*,                                    &
          'Stepping away from minimum along mode ',IMODE,' - direction'
      AWAY=.TRUE.
    ELSE IF ((NCYCLE.EQ.1).AND.(INEG.GT.1)) THEN
      IF (IVEC.EQ.0) IVEC=1
      AWAY=.TRUE.
      IF (IVEC.GT.0) PRINT*,                                    &
          'Stepping away from higher order saddle along mode ',             &
          IMODE,' + direction'
      IF (IVEC.LT.0) PRINT*,                                    &
          'Stepping away from higher order saddle along mode ',             &
          IMODE,' - direction'
    ELSE IF (MOD(NCYCLE,4).EQ.0) THEN
      PRINT*,'Stepping away from solution of wrong index'
      IF (PUSHOFF.EQ.0.0D0) THEN
        IF (INEG.EQ.0) FOB(IMODE)=stepmax
        DO J1=1,INEG-1
          IF (DABS(FOB(IMODE-J1)).EQ.0.0D0) THEN
            FOB(IMODE-J1)=1D4*stepmax
          ELSE IF (DABS(FOB(IMODE-J1)).LT.0.001D0) THEN
            FOB(IMODE-J1)=1D4*stepmax                           &
                *DABS(FOB(IMODE-J1))/FOB(IMODE-J1)
          ENDIF
        ENDDO
      ELSE
        IF (INEG.EQ.0) FOB(IMODE)=PUSHOFF
        DO J1=1,INEG-1
          FOB(IMODE-J1)=PUSHOFF
        ENDDO
      ENDIF
    ENDIF
  ENDIF
  IF ((MINTEST).AND.(INEG.NE.0)) THEN
    IF ((NCYCLE.EQ.1).AND.(INEG.EQ.1)) THEN
      IF (IVEC.EQ.0) IVEC=1
      AWAY=.TRUE.
      IF (IVEC.GT.0) PRINT*,                                    &
          'Stepping away from transition state along mode ',IMODE,          &
          ' + direction'
      IF (IVEC.LT.0) PRINT*,                                    &
          'Stepping away from transition state along mode ',IMODE,          &
          ' - direction'
    ELSE IF ((NCYCLE.EQ.1).AND.(INEG.GT.1)) THEN
      IF (IVEC.EQ.0) IVEC=1
      AWAY=.TRUE.
      IF (IVEC.GT.0) PRINT*,                                    &
          'Stepping away from higher order saddle along mode ',IMODE,       &
          ' + direction'
      IF (IVEC.LT.0) PRINT*,                                    &
          'Stepping away from higher order saddle along mode ',IMODE,       &
          ' - direction'
    ELSE IF (MINTEST.AND.MOD(NCYCLE,4).EQ.0) THEN
      PRINT*,'Stepping away from solution of wrong index'
      IF (PUSHOFF.EQ.0.0D0) THEN
        DO J1=0,INEG-1
          IF (DABS(FOB(NTOT-J1)).EQ.0.0D0) THEN
            FOB(NTOT-J1)=1D4*stepmax
          ELSE IF (DABS(FOB(NTOT-J1)).LT.0.001D0) THEN
            FOB(NTOT-J1)=1D4*stepmax                          &
                *DABS(FOB(NTOT-J1))/FOB(NTOT-J1)
          ENDIF
        ENDDO
      ELSE
        DO J1=0,INEG-1
          FOB(NTOT-J1)=PUSHOFF
        ENDDO
      ENDIF
    ENDIF
  ENDIF
ENDIF

DO I=1,NTOT
  PSTEP(I)=0.0D0
  IF (ZT(I)) THEN
    !            IF (DABS(DIAG(1)/DIAG(I)).GT.TEMP)TEMP=DABS(DIAG(1)/DIAG(I))
    IF(DIAG(I).NE.0.0)THEN
      LP1=DABS(DIAG(I))/2.0D0
      LP2=1.0D0 + 4.0D0*(FOB(I)/DIAG(I))**2
      LP=LP1*(1.0D0+DSQRT(LP2))

      IF ((I.EQ.IMODE).AND.(TSTEST)) LP=-LP

      !  pseudo-Newton-Raphson

      IF (NRTEST) LP=LP*DIAG(I)/DABS(DIAG(I))
      PSTEP(I)=-FOB(I)/LP
    ELSE
      PSTEP(I)=0.0
    END IF

  ENDIF
ENDDO
IF (AWAY) THEN
  IF (PUSHOFF.NE.0.0D0) THEN
    PSTEP(IMODE)=IVEC*PUSHOFF/ABS(IVEC)
  ELSE
    PSTEP(IMODE)=IVEC*stepmax/(10D0*ABS(IVEC))
  ENDIF
ENDIF
IF (DEBUG(9)) THEN
  DO I=1,NTOT
    WRITE(*,365) I, PSTEP(I)
365 FORMAT('Step for mode ',I3,'=',F20.10)
  ENDDO
ENDIF
#ifdef TEST_ORSES
IF (TEMP.GT.1.0D0)                                                &
    PRINT*,' Largest modulus ratio of non-zero e/vectors=',TEMP
#endif
!  Try adjusting the step using numerical third derivative information.
!  Guess that the mean second derivative is of a similar order to the
!  mean third derivative! Useful for some sensitive reaction paths,
!  but may waste time in ts searches.


IF ((NCYCLE.GT.1).AND.((INR.EQ.3).OR.(INR.EQ.4))) THEN
  PRINT*,'Pseudo-third derivative corrections will be applied'
  !        PRINT*,' Mode    gradient    2nd deriv       lambda
  !    1   step  corrected step'
  DO I=1,NTOT
    !           IF (.NOT.ZT(I)) GOTO 362

    !           LP1=DABS(DIAG(I))/2.0D0
    !           LP2=1.0D0 + 4.0D0*(FOB(I)/DIAG(I))**2
    !           LP=LP1*(1.0D0+DSQRT(LP2))
    !           LP1=PSTEP(I)

    THIRD=SUM
    XALPHA=1.0D10
    IF (DABS(FOB(I)).GT.1.0D-10) XALPHA=-2.0D0*THIRD/FOB(I)
    PSTEP(I)=2.0*PSTEP(I)                                        &
        /(1.0D0+DSQRT(1.0D0+DABS(XALPHA)*PSTEP(I)**2))
    !
#ifdef TEST_ORSES
    WRITE(*,361) I,FOB(I),DIAG(I),LP,LP1,PSTEP(I)
361 FORMAT(I4,5F15.6)
#endif
  end DO
ENDIF

!  Calculate scaling factor from the steps in the normal mode
!  basis. Seems to work best for scaling of step in ev basis
!  for Cartesians and coordinate basis for internals.

ISTCRT=1

!  Scale according to step size in ev basis

IF (ISTCRT.EQ.10) THEN
  CALL VSTAT(PSTEP(1:ntot),AV,NTOT)
  SCALE=MIN(STEPMAX/MAX(AV(1),1D-10),1.0D0)
  STPMAG=MAX(AV(1),1D-10)
  pstep(1:ntot)=scale*pstep(1:ntot)
  !**      CALL SCDOT(SCALE,PSTEP(1),NTOT)
ENDIF

  !  Calculate the steps in the normal rather than the EV basis.

scratch=matmul(hnew,pstep)
!**      DO 380 J=1,NTOT
!**         SCRATCH(J+NTOT)=0.0D0
!**         DO 370 I=1,NTOT
!**            SCRATCH(J+NTOT)=SCRATCH(J+NTOT)+PSTEP(I)*HNEW(J,I)
!**370      CONTINUE
!**380   CONTINUE

!  Scaling if required in the coordinate basis

IF (ISTCRT.EQ.1) THEN
  CALL VSTAT(SCRATCH,AV,NTOT)
  SCALE=MIN(STEPMAX/MAX(AV(1),1D-10),1.D0)
  STPMAG=MAX(AV(1),1D-10)
  scratch=scale*scratch
  !**      CALL SCDOT(SCALE,SCRATCH(NTOT+1),NTOT)

  !  get the scaled steps in the EV basis for archiving

  pstep=scale*pstep
  !**      CALL SCDOT(SCALE,PSTEP(1),NTOT)
  IF (DEBUG(9)) THEN
    WRITE(*,290)
290 FORMAT(50('-'))
    WRITE(*,300)
300 FORMAT('Vector      Gradient        Secder      ',           &
        ' Step')
    WRITE(*,290)
    DO I=1,NTOT
      WRITE(*,311) I,FOB(I),DIAG(I),PSTEP(I)
311   FORMAT(I4,1X,E15.6,1X,E15.6,1X,E13.6)
    end DO
    WRITE(*,290)
  ENDIF


  !   If scaling criteria based on *absolute* step size, take care
  !   of that here
  
ELSE IF (ISTCRT.EQ.0) THEN
  CALL VSTAT(SCRATCH,AV,NTOT)
  stpmag=sqrt(dot_product(scratch,scratch))
  !**   STPMAG=DSQRT(DOT(SCRATCH(NTOT+1),
  !**  1          SCRATCH(NTOT+1),NTOT))
  SCALE=MIN(STEPMAX/MAX(STPMAG,1D-10),1.D0)
  scratch=scale*scratch
  !**      CALL SCDOT(SCALE,SCRATCH(NTOT+1),NTOT)
ENDIF

! Take step

!  Next line is a mystery
!**   scratch(1:ntot)=scratch(1:ntot)+scratch(ntot+1:ntot+ntot)
!**   CALL VADD(SCRATCH(1),SCRATCH(1),SCRATCH(NTOT+1),NTOT,1)
step=scratch
!**   DO J=1,NTOT
!**      STEP(J)=SCRATCH(NTOT+J)
!**   ENDDO

! Summarize

if (print .ge. 1) then
  WRITE(*,'(A,F12.7)')                                              &
      'Eigenvector-following step: the maximum unscaled step is ',    &
      STPMAG
endif

IF (PRINT .GE. 3 .AND. MOD(ISTATUS,10).EQ.0                        &
    .OR. DEBUG(9)) THEN
  WRITE(*,480)
  WRITE(*,470)
470 FORMAT(T3,'Parameter',T20,'dV/dR',T32,'Step',T46,'Rold',         &
        T56,'Rnew')
  WRITE(*,480)
480 FORMAT(64('-'))
  DO I=1,NTOT
    WRITE(*,500)' ',VNEW(I),STEP(I),Q(I),STEP(I)+Q(I)
500 FORMAT(T7,A,T17,F10.6,T29,F10.5,T41,F10.5,T53,F10.5)
  ENDDO
  WRITE(*,480)
ENDIF

! Generate statistics using ZIP to hold scratch vector.

zip(1:ntot)=0d0
!**   CALL ZERO(ZIP,NTOT)
zip(1:ntot)=vnew(1:ntot)
!**   DO I=1,NTOT
!**      ZIP(I)=VNEW(I)
!**   ENDDO
CALL VSTAT(ZIP,TEMP1,NTOT)
if (print .ge. 1) then
  WRITE(*,'(2(A,F15.10))') 'Minimum force: ',TEMP1(2),              &
      ' / RMS force: ',TEMP1(5)
end if
PRMS=TEMP1(5)
CFLAG=.FALSE.
IF ((STPMAG.LT.THRESH).AND.                                        &
    (TEMP1(5).LT.THRESH).AND.(NRTEST.OR.                           &
    (MINTEST.AND.(INEG.EQ.0)).OR.                                   &
    (TSTEST.AND.(INEG.EQ.1))))                                     &
    THEN
  CFLAG=.TRUE.
ENDIF

END SUBROUTINE efol

!-----------------------------------------------------------------------

SUBROUTINE BUILDO(PMAT,Q,AXLIN,PLIN,B,S,SINV)

!   Constructs translation/rotation projection matrix for orient


USE switches
!      IMPLICIT DOUBLE PRECISION (A-H, O-Z)

DOUBLE PRECISION PMAT(:,:), Q(:), AXLIN(:), B(:,:), S(:,:), SINV(:,:)
INTEGER PLIN(0:)

DOUBLE PRECISION D, RQ(NTOT), BNEW(NTOT,6+2*NMOLS)
INTEGER INDX(6+2*NMOLS)

DOUBLE PRECISION :: dat, epsilon, temp
INTEGER :: j, j1, j2, j3, j4, k, ncount, np

EPSILON=1.0D-5

!  Molecular coordinates for x rotation

#ifdef TEST_ORSES
print "(a)", "Entering BUILDO"
#endif
DO J=1,NTRANS/3
  K=3*(J-1)
  RQ(K+1)=Q(K+1)
  RQ(K+2)=Q(K+2)-EPSILON*Q(K+3)
  RQ(K+3)=Q(K+3)+EPSILON*Q(K+2)
ENDDO
DO J=1,NTRANS
  B(J,4)=RQ(J)-Q(J)
ENDDO
DO J=NTRANS/3+1,NTRANS/3+NMOL
  B(3*(J-1)+1,4)=EPSILON
  B(3*(J-1)+2,4)=0.0D0
  B(3*(J-1)+3,4)=0.0D0
ENDDO

!  Molecular coordinates for y rotation

DO J=1,NTRANS/3
  K=3*(J-1)
  RQ(K+1)=Q(K+1)+EPSILON*Q(K+3)
  RQ(K+2)=Q(K+2)
  RQ(K+3)=Q(K+3)-EPSILON*Q(K+1)
ENDDO
DO J=1,NTRANS
  B(J,5)=RQ(J)-Q(J)
ENDDO
DO J=NTRANS/3+1,NTRANS/3+NMOL
  B(3*(J-1)+1,5)=0.0D0
  B(3*(J-1)+2,5)=EPSILON
  B(3*(J-1)+3,5)=0.0D0
ENDDO

!  Molecular coordinates for z rotation

DO J=1,NTRANS/3
  K=3*(J-1)
  RQ(K+1)=Q(K+1)-EPSILON*Q(K+2)
  RQ(K+2)=Q(K+2)+EPSILON*Q(K+1)
  RQ(K+3)=Q(K+3)
ENDDO
DO J=1,NTRANS
  B(J,6)=RQ(J)-Q(J)
ENDDO
DO J=NTRANS/3+1,NTRANS/3+NMOL
  B(3*(J-1)+1,6)=0.0D0
  B(3*(J-1)+2,6)=0.0D0
  B(3*(J-1)+3,6)=EPSILON
ENDDO

!  Translations

DAT=1.0D-5
DO J1=1,NTRANS/3
  J3=3*(J1-1)
  B(J3+1,1)=DAT
  B(J3+2,1)=0.0D0
  B(J3+3,1)=0.0D0
  B(J3+1,2)=0.0D0
  B(J3+2,2)=DAT
  B(J3+3,2)=0.0D0
  B(J3+1,3)=0.0D0
  B(J3+2,3)=0.0D0
  B(J3+3,3)=DAT
  B(NTRANS+J3+1,1)=0.0D0
  B(NTRANS+J3+2,1)=0.0D0
  B(NTRANS+J3+3,1)=0.0D0
  B(NTRANS+J3+1,2)=0.0D0
  B(NTRANS+J3+2,2)=0.0D0
  B(NTRANS+J3+3,2)=0.0D0
  B(NTRANS+J3+1,3)=0.0D0
  B(NTRANS+J3+2,3)=0.0D0
  B(NTRANS+J3+3,3)=0.0D0
ENDDO

!  Add columns for linear molecules

NP=0
NCOUNT=0
666 IF (PLIN(NP).NE.0) THEN
  NP=PLIN(NP)
  NCOUNT=NCOUNT+1
  DO J2=1,NTOT
    B(J2,6+NCOUNT)=0.0D0
  ENDDO
  B(NP,  6+NCOUNT)= AXLIN(NP)
  B(NP+1,6+NCOUNT)= AXLIN(NP+1)
  B(NP+2,6+NCOUNT)= AXLIN(NP+2)
  GOTO 666
ENDIF
IF (NCOUNT.NE.NLIN) THEN
  PRINT*,'Failed consistency check for linear molecules:'
  PRINT*,'In buildo NCOUNT=',NCOUNT,' but NLIN=',NLIN
  STOP
ENDIF

!  If B has a zero column (e.g. all atoms on
!  a coordinate axis) we are in trouble.
!  Need to remove this constraint condition.
!  The total number of zero Hessian eigenvalues is set here.

NZEROS=0
DO J=1,6+NLIN
  TEMP=0.0D0
  DO K=1,NTOT
    TEMP=TEMP+B(K,J)**2
  ENDDO
  TEMP=DSQRT(TEMP)
  IF (TEMP.NE.0.0D0) THEN
    NZEROS=NZEROS+1
    DO K=1,NTOT
      BNEW(K,NZEROS)=B(K,J)/TEMP
    ENDDO
  ENDIF
ENDDO

#ifdef TEST_ORSES
DO J=1,NTOT
  WRITE(*,210) (BNEW(J,K),K=1,NZEROS)
210 FORMAT(6F15.6)
ENDDO
#endif

12 DO J1=1,NZEROS
  DO J2=J1,NZEROS
    SINV(J2,J1)=0.0D0
    SINV(J1,J2)=0.0D0
    TEMP=0.0D0
    DO J3=1,NTOT
      TEMP=TEMP+BNEW(J3,J2)*BNEW(J3,J1)
    ENDDO
    S(J2,J1)=TEMP
    S(J1,J2)=TEMP
  ENDDO
  SINV(J1,J1)=1.0D0
ENDDO
#ifdef TEST_ORSES
PRINT*,'S matrix:'
WRITE(*,250) ((S(J1,J2),J2=1,NZEROS),J1=1,NZEROS)
250 FORMAT(6F12.4)
#endif
CALL LUDCMP(S,NZEROS,6+2*NMOLS,INDX,D)
DO J=1,NZEROS
  D=D*S(J,J)
end DO
#ifdef TEST_ORSES
PRINT*,'DET=',D
#endif
IF (DABS(D).LT.1.0D-10) THEN
  NZEROS=NZEROS-1
  if (print .ge. 1) then
    PRINT*,'Projection matrix is nearly singular, D =', D
    PRINT*,'Reducing dimension by one'
    PRINT*,'NZEROS=',NZEROS
  end if
  GOTO 12
ENDIF
DO J1=1,NZEROS
  CALL LUBKSB(S,NZEROS,6+2*NMOLS,INDX,SINV(:,J1))
ENDDO
DO J1=1,NTOT
  DO J2=J1,NTOT
    TEMP=0.0D0
    DO J3=1,NZEROS
      DO J4=1,NZEROS
        TEMP=TEMP+BNEW(J2,J4)*SINV(J4,J3)*BNEW(J1,J3)
      ENDDO
    ENDDO
    PMAT(J2,J1)=-TEMP
    PMAT(J1,J2)=-TEMP
  ENDDO
  PMAT(J1,J1)=1.0D0+PMAT(J1,J1)
ENDDO
#ifdef TEST_ORSES
PRINT "(A)",'Projection matrix in buildc:'
call matwrt(PMAT(1:NTOT,1:NTOT))
#endif

END SUBROUTINE buildo

!-----------------------------------------------------------------------

SUBROUTINE BUILDC(PMAT,Q,AXLIN,PLIN,B,S,SINV)

!   Constructs translation/rotation projection matrix for orient with
!   constraints

USE switches
!      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
DOUBLE PRECISION PMAT(:,:), Q(:), AXLIN(:), B(:,:), S(:,:),       &
    SINV(:,:)
INTEGER PLIN(0:)

DOUBLE PRECISION D, BNEW(NTOT,6+2*NMOLS)
INTEGER INDX(6+2*NMOLS)

DOUBLE PRECISION :: epsilon, temp
INTEGER :: j, j1, j2, j3, j4, k, ncount, np

#ifdef TEST_ORSES
print "(a)", "Entering BUILDC"
#endif
EPSILON=1.0D-5

!  Columns for unfrozen linear molecules

NP=0
NCOUNT=0
666 IF (PLIN(NP).NE.0) THEN
  NP=PLIN(NP)
  NCOUNT=NCOUNT+1
  DO J2=1,NTOT
    B(J2,NCOUNT)=0.0D0
  ENDDO
  B(NP,  NCOUNT)= AXLIN(NP)
  B(NP+1,NCOUNT)= AXLIN(NP+1)
  B(NP+2,NCOUNT)= AXLIN(NP+2)
  GOTO 666
ENDIF

!  If B has a zero column (e.g. all atoms on
!  a coordinate axis) we are in trouble.
!  Need to remove this constraint condition.
!  The total number of zero Hessian eigenvalues is set here.

NZEROS=0
DO J=1,NCOUNT
  TEMP=0.0D0
  DO K=1,NTOT
    TEMP=TEMP+B(K,J)**2
  ENDDO
  TEMP=DSQRT(TEMP)
  IF (TEMP.NE.0.0D0) THEN
    NZEROS=NZEROS+1
    DO K=1,NTOT
      BNEW(K,NZEROS)=B(K,J)/TEMP
    ENDDO
  ENDIF
ENDDO

#ifdef TEST_ORSES
DO J=1,NTOT
  WRITE(*,210) (BNEW(J,K),K=1,NZEROS)
210 FORMAT(6F15.6)
ENDDO
#endif

12 DO J1=1,NZEROS
  DO J2=J1,NZEROS
    SINV(J2,J1)=0.0D0
    SINV(J1,J2)=0.0D0
    TEMP=0.0D0
    DO J3=1,NTOT
      TEMP=TEMP+BNEW(J3,J2)*BNEW(J3,J1)
    ENDDO
    S(J2,J1)=TEMP
    S(J1,J2)=TEMP
  ENDDO
  SINV(J1,J1)=1.0D0
ENDDO
#ifdef TEST_ORSES
PRINT*,'S matrix:'
WRITE(*,250) ((S(J1,J2),J2=1,NZEROS),J1=1,NZEROS)
250 FORMAT(6F12.4)
#endif
CALL LUDCMP(S,NZEROS,6+2*NMOLS,INDX,D)
DO J=1,NZEROS
  D=D*S(J,J)
end DO
#ifdef TEST_ORSES
PRINT*,'DET=',D
#endif
IF (DABS(D).LT.1.0D-10) THEN
  NZEROS=NZEROS-1
  if (print .ge. 1) then
    PRINT*,'Projection matrix is nearly singular, D =', D
    PRINT*,'Reducing dimension by one'
    PRINT*,'NZEROS=',NZEROS
  end if
  GOTO 12
ENDIF
DO J1=1,NZEROS
  CALL LUBKSB(S,NZEROS,6+2*NMOLS,INDX,SINV(:,J1))
ENDDO
DO J1=1,NTOT
  DO J2=J1,NTOT
    TEMP=0.0D0
    DO J3=1,NZEROS
      DO J4=1,NZEROS
        TEMP=TEMP+BNEW(J2,J4)*SINV(J4,J3)*BNEW(J1,J3)
      ENDDO
    ENDDO
    PMAT(J2,J1)=-TEMP
    PMAT(J1,J2)=-TEMP
  ENDDO
  PMAT(J1,J1)=1.0D0+PMAT(J1,J1)
ENDDO

!     PRINT "(A)",'Projection matrix in buildc:'
!     call matwrt(PMAT(1:NTOT,1:NTOT))

END SUBROUTINE buildc

!-----------------------------------------------------------------------

SUBROUTINE adm(q)
!       IMPLICIT DOUBLE PRECISION (A-H, O-Z)

!  Prints lower diagonal atomic distance matrix for aces program
!  system.

DOUBLE PRECISION, INTENT(IN) :: Q(:)
DOUBLE PRECISION, PARAMETER :: tol = 0.5d0

DOUBLE PRECISION :: dis
INTEGER :: i, icn, iexit, j, jbot, jtop

#ifdef TEST_ORSES
print "(a)", "Entering ADM"
#endif
IF (NATOMS+NMOL .LE. 55 .AND. MOD(ISTATUS,10) .EQ. 0) THEN
  WRITE(6,1092)
1092 FORMAT('Interatomic distance matrix ')
  JBOT=1
  DO WHILE (JBOT .LE. NATOMS+NMOL)
    JTOP=MIN(NATOMS+NMOL,JBOT+4)
    WRITE(6,*)
    WRITE(6,142)("OR",ICN=JBOT,JTOP)
142 FORMAT(18X,A3,4(10X,A3))
    WRITE(6,144)(ICN,ICN=JBOT,JTOP)
144 FORMAT(16X,:'[',I3,']',4(8X,:'[',I3,']'))
    DO I=JBOT,NATOMS+NMOL
      WRITE(6,143)                                                &
          "OR",I,                                                 &
          (sqrt((Q(3*I-2)-Q(3*J-2))**2+                           &
          (Q(3*I-1)-Q(3*J-1))**2+                           &
          (Q(3*I)-Q(3*J))**2),J=JBOT,JTOP)
143   FORMAT(T3,A3,'[',I3,']',5(3X,F10.5))
    end DO
    JBOT=JTOP+1
  END DO
ENDIF

!  Check distances to see if any are too short.  Call exit if below
!  a certain tolerance.

iexit=0
DO I=1,NATOMS+NMOL
  DO J=I+1,NATOMS+NMOL
    DIS=sqrt((Q(3*I-2)-Q(3*J-2))**2+                              &
        (Q(3*I-1)-Q(3*J-1))**2+                                   &
        (Q(3*I)-Q(3*J))**2)
    IF(DIS.LT.TOL)THEN

      !  If one is a dummy, go on.

      WRITE(6,"(A,i2,A,i2,A/A,f7.4,A,f7.4,A)")                    &
          'Atoms ',I,' and ',J,' are rather close.',              &
          'Distance of ',DIS,' is below threshold of ',TOL,'.'
      IEXIT=1
    ENDIF
  end DO
end DO
IF (IEXIT .eq. 1) then
  Write (6,*) ' *Inspect distance matrix and correct Z-matrix.'
  STOP
EndIf
#ifdef TEST_ORSES
DO J1=1,NATOMS+NMOL
  J2=3*(J1-1)
  WRITE(1,65) Q(J2+1),Q(J2+2),Q(J2+3)
65 FORMAT(1X,3F20.15)
end DO
CLOSE(UNIT=1)
REWIND(UNIT=1)
#endif

END SUBROUTINE adm

!  Various private matrix manipulation routines

!-----------------------------------------------------------------------

SUBROUTINE vstat(v,zq,length)

! RETURNS STATISTICAL INFO ABOUT VECTOR V in ZQ
!     ZQ(1)  Largest absolute magnitude
!     ZQ(2)  Smallest absolute magnitude
!     ZQ(3)  Largest value
!     ZQ(4)  Smallest value
!     ZQ(5)  2-norm
!     ZQ(6)  Dynamic range of the vector (abs. min. - abs. max.)

IMPLICIT NONE
DOUBLE PRECISION, INTENT(IN) :: V(:)
DOUBLE PRECISION, INTENT(OUT) :: ZQ(:)
INTEGER, INTENT(IN) :: length

DOUBLE PRECISION :: u
INTEGER :: i

#ifdef TEST_ORSES
print "(a)", "Entering VSTAT"
#endif
u=0.d0
zq(1:6)=0d0
!**   call zero(zq,6)
zq(2)=dabs(v(1))
zq(3)=v(1)
zq(4)=v(1)
#ifdef TEST_ORSES
print*,'Statistics reported for vector:'
write(*,"(6f12.6)") v(1:length)
!10    format(i4,f20.10)
#endif
do i=1,length
  zq(1)=max(zq(1),dabs(v(i)))
  zq(2)=min(zq(2),dabs(v(i)))
  zq(3)=max(zq(3),v(i))
  zq(4)=min(zq(4),v(i))
  u=u+v(i)*v(i)
end do
if (length .ne. 0) zq(5)=sqrt(u/length)
zq(6)=zq(2)-zq(1)

END SUBROUTINE vstat

!-----------------------------------------------------------------------

SUBROUTINE projg(a,fc,fi,nt,rms)
USE switches, only : print
IMPLICIT NONE
DOUBLE PRECISION, INTENT(IN) :: a(:,:),fi(:)
INTEGER, INTENT(IN) :: nt
DOUBLE PRECISION, INTENT(OUT) :: rms,fc(:)

DOUBLE PRECISION, SAVE :: temp(6)

! Projects out translation and rotation from the gradient.
! Check the components of the unprojected gradient


#ifdef TEST_ORSES
print "(a)", "Entering PROJG"
#endif
if (nt.eq.0) then
  call vstat(fi,temp,ntot)
  if (print .ge. 1) then
    print "(/a,F18.10)", "Unprojected  RMS force: ", temp(5)
    !  else
    !   print *
  endif
endif
rms=temp(5)
fc=matmul(a,fi)
#ifdef TEST_ORSES
print*,'Projection matrix A:'
call matwrt(a(1:ntot,1:ntot))
#endif

END SUBROUTINE projg

!-----------------------------------------------------------------------

SUBROUTINE projh(a,hc)
!  Projects translation/rotation out of the Hessian.

DOUBLE PRECISION A(:,:),HC(:,:)
DOUBLE PRECISION SCRATCH(size(A,1),size(hc,2))

scratch=MATMUL(HC,A)
HC=matmul(A,SCRATCH)

END SUBROUTINE projh

!-----------------------------------------------------------------------

!     This subprogram performs a sort on the input data and
!     arranges it from smallest to biggest. The exchange-sort
!     algorithm is used.

SUBROUTINE SORT(N,A,NA)
!      IMPLICIT DOUBLE PRECISION (A-H,P-Z)
INTEGER J1, L, N, J2, NA(:), NTEMP
DOUBLE PRECISION TEMP, A(:)

DO J1=1,N-1
  L=J1
  DO J2=J1+1,N
    IF (A(L).GT.A(J2)) L=J2
  end do
  TEMP=A(L)
  A(L)=A(J1)
  A(J1)=TEMP
  NTEMP=NA(L)
  NA(L)=NA(J1)
  NA(J1)=NTEMP
end do

END SUBROUTINE SORT

!-----------------------------------------------------------------------

SUBROUTINE eigsrt(d,v,n)
!  Copyright (C) 1986, 1992 Numerical Recipes Software

!  Numerical recipes routine to sort the eigenvalues d(i) into descending
!  order and to rearrange the eigenvectors v(:,i) accordingly. Probably
!  not very efficient. The eigenvectors should be returned in ascending
!  order by dsygv but this doesn't always happen.
INTEGER, INTENT(IN) :: n
DOUBLE PRECISION :: d(:),v(:,:)
INTEGER i,j,k
DOUBLE PRECISION :: p, q(n)
do i=1,n-1
  k=i
  p=d(i)
  do j=n,i+1,-1
    if (d(j).ge.p) then
      k=j
      p=d(j)
    endif
  end do
  if(k.ne.i)then
    d(k)=d(i)
    d(i)=p
    q(:)=v(:,i)
    v(:,i)=v(:,k)
    v(:,k)=q(:)
  endif
end do

END SUBROUTINE eigsrt

END MODULE orses_module
