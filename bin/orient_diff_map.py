#!/usr/bin/python
#  -*-  coding  iso-8859-1  -*-

"""Generate difference potential maps for N-benzyl-N'-methyl imidazolium cation.
"""

import argparse
import re
import os
# import string
import subprocess

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Generate difference potential maps for N-benzyl-N'-methyl imidazolium cation.
""",epilog="""
E.g.:
$ORIENT/bin/orient_diff_map.py bmi135_chelpg.defn --name bmi135 --reference bmi135_vdW2_potential.grid \\
     --title "aug-cc-pVDZ CHELPG charges - Sadlej exact"
""")


parser.add_argument("file", help="Molecule and multipole definition file")
parser.add_argument("--name", help="molecule name", default="bmi135")
parser.add_argument("--reference", help="reference potential grid")
parser.add_argument("--title", help="Title for picture", default="")
parser.add_argument("--range", help="Voltage range for display",
                    default=0.12)
parser.add_argument("--out", "-o", help="Orient output file", default="")


args = parser.parse_args()

if args.out == "":
  if re.search(r'\.defn$', args.file):
    out = re.sub(r'\.defn$', ".out", args.file)
    data = re.sub(r'\.defn$', ".data", args.file)
  else:
    out = args.file + ".out"
    data = args.file + ".data"
else:
  out = args.out
  data = args.file + ".data"

with open(args.file) as DEFN:
  defn = DEFN.read()

with open(data,"w") as IN:
  IN.write("""
Parameters
  Sites  32
End

Types
  H  Z  1
  C  Z  6
  N  Z  7
  O  Z  8
End

""")
  IN.write(defn)

  IN.write("""
Find {n} cm

Edit {n}
  Bonds auto
End

Display potential
  Title "{n} {t}"
  Molecule {n}
  Viewport 15
  Viewpoint 1 -1 0 up 1 1 1 
  #include /home/ajs1/molecules/colourmaps/colourmap
  ball-and-stick
  lighting off
  Colour-scale min -{m} max {m}
  import reference {r} with values

End

Finish
  """.format(n=args.name,t=args.title,r=args.reference, m=args.range))

with open(data) as IN, open(out,"w") as OUT:
  subprocess.call(["orient"], stdin=IN, stdout=OUT)

