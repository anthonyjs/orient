SUBROUTINE lists(k1,k2,limit)

!  Find the S functions that are needed for the interaction between
!  sites k1 and k2, and list them in numerical order.

USE indparams
USE consts, ONLY : eslimit
USE labels
USE switches
USE types
USE sites
USE sizes
USE molecules
USE induction
USE interact
USE binomials
USE Sfns, ONLY : t1s, t2s, js, maxixes
IMPLICIT NONE


INTEGER, INTENT(IN) :: k1, k2
INTEGER, INTENT(IN), OPTIONAL :: limit
INTEGER :: i, ix, ixp, t1, t2, u1,u2, n, maxjes,                  &
    pk1, pk2, ps1, ps2, sk1, sk2
LOGICAL :: needed, iterate


!  Limit electrostatic interactions to R^(-limit) terms. Default limit = eslimit.
if (present(limit)) then
  maxjes = min(l(k1)+l(k2),limit-1,eslimit-1)
else
  maxjes = min(l(k1)+l(k2),eslimit-1)
endif
if (l(k1) < 0 .or. l(k2) < 0) maxjes = -1
if (debug(10))                                                     &
    print '(/A,2I3/2A)', 'Sites', k1, k2,                          &
    '    S function        Q1          Q2       binom',         &
    '      rho      alpha       C6 '

iterate = switch(8)

t1 = type(k1)
t2 = type(k2)
sk1 = qsize(l(k1))
sk2 = qsize(l(k2))
pk1 = ps(k1)
pk2 = ps(k2)
ps1 = -1
ps2 = -1
if (pk1 > 0) ps1 = pz(k1)
if (pk2 > 0) ps2 = pz(k2)

ixp = 0
!  erfactor = Bmunit(0,0)
!  Find start of list of S functions needed for repulsion-dispersion
pair = pairindex(t1,t2)
if (pair > 0) then
  ixp = sptr(pair)
  !  erfactor = Bmunit(0,pair)
endif
!  isdisp(n) is true if we have R^-n dispersion terms
do n = 6,12
  isdisp(n) = .false.
end do
!  list(n) is the index number of the S function needed at position n.

!  Identify electrostatic terms
n = 0
do ix = 1,maxixes(maxjes)
  u1 = t1s(ix)
  u2 = t2s(ix)
!  Not optimised
  needed = ((Q(u1,k1) .ne. 0d0 .and. u1 .le. sk1)                   &
      .and. (Q(u2,k2) .ne. 0d0 .and. u2 .le. sk2)                 &
      .or. ixs(ixp) .eq. ix                                       &
      .or. u1 .le. ps1 .and. (Q(u2,k2) .ne. 0 .and. u2 .le. sk2)  &
      .or. u2 .le. ps2 .and. (Q(u1,k1) .ne. 0 .and. u1 .le. sk1)  &
      .or. iterate .and. u1 .le. ps1 .and. u2 .le. ps2            &
      .or. ix .eq. 1) ! Always include isotropic term
!  Optimised (different order might be better!)
!       needed = Q(u1,k1) .ne. 0d0 .and. Q(u2,k2) .ne. 0d0
!       if (.not. needed) then
!        needed = ixs(ixp) .eq. ix
!        if (.not. needed) then
!         needed = u1 .le. ps1 .and. Q(u2,k2) .ne. 0
!         if (.not. needed) then
!          needed = u2 .le. ps2 .and. Q(u1,k1) .ne. 0
!          if (.not. needed) then
!           needed = iterate .and. u1 .le. ps1 .and. u2 .le. ps2
!           if (.not. needed) then
!            needed = ix .eq. 1
!           end if
!          end if
!         end if
!        end if
!       end if
!  End optimised
  if (needed) then
    n = n+1
    list(n) = ix
    fac(n) = binom(js(ix),lq(u1))
    q1(n) = Q(u1,k1)
    q2(n) = Q(u2,k2)
    power(n) = js(ix)+1
    ranks(:,n) = [lq(u1),lq(u2)]
    if (ixs(ixp) .eq. ix) then
!  We need this one for repulsion-dispersion too.
      index(n) = ixp
      rhox(n) = rho(ixp)
      alphax(n) = alph(ixp)
      do i = 6,12
        Cx(n,i) = C(ixp,i)
        if (Cx(n,i) .ne. 0d0) isdisp(i) = .true.
      end do
      ixp = nexts(ixp)
    else
!  Record zeros for repulsion-dispersion
      index(n) = 0
      rhox(n) = 0d0
      alphax(n) = 0d0
      do i = 6,12
        Cx(n,i) = 0d0
      end do
    endif
    if (debug(10)) print '(I4,2X,2A,I2,2F12.6,F7.1,3F11.4)',       &
        ix, label(u1), label(u2), js(ix),                          &
        q1(n), q2(n), fac(n), rhox(n), alphax(n), Cx(n,6)
  endif
end do
nesmax = n

!  There may be repulsion-dispersion terms that can't occur in the electrostatic
!  energy. Add them to the list.
do while (ixp .ne. 0)
  ix = ixs(ixp)
  n = n+1
  list(n) = ix
  q1(n) = 0d0
  q2(n) = 0d0
  fac(n) = 0d0
  power(n) = 0
  index(n) = ixp
  rhox(n) = rho(ixp)
  alphax(n) = alph(ixp)
  do i = 6,12
    Cx(n,i) = C(ixp,i)
    if (Cx(n,i) .ne. 0d0) isdisp(i) = .true.
  end do
  if (debug(10)) then
    u1 = t1s(ix)
    u2 = t2s(ix)
    print '(I4,2X,2A,I2,2F14.8,F7.1,3F14.8)',                      &
        ix, label(u1), label(u2), js(ix),                          &
        q1(n), q2(n), fac(n), rhox(n), alphax(n), Cx(n,6)
  endif
  ixp = nexts(ixp)
end do
!  Mark end of list
nmax = n
list(n+1) = 0

END SUBROUTINE lists
