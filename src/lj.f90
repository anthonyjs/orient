MODULE lj

IMPLICIT NONE

PRIVATE
PUBLIC :: lj_sigma, lj_sigma6, lj_sigma12, lj_epsilon,             &
    lj_flag, takec6c12_flag, ljatoms_keyword,                      &
    setupljarray, ljforces, ljforces_symp, heteroljforces,         &
    init_chargearray, takecharges_flag, chargecontribution

INTEGER, PARAMETER :: maxljpair=5000

DOUBLE PRECISION :: lj_sigma, lj_sigma6, lj_sigma12, lj_epsilon
LOGICAL :: lj_flag, takec6c12_flag

INTEGER :: nljpair,m1_start,k1_start,m2_start,k2_start
INTEGER :: sljpair(maxljpair,4)
DOUBLE PRECISION :: cljpair(maxljpair,2)
LOGICAL :: ljoverload_flag

!     maxchargepairs maximum number of charge-charge interacting pairs
!     nchargepairs   actual number of charge-charge interacting pairs
!     chargepairs    array of site numbers for each pair
!     chargearray    array of charge1*charge2 for each pair

INTEGER, PARAMETER :: maxchargepairs=500
INTEGER :: nchargepairs, chargepairs(maxchargepairs,4)
LOGICAL :: takecharges_flag
DOUBLE PRECISION :: chargearray(maxchargepairs)

CONTAINS

SUBROUTINE setupljarray

IMPLICIT NONE

ljoverload_flag=.false.
call setljstartvals
call loadljarray

if (k1_start .ne. -1) then
  print *,'         ...LJ-pair array overloading used!'
  call setljstartvals
  ljoverload_flag=.true.
end if

END SUBROUTINE setupljarray

!-----------------------------------------------------------------------

SUBROUTINE setljstartvals

IMPLICIT NONE

m1_start=1
k1_start=-1
m2_start=-1
k2_start=-1

END SUBROUTINE setljstartvals

!-----------------------------------------------------------------------

SUBROUTINE loadljarray

USE types
USE sites
USE molecules
IMPLICIT NONE

INTEGER :: m1, k1, m2, k2, ip, m2_tmpstart

nljpair=0
do m1=m1_start,mols-1
  !  Loop over all connected sites
  if (k1_start .eq. -1) then
    k1=head(m1)
  else
    k1=k1_start
    k1_start=-1
  end if
  do while (k1 .ne. 0)
    if (m2_start .ne. -1) then
      m2_tmpstart=m2_start
      m2_start=-1
    else
      m2_tmpstart=m1+1
    end if
    do m2=m2_tmpstart,mols
      if (k2_start .eq. -1) then
        k2=head(m2)
      else
        k2=k2_start
        k2_start=-1
      end if
      do while (k2 .ne. 0)
        ip=pairindex(type(k1),type(k2))
        if ((C(sptr(ip),6)*C(sptr(ip),12)) .ne. 0.0d0) then
          nljpair=nljpair+1
          sljpair(nljpair,1)=k1            !site number
          sljpair(nljpair,2)=k2
          sljpair(nljpair,3)=m1            !molecule number
          sljpair(nljpair,4)=m2
          cljpair(nljpair,1)=C(sptr(ip),6) !LJ parameters
          cljpair(nljpair,2)=C(sptr(ip),12)
        end if
        k2=next(k2)
        if (nljpair .ge. maxljpair) then
          m1_start=m1
          k1_start=k1
          m2_start=m2
          k2_start=k2
          return
        end if
      end do
    end do !m2
    k1=next(k1)
  end do !k1
end do !m1

call setljstartvals

END SUBROUTINE loadljarray

!-----------------------------------------------------------------------

SUBROUTINE ljforces(forder,vkt,etotal)

USE derivs
USE molecules
IMPLICIT NONE

INTEGER :: forder,imol,jmol,i
DOUBLE PRECISION :: etotal
DOUBLE PRECISION :: vkt(14*nmols),rij(3),r2,r6,forceval

etotal=0.0d0
if (forder .eq. 1) then
  do imol=1,mols
    do i=1,3
      deriv1(i,imol)=0.0d0
    end do
  end do
end if

do imol=1,mols-1
  do jmol=imol+1,mols
    do i=1,3
      rij(i)=vkt(i+6*(imol-1))-vkt(i+6*(jmol-1))
    end do
    r2=rij(1)*rij(1)+rij(2)*rij(2)+rij(3)*rij(3)
    r6=r2*r2*r2
    etotal=etotal+4.0d0*lj_epsilon*(lj_sigma12/(r6*r6)-lj_sigma6/r6)

    !  Force calculation
    if (forder .eq. 1) then
      do i=1,3
        forceval=24.0d0*lj_epsilon*rij(i)*                             &
            (lj_sigma6/(r6*r2)-2.0d0*lj_sigma12/(r6*r6*r2))
        !  Newton's law
        deriv1(i,imol)=deriv1(i,imol)+forceval
        deriv1(i,jmol)=deriv1(i,jmol)-forceval
      end do
    end if
  end do
end do

END SUBROUTINE ljforces

!-----------------------------------------------------------------------

SUBROUTINE ljforces_symp(forder,etotal)

USE derivs
USE sites
USE molecules
IMPLICIT NONE

INTEGER :: forder,imol,jmol,i
DOUBLE PRECISION :: etotal
DOUBLE PRECISION :: rij(3),r2,r6,forceval

etotal=0.0d0
if (forder .eq. 1) then
  do imol=1,mols
    do i=1,3
      deriv1(i,imol)=0.0d0
    end do
  end do
end if

do imol=1,mols-1
  do jmol=imol+1,mols
    do i=1,3
      rij(i)=SX(i,CM(imol))-SX(i,CM(jmol))
    end do
    r2=rij(1)*rij(1)+rij(2)*rij(2)+rij(3)*rij(3)
    r6=r2*r2*r2
    etotal=etotal+4.0d0*lj_epsilon*(lj_sigma12/(r6*r6)-lj_sigma6/r6)

    !  Force calculation
    if (forder .eq. 1) then
      do i=1,3
        forceval=24.0d0*lj_epsilon*rij(i)*                             &
            (lj_sigma6/(r6*r2)-2.0d0*lj_sigma12/(r6*r6*r2))
        !  Newton's law
        deriv1(i,imol)=deriv1(i,imol)+forceval
        deriv1(i,jmol)=deriv1(i,jmol)-forceval
      end do
    end if
  end do
end do

END SUBROUTINE ljforces_symp

!-----------------------------------------------------------------------

SUBROUTINE heteroljforces(forder,etotal)

USE derivs
USE sites
USE molecules
IMPLICIT NONE

INTEGER, INTENT(IN) :: forder
DOUBLE PRECISION, INTENT(OUT) :: etotal
INTEGER :: i, imol, ipair, j, k, jmol
INTEGER :: mol1, mol2, coor1, coor2, site1, site2, sk1, sk2
DOUBLE PRECISION :: rij(3), r2, r6, r12, ab, pmsign(0:1), forceval(3)
DOUBLE PRECISION :: df_dr, df_dr2, df_dr2dr2, rcm(6), addterm, rxab(6)
DOUBLE PRECISION :: raa(0:1)

etotal=0.0d0
if (forder .ge. 1) then
  do imol=1,mols
    do i=1,6
      deriv1(i,imol)=0.0d0
    end do
  end do

  if (forder .eq. 2) then
    do imol=1,mols
      do i=1,6
        do jmol=1,mols
          do j=1,6
            deriv2(i,j,imol,jmol)=0.0d0
          end do
        end do
      end do
    end do
    pmsign(0)=1.0d0
    pmsign(1)=-1.0d0
  end if !(forder .eq. 2)

end if !(forder .ge. 1)

9999 if (ljoverload_flag) then
  call loadljarray
end if

do ipair=1,nljpair
  do i=1,3
    rij(i)=SX(i,sljpair(ipair,1))-SX(i,sljpair(ipair,2))
  end do
  r2=rij(1)*rij(1)+rij(2)*rij(2)+rij(3)*rij(3)
  r6=r2*r2*r2
  r12=r6*r6
  etotal=etotal-cljpair(ipair,2)/(r12)-cljpair(ipair,1)/r6

  !  Force calculation
  if (forder .ge. 1) then

    df_dr=12.0d0*cljpair(ipair,2)/(r12*r2)  & !factor 1/r included
        +6.0d0*cljpair(ipair,1)/(r6*r2)

    site1=sljpair(ipair,1)
    site2=sljpair(ipair,2)
    mol1=sljpair(ipair,3)
    mol2=sljpair(ipair,4)

    do i=1,3  !site-CM distance
      rcm(i)  =sx(i,site1)-sx(i,cm(mol1))
      rcm(i+3)=sx(i,site2)-sx(i,cm(mol2))
    end do

    !  Translational derivatives dU/dr*dr/dx
    do i=1,3
      forceval(i)=rij(i)*df_dr
      !  Newton's law
      deriv1(i,mol1)=deriv1(i,mol1)+forceval(i)
      deriv1(i,mol2)=deriv1(i,mol2)-forceval(i)
    end do

    !  Angular derivatives dU/dr*dr/dTheta
    do i=1,3
      sk1=mod(i,3)+1
      sk2=mod(i+1,3)+1
      deriv1(3+i,mol1)=deriv1(3+i,mol1)                               &
          +rcm(sk1)*forceval(sk2)-rcm(sk2)*forceval(sk1)
      deriv1(3+i,mol2)=deriv1(3+i,mol2)                               &
          -rcm(3+sk1)*forceval(sk2)+rcm(3+sk2)*forceval(sk1)
    end do

    !  Second derivatives
    if (forder .eq. 2) then
      df_dr2   =12.0d0*cljpair(ipair,2)/(r12*r2)      & !factor 2 included
          +6.0d0*cljpair(ipair,1)/(r6*r2)
      df_dr2dr2=-168.0d0*cljpair(ipair,2)/(r12*r2*r2) & !factor 4 included
          -48.0d0*cljpair(ipair,1)/(r6*r2*r2)

      !  Translational dU/dxdy
      do i=1,3
        do j=i+1,3
          do k=0,1
            mol1=sljpair(ipair,3+k)
            mol2=sljpair(ipair,4-k)
            addterm=df_dr2dr2*rij(i)*rij(j)
            deriv2(i,j,mol1,mol1)=deriv2(i,j,mol1,mol1)+addterm
            deriv2(j,i,mol1,mol1)=deriv2(i,j,mol1,mol1) !!!!could go down
            deriv2(i,j,mol1,mol2)=deriv2(i,j,mol1,mol2)-addterm
            deriv2(j,i,mol1,mol2)=deriv2(i,j,mol1,mol2) !!!!could go down
          end do
        end do
      end do

      do i=1,3    !trace derivatives contain term dU/d(r2)*d(r2)/dx1dx2
        do k=0,1
          mol1=sljpair(ipair,3+k)
          mol2=sljpair(ipair,4-k)
          addterm=df_dr2dr2*rij(i)**2+df_dr2
          deriv2(i,i,mol1,mol1)=deriv2(i,i,mol1,mol1)+addterm
          deriv2(i,i,mol1,mol2)=deriv2(i,i,mol1,mol2)-addterm
        end do
      end do

      !  Mixed dU/dxdTheta
      ab=0.0d0
      raa(0)=0.0d0
      raa(1)=0.0d0
      do i=1,3
        ab=ab+rcm(i)*rcm(i+3)
        raa(0)=raa(0)+(rij(i)-rcm(i))*rcm(i)
        raa(1)=raa(1)+(rij(i)+rcm(i+3))*rcm(i+3)
      end do

      do i=1,6
        coor1=mod(i,3)+1
        coor2=mod(i+1,3)+1
        k=(i-1)/3
        rxab(i)=rij(coor1)*rcm(coor2+k*3)                              &
            -rij(coor2)*rcm(coor1+k*3)
      end do

      do i=1,3
        do j=4,6
          coor1=mod(j-3,3)+1
          coor2=mod(j-2,3)+1
          do k=0,1
            mol1=sljpair(ipair,3+k)
            mol2=sljpair(ipair,4-k)
            addterm=df_dr2dr2*rij(i)*rxab(j-3+k*3)
            deriv2(i,j,mol1,mol1)=deriv2(i,j,mol1,mol1)-addterm
            deriv2(j,i,mol1,mol1)=deriv2(i,j,mol1,mol1) !!!!could go down
            deriv2(i,j,mol1,mol2)=deriv2(i,j,mol1,mol2)                  &
                +df_dr2dr2*rij(i)*rxab(j-k*3)
            deriv2(j,i,mol1,mol2)=deriv2(j,i,mol1,mol2)+addterm
          end do
        end do
      end do

      do j=0,1 !positive and negative "Levi-Civita type" contributrion
        do i=1,3
          coor1=mod(i+j,3)+1
          coor2=mod(i+1-j,3)+1
          do k=0,1
            mol1=sljpair(ipair,3+k)
            mol2=sljpair(ipair,4-k)
            addterm=pmsign(j)*df_dr2*rcm(coor2+k*3)
            deriv2(i,coor1+3,mol1,mol1)=deriv2(i,coor1+3,mol1,mol1)      &
                +addterm
            deriv2(i+3,coor1,mol1,mol1)=deriv2(i+3,coor1,mol1,mol1)      &
                -addterm
            deriv2(i,coor1+3,mol1,mol2)=deriv2(i,coor1+3,mol1,mol2)      &
                -pmsign(j)*df_dr2*rcm(coor2+3-3*k)
            deriv2(i+3,coor1,mol1,mol2)=deriv2(i+3,coor1,mol1,mol2)      &
                +addterm
          end do
        end do
      end do

      !  Pure rotational dU/dTheta dTheta
      do j=4,5    !off trace only
        do i=j+1,6
          do k=0,1
            mol1=sljpair(ipair,3+k)
            mol2=sljpair(ipair,4-k)
            addterm=df_dr2dr2*rxab(i-3+3*k)*rxab(j-3+3*k)
            deriv2(i,j,mol1,mol1)=deriv2(i,j,mol1,mol1)+addterm          &
                +df_dr2*pmsign(k)                                          &
                *(rij(i-3)-pmsign(k)*rcm(i-3+3*k))*rcm(j-3+3*k)
            deriv2(j,i,mol1,mol1)=deriv2(j,i,mol1,mol1)+addterm          &
                +df_dr2*pmsign(k)                                          &
                *(rij(j-3)-pmsign(k)*rcm(j-3+3*k))*rcm(i-3+3*k)

            addterm=-df_dr2dr2*rxab(i-3+3*k)*rxab(j-3*k)                 &
                +df_dr2*rcm(i-3*k)*rcm(j-3+3*k)
            deriv2(i,j,mol1,mol2)=deriv2(i,j,mol1,mol2)+addterm
            deriv2(j,i,mol2,mol1)=deriv2(j,i,mol2,mol1)+addterm
          end do
        end do
      end do

      do i=4,6    !trace only
        do k=0,1
          mol1=sljpair(ipair,3+k)
          deriv2(i,i,mol1,mol1)=deriv2(i,i,mol1,mol1)                   &
              +df_dr2dr2*rxab(i-3+3*k)**2                                 &
              +df_dr2*pmsign(k)                                           &
              *((rij(i-3)-pmsign(k)*rcm(i-3+k*3))*rcm(i-3+k*3)-raa(k))
        end do
        mol1=sljpair(ipair,3)
        mol2=sljpair(ipair,4)
        deriv2(i,i,mol1,mol2)=deriv2(i,i,mol1,mol2)                    &
            -df_dr2dr2*rxab(i-3)*rxab(i)                   &
            +df_dr2*(rcm(i)*rcm(i-3)-ab)
        deriv2(i,i,mol2,mol1)=deriv2(i,i,mol1,mol2)      !!!!could go down
      end do

    end if !forder==2

  end if !forder>=1

end do !ipair

if (k1_start .ne. -1) then
  goto 9999
end if

!  Electrostatic from charges
if (takecharges_flag) then
  call chargecontribution(forder,etotal)
end if

END SUBROUTINE heteroljforces

!-----------------------------------------------------------------------

SUBROUTINE init_chargearray

USE sites
USE molecules
IMPLICIT NONE


INTEGER :: imol1,k1,imol2,k2

nchargepairs=0
do imol1=1,mols-1
  !  Loop over all connected sites
  !  cm changed to head -- ajs, april 2001
  k1=head(imol1)
  do while (k1 .ne. 0)
    !  Charge located
    if (Q(1,k1) .ne. 0.0d0) then
      do imol2=imol1+1,mols
        k2=head(imol2)
        do while (k2 .ne. 0)
          !  Pairing charge
          if (Q(1,k2) .ne. 0.0d0) then
            nchargepairs=nchargepairs+1
            if (nchargepairs .gt. maxchargepairs) then
              stop 'ERROR: maxchargepairs too small (chargesdata.h)'
            end if
            chargepairs(nchargepairs,1)=k1
            chargepairs(nchargepairs,2)=k2
            chargepairs(nchargepairs,3)=imol1
            chargepairs(nchargepairs,4)=imol2
            chargearray(nchargepairs)=Q(1,k1)*Q(1,k2)
          end if
          k2=next(k2)
        end do
      end do!imol2
    end if
    k1=next(k1)
  end do!loop over sites
end do!imol1

END SUBROUTINE init_chargearray

!-----------------------------------------------------------------------

SUBROUTINE chargecontribution(forder,etotal)

!  Contains many redundancies with subroutine heteroljforces

USE derivs
USE sites
USE molecules
IMPLICIT NONE

INTEGER, INTENT(IN) :: forder
DOUBLE PRECISION, INTENT(OUT) :: etotal
INTEGER :: i ,ipair, j, k
INTEGER :: mol1 ,mol2, coor1, coor2, site1, site2, sk1, sk2
DOUBLE PRECISION :: rij(3), r2, ab, pmsign(0:1), forceval(3)
DOUBLE PRECISION :: df_dr, df_dr2, df_dr2dr2, rcm(6), addterm, rxab(6)
DOUBLE PRECISION :: raa(0:1), radial

pmsign(0)=1.0d0
pmsign(1)=-1.0d0

do ipair=1,nchargepairs
  do i=1,3
    rij(i)=SX(i,chargepairs(ipair,1))-SX(i,chargepairs(ipair,2))
  end do
  r2=rij(1)*rij(1)+rij(2)*rij(2)+rij(3)*rij(3)
  radial=sqrt(r2)

  !  Energy calculation
  etotal=etotal+chargearray(ipair)/radial

  !  Force calculation
  if (forder .ge. 1) then

    df_dr=-chargearray(ipair)/(radial*r2) !factor 1/r included

    site1=chargepairs(ipair,1)
    site2=chargepairs(ipair,2)
    mol1=chargepairs(ipair,3)
    mol2=chargepairs(ipair,4)

    do i=1,3  !site-CM distance
      rcm(i)  =sx(i,site1)-sx(i,cm(mol1))
      rcm(i+3)=sx(i,site2)-sx(i,cm(mol2))
    end do

    !  Translational derivatives dU/dr*dr/dx
    do i=1,3
      forceval(i)=rij(i)*df_dr
      !  Newton's law
      deriv1(i,mol1)=deriv1(i,mol1)+forceval(i)
      deriv1(i,mol2)=deriv1(i,mol2)-forceval(i)
    end do

    !  Angular derivatives dU/dr*dr/dTheta
    do i=1,3
      sk1=mod(i,3)+1
      sk2=mod(i+1,3)+1
      deriv1(3+i,mol1)=deriv1(3+i,mol1)                               &
          +rcm(sk1)*forceval(sk2)-rcm(sk2)*forceval(sk1)
      deriv1(3+i,mol2)=deriv1(3+i,mol2)                               &
          -rcm(3+sk1)*forceval(sk2)+rcm(3+sk2)*forceval(sk1)
    end do

    !  Second derivatives
    if (forder .eq. 2) then
      df_dr2   =-1.0d0*chargearray(ipair)/(radial*r2)    !factor 2 included
      df_dr2dr2= 3.0d0*chargearray(ipair)/(radial*r2*r2) !factor 4 included

      !  Translational dU/dxdy
      do i=1,3
        do j=i+1,3
          do k=0,1
            mol1=chargepairs(ipair,3+k)
            mol2=chargepairs(ipair,4-k)
            addterm=df_dr2dr2*rij(i)*rij(j)
            deriv2(i,j,mol1,mol1)=deriv2(i,j,mol1,mol1)+addterm
            deriv2(j,i,mol1,mol1)=deriv2(i,j,mol1,mol1) !!!!could go down
            deriv2(i,j,mol1,mol2)=deriv2(i,j,mol1,mol2)-addterm
            deriv2(j,i,mol1,mol2)=deriv2(i,j,mol1,mol2) !!!!could go down
          end do
        end do
      end do

      do i=1,3    !trace derivatives contain term dU/d(r2)*d(r2)/dx1dx2
        do k=0,1
          mol1=chargepairs(ipair,3+k)
          mol2=chargepairs(ipair,4-k)
          addterm=df_dr2dr2*rij(i)**2+df_dr2
          deriv2(i,i,mol1,mol1)=deriv2(i,i,mol1,mol1)+addterm
          deriv2(i,i,mol1,mol2)=deriv2(i,i,mol1,mol2)-addterm
        end do
      end do

      !  Mixed dU/dxdTheta
      ab=0.0d0
      raa(0)=0.0d0
      raa(1)=0.0d0
      do i=1,3
        ab=ab+rcm(i)*rcm(i+3)
        raa(0)=raa(0)+(rij(i)-rcm(i))*rcm(i)
        raa(1)=raa(1)+(rij(i)+rcm(i+3))*rcm(i+3)
      end do

      do i=1,6
        coor1=mod(i,3)+1
        coor2=mod(i+1,3)+1
        k=(i-1)/3
        rxab(i)=rij(coor1)*rcm(coor2+k*3)                              &
            -rij(coor2)*rcm(coor1+k*3)
      end do

      do i=1,3
        do j=4,6
          coor1=mod(j-3,3)+1
          coor2=mod(j-2,3)+1
          do k=0,1
            mol1=chargepairs(ipair,3+k)
            mol2=chargepairs(ipair,4-k)
            addterm=df_dr2dr2*rij(i)*rxab(j-3+k*3)
            deriv2(i,j,mol1,mol1)=deriv2(i,j,mol1,mol1)-addterm
            deriv2(j,i,mol1,mol1)=deriv2(i,j,mol1,mol1) !!!!could go down
            deriv2(i,j,mol1,mol2)=deriv2(i,j,mol1,mol2)                  &
                +df_dr2dr2*rij(i)*rxab(j-k*3)
            deriv2(j,i,mol1,mol2)=deriv2(j,i,mol1,mol2)+addterm
          end do
        end do
      end do

      do j=0,1 !positive and negative "Levi-Civita type" contributrion
        do i=1,3
          coor1=mod(i+j,3)+1
          coor2=mod(i+1-j,3)+1
          do k=0,1
            mol1=chargepairs(ipair,3+k)
            mol2=chargepairs(ipair,4-k)
            addterm=pmsign(j)*df_dr2*rcm(coor2+k*3)
            deriv2(i,coor1+3,mol1,mol1)=deriv2(i,coor1+3,mol1,mol1)      &
                +addterm
            deriv2(i+3,coor1,mol1,mol1)=deriv2(i+3,coor1,mol1,mol1)      &
                -addterm
            deriv2(i,coor1+3,mol1,mol2)=deriv2(i,coor1+3,mol1,mol2)      &
                -pmsign(j)*df_dr2*rcm(coor2+3-3*k)
            deriv2(i+3,coor1,mol1,mol2)=deriv2(i+3,coor1,mol1,mol2)      &
                +addterm
          end do
        end do
      end do

      !  Pure rotational dU/dTheta dTheta
      do j=4,5    !off trace only
        do i=j+1,6
          do k=0,1
            mol1=chargepairs(ipair,3+k)
            mol2=chargepairs(ipair,4-k)
            addterm=df_dr2dr2*rxab(i-3+3*k)*rxab(j-3+3*k)
            deriv2(i,j,mol1,mol1)=deriv2(i,j,mol1,mol1)+addterm          &
                +df_dr2*pmsign(k)                                          &
                *(rij(i-3)-pmsign(k)*rcm(i-3+3*k))*rcm(j-3+3*k)
            deriv2(j,i,mol1,mol1)=deriv2(j,i,mol1,mol1)+addterm          &
                +df_dr2*pmsign(k)                                          &
                *(rij(j-3)-pmsign(k)*rcm(j-3+3*k))*rcm(i-3+3*k)

            addterm=-df_dr2dr2*rxab(i-3+3*k)*rxab(j-3*k)                 &
                +df_dr2*rcm(i-3*k)*rcm(j-3+3*k)
            deriv2(i,j,mol1,mol2)=deriv2(i,j,mol1,mol2)+addterm
            deriv2(j,i,mol2,mol1)=deriv2(j,i,mol2,mol1)+addterm
          end do
        end do
      end do

      do i=4,6    !trace only
        do k=0,1
          mol1=chargepairs(ipair,3+k)
          deriv2(i,i,mol1,mol1)=deriv2(i,i,mol1,mol1)                   &
              +df_dr2dr2*rxab(i-3+3*k)**2                                 &
              +df_dr2*pmsign(k)                                           &
              *((rij(i-3)-pmsign(k)*rcm(i-3+k*3))*rcm(i-3+k*3)-raa(k))
        end do
        mol1=chargepairs(ipair,3)
        mol2=chargepairs(ipair,4)
        deriv2(i,i,mol1,mol2)=deriv2(i,i,mol1,mol2)                    &
            -df_dr2dr2*rxab(i-3)*rxab(i)                   &
            +df_dr2*(rcm(i)*rcm(i-3)-ab)
        deriv2(i,i,mol2,mol1)=deriv2(i,i,mol1,mol2)      !!!!could go down
      end do

    end if !forder==2

  end if !forder>=1

end do !ipair

END SUBROUTINE chargecontribution

!-----------------------------------------------------------------------

SUBROUTINE ljatoms_keyword

USE consts
USE input
!     USE mdmc, ONLY :
USE molecules, ONLY : natoms, mols
IMPLICIT NONE

CHARACTER(LEN=12) :: WW

lj_flag=.true.

!  Check, if only atoms
if (natoms .ne. mols) then
  call die('MDLJATOMS is used for molecules!',.true.)
end if

do while (item .lt. nitems)
  call readu(ww)
  if (ww .eq. 'SIGMA') then
    call readf(lj_sigma)
    lj_sigma=lj_sigma/rfact
    lj_sigma6=lj_sigma**6
    lj_sigma12=lj_sigma6*lj_sigma6
  else if (ww .eq. 'EPSILON') then
    call readf(lj_epsilon)
    lj_epsilon=lj_epsilon/efact
  else
    call die('MDLJATOMS keyword not recognized',.true.)
  endif
end do

END SUBROUTINE ljatoms_keyword

END MODULE lj
