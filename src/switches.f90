MODULE switches

INTEGER, SAVE :: print=0, xprint=0
LOGICAL, DIMENSION(12), SAVE, TARGET :: switch =                       &
    (/ .false., .false., .false., .false.,                             &
    .true., .true., .false., .true.,                                   &
    .false., .false., .false., .false. /)
LOGICAL, DIMENSION(16), SAVE :: debug = .false.
CHARACTER(LEN=8), DIMENSION(12) :: swname=(/"        ", "Internal",    &
    "PSites  ", "Online  ", "Reflect ", "Final   ", "Induce  ",        &
    "Iterate ", "Retain  ", "SkipPol ", "Lattice ", "Recalc_S"/)
LOGICAL, POINTER :: recalc_sfns

!  Significance of switches:
!    1
!    2 (INTERNAL) if TRUE, intramolecular contributions to the energy are
!      to be included. Electrostatic interactions between atoms that are
!      linked to each other or to a common atom are ignored.
!    3 if TRUE, polarizable sites are present.
!    4 (ONLINE) TRUE if input is from the terminal.
!    5 (REFLECT) if TRUE, causes details of site and molecule specifications
!      to be reflected to the output. It also causes the positions of all
!      molecules to be printed before and after an optimisation (and their
!      sites afterwards, if FINAL is set); otherwise the position of
!      molecule 1 is omitted.
!    6 (FINAL) if TRUE, causes the site positions to be printed after an
!      optimisation is completed.
!    7 (INDUCE) if TRUE, causes induction energies to be evaluated
!      and included with the electrostatic energy.
!    8 (ITERATE) if TRUE, causes the induced moments to be calculated
!      iteratively to convergence. If FALSE, only one iteration is
!      performed, to give first-order induced moments and the
!      corresponding energy.
!    9 (RETAIN) if TRUE, induced moments from the previous induction
!      calculation are not cleared to zero when a new induction calculation
!      is started, unless first-order induction is required. This is set
!      in the optimization routine and normally need not be set explicitly.
!   10 if TRUE, POLARIZABILITY commands in the input are ignored.
!   11 (LATTICE) if TRUE, a lattice of point charges has been defined
!      and the interaction energy of molecules with the lattice is to
!      be included in the calculation.
!   12 (RECALC_S) if TRUE, S functions and their derivatives used in the
!      induction energy calculation are recalculated at every iteration.
!      Otherwise they are calculated once and stored, but this may require
!      too much memory for large problems.
!  Switches 1-4 and 9-12 are FALSE by default, and switches 5-8 TRUE.
!  However, if no polarizabilities have been defined, switch(7) is set FALSE.

!  Debug switches:
!    1 GENRAT: Detailed printing of generated points
!    2 GENRAT: Limited printing only
!    3 DIFFER: Print details of differences
!    4 EDIT:   Print confirmation of editing actions
!    5 REDUCE: Print molecular axes
!    6 INDUCE: Print intermediate values in construction of induced moments
!    7 AXES:   Print molecular geometry in abbreviated form
!    8 FDIFF:  Calculate derivatives by finite difference and compare with
!              analytic results.
!    9 ORSES:  Debug printing in ORSES routines.
!   10 SFNS:   Information about S function values and electrostatic terms.
!   11 DDIFF:  Check damping function derivatives by finite difference.
!   12         Extra printing in fit routines.

CONTAINS

SUBROUTINE readswitch

USE input
IMPLICIT NONE
CHARACTER*12 w
INTEGER k

do while (item .lt. nitems)
  call readu(w)
  select case(w)
  case("INTERNAL")
    K=2
  case("REFLECT")
    K=5
  case("FINAL")
    K=6
  case("INDUCE","INDUCTION")
    K=7
  case("ITERATE","ITERATION")
    K=8
  case("LATTICE")
    K=11
  case("RECALC_S","RECALC_SFNS")
    K=12
  case default
    call reread(-1)
    call readi(k)
  end select
  call readu(w)
  if (w .eq. "ON" .or. w .eq. " ") then
    switch(k)=.true.
  else if (w .eq. "OFF") then
    switch(k)=.false.
  else
    switch(k)=.true.
    call reread(-1)
  endif
end do

END SUBROUTINE readswitch

SUBROUTINE debugswitch

USE input
IMPLICIT NONE
INTEGER k

do while (item .lt. nitems)
  call readi(k)
  if (k .gt. 0 .and. k .le. 16) then
    debug(k)=.true.
  else if (k .lt. 0 .and. k .ge. -16) then
    debug(-k)=.false.
  else
    call die("Debug number out of range",.true.)
  endif
end do

END SUBROUTINE debugswitch

END MODULE switches
