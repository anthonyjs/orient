#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Run test jobs for Orient.
"""

import argparse
# import re
import os
# import string
import subprocess
import datetime

#  Get the absolute path to this file
this = os.path.abspath(__file__)
# print this

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Run test jobs for Orient.
""",epilog="""
Standard tests (run by the --all flag) are
H2CO..HF formamide water_hexamer HF..HF H2O..H2O virialcoeff
fit_CH4 NaCl..w2 MD basinhop dielectric

Other tests (run if named explicitly on the command line):
urea_nanocrystal  (takes more time)

--compiler, with or without --debug, can be used to specify a particular
compiled binary version. Default is to use the version pointed to by
bin/orient.

Examples:
orient_tests.py --compiler ifort --all
orient_tests.py formamide water_hexamer --force

""")

if "ARCH" in os.environ:
    arch = os.environ["ARCH"]
else:
    arch = "x86-64"
if "COMPILER" in os.environ:
    compiler = os.environ["COMPILER"]
else:
    compiler = "gfortran"

parser.add_argument("tests", help="Tests to run", nargs="*")
parser.add_argument("--arch", default=arch,
                    help="Machine architecture (default $ARCH)")
which = parser.add_mutually_exclusive_group()
which.add_argument("--compiler", default="",
                    help='Use executable from this compiler (default "")')
which.add_argument("--exe", help="Executable Orient file", default="")
parser.add_argument("--debug", help="Use debug version",
                    action="store_true")
parser.add_argument("--clear", help="Mark all or specified tests as not done",
                    action="store_true")
parser.add_argument("--clean", action="store_true",
                    help="Delete output files for all or specified tests, and mark them as not done")
parser.add_argument("--all", help="Run all tests", action="store_true")
parser.add_argument("--force", help="Force tests to be run",
                    action="store_true")
parser.add_argument("--omit", help="Omit specified tests", nargs="+", default=[])


args = parser.parse_args()

# if "ORIENT" in os.environ:
#   base = os.environ["ORIENT"]
# else:
base = os.path.dirname(os.path.dirname(this))
os.chdir(os.path.join(base,"tests"))
here = os.getcwd()
print("Working in directory {}".format(here))

test_list = ["H2CO..HF", "formamide", "water_hexamer", "HF..HF",
           "H2O..H2O", "virialcoeff", "fit_CH4", "NaCl..w2",
               "MD", "basinhop", "dielectric"]
#  ,"urea_nanocrystal"

if args.omit:
    for test in args.omit:
        if test in test_list:
            test_list.remove(test)
        else:
            print("No test {}".format(test))

def clean(test):
    #  Remove test output files
    os.chdir(test)
    if os.path.exists("clean"):
        try:
            subprocess.call("clean", shell=True)
        except subprocess.CalledProcessError:
            print("Failed to clean output files for", test)
        # print "{} test output files cleared".format(test)
    os.chdir("..")
    #  Remove "test done" flag file
    if os.path.exists(test+".done"):
        os.remove(test+".done")

#  Which tests?
if args.all or len(args.tests) == 0:
    tests = test_list
else:
    tests = args.tests

if args.clear:
    for test in tests:
        if os.path.exists(test+".done"):
            os.remove(test+".done")
    exit(0)

if args.clean:
    for test in tests:
        #  Remove test output files and exit
        clean(test)
    exit(0)

if args.exe:
    program = args.exe
elif args.compiler:
    if args.debug:
        program = os.path.join(base,arch,args.compiler,"debug","orient")
    else:
        program = os.path.join(base,arch,args.compiler,"exe","orient")
else:
    program = os.path.join(base,"bin","orient")

def put(string):
    #  Note: explicit newline needed at end of string
    global LOG
    LOG.write(string)
    print(string)

if os.path.exists("test.log"):
    os.rename("test.log","test.log.old")

done = 0
with open("test.log","w") as LOG:

    put("""Program version:
  {}\n""".format(program))
    now = datetime.datetime.now()
    #  echo $(date "+%H:%M on %A %d %B %Y")
    put(now.strftime("  %H:%M on %A %d %B %Y\n"))

    for test in tests:
        os.chdir(here)
        if os.path.exists(test+".done"):
            if args.force:
                os.remove(test+".done")
            else:
                #  Skip this test
                continue
        print("-----------------------------------------------")
        print(f"Test {test} ... ")
        clean(test)
        os.chdir(test)
        if os.path.exists("prologue"):
            with open("prologue") as PRLG:
                print(PRLG.read())
    
        datafile = "data"
        while os.path.exists(datafile):
            with open(datafile) as IN, open("out","w") as OUT:
                rc = subprocess.call([program],stdin=IN, stdout=OUT, 
                             stderr=subprocess.STDOUT)
            done += 1
            if rc > 0:
                put("Program failed in " + test)
                break
            rc = subprocess.call(["./check.py"], shell=True)
            if rc == 0:
                put(f"Test {test+':':15s} successful\n")
                # print os.path.join(here,test+".done")
                subprocess.call(["/usr/bin/touch", os.path.join(here,test+".done")])
                break
            elif datafile == "data" and os.path.exists("data2"):
                print ("""
That didn't work. Trying again with alternative data file.
(See data file for explanation.)
""")
                datafile = "data2"
            else:
                break
        if rc > 0:
            put(f"Test {test+':':15s} FAILED\n")

if done == 0:
    print("""No tests carried out. (Perhaps tests have already been carried out.)
Use --force to re-do tests (or FRC=--force if using make).""")

os.chdir(here)
if os.path.exists("test.log"):
    if done == 0:
        print("\nSummary of previous test:\n")
    else:
        print("\nSummary:\n")
    with open("test.log") as LOG:
        print(LOG.read())
print("""
See tests/<test>/data and tests/<test>/out for full data
and output for <test>.
""")
