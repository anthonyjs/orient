#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Construct adlayer diagram for Orient manual.
"""

import argparse
# import re
# import os.path
# import string
import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Construct adlayer diagram for Orient manual.
""",epilog="""
{} args
""".format(this))


# parser.add_argument("req", help="Positional argument")
# parser.add_argument("--flag", help="option flag")

args = parser.parse_args()

with open("adlayer.tex","w") as TEX:
  TEX.write(r"""\documentclass[pslatex,12pt]{paper}
\usepackage{pstricks}
%%%  Bold symbols

\DeclareSymbolFont{mathbold}{OML}{cmm}{b}{it}
\DeclareMathSymbol{\ba}{\mathord}{mathbold}{`a}
\DeclareMathSymbol{\bb}{\mathord}{mathbold}{`b}

\begin{document}
\begin{center}
\setlength{\unitlength}{1mm}
\begin{picture}(80,50)
\put(0,30){\begin{minipage}[b]{20mm}
\tt CELL\\
\quad A 1 1\\
\quad B 1 -1\\
END
\end{minipage}}
\put(20,0){%
\begin{pspicture}(0mm,-20mm)(60mm,30mm)
\psset{unit=1mm}
\psset{linewidth=0.5pt,linestyle=dashed,arrowsize=2pt 3}
\psline(0,20)(60,30)\psline(0,10)(60,20)\psline(0,0)(60,10)
\psline(0,-10)(60,0)\psline(0,-20)(60,-10)
\psline(0,-20)(0,20)\psline(15,-17.5)(15,22.5)\psline(30,-15)(30,25)
\psline(45,-12.5)(45,27.5)\psline(60,-10)(60,30)
\psset{linestyle=solid}
\psline{<->}(0,0)(0,-10)(15,-7.5)\rput[l](1,-2){$\bb$}\rput(12,-6){$\ba$}
\psset{linewidth=1pt,linestyle=solid}
\psline(15,2.5)(30,15)(45,7.5)(30,-5)(15,2.5)
\end{pspicture}}
\end{picture}
\end{center}
\end{document}
""")

subprocess.call("latex adlayer.tex", shell=True)
subprocess.call("dvips -o adlayer.pdf adlayer.dvi", shell=True)

