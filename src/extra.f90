SUBROUTINE extra(word)

USE input, ONLY : die
USE fit, ONLY : adjust, readimpt, define_params
USE virial_module, ONLY : virial

IMPLICIT NONE
CHARACTER*(*) word
DOUBLE PRECISION e3

select case(word)

case('ADJUST')
  call adjust
case('ANALYSE')
  ! Analyse [depth]
  call analyse
case("DEFINE")
  call define_params
case('FIT')
  call readimpt
case('FOURIER')
  call fourier
case('SLICE')
  call slice
case('TRIPLE-DIPOLE')
  call triple(e3)
case('VIRIAL')
  call virial
case('COMPARE')
  call compare
! case("DEPOLARIZE")
!   call depolarize
! case("REPOLARIZE")
!   call repolarize
case default
  call die('Unrecognized command '//WORD,.TRUE.)
end select

END SUBROUTINE extra
