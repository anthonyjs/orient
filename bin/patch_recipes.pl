#!/usr/bin/perl -w

@files=();
&help, exit unless @ARGV;

if ( defined $ENV{RECIPES} ) {
  $recipes=$ENV{RECIPES};
}
else {
  $recipes="";
}

while ( @ARGV ) {
  $f=shift;
  &help, exit if $f eq "-h" || $f eq "--help";
  $recipes=shift, next if $f eq "--recipes" || $f eq "-r";
  $recipes=$f if $recipes eq "";
  push @files, $f;
}

@files=(qw"recipes.f90 virial_module.F90 histograms.f90 md.F90") unless @files;

for ( @files ) {

  if ( /recipes.f90/ ) {

    #  Generate recipes.f90
    #  Skip if it exists already
    if ( -e "recipes.f90" ) {
      print "recipes.f90 is already present. Move or delete it if you wish to proceed.\n";
    } else {
      #  Construct raw version
      system "cat $recipes/{jacobi.f,ludcmp.f,lubksb.f,pythag.f,sobseq.f,svdcmp.f} > ./recipes.tmp1";
      
      die "Failed to create temporary file" if !-e "recipes.tmp1" || -z "recipes.tmp1";
      open (IN,"recipes.tmp1") or die ;
      open (OUT,">recipes.tmp2");
      while (<IN>) {
	s/REAL/DOUBLE PRECISION/;
	s/(\d+\.\d*)e/$1d/g;
	s/(\d+\.\d*)(?![0-9d])/$1d0/g;
	s/ ( +)pause/!$1pause/;
	print OUT;
	      }
      close IN;
      close OUT;

      unlink "recipes.tmp1";

      system "patch < recipes.diff";
      rename("recipes.tmp2","recipes.f90") or die "Can't rename patched file recipes.tmp2";
    }
  }
#-----------------------------

  elsif ( /virial_module.F90/ ) {
    #   Generate virial_module.F90

    #  Skip if it exists already
    if ( -e "virial_module.F90" ) {
      print "virial_module.F90 is already present. Move or delete it if you wish to proceed.\n";
    }
    else {

      #  Append NR routine to stripped file
      open(IN,"virial_module.strip") or die "Can't open virial_module.strip";
      open (OUT,">virial_module.tmp");
      while (<IN>) {
	print OUT;
	      }
      open (IN,"$recipes/gauleg.f") or die "Can't open $recipes/gauleg.f";
      while (<IN>) {
	print OUT;
	      }
      print OUT "\n      END MODULE virial_module\n";
      close OUT;
      close IN;
      
      #  Apply patch
      system "patch < virial_module.diff";
      rename "virial_module.tmp", "virial_module.F90";
    }
  }

#-----------------------------

  elsif ( /histograms.f90/ ) {
    #   Generate histograms.f90
    #  Skip if it exists already
    if ( -e "histograms.f90" ) {
      print "histograms.f90 is already present. Move or delete it if you wish to proceed.\n";
    }
    else {

      system "cp -f histograms.strip histograms.tmp";
      chmod (0644, "histograms.tmp");
      #  Append NR routines and convert to double precision
      open (OUT,">>histograms.tmp") or die "Can't open histograms.tmp for append";
      for $file ( qw"mrqmin.f mrqcof.f covsrt.f gaussj.f fgauss.f" ) {
	open (IN,"$recipes/$file") or die "Can't open $recipes/$file";
	while (<IN>) {
	  s/REAL/DOUBLE PRECISION/;
	  s/(\d+\.\d*)e/$1d/g;
	  s/(\d+\.\d*)(?![0-9d])/$1d0/g;
	  s/ ( +)pause/!$1pause/;
	  print OUT;
		}
	close IN;
	      }
      print OUT "\n      END MODULE histograms\n";
      close OUT;
      system "patch < histograms.diff";
      rename "histograms.tmp", "histograms.f90";
    }
  }

#-----------------------------

  elsif ( /md.F90/ ) {
    #   Generate md.F90

    #  Skip if it exists already
    if ( -e "md.F90" ) {
      print "md.F90 is already present. Move or delete it if you wish to proceed.\n";
    }
    else {
      open (IN,"md.strip") or die "Can't open md.strip";
      open (OUT,">md.tmp") or die "Can't open md.tmp for output";
      #  Insert raw NR odeint routine and convert to double precision
      while (<IN>) {
	if (/SUBROUTINE my_odeint/) {
	  open(NR,"$recipes/odeint.f") or die "Can't open $recipes/odeint.f";
	  while (<NR>) {
	    s/REAL/DOUBLE PRECISION/;
	    s/(\d+\.\d*)e/$1d/g;
	    s/(\d+\.\d*)(?![0-9d])/$1d0/g;
	    s/ ( +)pause/!$1pause/;
	    print OUT;
		  }
	  close NR;
        }
	else {
	  print OUT;
	}
      }
      close IN;
      #  Append other NR routines and convert to double precision
      for $file ( qw"bsstep.f mmid.f pzextr.f rkqs.f rkck.f" ) {
	open (IN,"$recipes/$file") or die "Can't open $recipes/$file";
	while (<IN>) {
	  s/REAL/DOUBLE PRECISION/;
	  s/(\d+\.\d*)e/$1d/g;
	  s/(\d+\.\d*)(?![0-9d])/$1d0/g;
	  s/ ( +)pause/!$1pause/;
	  print OUT;
	}
	close IN;
      }
      print OUT "\n      END MODULE md\n";
      close OUT;
      system "patch < md.diff";
      rename "md.tmp", "md.F90";
    }
  }

}


sub help {
  print "Merge the Numerical Recipes files, with local modifications, into
the Orient files that use them.

Usage: $0 [ -r | --recipes ] <recipes-directory> [ file file ... ]

Run the command in the Orient src directory. The file arguments may include
one or more of
recipes.f90
virial_module.F90
histograms.f90
md.F90
If no file argument is given, all are processed. The script will refuse
to generate any of these files that already exists.

The procedure starts with file.strip (or an empty file in the case of
recipes.f90). The necessary recipes files are added, with minor
modifications to convert to double precision. The result is patched
using file.diff to regenerate file.f90 or file.F90. Thus there are 3
files in each case: file.strip (except for recipes.f90), file.diff, and
file.f90 or file.F90.


The recipes-directory should contain the single-precision Fortran77 versions
of the necessary Numerical Recipes files, namely

bsstep.f
covsrt.f
fgauss.f
gauleg.f
gaussj.f
jacobi.f
ludcmp.f
lubksb.f
mmid.f
mrqmin.f
mrqcof.f
odeint.f
pythag.f
pzextr.f
rkck.f
rkqs.f
sobseq.f
svdcmp.f


";
}
