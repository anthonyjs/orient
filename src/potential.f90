MODULE potential

USE parameters
USE atom_grids, ONLY : xl => x, yl => y, zl => z, Lebedev, test_angular_grid
USE sites

IMPLICIT NONE

DOUBLE PRECISION :: xg(3,:), xt(3,:)

INTEGER, PRIVATE :: i
DOUBLE PRECISION, PARAMETER :: vdwdef=2.5d0
!  Bondi van der Waals radii, J Phys Chem (1964) 68, 441
!  These values are in bohr.
DOUBLE PRECISION, PARAMETER :: vdw_radius(0:82) = (/ 0.00d0,           &
    2.268d0, 2.646d0,                   & !  H, He
    3.440d0, vdwdef,  vdwdef,  3.213d0, & !  Li, Be, B, C
    2.929d0, 2.872d0, 2.778d0, 2.910d0, & !  N, O, F, Ne
    4.290d0, 3.270d0, vdwdef,  3.968d0, & !  Na, Mg, Al, Si
    3.402d0, 3.402d0, 3.307d0, 3.553d0, & !  P, S, Cl, Ar
    5.197d0, vdwdef,  vdwdef,  vdwdef,  & !  K, Ca, Sc, Ti
    vdwdef,  vdwdef,  vdwdef,  vdwdef,  & !  V, Cr, Mn, Fe
    vdwdef,  3.080d0, 2.646d0, 2.627d0, & !  Co, Ni, Cu, Zn
    3.534d0, vdwdef,  3.496d0, 3.590d0, & !  Ga, Ge, As, Se
    3.496d0, 3.817d0, vdwdef,  vdwdef,  & !  Br, Kr, Rb, Sr
    vdwdef,  vdwdef,  vdwdef,  vdwdef,  & !  Y, Zr, Nb, Mo
    vdwdef,  vdwdef,  vdwdef,  3.080d0, & !  Tc, Ru, Rh, Pd
    3.250d0, 2.986d0, 3.647d0, 4.100d0, & !  Ag, Cd, In, Sn
    vdwdef,  3.893d0, 3.742d0, 4.082d0, & !  Sb, Te, I, Xe
    (vdwdef,i=55,72),                   & !  Cs, Ba, La-Hf
    vdwdef,  vdwdef,  vdwdef,  vdwdef,  & !  Ta, W, Re, Os
    vdwdef,  3.250d0, 3.137d0, 2.929d0, & !  Ir, Pt, Au, Hg
    3.704d0, 3.817d0 /)                   !  Tl, Pb


SUBROUTINE shell(m,n,r)

!  Construct a grid around molecule m, using a Lebedev grid with at
!  least n points per site initially, though points within the shell of
!  another site are omitted. Each atom shell is at r times the van der
!  Waals radius of the site. The van der Waals radius is based on the
!  atomic number assigned to the site type. Site with zero radius do
!  not have any grid points assigned to them.


INTEGER, INTENT(IN) :: m
INTEGER, INTENT(INOUT) :: n
DOUBLE PRECISION, INTENT(IN) :: r

INTEGER :: i, k, p, zk(NSITES)
DOUBLE PRECISION :: rk(NSITES)

Lebedev=.true.
call test_angular_grid(n)

! print "(i0,a)", n, " points"
! print "(4f12.6)", (x(i), y(i), z(i), w(i), i=1,n)

call axes(m)
k=head(m)
do while (k>0)
  zk(k)=atnum(type(k))
  if (zk(k) .eq. 0) cycle
  rk(k)=vdw_radius(zk(k))*r
  k=next(k)
end do

k=head(m)
p=0
pmax=size(x,2)
do while (k>0)
  if (type(k) .eq. 0) cycle
  if (zk(k) .eq. 0) cycle
  do i=1,n
    p=p+1
    if (p > pmax) then
      allocate (xt(3,pmax))
      xt=x
      deallocate (x)
      allocate (x(3,2*pmax))
      x(:,1:pmax)=(xt(:,1:pmax))
      deallocate (xt)
      pmax=2*pmax
    end if
    x(1,p)=sx(1,k)+x(i)*rk(k)
    x(2,p)=sx(2,k)+y(i)*rk(k)
    x(3,p)=sx(3,k)+z(i)*rk(k)
    kk=head(m)
    do while (kk>0)
      if (type(kk) .eq. 0) cycle
      if (zk(kk) .eq. 0) cycle
      if ((x(1,p)-sx(1,kk))**2+(x(2,p)-sx(2,kk))**2+(x(3,p)-sx(3,kk))**2 &
          < rk(kk)**2) then
        p=p-1
        exit
      end if
      kk=next(kk)
    end do
  end do
end do

END PROGRAM shell

END MODULE potential
