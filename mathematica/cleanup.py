#!/usr/bin/env python3
#  -*-  coding:  iso-8859-1  -*-

"""Tidy up the S-function F90 file.
"""

import os
import re
import argparse

parser = argparse.ArgumentParser(formatter_class = argparse.RawDescriptionHelpFormatter,
    description="""Tidy up the S-function F90 file.
""", epilog="""
The original file is renamed as file.bak.
""")
parser.add_argument("file", help="Fortran90 file produced by Mathematica") 
args = parser.parse_args()

os.rename(args.file, args.file+".bak")

with open(args.file,"w") as OUT:
    with open(args.file+".bak") as IN:
        for line in IN:
            line = re.sub('"', '', line)
            if re.search(r' *= *0$', line):
                continue
            if re.search(r'(case|level|end *if)', line) or re.match(r' *!', line) or re.match(r' *$', line):
                OUT.write(line)
                continue
            # print line,
            [p, sep, line] = re.split('(= *)', line)
            p += sep
            # print p
            while re.search(r'\d+', line):
                m = re.match(r'(.*?)(\d+(\.\d*)?)', line)
                if m:
                    p += m.group(1)
                    n = m.group(2)
                    line = re.sub(r'(.*?)(\d+(\.\d*)?)', '', line, count=1)
                    # print p, ":", n, ":", line,
                    if re.search(r'\*\*$', p):
                        p += n
                    else:
                        p += n + "d0"
            p += line
            # print p
            p = re.sub(r'sqrt\((\d+)d0\)', r'rt\1', p, flags=re.I)
            m = re.match(r'( *)', p)
            pad = m.group(1) + "    "
            while len(p) > 72:
                m = re.match(r'(.{1,71}) ', p)
                s = m.group(1)
                p = pad + re.sub(r'(.{1,71}) ', "", p, count=1)
                OUT.write(s + " "*(72-len(s)) + "&\n")
            OUT.write(p)
