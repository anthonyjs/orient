MODULE properties

!  Various routines for manipulating multipole moments
!  Also for calculating molecular centres of mass, moments of inertia
!  and inertial axes.

IMPLICIT NONE

PRIVATE
PUBLIC :: inertia, calclinks, totalq, solidh, shiftq,                  &
    realtocomplex, complextoreal

CONTAINS

SUBROUTINE inertia (mm,site,nomoments, print)

USE types
USE input, only : die
USE sites
USE molecules, ONLY: mol, mols, cm, mmass, mi, head
USE rotations, ONLY: angleaxis

!  Finds centre of mass, the inertial axes and the moments of inertia
!  for molecule mm, or for the whole system if mol=0.
!  This requires
!    (a) Atom coordinates in array sx,
!    (b) Atom types correctly set up to supply the atomic masses.

!  If the optional argument site is present, the coordinates of the
!  centre of mass are written to sx(:,site). 

!  Otherwise, if a centre-of-mass site has been allocated for this
!  molecule it is used; if not, a new site km is allocated for the
!  centre of mass. Site 0 is used for the system moments of inertia.
!  On exit the position of this site (sx(i,km)) is the centre of mass.

!  If the optional argument nomoments is present and is true, the
!  moments of inertia are not calculated.

!  Otherwise the orientation (se(i,j,km)) gives the inertial axes, and
!  the moments of inertia (a.m.u. bohr^2) are in mi(i,mol), in ascending
!  order of magnitude unless the molecule is a prolate symmetric top.

INTEGER, INTENT(IN) :: mm
INTEGER, INTENT(IN), OPTIONAL :: site
LOGICAL, INTENT(IN), OPTIONAL :: nomoments, print

DOUBLE PRECISION :: rmass, mim(3,3), u(3,3), x(3), xcm(3)
INTEGER :: m, m1, m2, k, km, i, j, nrot

m=mm
if (m .ne. 0) then
  if (mol(m)%mdim .eq. 0) then
    !  Atom: no need for a separate site for c.m.
    cm(m)=head(m)
    mmass(m)=mass(type(head(m)))
    mi(:,m)=0d0
    mol(m)%cm=sx(:,cm(m))
    return
  end if
end if

!  Find centre of mass and moments of inertia
mmass(m)=0d0
xcm(:)=0d0
mim(:,:)=0d0

if (mm .eq. 0) then
  m1=1
  m2=mols
else
  m1=mm
  m2=mm
endif
do m=m1,m2
  k=head(m)
  do while (k .ne. 0)
    !    print "(I3,2x,a,i3,2x,a,2x,F10.5)",                            &
    !       k, name(k), type(k), tname(type(k)), mass(type(k))
    if (type(k) .gt. 0 .and. mass(type(k)) .ne. 0d0) then
      rmass=mass(type(k))
      mmass(mm)=mmass(mm)+rmass
      xcm(:)=xcm(:)+rmass*sx(:,k) ! centre of mass in global coordinates
      x(:)=sx(:,k)      ! refer moment of inertia to global origin
      do i=1,3
        mim(i,i)=mim(i,i)+rmass*(x(1)**2+x(2)**2+x(3)**2)
        do j=1,3
          mim(i,j)=mim(i,j)-rmass*x(i)*x(j)
        end do
      end do
    endif
    k=next(k)
  end do  ! end loop over sites of rigid body m
end do  ! end loop over molecules

if (mmass(mm) .eq. 0d0) then
  if (mm==0) then
    call die ("No masses defined for this system")
  else
    call die("No masses defined for "//trim(name(head(mm))))
  end if
end if
xcm(:)=xcm(:)/mmass(mm)
x(:)=xcm(:)   ! Centre of mass relative to global origin

!  Here we have, if mm>0,
!    the centre of mass of molecule mm in xcm(:) and mol(mm)%cm
!    the moment of inertia of molecule mm, relative to the global origin,
!    in mim(:,:),
!  or, if mm=0,
!    the centre of mass of the whole system in xcm(:)
!    the moment of inertia of the whole system, relative to the global
!    origin, in mim(:,:).

m=mm
if (present(site)) then
  km=site
else if (mm==0) then
  km=0
else if (cm(m) .ne. 0) then
  km=cm(m)
else
!  Assign site for centre of mass. (Note: we need a new site, even if the
!  centre of mass coincides with the molecular origin, because its orientation
!  is the orientation of the inertial axes. The same is true for the system
!  centre of mass; site 0, as previously used, describes the global origin
!  and axes.)
  call new_site(km)
  cm(m)=km
  sm(km)=km
  next(km)=head(m)
  link(km)=0
  type(km)=0
  sf(km)=sf(head(m))
  name(km)="c.m. "//name(head(m))
endif

sx(:,km)=xcm
if (m>0) mol(m)%cm=xcm

if (present(nomoments)) then
  if (nomoments) return
endif

!       print '(a/a/(19x,3f12.6))',
!    &    'Inertia tensor relative to global origin',
!    &    '                        x           y           z',
!    &    ((mim(i,j), j=1,3), i=1,3)

!  Refer inertia tensor to centre of mass
do i=1,3
  mim(i,i)=mim(i,i)-mmass(mm)*(x(1)**2+x(2)**2+x(3)**2)
  do j=1,3
    mim(i,j)=mim(i,j)+mmass(mm)*x(i)*x(j)
  end do
end do

!       print '(a/a/(19x,3f12.6))',
!    &    'Inertia tensor relative to centre of mass:',
!    &    '                        x           y           z',
!    &    ((mim(i,j), j=1,3), i=1,3)
!  Eigenvalues and eigenvectors of inertia tensor. We don't sort them, so
!  that the new axes are as close as possible to the old ones.
if (mm .ne. 0) then
  !  Transform molecular inertia tensor to molecular coordinate system
  !  call transform(3,mim,3,se(1,1,head(mm)),3,u,3,.true.)
  mim=matmul(transpose(se(:,:,head(mm))),matmul(mim,se(:,:,head(mm))))
endif
call jacobi(mim,3,3,mi(1,mm),u,nrot)
!  call hdiag(.true.,.false.,mim,u,3,3)
if (mm .eq. 0) then
  se(:,:,km)=u
else
  !   print '(a/A,3f12.6/a,3f12.6)',
  !  &    name(head(m)),
  !  &    'Centre of mass:    ', (xcm(i), i=1,3),
  !  &    'Moments of inertia:', (mi(i,m), i=1,3)
  !   print '(a/a/(19x,3f12.6))',                                       &
  !       'Inertial axes relative to molecular axes:',                  &
  !       '                        x           y           z',          &
  !       ((u(i,j), j=1,3), i=1,3)
  !  Transform inertial principal axes back to global coordinate system
  se(:,:,km)=matmul(se(:,:,head(mm)),u)
  !  Express rotation in angle-axis form
  call angleaxis(se(:,:,km),mol(mm)%p)
endif
if (present(print)) then
  if (print) then
    print '(A/A/(2x,f14.6,3X,3f12.6))',                                 &
        'Moments of inertia and inertial axes relative to global axes:', &
        '                        X           Y           Z',            &
        (mi(i,m), (se(i,j,km), j=1,3), i=1,3)
  end if
end if

!  Set pointers and flags for the new site
if (km .ne. head(m) .and. km .ne. cm(m) .and. km .ne. 0) then
  cm(m)=km
  name(km)='c.m. '//name(head(m))
  type(km)=0
  sm(km)=km
  sf(km)=33  ! Set rotatable using Euler angles, position cartesian
  sr(1,km)=0 ! Set zero rotation pointers
  sr(2,km)=0 ! These values will be updated at the end of the
  sr(3,km)=0 ! optimization
  sp(1,km)=0 ! Set zero position pointers
  sp(2,km)=0
  sp(3,km)=0
  st(km)=0
  l(km)=-1
  level(km)=0 ! Dummy value
else
  if (iand(sf(km),3) .eq. 0) sf(km)=ior(sf(km),1)
endif

END SUBROUTINE inertia

!-----------------------------------------------------------------------

SUBROUTINE calclinks(force)

!  Calculate the site-link vectors from the centre of mass of each
!  molecule to all its sites in the molecular principal-axis system.

USE global
USE molecules
USE rotations, ONLY: angleaxis
! USE interact, ONLY: aaderiv
USE sites, ONLY: slink, srotlink, sx, sm, se, next, name

IMPLICIT NONE

LOGICAL, INTENT(IN), OPTIONAL :: force

INTEGER :: i, k, m
DOUBLE PRECISION :: r(3,3)
LOGICAL :: done=.false., verbose=.false.

if (present(force)) then
  if (done  .and..not. force) return
else
  if (done) return
end if

do m=1,mols
  !  Construct site positions and orientations relative to global axes
  call axes(m,.false.)
  !  Find centre of mass and inertial axes, assigning a new site cm(m)
  !  for them if necessary
  call inertia(m)
  mol(m)%cm=sx(:,cm(m))
  !  Find the angle-axis variables
  call angleaxis(se(:,:,cm(m)),mol(m)%p)
  ! Check accuracy of angleaxis routine
  ! call aaderiv(m)
  ! if (any(abs(se(:,:,cm(m))-mol(m)%M)>1d-12)) then
  !   call die("Matrix mismatch")
  ! end if
  if (verbose) print "(a)", "Links"
  slink(:,cm(m))=0d0
  srotlink(:,:,cm(m))=0d0
  do i=1,3
    srotlink(i,i,cm(m))=1d0
  end do
  r=transpose(se(:,:,cm(m)))
  k=head(m)
  do while (k .ne. 0)
    !  Direction vector from the centre of mass to this site, in global axes
    !  a(:)=sx(:,k)-sx(:,cm(m))
    !  Calculate direction vectors from the centre of mass to this site,
    !  and the site rotation matrix, in principal axes.
    slink(:,k)=matmul(r,sx(:,k)-sx(:,cm(m)))
    if (k .eq. sm(k)) then
      srotlink(:,:,k)=matmul(r,se(:,:,k))
      if (verbose) print "(a, 3f12.6/(3f10.6))",                       &
          trim(name(k)), slink(:,k), srotlink(:,:,k)
    else
      if (verbose) print "(a, 3f12.6)", trim(name(k)), slink(:,k)
    end if
    k=next(k)
  end do !k
end do !mols

done=.true.

END SUBROUTINE calclinks

!-----------------------------------------------------------------------

SUBROUTINE totalq(m,qt,ind,start,origin)

!  Calculate the total molecular moments of molecule m relative to its
!  origin. If m=0, calculate the total moments of the whole system
!  relative to the global origin.
!  However, if the optional argument start is present, the head of the
!  list of moments is at start rather than head(m), and m is irrelevant.

!  If the optional argument origin is present, it is the index of a site
!  and the total moments are to be referred to an origin at that site,
!  in global axes.

!  If ind=.true., add in the induced moments; otherwise not.


USE induction, ONLY: dq0, new
USE molecules, molecule => mol  !  Avoid name clash
USE rotations, ONLY : wigner
USE sites
USE sizes
IMPLICIT NONE

INTEGER, INTENT(IN) :: m
DOUBLE PRECISION, INTENT(OUT) :: qt(:)
LOGICAL, INTENT(IN) :: ind
INTEGER, INTENT(IN), OPTIONAL :: start, origin

DOUBLE PRECISION :: v(sq,sq), qi(sq), o(3)
LOGICAL :: indi, debug=.false.
INTEGER :: k,mol,m1,m2,n,np

indi=ind
if (indi .and. .not. allocated(dq0)) then
  print "(/a)", "Induced moments not yet calculated -- static moments only"
  indi=.false.
else if (indi) then
  print "(/a)", "Induced moments for the system will be included"
else
  print "(/a)", "Static moments only"
endif
qt(:)=0d0
if (m > 0 .or. present(start)) then
  m1=m
  m2=m
  k=head(m)
  o(:)=sx(:,k)
else
  m1=1
  m2=mols
  o(:)=0d0
endif
if (present(origin)) then
  o(:)=sx(:,origin)
endif

do mol=m1,m2
  if (debug) print '(a,a)', 'Molecule ', name(head(m))
  if (present(start)) then
    k=start
  else
    k=head(mol)
  endif
  do while (k > 0)
    if (l(k) >= 0) then
      if (debug) print '(a,i3,a,3f10.6)', 'Site ', k, " at ", sx(:,k)
      !  Transform multipoles to global axis system
      !  Construct transformation matrix
      call wigner(se(:,:,sm(k)),v,l(k))
      !  Transform multipoles
      !  QI=V*Q0
      n = qsize(min(l(k),4))
      if (debug) print "(a,i0,a,9f9.4)", "site ", k, " static:  ",      &
          q(1:min(n,9),k)
      qi = 0d0
      qi(1:n) = matmul(v(1:n,1:n),q(1:n,k))
      ! call dgemv('N',n,n,1d0,v,sq, q(1,k),1,0d0,qi,1)
      if (indi .and. ps(k) > 0) then
        !  This is a polarizable site; add the induced moments.
        np = pz(k)
        if (debug) print "(a,i0,a,9f9.4)", "site ", k, " induced: ",    &
            dq0(1:min(np,9),ps(k),new)
        qi(1:np) = qi(1:np) + matmul(v(1:np,1:np),dq0(1:np,ps(k),new))
        ! call dgemv('N',np,np,1d0,v,sq,dq0(1,ps(k),0),1,1d0,qi,1)
      endif
      if (debug) call printq(qi,n,"(a,9f9.4)")
      call shiftq(qi, 0,l(k),                                     &
          sx(1,k)-o(1),sx(2,k)-o(2),sx(3,k)-o(3), qt,4)
    endif
    k=next(k)
  end do
end do

if (m .gt. 0 .and. .not. present(origin)) then
!  Transform to local molecular axes. (Already at molecular origin.)
  qi(:)=qt(:)
  qt(:)=0d0
  call wigner(se(:,:,sm(head(m))),v,4)
  call dgemv('T',25,25,1d0,v,sq,qi,1,0d0,qt,1)
end if

END SUBROUTINE totalq

!-----------------------------------------------------------------------

SUBROUTINE shiftq(q,lmin,lmax, x,y,z, qn,lnmax)

!  Shift multipoles of ranks LMIN to LMAX inclusive from source Q
!  at X,Y,Z to target QN at 0,0,0, keeping multipoles up to lnmax.

USE binomials
IMPLICIT NONE

INTEGER, INTENT(IN) :: lmin,lmax,lnmax

DOUBLE PRECISION, INTENT(IN) :: x, y, z, Q(:)
DOUBLE PRECISION, INTENT(INOUT) :: QN(:)

DOUBLE PRECISION, PARAMETER :: rthalf=0.7071067811865475244D0
DOUBLE PRECISION, PARAMETER :: A(-12:12) =                       &
    (/ rthalf, rthalf, rthalf, rthalf, rthalf, rthalf,           &
    rthalf, rthalf, rthalf, rthalf, rthalf, rthalf, 0.5d0,       &
    -rthalf,rthalf, -rthalf,rthalf, -rthalf,rthalf,              &
    -rthalf,rthalf, -rthalf,rthalf, -rthalf,rthalf /)
DOUBLE PRECISION ::R(36), RC, RS, QC, QS
INTEGER :: kn, ln, mn, l, m, lr, mr

call solidh(x,y,z, lnmax, r)
!  LN is rank of target multipole
do ln=lmin,lnmax
  kn=ln**2+1
  mn=0
  do while (mn .le. ln)
    !  L is rank of source multipoles
    do l=lmin,min(ln,lmax)
      lr=ln-l
      do m=max(-l,mn-lr),min(l,mn+lr)
        if (m .lt. 0) then
          qc=q(l**2-2*m)
          qs=-q(l**2-2*m+1)
        else if (m .eq. 0) then
          qc=q(l**2+1)
          qs=0
        else
          qc=q(l**2+2*m)
          qs=q(l**2+2*m+1)
        endif
        mr=mn-m
        if (mr .lt. 0) then
          rc=r(lr**2-2*mr)
          rs=-r(lr**2-2*mr+1)
        else if (mr .eq. 0) then
          rc=r(lr**2+1)
          rs=0
        else
          rc=r(lr**2+2*mr)
          rs=r(lr**2+2*mr+1)
        endif
        qn(kn)=qn(kn)+rtbinom(ln-mn,l-m)*rtbinom(ln+mn,l+m)*      &
            (0.5d0*a(mn)/(a(m)*a(mr)))*(qc*rc-qs*rs)
        if (mn .gt. 0)                                            &
            qn(kn+1)=qn(kn+1)+rtbinom(ln-mn,l-m)*rtbinom(ln+mn,l+m)* &
            (0.5d0*a(mn)/(a(m)*a(mr)))*(qc*rs+qs*rc)
      end do
    end do
    
    mn=mn+1
    kn=ln**2+2*mn
  end do
end do

END SUBROUTINE shiftq

!-----------------------------------------------------------------------

SUBROUTINE solidh (x,y,z, j, r)

USE binomials
IMPLICIT NONE

!  Computes regular solid harmonics r**k Ckq(theta,phi) for ranks k up
!  to J, if J >= 0;
!  or irregular solid harmonics r**(-k-1) Ckq(theta,phi) for ranks k up
!  to |J|, if J < 0.


DOUBLE PRECISION, INTENT(IN) :: x, y, z
INTEGER, INTENT(IN) :: j
DOUBLE PRECISION, INTENT(OUT) :: r(:)

DOUBLE PRECISION :: rr, rfx, rfy, rfz, a2kp1, s
INTEGER :: k, l, m, n, lk, ln, lp

!  Locations in R are used as follows:

!  1    2    3    4    5    6    7    8    9   10   11  ...
!  kq = 00   10   11c  11s  20   21c  21s  22c  22s  30   31c ...

!  R(k,0) is real and is left in location k**2 + 1.
!  R(k,mc) and R(k,ms) are sqrt(2) times the real and imaginary parts
!  respectively of the complex solid harmonic R(k,-m)* = (-1)**m R(k,m),
!  and are left in locations K**2 + 2m and K**2 + 2m + 1 respectively.


l=abs(j)
if ((l+1)**2 .gt. size(r)) then
  print '(a,i3)',                                              &
      'Insufficient array space for harmonics up to rank', L
  stop
endif
rr=x**2+y**2+z**2

if (j .lt. 0) then
!  Irregular
  rr=1.0/rr
  rfx=x*rr
  rfy=y*rr
  rfz=z*rr
  r(1)=sqrt(rr)
  r(2)=rfz*r(1)
  r(3)=rfx*r(1)
  r(4)=rfy*r(1)

else
!  Regular
  r(1)=1.0
  r(2)=z
  r(3)=x
  r(4)=y
  rfz=z
  rfx=x
  rfy=y
endif

!  Remaining values are found using recursion formulae, relating
!  the new set N to the current set K and the previous set P.

k=1
do while (k .lt. l)
  n=k+1
  ln=n*n+1
  lk=k*k+1
  lp=(k-1)**2+1
  a2kp1=k+k+1

  !  Obtain R(k+1,0) from R(k,0)*R(1,0) and R(k-1,0)

  r(ln)=(a2kp1*r(lk)*rfz-k*rr*r(lp))/(k+1)

  m=1
  ln=ln+1
  lk=lk+1
  lp=lp+1

!  Obtain R(k+1,m) from R(k,m)*R(1,0) and R(k-1,m)

  if (k .gt. 1) then
    do while (m .lt. k)
      r(ln)=(a2kp1*r(lk)*rfz-rt(k+m)*rt(k-m)*rr*r(lp))            &
          /(rt(n+m)*rt(n-m))
      r(ln+1)=(a2kp1*r(lk+1)*rfz-rt(k+m)*rt(k-m)*rr*r(lp+1))      &
          /(rt(n+m)*rt(n-m))
      m=m+1
      ln=ln+2
      lk=lk+2
      lp=lp+2
    end do
  endif

  !  Obtain R(k+1,k) from R(k,k)*R(1,0)

  r(ln)=rt(n+k)*r(lk)*rfz
  r(ln+1)=rt(n+k)*r(lk+1)*rfz
  ln=ln+2

  !  Obtain R(k+1,k+1) from R(k,k)*R(1,1)

  s=rt(n+k)/rt(n+n)
  r(ln)=s*(rfx*r(lk)-rfy*r(lk+1))
  r(ln+1)=s*(rfx*r(lk+1)+rfy*r(lk))

  k=k+1
end do

END SUBROUTINE solidh

!-----------------------------------------------------------------------

SUBROUTINE realtocomplex(q,qc,jmax)

!  Convert a set of real spherical harmonics or multipoles in the
!  standard order 00, 10, 11c, 11s, 20, 21c, 21s, ... to complex form
!  in the order 00, 1-1, 10, 11, 2-2, 2-1, 20, 21, 22, ...

USE consts, ONLY : DP, rthalf
IMPLICIT NONE

DOUBLE PRECISION, INTENT(IN) :: q(:)
INTEGER, INTENT(IN) :: jmax
COMPLEX(kind=DP), INTENT(OUT) :: qc(:)

INTEGER :: base, j, m
DOUBLE PRECISION :: sign

qc=(0d0,0d0)
do j=0,jmax
  base=j*j+1
  qc(base+j)=q(base)
  sign=1d0
  do m=1,j
    sign=-sign
    qc(base+j+m)=rthalf*sign*cmplx(q(base+2*m-1),q(base+2*m),DP)
    qc(base+j-m)=rthalf*cmplx(q(base+2*m-1),-q(base+2*m),DP)
  end do
end do

END SUBROUTINE realtocomplex

!-----------------------------------------------------------------------

SUBROUTINE complextoreal(qc,q,jmax)

USE consts, ONLY : DP, rthalf
IMPLICIT NONE

COMPLEX(kind=DP), INTENT(IN) :: qc(:)
INTEGER, INTENT(IN) :: jmax
DOUBLE PRECISION, INTENT(OUT) :: q(:)

INTEGER :: base, j, m

q=0d0
do j=0,jmax
  base=j*j+1
  q(base)=qc(base+j)
  do m=1,j
    q(base+2*m-1)=2d0*rthalf*real(qc(base+j-m))
    q(base+2*m)=-2d0*rthalf*aimag(qc(base+j-m))
  end do
end do

END SUBROUTINE complextoreal

END MODULE properties
