MODULE output

!  Routines for item-by-item output. These routines collect output in a
!  buffer, which is printed when complete by CALL PUTOUT(N).

!     CALL PUT_UNIT(N)
!  Select stream N for subsequent output. Default is 6.

!     CALL PUTLEN(L)
!  Set maximum line length to L. Default and maximum is 132.

!     CALL PUTOUT(N)
!  Print output buffer if not empty, followed by N blank lines.

!     CALL PUTSTR(W,N)
!  Add the character string W to the buffer, starting at the current
!  position. Trailing spaces are suppressed, but N spaces are added
!  after the last non-blank character. If there is not enough room
!  on the present line, a new line is started, indented by VPAD spaces.

!     CALL PUTTAB(N)
!  Move output buffer pointer so that the next character will be at
!  position N, on the next line if the current position lies beyond N.

!     CALL PUTMOV(N)
!  Move output buffer pointer so that the next character will be at
!  position N, on the SAME line. Overwriting may ensue. N should not
!  exceed the line length; if it does, the current line is output and
!  the pointer is left at the start of the new line.

!     CALL PUTSP(N)
!  Insert N spaces in output buffer, overwriting any characters present.
!  Stops at the end of the buffer if N is larger than the space available.

!     CALL PUTF(X,FMT,LJ)
!  Print the real (double precision) number X according to the format in
!  the character string FMT. If the logical LJ (Left Justify) is true,
!  leading spaces are stripped.

!     CALL PUTG(X,NW,NS,LJ)
!  Print the real (double precision) number X, printing NS significant
!  figures in F format if possible, and otherwise NS-4 significant figures
!  in E format. The number is printed in a field NW characters wide if
!  LJ is false. If NW is less than NS+3 the latter value is used instead.

!     CALL PUTI(I,FMT,LJ)
!  Print the integer I. Other arguments as for PUTF.

!     CALL PUTPTR(N,L)
!  Return in N the position of the buffer pointer, i.e. the position
!  where the last character was inserted in the buffer. The first
!  character position is 1; if the buffer is empty then 0 is returned.
!  L is set to the position of the rightmost character inserted in the
!  buffer; it may be different from N if backspacing has occurred.

!     CALL PUT_PARAMS(IW,BUFFER_LENGTH,INDENT)
!  Set parameters for the output routines:
!    IW is the output stream (default 6)
!    BUFFER_LENGTH is the record length for output (default and maximum 132)
!    INDENT is the indent tab for overflowed lines (default 1, i.e. insert
!       0 spaces at the beginning of overflowed lines)
!  Impossible values are ignored, so zeros may be used to leave existing
!  values unchanged, but the last two arguments are optional.

IMPLICIT NONE

CHARACTER(LEN=132), SAVE :: out_buffer=""
CHARACTER, SAVE :: cc=""

INTEGER, SAVE :: iw=6, filled=0, length=132, ptr=0, vpad=0
LOGICAL, SAVE :: empty=.true.

PRIVATE
PUBLIC :: putout, put_unit, putlen, putstr, puttab, putmov,              &
    putsp, putf, putg, puti, putptr, put_params

CONTAINS

SUBROUTINE putout(n,noclear)

!  Print out the line buffer, unless it is empty, followed by N blank
!  lines.

!  OUT_BUFFER is a 132-character output line buffer; CC is the carriage
!  control character (empty). PTR points to the character last inserted in
!  the buffer. EMPTY is TRUE if no characters have been inserted in
!  the buffer and the buffer pointer has not been moved since the
!  last PUTOUT. VPAD is the number of spaces to be inserted at the
!  beginning of an overflowed line. FILLED is the last character
!  position written in the buffer.

!  The buffer is cleared unless the optional argument noclear is present
!  and true.

IMPLICIT NONE
INTEGER, INTENT(IN) :: n
LOGICAL, INTENT(IN), OPTIONAL :: noclear

INTEGER :: i
LOGICAL :: clear

if (present(noclear)) then
  clear=.not. noclear
else
  clear=.true.
end if

if (.not. empty) then
  if (filled == 0) then
    write (iw,"(a)") cc
  else
    write (iw,"(a)") out_buffer(1:filled)
  endif
endif
if (clear) then
  out_buffer=" "
  empty=.true.
  ptr=0
  filled=0
end if
do i=1,n
  write (iw,"(1x)")
end do
cc=""

END SUBROUTINE putout

!-----------------------------------------------------------------------

SUBROUTINE putstr(string,ns)
CHARACTER(LEN=*) :: string
INTEGER, INTENT(IN) :: ns

INTEGER :: l

if (string .ne. "") then
  l=len_trim(string)
  if (ptr+l > length) then
    call putout(0)
    call putsp(vpad)
  endif
  if (l > 0) out_buffer(ptr+1:ptr+l)=trim(string)
  ptr=ptr+l
  filled=max(filled,ptr)
  empty=.false.
endif
call putsp(ns)

END SUBROUTINE putstr

!-----------------------------------------------------------------------

SUBROUTINE puttab(n)

INTEGER, INTENT(IN) :: n

!  Move output buffer pointer so that the next character will be at
!  position N, on the next line if the current position lies beyond N.

if (ptr .ge. n) call putout(0)
call putmov(n)

END SUBROUTINE puttab

!-----------------------------------------------------------------------

SUBROUTINE putmov(n)

INTEGER, INTENT(IN) :: n

if (n > length) then
  call putout(0)
else if (n > 1) then
  ptr=n-1
  empty=.false.
else
  ptr=0
endif
filled=max(filled,n)

END SUBROUTINE putmov

!-----------------------------------------------------------------------

SUBROUTINE putsp(n)

INTEGER, INTENT(IN) :: n

!  Insert N spaces in output buffer, overwriting.

if (n .eq. 0) return
if (ptr+n > length) then
  out_buffer(ptr+1:length)=" "
  filled=ptr
  ptr=length
else
  out_buffer(ptr+1:ptr+n)=" "
  if (filled < ptr) filled=ptr
  ptr=ptr+n
endif
filled=max(filled,ptr)
empty=.false.

END SUBROUTINE putsp

!-----------------------------------------------------------------------

SUBROUTINE puti(i,fmt,lj)
INTEGER, INTENT(IN) :: i
CHARACTER(LEN=*), INTENT(IN) :: fmt
LOGICAL, INTENT(IN) :: lj

CHARACTER(LEN=40) :: buffer

write (buffer,"("//fmt//")") i

if (lj) then
  buffer = adjustl(buffer)
endif
call putstr(buffer,0)

END SUBROUTINE puti

!-----------------------------------------------------------------------

SUBROUTINE putf(x,fmt,lj)
DOUBLE PRECISION, INTENT(IN) :: x
CHARACTER(LEN=*), INTENT(IN) :: fmt
LOGICAL, INTENT(IN) :: lj

CHARACTER(LEN=40) :: BUFFER

write (buffer,"("//fmt//")") x

if (lj) then
  buffer = adjustl(buffer)
endif
call putstr(buffer,0)

END SUBROUTINE putf

!-----------------------------------------------------------------------

SUBROUTINE putg(x,nw,ns,lj)
DOUBLE PRECISION, INTENT(IN) :: x
INTEGER, INTENT(IN) :: nw, ns
LOGICAL, INTENT(IN) :: lj

CHARACTER(LEN=10) :: fmt="(  f10. 5)"
INTEGER :: m

write (fmt(5:6),"(i2)") max(nw,ns+3)
fmt(2:4)="  f"
if (abs(x) < 1d-20) then
  write (fmt(8:9),"(i2)") ns-1
  call putf(x,fmt,lj)
else
  m=nint(log10(abs(x))-0.5d0)
  if (m .le. 0 .and. m > -ns) then
    write (fmt(8:9),"(i2)") ns-1
  else if (m > 0 .and. m < ns-1) then
    write (fmt(8:9),"(i2)") ns-m-1
  else
    write (fmt(8:9),"(i2)") ns-5
    fmt(2:4)="1pe"
  endif
  call putf(x,fmt,lj)
endif

END SUBROUTINE putg

!-----------------------------------------------------------------------

SUBROUTINE putptr(n,l)

!  Returns in N the current position of the buffer pointer, i.e. the
!  position of the character last inserted in the buffer, and in L the
!  position of the rightmost character inserted in the buffer.

INTEGER, INTENT(OUT) :: n, l

n=ptr
l=filled

END SUBROUTINE putptr

!-----------------------------------------------------------------------

SUBROUTINE put_unit(n)

INTEGER, INTENT(IN) :: n

! if (.not. empty) call putout(0)
iw=n

END SUBROUTINE put_unit

!-----------------------------------------------------------------------

SUBROUTINE putlen(l)

INTEGER, INTENT(IN) :: l

length=l

END SUBROUTINE putlen

!-----------------------------------------------------------------------

SUBROUTINE put_params(iw1,buffer_length,indent)

INTEGER, INTENT(IN) :: iw1
INTEGER, INTENT(IN), OPTIONAL :: buffer_length, indent

if (iw1 > 0 .and. iw1 .ne. iw) then
  if (.not. empty) call putout(0)
  iw=iw1
endif
if (present(buffer_length)) length = buffer_length
if (present(indent)) vpad = indent

END SUBROUTINE put_params

END MODULE output
