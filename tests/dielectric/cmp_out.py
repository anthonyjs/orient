#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Compare output file with reference in check directory.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog=f"""
Usage: {this} [output-file]
""")

parser.add_argument("file", default=["out"], nargs="*")

args = parser.parse_args()

eps = 1.0e-6
ok = True

file = args.file[0]
chk = ofile("check/"+file)
new = ofile(file)
print(f"\nChecking {file} against check/{file}")

if cmp_after(".* converged",chk,new,skip=1):
    pass
else:
    die("Convergence match failed")
if cmp_after(" +2048 +orientations",chk,new,skip=3):
    print("Results match")
else:
    die("Results failed to match")
print("File comparison successful")
exit(0)
