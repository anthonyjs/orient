#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Compare output file with reference in check directory.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog=f"""
Usage: {this} out
""")

parser.add_argument("file", default=["out"], nargs="*")

args = parser.parse_args()

eps = 1.0e-6
ok = True

file = args.file[0]

chk = ofile("check/"+file)
new = ofile(file)
print(f"\nChecking {file} against check/{file}")

go_to("Minimum",chk,new)
go_to("Converged",chk,new)
Ea = get_float(chk.lines[chk.p])
Eb = get_float(new.lines[new.p])
if abs(Ea-Eb) < eps:
    print("Minimum: energies match")
else:
    print("Minimum: energy mismatch")
    ok = False

go_to("Minimum, fixed distance",chk,new)
go_to("Converged",chk,new)
Ea = get_float(chk.lines[chk.p])
Eb = get_float(new.lines[new.p])
if abs(Ea-Eb) < eps:
    print("Minimum, fixed distance: energies match")
else:
    print("Minimum, fixed distance: energy mismatch")
    ok = False

if ok:
    print("File comparison successful")
    exit(0)
else:
    print("File comparison failed")
    exit(1)
  
