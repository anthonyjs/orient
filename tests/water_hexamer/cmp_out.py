#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Compare output file with reference in check directory.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog=f"""
{this} args
""")

parser.add_argument("file")

args = parser.parse_args()

chk = ofile(f"check/{args.file}")
new = ofile(args.file)
print(f"\nChecking {args.file} against check/{args.file}")

go_to("Minimum",chk,new)
go_to("Total energy",chk,new)
E_chk = get_float(chk.lines[chk.p])
E_new = get_float(new.lines[new.p])

if (abs(E_chk-E_new)) > 1e-5:
    die(f"Energies differ: {E_chk:12.6f}  {E_new:12.6f}")


if cmp_after("Triple-dipole terms",chk,new):
    print("Triple-dipole terms match")
else:
    die("Triple-dipole terms: match failed")
if cmp_after("Many-body analysis",chk,new):
    print("Many-body analysis matches")
else:
    die("Many-body analysis match failed")
print("File comparison successful")
exit(0)
