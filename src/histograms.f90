      MODULE histograms

      USE input, ONLY : die
      USE mdmc, ONLY : points
      IMPLICIT NONE

      INTEGER, PARAMETER :: maxhistsize=10000, hfnum=90, jpfnum=91,     &
          dvfnum=92, maxfitorder=48
      LOGICAL :: histofile_flag,jumper_flag,adjust_flag,takeold_flag
      LOGICAL :: diver_flag,histfit_flag,openend_flag,caloric_flag
      LOGICAL :: fitreport_flag,restart_histofile_flag
      INTEGER :: fitorder,histsize,maxfitconv
      INTEGER :: maxnecconv,buffersize,doublelen,caloricgrid
      DOUBLE PRECISION :: minhist,maxhist
      CHARACTER(LEN=32) :: histfile,oldhistfile,jumperfile,diverfile,   &
          fitfile,  caloricfile,restart_histfile

      DOUBLE PRECISION :: histogram(maxhistsize)

!     Variables for the pair-distance distribution function
!     pairdist_len  : maximum length used in distribution function
!     pairdist_size : size of the distribution histogram
!     pairdist_file : filename for the distribution function
!     pairdist_site : sitename between the distances are evaluated
!     pairdist_max  : maximum value of pairdist_size
!     pairdist_maxs : maximum number of sites involved
!     pairdist_histo: distribution histogram

      INTEGER, PARAMETER :: pairdist_max=1000,pairdist_maxs=100
      LOGICAL :: pairdist_flag,restart_pairdist_flag
      DOUBLE PRECISION :: pairdist_len
      INTEGER :: pairdist_size,pairdist_nr, pairdist_nr2
      INTEGER :: pairdist_histoii(pairdist_max), ndist_sites
      INTEGER :: pairdist_histoij(pairdist_max), pairdist_histojj(pairdist_max)
      INTEGER :: pairdisp_nsite2(pairdist_max),pairdisp_nsite(pairdist_maxs)
      CHARACTER(LEN=32) :: pairdist_file,old_pairdist_file
      CHARACTER(LEN=20) :: pairdist_site, pairdist_site2

      PRIVATE
      PUBLIC :: maxhistsize,hfnum,maxfitorder,jpfnum,dvfnum,             &
          maxhist, minhist, histsize, histogram,                         &
          fitorder, maxfitconv,                                          &
          pairdist_flag, pairdist_size, pairdist_len, pairdist_site,     &
          pairdist_file, pairdist_max, pairdist_maxs, pairdist_site2,    &
          histofile_flag,jumper_flag,adjust_flag,takeold_flag,           &
          diver_flag,histfit_flag,openend_flag,caloric_flag,             &
          fitreport_flag,                                                &
          maxnecconv,buffersize,doublelen,caloricgrid,                   &
          histfile,oldhistfile,jumperfile,diverfile,fitfile,             &
          caloricfile,                                                   &
          default_pairdist, init_pairdist, update_pairdist,              &
          output_pairdist,                                               &
          clearhistogram, evaluatehist, savehistogram,restart_histogram, &
          restart_pairdist_flag, old_pairdist_file,                      &
          restart_histofile_flag,restart_histfile

      CONTAINS

!-----------------------------------------------------------------------

      SUBROUTINE clearhistogram

      IMPLICIT NONE

      INTEGER :: i

      do i=1,maxhistsize
       histogram(i)=0.0d0
      end do

      END SUBROUTINE clearhistogram

!-----------------------------------------------------------------------

      SUBROUTINE restart_histogram(filename,npoints)

      CHARACTER(LEN=32) :: filename
      DOUBLE PRECISION :: nrj(histsize),dx,npoints
      INTEGER :: i

      open(hfnum,FILE=filename,STATUS='OLD')
      do i=1,histsize
        read (hfnum,'(1p,e20.12,1x,1p,e20.12)') nrj(i),histogram(i)
      end do
      print '(A)','Restarting histogram calculation'
      print '(2A)','Old histogram read from ',trim(filename)
      close(hfnum)
      minhist=nrj(1)
      maxhist=nrj(histsize)
      dx=nrj(2)-nrj(1)

! Undo the normalisation
      do i=1,histsize
        histogram(i)=dx*npoints*histogram(i)
      end do

      END SUBROUTINE restart_histogram

!-----------------------------------------------------------------------

      SUBROUTINE evaluatehist(filename,temperature)

      USE consts
      USE mdmc
      IMPLICIT NONE

      CHARACTER(LEN=32) :: filename
      DOUBLE PRECISION :: xbase(maxhistsize),coeffs(maxfitorder)
      DOUBLE PRECISION :: new_hist(maxhistsize),temperature
      INTEGER :: i
      LOGICAL :: fexist_flag

!  Old histogram from disk
      if (takeold_flag) then
!  Checking the filename
       inquire (FILE=filename,EXIST=fexist_flag)
       if (.not. fexist_flag) then
        print '(/A/)','ERROR: Histogram-file does not exist'
        print '(/A/)','... Histogram evaluation aborted'
        return
       end if
       print '(/A,A/)' ,'... old histogram taken from ',filename
!  Loading histogram
       open(hfnum,FILE=filename,STATUS='OLD')
       do i=1,histsize
        read (hfnum,'(1p,e20.12,1x,1p,e20.12)') xbase(i),histogram(i)
       end do
       close(hfnum)
       minhist=xbase(1)
       maxhist=xbase(histsize)
      else
       do i=1,histsize
        call xhistogram(i,xbase(i))
       end do
      end if !takeold_flag

!  Empty histogram check
      if ((maxhist-minhist) .eq. 0.0d0) then
       print '(/A/)','Histogram is empty'
       return
      end if

!  Normalize histogram
      call normalizehist(dabs(xbase(2)-xbase(1)),                        &
                         histogram,histsize)
      if (histofile_flag) then
       call savehistogram
      end if

!  Fitting the histogram
      if (histfit_flag) then
       call fithist(xbase,new_hist,histsize,coeffs)
      end if

!  Calculate caloric curve via ferrenberg method
      if (caloric_flag) then
       print '(/A/)' ,'Calculating caloric curve ...'
       call expectation_epot(caloricmin,caloricmax,temperature,          &
            caloricgrid,xbase,histogram,histsize,efact,                  &
            trim(caloricfile)//tempcode)
       if (histfit_flag) then
        call expectation_epot(caloricmin,caloricmax,temperature,         &
             caloricgrid,xbase,new_hist,histsize,efact,                  &
             trim(caloricfile)//'.fit'//tempcode)
       end if
      end if

      END SUBROUTINE evaluatehist

!-----------------------------------------------------------------------

      SUBROUTINE xhistogram(i,engval)
      IMPLICIT NONE

      INTEGER :: i
      DOUBLE PRECISION :: engval

      engval=minhist+dble(i-1)/(dble(histsize)-1.0d0)*(maxhist-minhist)

      END SUBROUTINE xhistogram


!-----------------------------------------------------------------------

      SUBROUTINE normalizegfit(a,na)

      IMPLICIT NONE

      integer na,i
      double precision integral,a(na),sqrtpi

!  Integration of the gaussians
      integral=0.d0
      sqrtpi=1.77245385090552d0
      do i=1,na-1,3
       integral=integral+a(i)*a(i+2)
      end do
      integral=integral*sqrtpi

!  Renormalization of the prefactors
      do i=1,na-1,3
       a(i)=a(i)/integral
      end do

      END SUBROUTINE normalizegfit

!-----------------------------------------------------------------------

      SUBROUTINE savefgauss(filename,coeffs)

      IMPLICIT NONE

      DOUBLE PRECISION :: coeffs(fitorder)
      CHARACTER(LEN=32) :: filename
      INTEGER :: i

      open(hfnum,FILE=filename,STATUS='UNKNOWN')
      do i=1,fitorder-1,3
       write(hfnum,                                                      &
         '(1p,e25.15,A,1p,e25.15,A,1p,e25.15,A/)')                          &
         coeffs(i),' Exp[ -(( x -',coeffs(i+1),' )/',                    &
         coeffs(i+2),' )^2 ]'
      end do
      close(hfnum)

      END SUBROUTINE savefgauss

!-----------------------------------------------------------------------

      SUBROUTINE normalizehist(dx,histogram,nhist)

      IMPLICIT NONE

      integer nhist,i
      double precision dx,histogram(nhist),intsum

      intsum=0.0d0
      do i=1,nhist
       intsum=intsum+histogram(i)*dx
      end do

      if (intsum .ne. 0.0d0) then
       do i=1,nhist
        histogram(i)=histogram(i)/intsum
       end do
      end if

      END SUBROUTINE normalizehist

!-----------------------------------------------------------------------

      SUBROUTINE fithist(xbase,new_hist,nhist,coeffs)

      USE mdmc
      IMPLICIT NONE

      INTEGER :: nhist,i,icoeffs(maxfitorder),converge
      DOUBLE PRECISION :: xbase(nhist),new_hist(nhist),coeffs(maxfitorder)
      DOUBLE PRECISION :: maximum,centre,sigma(maxhistsize),alamda,chisq
      DOUBLE PRECISION :: chisqbak,covar(maxfitorder,maxfitorder)
      DOUBLE PRECISION :: alpha(maxfitorder,maxfitorder)
      DOUBLE PRECISION :: dyda(maxfitorder),presenterr

!  Update parameters
      maximum=0.0d0
      centre=0.0d0
      chisqbak=0.0d0
      do i=1,nhist
       sigma(i)=1.0d0
       if (histogram(i) .gt. maximum) then
        maximum=histogram(i)
        centre=xbase(i)
       end if
      end do

!  Init Levenberg-Marquardt Method
      alamda=-1.0d0
      do i=1,fitorder
       icoeffs(i)=1
      end do
      do i=0,fitorder/3-1
       coeffs(3*i+1)=maximum
       coeffs(3*i+2)=centre !+(dble(2*i)/dble(fitorder/3-1)-1.0d0)*
!     +                      (maxhist-minhist)/12.0d0
       coeffs(3*i+3)=(maxhist-minhist)/6.0d0
      end do

      call mrqmin(xbase,histogram,sigma,nhist,                           &
                  coeffs,icoeffs,fitorder,covar,alpha,maxfitorder,       &
                  chisq,fgauss,alamda)

!  Convergence procedure
      converge=0
      i=0
      do while ((converge .lt. maxnecconv) .and. (i .lt. maxfitconv))
       i=i+1
       call mrqmin(xbase,histogram,sigma,nhist,                          &
                   coeffs,icoeffs,fitorder,covar,alpha,maxfitorder,      &
                   chisq,fgauss,alamda)
       presenterr=dabs((chisqbak-chisq)/chisq)
       if (presenterr .lt. fitquality) then
        converge=converge+1
       else
        converge=0
       end if
       chisqbak=chisq

!  Smoothing information
       if (fitreport_flag) then
        print '(I4,A)',                                                  &
         i,'.  smoothing of the histogram now done.'
        if (converge .ne. 0) then
         print '(A,I4,A)',                                               &
          '-> ',converge,'. time in a row over fit-quality requirement.'
         print '(A,I3,A/)',                                              &
          '   (',maxnecconv,' -times is necessary for convergence)'
        end if
       end if

      end do !converge

!  Fit not converged
      if (i .eq. maxfitconv) then
       print '(/A/)',                                                    &
        'WARNING: Histogram-fit is not converged to fit-quality'
      end if

!  Final call
      alamda=0.0d0
      call mrqmin(xbase,histogram,sigma,nhist,                           &
                  coeffs,icoeffs,fitorder,covar,alpha,maxfitorder,       &
                  chisq,fgauss,alamda)

!  Normalization of the fit
      call normalizegfit(coeffs,fitorder)

!  Put fit in new histogram
      do i=1,nhist
       call fgauss(xbase(i),coeffs,new_hist(i),dyda,fitorder)
       if (dabs(new_hist(i)) .lt. 1.0D-20) then
        new_hist(i)=0.0d0
       end if
      end do

!  Save fit
      open(99,FILE=trim(fitfile)//tempcode                               &
             ,STATUS='UNKNOWN')
      do i=1,nhist
       write (99,'(1p,e20.12,A,1p,e20.12)') xbase(i),' ',new_hist(i)
      end do
      close(99)

      call savefgauss(trim(fitfile)//'.anl'//tempcode,                   &
                      coeffs)

      END SUBROUTINE fithist

!-----------------------------------------------------------------------

      SUBROUTINE savehistogram

      USE mdmc
      IMPLICIT NONE

      INTEGER :: i
      DOUBLE PRECISION :: engval

      if ((maxhist-minhist) .gt. 0.0d0) then
       open(99,FILE=trim(histfile)//tempcode                             &
              ,STATUS='UNKNOWN')
       do i=1,histsize
        call xhistogram(i,engval)
        write (99,'(1p,e20.12,A,1p,e20.12)') engval,' ',histogram(i)
       end do
       close(99)
      end if

      END SUBROUTINE savehistogram

!-----------------------------------------------------------------------

      SUBROUTINE default_pairdist(filename)

       USE consts
       IMPLICIT NONE

       CHARACTER(LEN=*) :: filename

       pairdist_flag=.false.
       pairdist_len =10.0d0/rfact
       pairdist_size=1000
       pairdist_file=filename
       pairdist_site='UNDEFINED'
       pairdist_site2='UNDEFINED'

      END SUBROUTINE default_pairdist

!-----------------------------------------------------------------------

      SUBROUTINE init_pairdist

       USE consts
       USE molecules
       USE sites
       USE types
       IMPLICIT NONE

       INTEGER :: i, imol, sitenr, number
       DOUBLE PRECISION :: dpnumber, ii, ij, jj, normii, normij, normjj
       CHARACTER(LEN=18) :: word
       CHARACTER(LEN=1) :: blank

!  Print info
       if (pairdist_site .eq. 'UNDEFINED') then
         stop 'ERROR: Site for radial distribution function undefined.'
       end if
       if (pairdist_site2 .eq. 'UNDEFINED') then
         ndist_sites = 1
       else
         if (pairdist_site2 .eq. pairdist_site) then
            call die('Two sites defined with the same name.',.true.)
         end if
         ndist_sites = 2
       end if

       if (ndist_sites .eq. 1) then
         print '(/A/,3A/,A,T32,F6.1,2A/,A,T32,I6/)',                       &
         'Calculating radial distribution function.',                      &
         '...site-site distances between all ',                            &
         trim(pairdist_site),' sites used.',                               &
         'Length                      : ',pairdist_len*rfact,' ',runit,    &
         'Histogram size              : ',pairdist_size
       else
         print '(/A/,3A,2A/,A,F6.1,2A/,A,I6/)',                            &
         'Calculating radial distribution function.',                      &
         '...site-site distances between all ',                            &
         trim(pairdist_site),' and ',trim(pairdist_site2),' sites used.',  &
         'Length                      : ',pairdist_len*rfact,' ',runit,    &
         'Histogram size              : ',pairdist_size
       end if

!  Find all sites called 'pairdist_site' or 'pairdist_site2'
       pairdist_nr=0
       pairdist_nr2=0
       do imol=1,mols
        sitenr=cm(imol)
        do while (sitenr .ne. 0)
         if (trim(pairdist_site) .eq. trim(tname(type(sitenr)))) then
           pairdist_nr=pairdist_nr+1
           if (pairdist_nr .gt. pairdist_maxs) then
             stop 'ERROR: pairdist_maxs too small'
           end if
           pairdisp_nsite(pairdist_nr)=sitenr
         else if (trim(pairdist_site2) .eq. trim(tname(type(sitenr)))) then
           pairdist_nr2=pairdist_nr2+1
           if (pairdist_nr2 .gt. pairdist_maxs) then
            stop 'ERROR: pairdist_maxs too small'
           end if
           pairdisp_nsite2(pairdist_nr2)=sitenr
!          print '(I3)',sitenr
!          print '(A)',trim(name(link(sitenr)))
         end if
         sitenr=next(sitenr)
        end do
       end do

       if (pairdist_nr .eq. 0 .and. trim(pairdist_site) .ne. 'SURFACE') then
        stop 'ERROR: Unknown sitename for pair distances.'
       else if (pairdist_nr2 .eq. 0 .and. ndist_sites .eq. 2           &
             .and. trim(pairdist_site2) .ne. 'SURFACE') then
          stop 'ERROR: Unknown sitename for pair distances.'
       end if

! DRN 4/7/2 The following reads in an old pairdist file

       if (restart_pairdist_flag) then
         open(unit=1,file=trim(old_pairdist_file))
         read (1,'(A)') word
         read (1,'(A,I9)') word, number
         if (ndist_sites .eq. 2) then
           read (1,'(3(A,F20.10))') word,normii,word,normij,word,normjj
           do i=1,pairdist_size
             read (1,'(F15.8,3(A,1F15.12))') dpnumber,blank,ii,blank,ij,blank,jj
             pairdist_histoii(i)=nint(ii*normii)
             pairdist_histoij(i)=nint(ij*normij)
             pairdist_histojj(i)=nint(jj*normjj)
           end do
         else
           do i=0,pairdist_size
             read (1,'(F15.8,A,1F20.10)') dpnumber,word,ii
             pairdist_histoii(i)=nint(ii*normii)
           end do
         end if
         close(1)
         print '(A/,2A/)','Restarting radial distribution function calculation', &
             'Old radial distribution read from ',trim(old_pairdist_file)
       else
!  Clear histogram
         pairdist_histoii(:)=0
         pairdist_histoij(:)=0
         pairdist_histojj(:)=0
       end if

       call output_pairdist

      END SUBROUTINE init_pairdist

!-----------------------------------------------------------------------

      SUBROUTINE update_pairdist

      USE sites
      USE molecules
      IMPLICIT NONE

       INTEGER :: i,j,binnr
       DOUBLE PRECISION :: distance

! Calculates distance between 'SITE1' sites

       if (trim(pairdist_site) .ne. 'SURFACE') then
         do i=1,pairdist_nr-1
          do j=i+1,pairdist_nr
           if (trim(name(link(pairdisp_nsite(i)))) .ne.   &
                    trim(name(link(pairdisp_nsite(j))))) then
!             print '(3A)', trim(name(link(pairdisp_nsite(i)))),&
!                    ' ', trim(name(link(pairdisp_nsite(j))))
             distance=(sx(1,pairdisp_nsite(i))-sx(1,pairdisp_nsite(j)))**2   &
                     +(sx(2,pairdisp_nsite(i))-sx(2,pairdisp_nsite(j)))**2   &
                     +(sx(3,pairdisp_nsite(i))-sx(3,pairdisp_nsite(j)))**2
             distance=sqrt(distance)
             if (distance .le. pairdist_len) then
              binnr=int(distance/pairdist_len*dble(pairdist_size-1))+1
              pairdist_histoii(binnr)=pairdist_histoii(binnr)+1
             end if
           end if  
          end do
         end do
       end if

! Calculates distance between 'SITE2' sites

       if (ndist_sites .eq. 2) then
         if (trim(pairdist_site2) .ne. 'SURFACE') then
           do i=1,pairdist_nr2-1
            do j=i+1,pairdist_nr2
             if (trim(name(link(pairdisp_nsite2(i)))) .ne.   &
                      trim(name(link(pairdisp_nsite2(j))))) then
!               print '(3A)', trim(name(link(pairdisp_nsite2(i)))),&
!                      ' ', trim(name(link(pairdisp_nsite2(j))))
                distance=(sx(1,pairdisp_nsite2(i))-sx(1,pairdisp_nsite2(j)))**2   &
                       +(sx(2,pairdisp_nsite2(i))-sx(2,pairdisp_nsite2(j)))**2   &
                       +(sx(3,pairdisp_nsite2(i))-sx(3,pairdisp_nsite2(j)))**2
               distance=sqrt(distance)
               if (distance .le. pairdist_len) then
                binnr=int(distance/pairdist_len*dble(pairdist_size-1))+1
                pairdist_histoij(binnr)=pairdist_histoij(binnr)+1
               end if
             end if  
            end do
           end do
         end if

! Calculates distances between 'SITE1' and 'SITE2' sites

         if (trim(pairdist_site) .ne. 'SURFACE' .and. trim(pairdist_site2)    &
             .ne. 'SURFACE') then
           do i=1,pairdist_nr
            do j=1,pairdist_nr2
             if (trim(name(link(pairdisp_nsite(i)))) .ne.  &
                      trim(name(link(pairdisp_nsite2(j))))) then
!               print '(3A)',trim(name(link(pairdisp_nsite(i)))), &
!                      ' ',trim(name(link(pairdisp_nsite2(j))))
               distance=(sx(1,pairdisp_nsite(i))-sx(1,pairdisp_nsite2(j)))**2   &
                       +(sx(2,pairdisp_nsite(i))-sx(2,pairdisp_nsite2(j)))**2   &
                       +(sx(3,pairdisp_nsite(i))-sx(3,pairdisp_nsite2(j)))**2
               distance=sqrt(distance)
               if (distance .le. pairdist_len) then
                binnr=int(distance/pairdist_len*dble(pairdist_size-1))+1
                pairdist_histojj(binnr)=pairdist_histojj(binnr)+1
               end if
             end if
            end do
           end do
         else if (trim(pairdist_site) .eq. 'SURFACE') then
           do j=1,pairdist_nr2
             distance=(sx(3,pairdisp_nsite2(j)))**2
             distance=sqrt(distance)
             if (distance .le. pairdist_len) then
              binnr=int(distance/pairdist_len*dble(pairdist_size-1))+1
              pairdist_histoij(binnr)=pairdist_histoij(binnr)+1
             end if
           end do
         else if (trim(pairdist_site2) .eq. 'SURFACE') then
           do i=1,pairdist_nr
             distance=(sx(3,pairdisp_nsite(i)))**2
             distance=sqrt(distance)
             if (distance .le. pairdist_len) then
              binnr=int(distance/pairdist_len*dble(pairdist_size-1))+1
              pairdist_histoij(binnr)=pairdist_histoij(binnr)+1
             end if
           end do
         end if
      end if  ! 2 sites

      END SUBROUTINE update_pairdist

!-----------------------------------------------------------------------

      SUBROUTINE output_pairdist

       USE consts
!      USE mdmc, ONLY : nrjcode
       IMPLICIT NONE

       INTEGER :: i
       DOUBLE PRECISION :: normii, normij, normjj

       normii=0.0d0
       do i=1,pairdist_size
        normii=normii+dble(pairdist_histoii(i))
       end do
       normii=normii*pairdist_len/dble(pairdist_size-1)
       if (normii .eq. 0.0d0) then
        normii=1.0d0
       end if

       if (ndist_sites .eq. 2) then
         normij=0.0d0
         do i=1,pairdist_size
          normij=normij+dble(pairdist_histoij(i))
         end do
         normij=normij*pairdist_len/dble(pairdist_size-1)
         if (normij .eq. 0.0d0) then
          normij=1.0d0
         end if
         normjj=0.0d0
         do i=1,pairdist_size
          normjj=normjj+dble(pairdist_histojj(i))
         end do
         normjj=normjj*pairdist_len/dble(pairdist_size-1)
         if (normjj .eq. 0.0d0) then
          normjj=1.0d0
         end if
       end if

!       open(1,FILE=trim(pairdist_file)//nrjcode,STATUS='UNKNOWN')
       open(1,FILE=trim(pairdist_file),STATUS='UNKNOWN')

       if (ndist_sites .eq. 1) then
         write (1,'(A,I9)') '#       Npoints = ',nint(points-1d0)
         write (1,'(A,F20.10)') '#        Normii = ',normii
         do i=1,pairdist_size
          write (1,'(F15.8,A,1F15.12)')                                    &
           dble(i-1)/dble(pairdist_size-1)*pairdist_len*rfact,' ',         &
           dble(pairdist_histoii(i))/normii
         end do
       else
         write (1,'(A)') '#       Dist     g(SITE1 SITE1)  g(SITE1 SITE2)  g(SITE2 SITE2)'
         write (1,'(A,I9)') '#       Npoints = ',nint(points-1d0) 
         write (1,'(3(A,F20.10))') '#        Normii = ',normii, &
                                  '         Normij = ',normij, &
                                  '         Normjj = ',normjj
         do i=1,pairdist_size
          write (1,'(F15.8,3(A,1F15.12))')                                    &
           dble(i-1)/dble(pairdist_size-1)*pairdist_len*rfact,' ',         &
           dble(pairdist_histoii(i))/normii,                               &
           ' ',dble(pairdist_histoij(i))/normij,                               &
           ' ',dble(pairdist_histojj(i))/normjj
         end do
       end if

       close(1)

      END SUBROUTINE output_pairdist

!-----------------------------------------------------------------------

      SUBROUTINE mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,nca,chisq,     &
          funcs,alamda)
!  Copyright (C) 1986, 1992 Numerical Recipes Software
      INTEGER :: ma,nca,ndata,ia(ma)
      DOUBLE PRECISION :: alamda, chisq, a(ma), alpha(nca,nca),             &
          covar(nca,nca), sig(ndata), x(ndata), y(ndata)
      EXTERNAL funcs
      INTEGER, PARAMETER :: MMAX=48
!U    USES covsrt,gaussj,mrqcof
      INTEGER j,k,l,m,mfit
      DOUBLE PRECISION ochisq,atry(MMAX),beta(MMAX),da(MMAX)
      SAVE ochisq,atry,beta,da,mfit

      if(alamda.lt.0.d0)then
        mfit=0
        do 11 j=1,ma
          if (ia(j).ne.0) mfit=mfit+1
11      continue
        alamda=0.001d0
        call mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,nca,chisq,funcs)
        ochisq=chisq
        do 12 j=1,ma
          atry(j)=a(j)
12      continue
      endif
      j=0
      do 14 l=1,ma
        if(ia(l).ne.0) then
          j=j+1
          k=0
          do 13 m=1,ma
            if(ia(m).ne.0) then
              k=k+1
              covar(j,k)=alpha(j,k)
            endif
13        continue
          covar(j,j)=alpha(j,j)*(1.d0+alamda)
          da(j)=beta(j)
        endif
14    continue
      call gaussj(covar,mfit,nca,da,1,1)
      if(alamda.eq.0.d0)then
        call covsrt(covar,nca,ma,ia,mfit)
        return
      endif
      j=0
      do 15 l=1,ma
        if(ia(l).ne.0) then
          j=j+1
          atry(l)=a(l)+da(j)
        endif
15    continue
      call mrqcof(x,y,sig,ndata,atry,ia,ma,covar,da,nca,chisq,funcs)
      if(chisq.lt.ochisq)then
        alamda=0.1d0*alamda
        ochisq=chisq
        j=0
        do 17 l=1,ma
          if(ia(l).ne.0) then
            j=j+1
            k=0
            do 16 m=1,ma
              if(ia(m).ne.0) then
                k=k+1
                alpha(j,k)=covar(j,k)
              endif
16          continue
            beta(j)=da(j)
            a(l)=atry(l)
          endif
17      continue
      else
        alamda=10.d0*alamda
        chisq=ochisq
      endif

      END SUBROUTINE mrqmin

!-----------------------------------------------------------------------

      SUBROUTINE mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,nalp,chisq,     &
          funcs)
!  Copyright (C) 1986, 1992 Numerical Recipes Software
      INTEGER :: ma,nalp,ndata,ia(ma)
      DOUBLE PRECISION :: chisq,a(ma), alpha(nalp,nalp), beta(ma),      &
      sig(ndata), x(ndata), y(ndata)
      EXTERNAL funcs
      INTEGER, PARAMETER :: MMAX=48
      INTEGER :: mfit,i,j,k,l,m
      DOUBLE PRECISION :: dy,sig2i,wt,ymod,dyda(MMAX)

      mfit=0
      do 11 j=1,ma
        if (ia(j).ne.0) mfit=mfit+1
11    continue
      do 13 j=1,mfit
        do 12 k=1,j
          alpha(j,k)=0.d0
12      continue
        beta(j)=0.d0
13    continue
      chisq=0.d0
      do 16 i=1,ndata
        call funcs(x(i),a,ymod,dyda,ma)
        sig2i=1.d0/(sig(i)*sig(i))
        dy=y(i)-ymod
        j=0
        do 15 l=1,ma
          if(ia(l).ne.0) then
            j=j+1
            wt=dyda(l)*sig2i
            k=0
            do 14 m=1,l
              if(ia(m).ne.0) then
                k=k+1
                alpha(j,k)=alpha(j,k)+wt*dyda(m)
              endif
14          continue
            beta(j)=beta(j)+dy*wt
          endif
15      continue
        chisq=chisq+dy*dy*sig2i
16    continue
      do 18 j=2,mfit
        do 17 k=1,j-1
          alpha(k,j)=alpha(j,k)
17      continue
18    continue

      END SUBROUTINE mrqcof

!-----------------------------------------------------------------------

      SUBROUTINE covsrt(covar,npc,ma,ia,mfit)
!  Copyright (C) 1986, 1992 Numerical Recipes Software
      INTEGER :: ma,mfit,npc,ia(ma)
      DOUBLE PRECISION :: covar(npc,npc)
      INTEGER :: i,j,k
      DOUBLE PRECISION :: swap
      do 12 i=mfit+1,ma
        do 11 j=1,i
          covar(i,j)=0.d0
          covar(j,i)=0.d0
11      continue
12    continue
      k=mfit
      do 15 j=ma,1,-1
        if(ia(j).ne.0)then
          do 13 i=1,ma
            swap=covar(i,k)
            covar(i,k)=covar(i,j)
            covar(i,j)=swap
13        continue
          do 14 i=1,ma
            swap=covar(k,i)
            covar(k,i)=covar(j,i)
            covar(j,i)=swap
14        continue
          k=k-1
        endif
15    continue

      END SUBROUTINE covsrt

!-----------------------------------------------------------------------

      SUBROUTINE gaussj(a,n,np,b,m,mp)
!  Copyright (C) 1986, 1992 Numerical Recipes Software
      INTEGER :: m,mp,n,np
      DOUBLE PRECISION :: a(np,np),b(np,mp)
      INTEGER, PARAMETER :: NMAX=50
      INTEGER :: i,icol,irow,j,k,l,ll,indxc(NMAX),indxr(NMAX),ipiv(NMAX)
      DOUBLE PRECISION :: big,dum,pivinv
      do 11 j=1,n
        ipiv(j)=0
11    continue
      do 22 i=1,n
        big=0.d0
        do 13 j=1,n
          if(ipiv(j).ne.1)then
            do 12 k=1,n
              if (ipiv(k).eq.0) then
                if (abs(a(j,k)).ge.big)then
                  big=abs(a(j,k))
                  irow=j
                  icol=k
                endif
              endif
12          continue
          endif
13      continue
        ipiv(icol)=ipiv(icol)+1
        if (irow.ne.icol) then
          do 14 l=1,n
            dum=a(irow,l)
            a(irow,l)=a(icol,l)
            a(icol,l)=dum
14        continue
          do 15 l=1,m
            dum=b(irow,l)
            b(irow,l)=b(icol,l)
            b(icol,l)=dum
15        continue
        endif
        indxr(i)=irow
        indxc(i)=icol
        if (a(icol,icol).eq.0.d0)                                       &
            call die('singular matrix in gaussj',.false.)
        pivinv=1.d0/a(icol,icol)
        a(icol,icol)=1.d0
        do 16 l=1,n
          a(icol,l)=a(icol,l)*pivinv
16      continue
        do 17 l=1,m
          b(icol,l)=b(icol,l)*pivinv
17      continue
        do 21 ll=1,n
          if(ll.ne.icol)then
            dum=a(ll,icol)
            a(ll,icol)=0.d0
            do 18 l=1,n
              a(ll,l)=a(ll,l)-a(icol,l)*dum
18          continue
            do 19 l=1,m
              b(ll,l)=b(ll,l)-b(icol,l)*dum
19          continue
          endif
21      continue
22    continue
      do 24 l=n,1,-1
        if(indxr(l).ne.indxc(l))then
          do 23 k=1,n
            dum=a(k,indxr(l))
            a(k,indxr(l))=a(k,indxc(l))
            a(k,indxc(l))=dum
23        continue
        endif
24    continue

      END SUBROUTINE gaussj

!-----------------------------------------------------------------------

      SUBROUTINE fgauss(x,a,y,dyda,na)
!  Copyright (C) 1986, 1992 Numerical Recipes Software
      INTEGER :: na
      DOUBLE PRECISION :: x,y,a(na),dyda(na)
      INTEGER :: i
      DOUBLE PRECISION :: arg,ex,fac

      y=0.d0
      do 11 i=1,na-1,3
        arg=(x-a(i+1))/a(i+2)
        ex=exp(-arg**2)
        fac=a(i)*ex*2.d0*arg
        y=y+a(i)*ex
        dyda(i)=ex
        dyda(i+1)=fac/a(i+2)
        dyda(i+2)=fac*arg/a(i+2)
11    continue

      END SUBROUTINE fgauss

      END MODULE histograms

