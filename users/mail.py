#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Mail users about Orient updates.
"""

import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Mail users about Orient updates.
""",epilog="""
This script sends an email message BCC'd to all addresses in the file
all_email. Lines in that file starting with "! " are ignored. 

If this command is ever re-used, the mail message in it will
need to be changed.


{} args
""".format(this))


parser.add_argument("file", help="File of user email addresses")
parser.add_argument("--force", help="Force send mail", action="store_true")

args = parser.parse_args()



#  Compile address list
address = []
with open(args.file) as IN:
  lineno = 0
  for line in IN.readlines():
    lineno += 1
    line = line.strip()
    if re.match(r'! ', line):
      #  Bounced address -- ignore
      pass
    elif line in address:
      print "Duplicate address {} at line {:1d} -- ignored".format(line,lineno)
    elif re.match(r'".+" <[-+\w\.]+@[-\w\.]+>$', line):
      address.append(line)
    else:
      print "Doubtful address {} at line {:1d} -- ignored".format(line,lineno)


print """
This script sends an email message BCC'd to all addresses in the file
all_email. Lines in that file starting with "! " are ignored. (They are
addresses that bounced.)

Before this command is used, the mail message in it will probably
need to be changed.

Use the --force option when ready to send the message. Otherwise executing
this script will just print the message file that would be submitted.
"""
    

body = """To users of my Orient program:

Version 5.0 of the program is now available for download from 
www-stone.ch.cam.ac.uk/programs.html#orient. The main difference is
that it's possible to specify different induction damping factors for
X...Y and Y...X interactions.

There are also various bug-fixes and minor improvements.

Note also that there was a major change to the Display procedure in
version 4.9 which is not backwards compatible with earlier versions.



You can download Orient 5.0 from www-stone.ch.cam.ac.uk/programs/orient.html,
using the user-name and password issued previously.

-- 
Professor Anthony Stone                 www-stone.ch.cam.ac.uk
University Chemical Laboratory,         Email:   ajs1@cam.ac.uk
Lensfield Road,                         Phone:  +44 1223 336375
Cambridge CB2 1EW
"""



msg = """Subject: Orient program update
From: ajs1@cam.ac.uk
To: ajs1@cam.ac.uk
Bcc: {bcc}

{b}
""".format(bcc=", ".join(address),b=body)

if args.force:
  s = smtplib.SMTP('ppsw.cam.ac.uk')
  s.sendmail(me, [me], msg)
  s.quit()
else:
  print msg
