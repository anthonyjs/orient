MODULE options

USE consts
USE dumper, ONLY: undump
USE global
USE induction, ONLY : default_maxind => maxind, induction_cvg
USE input
USE molecules
USE sites
USE switches
USE utility
USE variables, ONLY: nv, x0, define_variables
USE properties, ONLY : inertia
USE geom

!  Optimization options

CHARACTER(LEN=4) :: opt_method="EVF" ! Default method is eigenvector-following

!  General options

DOUBLE PRECISION ::         &
    thresh=1d-6                ! convergence threshold on gradient (a.u.)

INTEGER ::         &
    maxind1=50,    &  ! Induction iterations for first step
    maxind2=50        ! Induction iterations for subsequent steps

LOGICAL ::                    &
    decpathway=.false.,       &  ! Print site positions at each step
    decdump=.false.,          &  ! Dump final geometry?
    symmetry=.false.,         &
    constrain=.false.,        &  ! Constrained optimization?
    show_e=.false.,           &  ! Show energy at each optimization step
    show_v=.false.,           &  ! Show variable values at each step
    show_g=.false.,           &  ! Show gradient at each step
    show_h=.false.,           &  ! Show hessian at each step (EVF only)
    evf_verbose=.true.           ! Verbose/quiet for evf

!  Eigenvector-following options
DOUBLE PRECISION ::         &
    pushoff=0.01d0,         &
    maxstep=0.1d0                ! default maximum step-size, in bohr

INTEGER ::         &
    inr=0,         &  ! Type of stationary point
    niter=100,     &  ! Maximum number of iterations
    ivec=0,        &  ! Default mode for search
    xzeros=0

CHARACTER(LEN=32) ::           &
    dumpfile="",               &  ! File to dump final geometry to
    restartfile="restart.dump"    ! File to undump initial geometry from

!  Variables for realignment routine
INTEGER :: malign           !  Molecule to be realigned
LOGICAL :: realign=.false.
DOUBLE PRECISION :: posn(3), align(3,3)

!  BFGS options
LOGICAL :: bfgs_hessian=.false., & ! Supply hessian diagonal to BFGS
    bfgs_verbose=.false.,        & ! Verbose/quiet for bfgs
    bfgs_debug=.false.
INTEGER :: nupdate=3
!  These values are in Hartree. maxbfgs is the maximum step size.
DOUBLE PRECISION :: maxerise=0.001d0, maxefall=-0.01d0,                &
    coldfusionlimit=-100d0, dguess=0.1d0, maxbfgs=0.4d0

!  Variable mapping
LOGICAL :: map_variables=.false.

!  Plotting options

LOGICAL :: decplot=.false.   !  True if a plot is required
INTEGER :: xyzunit           !  Unit for xyz plot output
INTEGER ::                 &
    nplot=0                  !  A plot is produced every nplot iterations
                             !  if nplot > 0
CHARACTER(LEN=8) ::        &
    whichplot="XYZ"          !  Type of plot to produce
CHARACTER(LEN=32) :: plotfile="orient.plot", plottitle=""
INTEGER ::                         &
    ax1=0,ax2=0,bx1=0,bx2=0,       &  !  Range of crystal cells to plot
    aml1=0,aml2=0,bml1=0,bml2=0,   &  !  Range of layer cells to plot
    anx1=0,anx2=0,bnx1=0,bnx2=0       !  Range of nanocrystal cells to plot


!  Options for frequency calculation and thermodynamic functions

LOGICAL ::                    &
    decfrequency=.false.,     &  !  Calculate frequencies?
    animations_flag=.false.,  &  !  Make xyz animations of the normal modes
    intensities_flag=.false., &  !  Calculate intensities
    vectors_flag=.false.,     &
    thermofuncs_flag=.false.
DOUBLE PRECISION ::             &
    tf_tempmin=9.437158972d-4,  & !  Tmin (298K) for Thermodynamical Functions
    tf_tempmax=9.437158972d-4,  & !  Tmax (298K) for Thermodynamical Functions
    tf_pressure=101325d0          !  Pascal (Pressure)
INTEGER ::                      &
    tf_tempgrid=1,              &  !  Resolution of Thermodynamical Functions
    tf_sigma=1,                 &  !  Symmetry number
    nframes=36                     !  Number of frames for normal mode animations
CHARACTER*32 ::                 &
    tf_file="thermofuncs.dat",  &  !  File for thermodynamic functions
    freqfile= "",               &  !  File for frequencies (none by default)
    openfile=""


CONTAINS

!-----------------------------------------------------------------------

SUBROUTINE default_options

IMPLICIT NONE

opt_method="EVF"

if (crystal .or. periodic) then
  constrain=.true.
else
  constrain=.false.
endif

!  By default, all molecules are free to move, but atom rotations are
!  suppressed in sortmolecules.
move(1:6,1:nmols)=.true.

decplot=.false.
decpathway=.false.
plottitle=" "
whichplot="XYZ"
ax1=0; ax2=0; bx1=0; bx2=0      !  Cell range for plotting crystal
aml1=0; aml2=0; bml1=0; bml2=0  !  Cell range for plotting layer
anx1=0; anx2=0; bnx1=0; bnx2=0  !  Cell range for plotting nanocrystal

decdump=.false.
restartfile="restart.dump"
realign=.false.
maxind1=default_maxind  ! Induction iterations for first step
maxind2=default_maxind  ! Induction iterations for subsequent steps
pushoff=0.01D0
maxstep=0.1          ! default maximum step-size, in bohr
thresh=1d-6          ! convergence threshold
niter=100            ! default maximum number of steps
ivec=0               ! default mode for search
nplot=0
symmetry=.false.
bfgs_hessian=.false.
nupdate=3

decfrequency=.false.
freqfile=""
animations_flag=.false.
intensities_flag=.false.
vectors_flag=.false.
thermofuncs_flag=.false.
nframes=36
tf_tempmin=9.437158972d-4 ! Tmin (298K) for Thermodynamical Fncs
tf_tempmax=9.437158972d-4 ! Tmax (298K) for Thermodynamical Fncs
tf_tempgrid=1             ! Resolution of Thermodynamical Functions
tf_pressure=101325d0      ! Pascal (Pressure)
tf_sigma=1                ! symmetry number
tf_file="thermofuncs.dat"

END SUBROUTINE default_options

!-----------------------------------------------------------------------

SUBROUTINE read_options

IMPLICIT NONE

LOGICAL :: eof, ok
CHARACTER(LEN=16) :: ww, word
CHARACTER(LEN=20) :: molname
CHARACTER(LEN=50) :: warn, error
INTEGER :: km, m
! CHARACTER(LEN=2) :: vtypec(6)=["x ","y ","z ","px","py","pz"]

ok=.true.
do
  warn=""
  error=""
  call read_line(eof)
  call readu(ww)
  select case(ww)
  case("", "NOTE","!")
    cycle
  case("END")
    exit
  case("RESET")
    call default_options
  case("TITLE")
    call reada(plottitle)
  case ("METHOD")
    call reada(ww)
    select case(upcase(ww))
    case("EVF","EF","EIGENVECTOR")
      opt_method="EVF"
    case("BFGS","LMBFGS")
      opt_method="BFGS"
    case default
      error="Optimization method "//trim(ww)//" unknown"
    end select
  case("EVF","EF","EIGENVECTOR")
    opt_method="EVF"
    do while (item<nitems)
      call reada(ww)
      select case(upcase(ww))
      case("QUIET")
        evf_verbose=.false.
      case("VERBOSE")
        evf_verbose=.true.
      case default
        call die ("Unrecognized EVF option "//ww,.true.)
      end select
    end do
  case("BFGS","LBFGS","LMBFGS")
    opt_method="BFGS"
    do while (item<nitems)
      call reada(ww)
      select case(upcase(ww))
      case("VERBOSE")
        bfgs_verbose=.true.
      case("QUIET")
        bfgs_verbose=.false.
      case("DEBUG")
        bfgs_debug=.true.
        debug(15)=.true.
      case("MAXERISE","MAX-RISE")
        call readf(maxerise,efact)
      case("MAXEFALL","MAX-FALL")
        call readf(maxefall,efact)
      case("REJECT","COLD-FUSION-LIMIT")
        call readf(coldfusionlimit,efact)
      case("MAX-STEP")
        call readf(maxbfgs)
      case default
        call die ("Unrecognized LMBFGS option "//ww,.true.)
      end select
    end do
  case("FIX")
    call fix
    constrain=.true.
!  case("MAP")
!    !  MAP [VARIABLES]
!    map_variables=.true.
  case("DEFINE")
    call define_variables
  case("POSITION","POSITIONS")
    call position_molecules
  case("PLOT")
    call plot_options
  case("FREQUENCY","FREQUENCIES")
    call freqinput
  case("THERMOFUNCS","THERMODYNAMIC","THERMOFUNCTIONS")
    call thermoptions
  case("SEARCH")
    call readi(inr)
  case("MINIMUM")
    inr=0
  case("THRESHOLD", "CONV", "CONVERGE", "CONVERGENCE")
    call readf(thresh)
    thresh=thresh*rfact/efact
    if (thresh .le. 0d0) thresh=1d-6
  case("NITER", "ITERATIONS")
    call readi(niter)
  case("INDUCTION")
    ! INDUCTION [[ITERATIONS] n1 [THEN n2]] [CONVERGENCE tol]
    do while (item < nitems)
      call reada(word)
      select case(upcase(word))
      case("ITERATIONS")
        cycle
      case("AND","THEN")
        call readi(maxind2)
      case("CONV","CONVERGE","CONVERGENCE")
        call readf(induction_cvg)
      case default
        call reread(-1)
        call readi(default_maxind)
        maxind1=default_maxind
        maxind2=default_maxind
      end select
    end do
  case("RESTART", "RESTORE")
    call reada(restartfile)
    if (restartfile .eq. "") then
      error="Dumpfile not specified for restart"
    else
! DRN 22/4/02
      do m=1,mols
        call axes(m,.false.)
        call inertia(m)
      end do
      call undump(restartfile)
      call geometry
    endif
  case("PRINTLEVEL")
    call readi(print)
  case("SHOW")
    do while (item .lt. nitems)
      call readu(word)
      select case(word)
      case("STEPS","ENERGY")
        show_e=.true.
      case("VARIABLES")
        show_v=.true.
      case("GRADIENT")
        show_g=.true.
      case("HESSIAN")
        show_h=.true.
      case("NONE","NOTHING")
        show_e=.false.
        show_v=.false.
        show_g=.false.
        show_h=.false.
      case default
        error="Unknown option "//trim(ww)//" "//trim(word)
      end select
    end do
  case("DUMP")
    decdump=.true.
    call reada(dumpfile)
    if (dumpfile .eq. "") then
      error="Dumpfile not specified"
    else
      print "(/A,A)",                                                  &
          "Final orientation will be dumped in file: ", trim(dumpfile)
    endif

!  Eigenvector-following options
  case("NEWTON-RAPHSON","SADDLE","MAXSTEP","STEP","PUSHOFF","MODE",    &
      "SYMMETRY","REALIGN","PATHWAY")
    if (opt_method .ne. "EVF") then
      error="Option "//trim(ww)//" is invalid for "//opt_method//" method"
    else
      select case(ww)
      case("NEWTON-RAPHSON")
        inr=1
      case("SADDLE")
        inr=2
      case("MAXSTEP", "STEP")
        call readf(maxstep)
        maxstep=maxstep/rfact
      case("PUSHOFF")
        call readf(pushoff)
      case("MODE")
        call readi(ivec)
      case("SYMMETRY")
        symmetry=.true.
      case("REALIGN")
        call reada(molname)
        malign=locate(molname)
        km=cm(malign)
        align(:,:)=se(:,:,sm(km))
        posn(:)=sx(:,km)
        realign=.true.
      case("PATHWAY")
        decpathway=.true.
      end select
    endif


!  BFGS option
  case("HESSIAN")
    if (opt_method .ne. "BFGS") then
      error="Option "//trim(ww)//" is invalid for "//opt_method//" method."
    else
      call readu(word)
      select case(word)
      case("OFF")
        bfgs_hessian=.false.
      case("ON","")
        bfgs_hessian=.true.
      case default
        error="Unknown option "//trim(ww)//" "//trim(word)
      end select
    endif

  case default
    error="Option "//trim(ww)//" not recognized."
  end select
  if (warn .ne. "") print "(2A)", "Warning: ", warn
  if (error .ne. "") then
    print "(2A)", "Error: ", error
    ok=.false.
  endif
end do
if (.not. ok) call die ("Errors in options",.true.)


END SUBROUTINE read_options

!-----------------------------------------------------------------------

SUBROUTINE plot_options

USE input, ONLY : item, nitems, readu, reada, readi, die

IMPLICIT NONE
CHARACTER(LEN=16) :: word
CHARACTER(LEN=32) :: newfile
CHARACTER(LEN=50) :: warn, error
LOGICAL :: open, ok

decplot=.true.
ok=.true.
do while (item .lt. nitems)
  warn=""
  error=""
  call readu(word)
  select case(word)
  case("NONE","NO","OFF")
    decplot=.false.
    exit
  case("XYZ","ESTGEN","EXPLORER")
    whichplot=word
  case("FILE")
    call reada(newfile)
    if (xyzunit>0) then
      inquire (unit=xyzunit,opened=open)
    else
      open=.false.
    endif
    if (newfile .ne. plotfile) then
      if (open) close(xyzunit)
      open=.false.
      plotfile=newfile
    endif
    if (.not. open) then
      xyzunit=find_io(24)
      if (plotfile .ne. " ") then
        open(unit=xyzunit,file=pfile(plotfile),status="unknown")
        open=.true.
      else
        warn="No xyz plotdata file specified"
      endif
    endif
  case("NEW")
    if (xyzunit>0) then
      inquire (unit=xyzunit,opened=open)
    else
      open=.false.
    end if
    if (open) close(xyzunit)
  case("EVERY")
    if (whichplot .ne. "XYZ")                                           &
        warn="`EVERY n' only permitted for xyz plot files"
    call readi(nplot)
  case("LAST")
    nplot=0
  case("TITLE")
    call reada(plottitle)
  case("CRYSTAL","LATTICE")
    call readi(ax1); call readi(ax2)
    call readi(bx1); call readi(bx2)
  case("MONOLAYER","LAYER","ADLAYER")
    call readi(aml1); call readi(aml2)
    call readi(bml1); call readi(bml2)
  case("NANOCRYSTAL")
    call readi(anx1); call readi(anx2)
    call readi(bnx1); call readi(bnx2)
  case default
    error="Plot option "//trim(word)//" not recognized"
  end select
  if (warn .ne. "") print "(2A)", "Warning: ", warn
  if (error .ne. "") then
    print "(2A)", "Error: ", error
    ok=.false.
  endif
end do
if (.not. ok) call die ("Errors in plot options",.true.)

END SUBROUTINE plot_options

!-----------------------------------------------------------------------

SUBROUTINE freqinput

USE input, ONLY : item, nitems, readu, reada, readi, die

IMPLICIT NONE
CHARACTER(LEN=12) :: word
CHARACTER(LEN=50) :: warn, error
LOGICAL :: ok

decfrequency=.true.
ok=.true.
do while (item .lt. nitems)
  warn=""
  error=""
  call readu(word)
  select case(word)
  case("FILE")
    call reada(freqfile)
  case("ANIMATIONS","ANIMATE")
    animations_flag=.true.
  case("INTENSITIES")
    intensities_flag=.true.
  case("VECTORS")
    vectors_flag=.true.
  case("FRAMES")
    call readi(nframes)
  case default
    error="FREQUENCY option "//trim(word)//" not recognized"
  end select
  if (warn .ne. "") print "(2A)", "Warning: ", warn
  if (error .ne. "") then
    print "(2A)", "Error: ", error
    ok=.false.
  endif
end do
if (.not. ok) call die ("Errors in plot options",.true.)

END SUBROUTINE freqinput

!-----------------------------------------------------------------------

SUBROUTINE thermoptions

USE input, ONLY : item, nitems, readu, reada, readi, readf, die

IMPLICIT NONE
CHARACTER(LEN=12) :: word
CHARACTER(LEN=50) :: error
LOGICAL :: ok

thermofuncs_flag=.true.
decfrequency=.true.
ok=.true.
do while (item .lt. nitems)
  error=""
  call readu(word)
  select case(word)
    case("FILE")
      call reada(tf_file)
    case("PRESSURE")
      call readf(tf_pressure)
    case("SIGMA")
      call readi(tf_sigma)
    case("TEMPMAX")
      call readf(tf_tempmax)
      tf_tempmax=tf_tempmax/ehvK
    case("TEMPMIN")
      call readf(tf_tempmin)
      tf_tempmin=tf_tempmin/ehvK
    case("TEMPGRID")
      call readi(tf_tempgrid)
      tf_tempgrid=max(1,tf_tempgrid)
    case default
      error="THERMOFUNCS keyword "//trim(word)//" not recognized"
    end select
  if (error .ne. "") then
    print "(2A)", "Error: ", error
    ok=.false.
  endif
end do
if (.not. ok) call die ("Errors in plot options",.true.)

END SUBROUTINE thermoptions

!-----------------------------------------------------------------------

SUBROUTINE fix

!  Syntax of FIX:
!  FIX
!    molecule [ANGLES | ORIENTATION] [POSITION] [X] [Y] [Z]
!    ...
!  END
!  where POSITION is equivalent to X Y Z. Position components may be
!  frozen individually but rotations for a particular molecule must be
!  all frozen or all free. If a molecule name is given without any
!  keywords, both its position and orientation are to be fixed.

!  To fix an layer (among several) the syntax is:
!  FIX 
!    LAYER layerindex [ANGLES | ORIENTATION] [POSITION] [X] [Y] [Z]
!    ... other fix commands
!  END
!  If layerindex=0, molecules not belonging to any layer are fixed.

IMPLICIT NONE
LOGICAL :: more, eof
INTEGER :: m, adin
CHARACTER(LEN=20) :: ww

xzeros=0
more=.true.
do while (more)
  call read_line(eof)
  if (eof) call die("Unexpected end of input file",.true.)
  call readu(ww)
  select case(ww)
  case ("END")
    more=.false.
  case ("ZEROS")
    call readi(xzeros)
  case("LAYER","ADLAYER")
    call readi(adin)
    if (adin .gt. nlayers) call die                                 &
        ("Layer "//trim(stri(adin))//" not defined ",.true.)
    m=layer(adin)
    if (item .eq. nitems) then
      do while (m .ne. 0)
        move(1:6,m)=.false.
        m=nextm(m)
      end do
    else
      do while (item .lt. nitems)
        m=layer(adin)
        call readu(ww)
        select case(ww)
        case("POSITION","POSN")
          do while (m .ne. 0)
            move(1:3,m)=.false.
            m=nextm(m)
          end do
        case("ANGLES","ORIENTATION")
          do while (m .ne. 0)
            move(4:6,m)=.false.
            m=nextm(m)
          end do
        case("X")
          do while (m .ne. 0)
            move(1,m)=.false.
            m=nextm(m)
          end do
        case("Y")
          do while (m .ne. 0)
            move(2,m)=.false.
            m=nextm(m)
          end do
        case("Z")
          do while (m .ne. 0)
            move(3,m)=.false.
            m=nextm(m)
          end do
        case default
          call die("Unrecognized keyword",.true.)
        end select
      end do
    endif
  case default
    call reread(-1)
    call reada(ww)
    m=locate(ww)
    if (m .eq. 0)                                                  &
        call die("Molecule "//ww//" not found",.true.)
    if (item .eq. nitems) then
      move(1:6,m)=.false.
    else
      do while (item .lt. nitems)
        call readu(ww)
        select case(ww)
        case("POSITION","POSN")
          move(1:3,m)=.false.
        case("ANGLES","ORIENTATION")
          move(4:6,m)=.false.
        case("X")
          move(1,m)=.false.
        case("Y")
          move(2,m)=.false.
        case("Z")
          move(3,m)=.false.
        case default
          call die("Unrecognized keyword",.true.)
        end select
      end do
    endif
  end select
end do

!  Special case: 2 molecules, positions fixed, orientations free
if (mols .eq. 2 .and.                                                   &
    .not. any(move(1:3,1:2))                                            &
    .and. all(move(4:6,1:2))) then
  xzeros=1
  print "(/A)", "Extra zero Hessian eigenvalue expected."
else
  xzeros=0
endif

END SUBROUTINE fix

END MODULE options
