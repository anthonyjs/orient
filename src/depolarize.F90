SUBROUTINE depolarize

!  Find a description of a molecule in which the site dipoles are replaced
!  by a set of reduced dipoles together with local polarizabilities, so
!  that internal polarization recovers the original dipoles.

!  The molecule should be specified as two separate pieces, each with
!  sites in the appropriate positions and orientations for the molecule
!  as a whole.

!  The calculation is carried out on the assumption that all moments
!  are referred to global axes.

USE molecules, ONLY: mol, head
USE sites, ONLY: L, next, sx, ps, Q, name, type, locate, nsites
USE alphas, ONLY: alpha, indexa
USE indexes, ONLY: lookup
USE input, ONLY: assert, reada, readf, item, nitems, upcase, die
USE timing, ONLY: time_and_date
USE types, ONLY: indform, indampf, pairindex
USE damping, ONLY: damp
#ifdef NAGF95
USE f90_unix_io, ONLY: flush
#endif

IMPLICIT NONE

CHARACTER(LEN=*), PARAMETER :: this_routine = "depolarize"
CHARACTER(LEN=80) :: ww 
DOUBLE PRECISION, ALLOCATABLE :: AT(:,:), qq(:), qu(:), qc(:), rtalphalen(:)
DOUBLE PRECISION :: alpha_a(3,3), alpha_b(3,3), T_ab(0:3,0:3),          &
    T_ba(0:3,0:3), xa(3), xb(3), R_ab(3), R_ba(3), R, u, fe, ft, thole_exp
INTEGER :: info, i, ix, j, k, ka, kb, m, m1, m2, n, n1, n2, p, pa, pb,  &
    np, pair
INTEGER, ALLOCATABLE :: ipiv(:)  ! Workspace for DGESV
LOGICAL :: verbose = .false., thole = .false.

call time_and_date(ww)
print "(4a)", "Entering ", this_routine, " at ", trim(ww)

!  [DEPOLARIZE] mol1 mol2 [THOLE thole_exp]
call reada(ww)
m1 = locate(ww)
call assert(m1 > 0, "Can't find molecule "//trim(ww))
call reada(ww)
m2 = locate(ww)
call assert(m2 > 0, "Can't find molecule "//trim(ww))
call assert(m1 .ne. m2, "Molecules must be different in "//this_routine)
thole_exp = 0d0
ft = 1d0
fe = 1d0
do while (item < nitems)
  call reada(ww)
  select case(upcase(ww))
  case("THOLE")
    call readf(thole_exp)
    thole = .true.
  case("VERBOSE")
    verbose = .true.
  end select
end do

allocate(rtalphalen(nsites))
np = 0
!  Find dimension of problem
m = m1
do
  n = 0
  k = head(m)
  do while (k > 0)
    if (verbose) print "(a,i0,1x,a)", "Site ", k, trim(name(k))
    if (l(k) .ge. 0) then  !  Skip sites with no moments
      !  We assume that otherwise sites carry charge and dipole only
      !  and that polarizabilities are local dipole--dipole only
      n = n+3
      rtalphalen(k) = 0d0
      if (ps(k) > 0) then
        ix=lookup(indexa,ka,ka)
        if (ix > 0) then
          alpha_a = alpha(2:4,2:4,ix)
          rtalphalen(k) = ((alpha(1,1,ix)+alpha(2,2,ix)+alpha(3,3,ix))/3d0)**(1d0/6d0)
        else
          print "(a,i0,a))", "Site ", ka,                               &
              " is marked polarizable but has no polarizability matrix"
          call die("Stopping",.true.)
        end if
      end if
    end if
    k = next(k)
  end do
  if (m == m2) then
    n2 = n
    exit
  end if
  n1 = n
  m = m2
end do
n = n1 + n2
allocate (AT(n,n), qq(n), qu(n), qc(n), ipiv(n), stat=info)
if (info > 0) call die("Can't allocate arrays in "//this_routine)
AT = 0d0
do i=1,n
  AT(i,i) = 1d0
end do
pa = 0

print "(a)", "Starting matrix construction loop"

!  AB section
ka = head(m1)
do while (ka > 0)
  if (verbose) print "(a)", trim(name(ka))
  if (l(ka) .ge. 0) then
    qq(pa+1:pa+3) = Q(2:4,ka)
    qc(pa+1:pa+3) = 0d0
    if (ps(ka) > 0) then
      ix=lookup(indexa,ka,ka)
      alpha_a = alpha(2:4,2:4,ix)
      xa = sx(:,ka)
      pb = n1
      kb = mol(m2)%head
      do while (kb > 0)
        if (l(kb) .ge. 0) then
          xb = sx(:,kb)
          !  Note order of components
          R_ab = [xb(3)-xa(3), xb(1)-xa(1), xb(2)-xa(2)]
          R = sqrt(R_ab(1)**2 + R_ab(2)**2 + R_ab(3)**2)
          if (R < 1d-2) then
            print "(a, 2i3)", "Sites too close: ", ka, kb
            call die ("Stopping")
          end if
          if (thole) then
            !  Thole damping
            u = R/(rtalphalen(ka)*rtalphalen(kb))
            fe = 1d0 - exp(-thole_exp*u**3)
            ft = fe - thole_exp*u**3 * exp(-thole_exp*u**3)
          end if
          T_ab(0,0) = 1d0/R
          T_ab(0,1:3) = - fe * R_ab(:)/R**3
          T_ab(1:3,0) = fe * R_ab(:)/R**3
          do i = 1,3
            do j = 1,3
              T_ab(i,j) = -3d0 * ft * R_ab(i) * R_ab(j)
            end do
            T_ab(i,i) = T_ab(i,i) + fe * R**2
          end do
          T_ab(1:3,1:3) = T_ab(1:3,1:3) / R**5
          AT(pa+1:pa+3,pb+1:pb+3) = - matmul(alpha_a,T_ab(1:3,1:3))
          qc(pa+1:pa+3) = qc(pa+1:pa+3) - matmul(alpha_a,T_ab(1:3,0)) * Q(1,kb)
          if (verbose) then
            print "(2a6)", name(ka), name(kb)
            print "(3f10.5)", (AT(i,pb+1:pb+3), i=pa+1,pa+3)
          end if
          pb = pb+3
        end if
        kb = next(kb)
      end do
    end if
    pa = pa+3
  end if
  ka = next(ka)
end do
if (pa .ne. n1) call die("Something's wrong in "//this_routine)

!  BA section
pb = n1
kb = mol(m2)%head
do while (kb > 0)
  if (verbose) print "(a)", trim(name(kb))
  if (l(kb) .ge. 0) then
    qq(pb+1:pb+3) = Q(2:4,kb)
    qc(pb+1:pb+3) = 0d0
    if (ps(kb) > 0) then
      ix=lookup(indexa,kb,kb)
      alpha_b = alpha(2:4,2:4,ix) 
      xb = sx(:,kb)
      pa = 0
      ka = mol(m1)%head
      do while (ka > 0)
        if (l(ka) .ge. 0) then
          xa = sx(:,ka)
          !  Note order of components
          R_ba = [xa(3)-xb(3), xa(1)-xb(1), xa(2)-xb(2)]
          R = sqrt(R_ba(1)**2 + R_ba(2)**2 + R_ba(3)**2)
          if (R < 1d-2) then
            print "(a, 2i3)", "Sites too close: ", kb, ka
            call die ("Stopping")
          end if
          if (thole) then
            !  Thole damping
            u = R/(rtalphalen(ka)*rtalphalen(kb))
            fe = 1d0 - exp(-thole_exp*u**3)
            ft = fe - thole_exp*u**3 * exp(-thole_exp*u**3)
          end if
          T_ba(0,0) = 1d0/R
          T_ba(0,1:3) = - fe * R_ba(:)/R**3
          T_ba(1:3,0) = fe * R_ba(:)/R**3
          do i = 1,3
            do j = 1,3
              T_ba(i,j) = -3d0 * ft * R_ba(i) * R_ba(j)
            end do
            T_ba(i,i) = T_ba(i,i) + fe * R**2
          end do
          T_ba(1:3,1:3) = T_ba(1:3,1:3) / R**5
          AT(pb+1:pb+3,pa+1:pa+3) = - matmul(alpha_b,T_ba(1:3,1:3))
          qc(pb+1:pb+3) = qc(pb+1:pb+3) - matmul(alpha_b,T_ba(1:3,0)) * Q(1,ka)
          if (verbose) then
            print "(2a6)", name(kb), name(ka)
            print "(3f10.5)", (AT(i,pa+1:pa+3), i=pb+1,pb+3)
          end if
          pa = pa+3
        end if
        ka = next(ka)
      end do
    end if
    pb = pb+3
  end if
  kb = next(kb)
end do
if (pb .ne. n) call die("Something's wrong in "//this_routine)

call flush(6)

!  Now solve qq(:)-qc(:) = AT(:,:) qu(:) for the 'depolarized' moments qu.
print "(a)", "Starting dgesv"
call flush(6)

qu = qq - qc
call dgesv(n,1,AT,n,ipiv,qu,n,info)
select case(info)
case(0)
  print "(a)", "Equations solved successfully"
case(-8:-1)
  print "(a,i0)", "Argument ", -info, " to DSEGV was invalid"
  call die("Stopping")
case(1:)
  call die("Singular matrix")
end select

call flush(6)
m = m1
pa = 0
do
  print "(/a)", trim(mol(m)%name)
  ka = mol(m)%head
  do while (ka > 0)
    if (l(ka) >= 0) then
      print "(/a6,a,10x,4f12.6)", name(ka), "original", qq(pa+1:pa+3)
      print "(a,4f12.6)", "less charge polarization", qq(pa+1:pa+3)-qc(pa+1:pa+3)
      print "(a,4f12.6)", "less dipole polarization", qu(pa+1:pa+3)
      print "(a,4f12.6)", "              difference", qq(pa+1:pa+3)-qu(pa+1:pa+3)
      !  Apply adjustment to site dipole moments
      Q(2:4,ka) = qu(pa+1:pa+3)
      ! print "(a6,4f10.6)", name(k), Q(1:4,ka)
      pa = pa+3
    end if
    ka = next(ka)
  end do
  if (m == m2) exit
  m = m2
end do

CONTAINS

!  Not used
DOUBLE PRECISION FUNCTION dampfn(ka,kb,R)

USE types, ONLY: radius

INTEGER, INTENT(IN) :: ka, kb
DOUBLE PRECISION, INTENT(IN) :: R

DOUBLE PRECISION :: vv

!  Tkatchenko & DiStasio, PRL 108, 236402 (2012)
vv = radius(type(ka)) + radius(type(kb))
dampfn = 1d0 - exp(-(R/vv)**2)

END FUNCTION dampfn

END SUBROUTINE depolarize
