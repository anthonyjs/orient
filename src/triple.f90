SUBROUTINE triple(e3)

!  Estimate the triple-dipole energy of the system. If no molecules are
!  listed, the triple-dipole energy is evaluated for each triple of
!  molecules using the same coefficient. If a set of 3 molecules is listed,
!  it is evaluated for that set only.

USE consts
USE input
USE sites
USE molecules
USE properties, ONLY : inertia
IMPLICIT NONE


DOUBLE PRECISION, INTENT(OUT) :: e3
DOUBLE PRECISION :: c9


DOUBLE PRECISION :: r12, r12sq, r13, r13sq, r23, r23sq,                &
    c1, c2, c3, e3m
INTEGER :: m1, m2, m3, k1, k2, k3

if (mols .le. 2) then
  print '(A,I2,A)',                                                    &
      "Only", mols, " molecules. Triple-dipole energy is zero."
  return
endif
call readf(c9)
print "(/a,f12.5,1x,a,1x,2a)", "Triple dipole energies. C9 = ", c9,    &
    trim(eunit), trim(runit), "^9"
c9=c9/(efact*rfact**9)
e3=0d0
if (item .eq. nitems) then
  !  Evaluate for each triple of molecules
  !  Find centre of mass if necessary
  do m1=1,mols
    call axes(m1,.false.)
    call inertia(m1)
  end do
  if (mols .ge. 4) print '(/a/2a)', 'Triple-dipole terms',             &
      "Molecules        Distances                  Angles",            &
      "                Energy"
  !  Loop over triples of molecules
  do m1=1,mols-2
    k1=cm(m1)
    do m2=m1+1,mols-1
      k2=cm(m2)
      r12sq=((sx(1,k1)-sx(1,k2))**2+(sx(2,k1)-sx(2,k2))**2             &
          +(sx(3,k1)-sx(3,k2))**2)
      r12=sqrt(r12sq)
      do m3=m2+1,mols
        k3=cm(m3)
        r13sq=((sx(1,k1)-sx(1,k3))**2+(sx(2,k1)-sx(2,k3))**2           &
            +(sx(3,k1)-sx(3,k3))**2)
        r13=sqrt(r13sq)
        r23sq=((sx(1,k2)-sx(1,k3))**2+(sx(2,k2)-sx(2,k3))**2           &
            +(sx(3,k2)-sx(3,k3))**2)
        r23=sqrt(r23sq)
        c1=(r12sq+r13sq-r23sq)/(2d0*r12*r13)
        c2=(r12sq+r23sq-r13sq)/(2d0*r12*r23)
        c3=(r13sq+r23sq-r12sq)/(2d0*r13*r23)
        ! $check=acos(c1)+acos(c2)+acos(c3)
        e3m=c9*(1+3*c1*c2*c3)/(r12*r13*r23)**3
        if (mols .ge. 4) then
          print '(3i3,3f8.3,3f9.2,'//efmt//',1x,a)', m1, m2, m3,       &
              r12*rfact, r13*rfact, r23*rfact, acos(c3)*afact,         &
              acos(c2)*afact, acos(c1)*afact, e3m*efact, eunit
        endif
        e3=e3+e3m
      end do
    end do
  end do
  print '(/a,'//efmt//',1x,a)', 'Total triple-dipole energy = ',       &
      e3*efact, trim(eunit)
else
  call readi(m1); call readi(m2); call readi(m3)
  if (m3 .eq. 0) call die("Specify 3 molecules",.true.)
  k1=cm(m1)
  k2=cm(m2)
  k3=cm(m3)
  r12sq=((sx(1,k1)-sx(1,k2))**2+(sx(2,k1)-sx(2,k2))**2                 &
      +(sx(3,k1)-sx(3,k2))**2)
  r12=sqrt(r12sq)
  r13sq=((sx(1,k1)-sx(1,k3))**2+(sx(2,k1)-sx(2,k3))**2                 &
      +(sx(3,k1)-sx(3,k3))**2)
  r13=sqrt(r13sq)
  r23sq=((sx(1,k2)-sx(1,k3))**2+(sx(2,k2)-sx(2,k3))**2                 &
      +(sx(3,k2)-sx(3,k3))**2)
  r23=sqrt(r23sq)
  c1=(r12sq+r13sq-r23sq)/(2d0*r12*r13)
  c2=(r12sq+r23sq-r13sq)/(2d0*r12*r23)
  c3=(r13sq+r23sq-r12sq)/(2d0*r13*r23)
  e3m=c9*(1+3*c1*c2*c3)/(r12*r13*r23)**3
  print '(/A/2A)', 'Triple-dipole energy',                             &
      "Molecules        Distances                  Angles",            &
      "                Energy"
  print '(3i3,3f8.3,3f9.5,'//efmt//',1x,A)', m1, m2, m3,               &
              r12*rfact, r13*rfact, r23*rfact, acos(c3)*afact,         &
              acos(c2)*afact, acos(c1)*afact, e3m*efact, eunit
endif

END SUBROUTINE triple

