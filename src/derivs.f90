MODULE derivs

!  Array deriv1(1:3,n) holds the first derivatives of the energy w.r.t.
!  the position coordinates of molecule 1, and deriv1(4:6,n) holds the
!  derivatives w.r.t. the orientation coordinates, which may be angles
!  about coordinate axes (in which case deriv1(4:6,n) is minus the torque
!  on molecule n) or components of the angle-axis vector.

!  Array deriv2 holds the second derivatives. Not currently implemented
!  for angle-axis variables.

DOUBLE PRECISION, DIMENSION (:,:), ALLOCATABLE, SAVE :: deriv1
DOUBLE PRECISION, DIMENSION (:,:,:,:), ALLOCATABLE, SAVE :: deriv2

PRIVATE
PUBLIC deriv1, deriv2, alloc_derivs

CONTAINS

SUBROUTINE alloc_derivs(order,n)

USE input, ONLY : die
IMPLICIT NONE
INTEGER :: order, n, ok

if (order .eq. 1) then

  if (allocated(deriv1)) then
    !  Check that it's big enough
    if (size(deriv1,2) .ge. n) then
      return
    else
      deallocate(deriv1)
    endif
  endif
  allocate(deriv1(6,n),stat=ok)
  if (ok .gt. 0) then
    call die ("Allocation failed for deriv1",.false.)
  endif

else if (order .eq. 2) then

  if (allocated(deriv2)) then
    !  Check that it's big enough
    if (size(deriv2,3) .ge. n) then
      return
    else
      deallocate(deriv2)
    endif
  endif
  allocate(deriv2(6,6,n,n),stat=ok)
  if (ok .gt. 0) then
    call die ("Allocation failed for deriv2",.false.)
  endif

endif

END SUBROUTINE alloc_derivs

END MODULE derivs
