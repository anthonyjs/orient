#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

import subprocess
import sys

rc = 0
rc += subprocess.call(["../cmp_dump.py", "min.dump"])
rc += subprocess.call(["../cmp_xyz.py", "opt.xyz"])
rc += subprocess.call(["./cmp_out.py", "out"])

# print "rc = ", rc
sys.exit(rc)

