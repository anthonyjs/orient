SUBROUTINE pair_es(m1,k1,m2,k2,order,es,factor)

!  Calculate the electrostatic term in the interaction energy
!  between a pair of sites. The contributions to the electrostatic term
!  and to the derivatives are multiplied by factor.

!  Based on molapair, but with repulsion and dispersion omitted.

USE consts
USE damping
USE derivs
USE interact
USE monolayer
USE pairderiv
USE switches

IMPLICIT NONE


INTEGER, INTENT(IN) ::  k1, k2, m1, m2, order
DOUBLE PRECISION, INTENT(IN) :: factor
DOUBLE PRECISION, INTENT(OUT) :: es

INTEGER :: jp, k, kmax, m, n
DOUBLE PRECISION :: vr(12), pk, qq, s

s=factor

!  The entries in u0(n), n=1..6 will be the R^{-n} terms in the
!  electrostatic energy. u1 and u2 hold the first and second derivatives
!  of the electrostatic terms.

vr(1)=1d0/r
do k=1,11
  vr(k+1)=vr(k)*vr(1)
end do
u0(1:eslimit)=0d0
u1=0d0
u2=0d0
es0=0d0
if (order .ge. 1) then
  es1(:)=0d0
  if (order .ge. 2) then
    es2(:,:)=0d0
  endif
endif

!  We don't need derivatives involving site k2 if it is in the lattice,
!  i.e. if m2=0.
if (m2 .ge. 1) then
  m=12
else
  m=6
endif
!  If m1=m2, we are dealing with a genuine periodic layer. In this case
!  the derivatives for m2,k2 need to be added to those for m1,k2, but we
!  need a factor of 1/2 to avoid double-counting

k=1
do n=1,nesmax
  do while (power(n) .gt. k)
    k=k+1
  end do
  qq=q1(n)*q2(n)*fac(n)
  !  print '(I4,I3,3F12.8)', list(n), k, qq,
  !  &      s0(n), qq*s0(n)*vr(k)
  u0(k)=u0(k)+qq*s0(n)
  if (order .ge. 1) then
    u1(1:m,k)=u1(1:m,k)+qq*d1s(1:m,n)
    if (order .ge. 2) then
      u2(1:m,1:m,k)=u2(1:m,1:m,k)+qq*d2s(1:m,1:m,n)
    endif
  endif
end do
kmax=k

!  We need to put in the r-dependence
pk=0d0
do k=1,kmax
  pk=pk+1d0
  if (debug(10)) print '(A,I1,F15.8)', 'R^-', k, u0(k)*vr(k)
  es0=es0+u0(k)*vr(k)
  if (order .ge. 1) then
    es1(1:m)=es1(1:m)+vr(k)*(u1(1:m,k)-vr(1)*u0(k)*pk*d1(16,1:m))
    if (order .ge. 2) then
      do jp=1,m
        es2(1:m,jp)=es2(1:m,jp)+vr(k)*(u2(1:m,jp,k)                &
            -pk*vr(1)*(u1(1:m,k)*d1(16,jp)+u1(jp,k)*d1(16,1:m)     &
            +u0(k)*d2(16,1:m,jp))                                  &
            +pk*(pk+1d0)*vr(2)*u0(k)*d1(16,1:m)*d1(16,jp))
      end do
    endif
  endif
end do


if (print .gt. 2) print '(2I4,F10.3,5'//EFMT//')',                &
    k1, k2, r*rfact, es0*efact
es=s*es0

if (order .ge. 1) then
  if (m2 .eq. m1) then
    !  Layer: no translational contribution to derivatives
    deriv1(4:6,m1)=deriv1(4:6,m1)+s*(es1(4:6)+es1(10:12))
  else if (m2 .gt. 0) then
    deriv1(1:6,m1)=deriv1(1:6,m1)+s*es1(1:6)
    deriv1(1:6,m2)=deriv1(1:6,m2)+s*es1(7:12)
  else
    deriv1(1:6,m1)=deriv1(1:6,m1)+s*es1(1:6)
  endif
  if (order .ge. 2) then
    if (m2 .eq. m1) then
      !  Layer: no translational contribution to derivatives
      deriv2(4:6,4:6,m1,m1)=deriv2(4:6,4:6,m1,m1)                 &
          +s*(es2(4:6,4:6)+es2(4:6,10:12)+es2(10:12,4:6)+es2(10:12,10:12))
    else if (m2 .gt. 0) then
      deriv2(1:6,1:6,m1,m1)=deriv2(1:6,1:6,m1,m1)                 &
          +s*es2(1:6,1:6)
      deriv2(1:6,1:6,m1,m2)=deriv2(1:6,1:6,m1,m2)                 &
          +s*es2(1:6,7:12)
      deriv2(1:6,1:6,m2,m1)=deriv2(1:6,1:6,m2,m1)                 &
          +s*es2(7:12,1:6)
      deriv2(1:6,1:6,m2,m2)=deriv2(1:6,1:6,m2,m2)                 &
          +s*es2(7:12,7:12)
    else
      deriv2(1:6,1:6,m1,m1)=deriv2(1:6,1:6,m1,m1)                 &
          +s*es2(1:6,1:6)
    endif
  endif
endif

END SUBROUTINE pair_es
