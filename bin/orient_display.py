#!/usr/bin/python
#  -*-  coding  iso-8859-1  -*-

"""Submit an Orient display job, selecting one or more specified SHOW commands.
"""

import argparse
import re
import os
# import string
import subprocess
import sys
from time import sleep

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Submit a set of Orient display jobs, each selecting one
of a set of specified SHOW commands.
""",epilog="""

E.g. orient_display.py CS2-vdw2.0-maps --show diff-isa diff-dma -p vdw2.0_

The Orient program can only display one picture, but often several are
needed using similar data. This script generates a set of data files
from a general data file and runs each one as a separate Orient job.

The file should include a display section containing all necessary map
definitions, followed by all required SHOW sections, each terminated by
END, in any order. A "--show <map>" argument implies that the data file
defines the specified <map> and contains a "show <map>" section. The
script moves the specified SHOW sections in turn to the end of the DISPLAY
section and deletes the others before submitting the resulting data file
to Orient. "--show all" causes all SHOW sections to be displayed. 
--show none" will calculate the data and statistics for the specified
maps, but produce no display.

The script assumes that the file contains nothing after the "END" that closes
the display section, or at least no other "END" lines.

The generated data files are <prefix><m>.data, where <m> is the map name
and <prefix> is the --prefix argument (default null), and the output files
are <prefix><m>.out.
""")

parser.add_argument("file", help="Orient input file containing all display data")
parser.add_argument("--show", help="Show commands to use", nargs="*")
parser.add_argument("--data",  default="data",
                    help="file suffix for constructed Orient data (default data)")
parser.add_argument("--out", "-o", default="out",
                    help="Output file suffix (default out)")
parser.add_argument("--prefix", "-p", help="File prefix", default="")
parser.add_argument("--list", help="List show commands and exit", action="store_true")
parser.add_argument("--summary", help="file for results summary",
                    default="summary")
parser.add_argument("--keep", help="Keep intermediate files",
                    action="store_true")

args = parser.parse_args()

orient = os.path.join(os.environ["ORIENT"],"bin","orient")

#  The "--show none" option will calculate the data and statistics
#  for the specified maps, but produce no display.
cmnd = {"none": ""}
lines = []
line = "--"
with open(args.file) as S:
  while line != "":
    line = S.readline()
    m = re.match(r' *show +(\S+) *$', line, flags=re.I)
    if m:
      name = m.group(1)
      body = line
      while True:
        line = S.readline()
        body += line
        if re.match(r' *end *$', line, flags=re.I):
          cmnd[name] = body
          break
    else:
      lines.append(line)

if args.list or not args.show:
  print "Available --show commands:"
  for key in sorted(cmnd.keys()):
    print key
  exit(0)

if args.show[0] == "all":
  show = cmnd.keys()
else:
  show = args.show

datafiles = []
outfile = {}
proc = {}
for map in show:
  if map not in cmnd:
    print "No command {} in {}".format(map,args.file)
    continue
  datafile = "{}{}.{}".format(args.prefix,map,args.data)
  outfile[map] = "{}{}.{}".format(args.prefix,map,args.out)
  datafiles.append(datafile)
  with open(datafile,"w") as D:
    l = -1
    while not re.match(r' *end *$', lines[l], flags=re.I):
      l -= 1
    for line in lines[0:l]:
      D.write(line)
    D.write(cmnd[map])
    #  These lines can be omitted -- they would never be executed --
    #  unless the "--show none" option is used.
    for line in lines[l:]:
      D.write(line)

  # print "{} -> {}".format(datafile,outfile[map])
  with open(datafile) as IN, open(outfile[map],"w") as OUT:
    #  This will terminate only when any display is closed
    proc[map] = subprocess.Popen([orient], stdin=IN, stdout=OUT, stderr=subprocess.STDOUT)

if "none" in proc:
  proc["none"].wait()
  print "Creating summary and appending to file {}".format(args.summary)
  with open(outfile["none"]) as OUT, open(args.summary,"a") as S:
    # S.write("\n")
    buffer = ""
    for line in OUT:
      m = re.match(r'Map \d+: +(.*)$', line)
      if m:
        S.write(buffer+"\n")
        buffer = ""
        S.write(line)
      elif re.match(r'Minimum scalar value', line):
        m = re.search(r'(-?\d+\.\d+) +(\S+) +\w+ +(-?\d+\.\d+)', line)
        buffer +=  "  {}  {}".format(m.group(1),m.group(3))
      elif re.match(r'R.m.s. difference', line):
        m = re.search(r'(-?\d+\.\d+) +(\S+)', line)
        buffer += "  r.m.s.  {}  {}".format(m.group(1),m.group(2))
    S.write(buffer)

if not args.keep:
  for file in datafiles:
    os.remove(file)
  for map in outfile.keys():
    os.remove(outfile[map])
  os.remove(args.file)
