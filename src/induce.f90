SUBROUTINE induce(order,maxit,eind)

!  Calculate the induction energy of the system
!  and the derivatives to order `order'.

!  If switch(8)=.false., no iteration is carried out, irrespective of
!  the value of maxit. Otherwise, maxit is the maximum number of
!  induction iterations to be performed.
!  If maxit=0, only the first-order induced moments are calculated.
!  If maxit<0, there is no limit: iteration continues until convergence
!  is reached.

USE alphas
USE consts
USE derivs
USE indparams
USE induction, ONLY: v0, v1, v2, dq0, dq1, dq2, old, new, first,        &
    induction_cvg
USE interact, ONLY: pairinfo
USE molecules
USE Sfns, ONLY : evals
USE sites
USE sizes
USE switches
! USE utility, ONLY : matwrt
IMPLICIT NONE

INTEGER, INTENT(IN) :: order, maxit
DOUBLE PRECISION, INTENT(OUT) :: eind

INTEGER, SAVE :: psize
INTEGER :: i, i1, i2, ip, j, jp, k, k1, k2, m, m1, m2, n,                  &
    p, pa, pk, pk1, pk2, t1, z, z1, z2, iterations
DOUBLE PRECISION :: eindm, dqsq
LOGICAL :: converged, polarizable,iterate
DOUBLE PRECISION, PARAMETER :: half=0.5d0

!  Calculate first-order induced moments
dq0(:,:,first)=0d0
if (order .ge. 1) dq1(:,:,:,:,first)=0d0
if (order .ge. 2) dq2(:,:,:,:,:,:,first)=0d0

! print "(/a)", "Fields"
! call matwrt(v0(1:psize,1:npol), fmt="(9f9.5)", ncols=npol, label=.false.) 

!  Loop over polarizable sites to calculate induced moments
!  and their derivatives
psize = 0
do m = 1,mols
  k1 = firstp(m)
  do while (k1 > 0)
    pk1 = ps(k1)
    z1 = pz(k1)
    psize = max(psize,z1)
    k2 = firstp(m)
    do while (k2 > 0)
      pk2 = ps(k2)
      pa = ixa(k1,k2)
      if (pa > 0) then
        z2 = pz(k2)
        do i = 1,z1
          do j = 1,z2
            dq0(i,pk1,first) = dq0(i,pk1,first)                         &
                - alpha(i,j,pa)*v0(j,pk2)
            if (order >= 1) then
              do m1 = 1,mols
                do ip = 1,6
                  dq1(ip,m1,i,pk1,first) = dq1(ip,m1,i,pk1,first)       &
                      - alpha(i,j,pa)*v1(ip,m1,j,pk2)
                  if (order >= 2) then
                    do m2 = 1,mols
                      do jp = 1,6
                        dq2(ip,jp,m1,m2,i,pk1,first)                    &
                            = dq2(ip,jp,m1,m2,i,pk1,first)              &
                            - alpha(i,j,pa)*v2(ip,jp,m1,m2,j,pk2)
                      end do
                    end do
                  endif
                end do
              end do
            endif
          end do
        end do
      endif
      k2 = nextp(k2)
    end do           ! k2
    k1 = nextp(k1)
  end do             ! k1
end do             !m

if (print >= 2) then
  do m = 1,mols
    k1 = firstp(m)
    do while (k1 > 0)
      pk1 = ps(k1)
      z1 = pz(k1)
      print '(A,I0,1x,a/A,F14.8)',                                      &
          'First-order induced moments on site ', k1, trim(name(k1)), &
          'Charge        ', dq0(1,pk1,first)
      if (z1 > 1) print '(A,3F14.8)',                                   &
          'Dipole        ', (dq0(t1,pk1,first), t1=2,min(4,z1))
      if (z1 > 4) print '(A,5F14.8)',                                   &
          'Quadrupole    ', (dq0(t1,pk1,first), t1=5,min(9,z1))
      if (z1 > 9) print '(A,7F14.8)',                                   &
          'Octopole      ', (dq0(t1,pk1,first), t1=10,min(16,z1))
      k1 = nextp(k1)
    end do
  end do
endif

iterate = switch(8)
if (.not. iterate .or. maxit == 0) then
  !  Don't iterate: just use first-order moments
  new = first

else

  !  Iterate
  iterations = 0
  converged = .false.
  if (.not. switch(9)) then
    !  Start iteration afresh; otherwise start from previous iterated moments
    old = first
  endif

  do while ((iterations < maxit .or. maxit < 0) .and. .not. converged)
    iterations = iterations+1
    !  Copy first-order induced moments into new set
    if (old >= 0) then
      new = 1 - old
    else
      new = 0
    endif
    dq0(:,:,new)=dq0(:,:,first)
    if (order .ge. 1) dq1(:,:,:,:,new) = dq1(:,:,:,:,first)
    if (order .ge. 2) dq2(:,:,:,:,:,:,new) = dq2(:,:,:,:,:,:,first)

    !  Calculate contributions induced by old induced moments
    !  Loop over molecules
    n = 1
    do m1 = 1,mols-1
      do m2 = m1+1,mols
        !  Loop over polarizable sites of molecule 1
        k1 = firstp(m1)
        do while (k1 > 0)
          pk1 = ps(k1)
          !  Loop over sites of molecule 2
          k2 = firstp(m2)
          do while (k2 > 0)
            pk2 = ps(k2)
            if (recalc_sfns) n = 1

            !  Calculate geometrical factors (scalar products and vector products of
            !  axis vectors, etc.) and assemble intermediate variables and their
            !  derivatives w.r.t. translations and rotations.
            call pairinfo(m1,k1,m2,k2,order)
            if (iterations == 1 .or. recalc_sfns) then

              !  Construct index of S functions needed
              call listinds(k1,k2,n)

              !  Calculate S functions and their derivatives w.r.t. scalar products
              !  Calculate derivatives of S functions w.r.t. translations and rotations.
              call evals(order,n)
            endif

            !  Calculate the induced moments at sites pk1 and pk2.
            !  Deltaq advances n to point to the next set of functions, if any.
            call deltaq(m1,k1,m2,k2,order,n)

            k2 = nextp(k2)
          end do         ! k2
          k1 = nextp(k1)
        end do           ! k1

      end do             ! m2
    end do               ! m1
    !  if (iterations == 1) then
    !    print "(a,i0)", "S functions used for induction calculation: ", n
    !  end if

    !  Find sum of squares of changes to induced moments
    dqsq=0d0
    do p=1,npol
      do i=1,psize
        dqsq=dqsq+(dq0(i,p,new)-dq0(i,p,old))**2
      end do
    end do
    converged=(dqsq < induction_cvg)
    if (print .ge. 3) print '(A,1p,e15.6)',                            &
        'Sum of squares of changes to induced moments:    ', dqsq

    !  Update "old" pointer
    old = new
  end do  !  end of iteration loop
endif   !  end if (.not. iterate)

if (print .ge. 1 .and. iterate) then
  if (converged) then
    print '(A,I3,A)', 'Induction calculation converged after',         &
        iterations, ' iterations'
  else if (iterations >= maxit .and. maxit > 0) then
    print '(A,I3,A)', 'Induction calculation not converged after',     &
        iterations, ' iterations'
    print '(A,1p,e15.6)',                                              &
        'Sum of squares of changes to induced moments at last iteration: ', dqsq
  endif
endif

!  Calculate induction energy, and its derivatives if required
eind = 0d0
do m = 1,mols
  eindm = 0d0
  polarizable = .false.
  k1=firstp(m)
  do while (k1 > 0)
    pk1 = ps(k1)
    polarizable = .true.
    if (print >= 2 .and. iterate) then
      z1 = pz(k1)
      print '(A,I0,1x,a/A,F14.8)',                                      &
          'Iterated induced moments on site ', k1, trim(name(k1)),      &
          'Charge        ', dq0(1,pk1,new)
      if (z1 > 1) print '(A,3F14.8)',                                   &
          'Dipole        ', (dq0(t1,pk1,new), t1=2,min(4,z1))
      if (z1 > 4) print '(A,5F14.8)',                                   &
          'Quadrupole    ', (dq0(t1,pk1,new), t1=5,min(9,z1))
      if (z1 > 9) print '(A,7F14.8)',                                   &
          'Octopole      ', (dq0(t1,pk1,new), t1=10,min(16,z1))
    endif
    if (print .ge. 8) then
      print '(A/(2I2, 6F12.8))', 'Derivatives of moments',              &
          ((t1, m1, (dq1(i1,m1,t1,pk1,new), i1=1,6),                    &
          m1=1,mols), t1=1,z1)
    endif
    do t1 = 1,z1
      eindm = eindm + dq0(t1,pk1,new) * v0(t1,pk1)
    end do
    k1 = nextp(k1)
  end do
  eindm = half * eindm
  if (polarizable) then
    if (print >= 2 .and. ((.not. iterate) .or. converged .or.        &
        iterations == maxit))                                        &
        print '(a,i0,a,f15.8,1x,a)',                                   &
        'Induction energy of molecule ', m, ': ', eindm*efact, trim(eunit)
    eind = eind + eindm
  endif
end do

if (order >= 1) then
  do m = 1,mols
    k = firstp(m)
    do while (k > 0)
      pk = ps(k)
      z = pz(k)
      do t1 = 1,z
        do m1 = 1,mols
          do i1 = 1,6
            deriv1(i1,m1) = deriv1(i1,m1)                               &
                + half*(dq1(i1,m1,t1,pk,new) * v0(t1,pk)                &
                + dq0(t1,pk,new) * v1(i1,m1,t1,pk))
            if (order >= 2) then
              do m2 = 1,mols
                do i2 = 1,6
                  deriv2(i1,i2,m1,m2) = deriv2(i1,i2,m1,m2)             &
                      + half * (dq2(i1,i2,m1,m2,t1,pk,new) * v0(t1,pk)  &
                      + dq0(t1,pk,new) * v2(i1,i2,m1,m2,t1,pk)          &
                      + dq1(i1,m1,t1,pk,new) * v1(i2,m2,t1,pk)          &
                      + dq1(i2,m2,t1,pk,new) * v1(i1,m1,t1,pk))
                end do
              end do
            endif
          end do
        end do
      end do
      k = nextp(k)
    end do
  end do
endif

!     print '(A)', 'Derivatives including induction'
!     do m1=1,mols
!       print '(3F20.10)', (deriv1(i1,m1), i1=1,6)
!     end do

END SUBROUTINE induce

!-----------------------------------------------------------------------

SUBROUTINE deltaq (m1,k1,m2,k2,order,n)

!  Calculate induced moments for induction energy if required. Here we
!  just calculate the contribution to the induced moment deltaQ at k1 (k2)
!  from the induced moments at k2 (k1).

!  Note that the first-order induced moments must be calculated elsewhere,
!  outside the loop over site pairs. We assume that it has been done
!  before this routine is called.

USE parameters
USE indparams
USE alphas, ONLY: alpha, ixa, nextp
USE consts
USE damping
USE induction, ONLY: dq0, dq1, dq2, new, old
USE molecules
USE sites
USE sizes
USE switches
USE types, ONLY : indform, indampf, pairindex
USE interact, ONLY: d1, d2, d1s, d2s, s0, fac, r, pair, list, power 
USE Sfns, ONLY : t1s, t2s
USE pairderiv

IMPLICIT NONE


INTEGER, INTENT(IN) :: k1, k2, m1, m2, order
INTEGER, INTENT(INOUT) :: n

INTEGER :: ip, jp, k, ix, k1a, k2a, m, mm, n0, pa, t1, t2, t1a, &
    t2a, pk1, pk1a, pk2, pk2a, z1, z2
DOUBLE PRECISION :: vr(7), fk, aa, qq, g0, g1(12), g2(12,12),           &
    rkd, rkd1, rkd2, f0, f1, f2
LOGICAL :: newd, newk

z1 = pz(k1)
z2 = pz(k2)
pk1 = ps(k1)
pk2 = ps(k2)

vr(1) = 1d0/r
vr(2) = vr(1)**2

!  Damped or undamped r dependence for T functions
!  using Tang-Toennies or other damping functions.
t1 = type(k1)
t2 = type(k2)
if (t1 > 0 .and. t2 > 0) then
  pair = pairindex(t1,t2)
else
  pair = 0
endif

!  Loop over electrostatic interactions
k = 1
fk = 1d0
newd = .true.
newk = .true.
n0 = n
do while (n == n0 .or. list(n) > 1)
  ix = list(n)
  t1 = t1s(ix)
  t2 = t2s(ix)
  if (t1 .le. z1 .or. t2 .le. z2) then
    do while (k < power(n))
      k = k + 1
      vr(k) = vr(k-1) * vr(1)
      newk = .true.
    end do
    if (newk) then
      !  Evaluate induction damping function f0 and its derivatives f1 and f2
      fk = k
      call damp(indform(pair),indampf(0:,pair),k,alpha0,newd, r, f0,f1,f2)
      newd = .false.
      !  rkd is the damped R^-k and rkd1 and rkd2 are its derivatives w.r.t. R
      !  g0 is the damped T function and g1 and g2 are its derivatives w.r.t.
      !  external variables
      rkd = vr(k) * f0
      newk = .false.
    endif
    g0 = fac(n) * rkd * s0(n)
    if (order >= 1) then
      rkd1 = vr(k) * f1 - fk * vr(1) * rkd
      do ip = 1,12
        g1(ip) = fac(n) * (rkd * d1s(ip,n) + s0(n) * rkd1 * d1(16,ip))
        if (order >= 2) then
          rkd2 = vr(k)*(f2-2d0*fk*f1*vr(1)) + fk*(fk+1d0)*vr(2)*rkd
          do jp = 1,12
            g2(ip,jp) = fac(n)*(rkd*d2s(ip,jp,n)                        &
                + rkd1*(d1s(ip,n)*d1(16,jp) + d1s(jp,n)*d1(16,ip)       &
                + s0(n)*d2(16,ip,jp))                                   &
                + rkd2*s0(n)*d1(16,ip)*d1(16,jp))
          end do
        endif
      end do
    endif
    !         if (debug(11)) then
    !           ix = list(n)
    !           print '(2A4, 2X, 2A4, I3, F15.8)', name(pp(pk1)), label(t1),
    !    &        name(pp(pk2)), label(t2), js(ix), g0
    !         endif

    !  Moments induced in the sites of molecule m1 by fields at k1. Here
    !  we calculate only the contribution from the induced moments at k2,
    !  not the static moments.
    if (t1 .le. z1) then
      k1a = firstp(m1)
      do while (k1a > 0)
        pk1a = ps(k1a)
        pa = ixa(k1,k1a)
        if (pa > 0) then
          do t1a = 1,pz(k1a)
            aa = alpha(t1,t1a,pa)
            if (aa .ne. 0d0) then
              qq = dq0(t2,pk2,old)*aa
              dq0(t1a,pk1a,new) = dq0(t1a,pk1a,new) - qq*g0
              if (order >= 1) then
                do ip = 1,6
                  dq1(ip,m1,t1a,pk1a,new)                               &
                      = dq1(ip,m1,t1a,pk1a,new) - qq*g1(ip)
                  dq1(ip,m2,t1a,pk1a,new)                               &
                      = dq1(ip,m2,t1a,pk1a,new) - qq*g1(ip+6)
                  !  Extra terms involving derivatives of induced moments at k2
                  do m = 1,mols
                    dq1(ip,m,t1a,pk1a,new) = dq1(ip,m,t1a,pk1a,new)     &
                        - aa*g0*dq1(ip,m,t2,pk2,old)
                  end do
                  if (order >= 2) then
                    do jp = 1,6
                      dq2(ip,jp,m1,m1,t1a,pk1a,new)                     &
                          = dq2(ip,jp,m1,m1,t1a,pk1a,new) - qq*g2(ip,jp)
                      dq2(ip,jp,m1,m2,t1a,pk1a,new)                     &
                          = dq2(ip,jp,m1,m2,t1a,pk1a,new) - qq*g2(ip,jp+6)
                      dq2(ip,jp,m2,m1,t1a,pk1a,new)                     &
                          = dq2(ip,jp,m2,m1,t1a,pk1a,new) - qq*g2(ip+6,jp)
                      dq2(ip,jp,m2,m2,t1a,pk1a,new)                     &
                          = dq2(ip,jp,m2,m2,t1a,pk1a,new) - qq*g2(ip+6,jp+6)

                      !  Extra terms involving derivatives of induced moments at k2
                      do m = 1,mols
                        !  ip belongs to m1 or m2, jp belongs to any m
                        dq2(ip,jp,m1,m,t1a,pk1a,new)                    &
                            = dq2(ip,jp,m1,m,t1a,pk1a,new)              &
                            - aa*g1(ip)*dq1(jp,m,t2,pk2,old)
                        dq2(ip,jp,m2,m,t1a,pk1a,new)                    &
                            = dq2(ip,jp,m2,m,t1a,pk1a,new)              &
                            - aa*g1(ip+6)*dq1(jp,m,t2,pk2,old)
                        !  jp belongs to m1 or m2, ip belongs to any m
                        dq2(ip,jp,m,m1,t1a,pk1a,new)                    &
                            = dq2(ip,jp,m,m1,t1a,pk1a,new)              &
                            - aa*g1(jp)*dq1(ip,m,t2,pk2,old)
                        dq2(ip,jp,m,m2,t1a,pk1a,new)                    &
                            = dq2(ip,jp,m,m2,t1a,pk1a,new)              &
                            - aa*g1(jp+6)*dq1(ip,m,t2,pk2,old)
                        do mm = 1,mols
                          dq2(ip,jp,m,mm,t1a,pk1a,new)                  &
                              = dq2(ip,jp,m,mm,t1a,pk1a,new)            &
                              - aa*g0*dq2(ip,jp,m,mm,t2,pk2,old)
                        end do
                      end do
                    end do
                  endif    ! order >= 2
                end do
              endif        ! order >= 1
            endif
          end do
        endif
        k1a = nextp(k1a)
      end do
    endif

    !  Moments induced in the sites of molecule m2 by fields at k2. Again
    !  we calculate only the contribution from the induced moments at k1,
    !  not the static moments.
    if (t2 .le. z2) then
      k2a = firstp(m2)
      do while (k2a .gt. 0)
        pk2a = ps(k2a)
        pa = ixa(k2,k2a)
        if (pa > 0) then
          do t2a = 1,pz(k2a)
            aa = alpha(t2,t2a,pa)
            if (aa .ne. 0d0) then
              qq = dq0(t1,pk1,old)*aa
              dq0(t2a,pk2a,new)=dq0(t2a,pk2a,new) - qq*g0
              if (order >= 1) then
                do ip = 1,6
                  dq1(ip,m1,t2a,pk2a,new)                               &
                      = dq1(ip,m1,t2a,pk2a,new) - qq*g1(ip)
                  dq1(ip,m2,t2a,pk2a,new)                               &
                      = dq1(ip,m2,t2a,pk2a,new) - qq*g1(ip+6)

                  !  Extra terms involving derivatives of induced moments at k1
                  do m = 1,mols
                    dq1(ip,m,t2a,pk2a,new) = dq1(ip,m,t2a,pk2a,new)     &
                        - aa*g0*dq1(ip,m,t1,pk1,old)
                  end do
                  if (order >= 2) then
                    do jp = 1,6
                      dq2(ip,jp,m1,m1,t2a,pk2a,new)                     &
                          = dq2(ip,jp,m1,m1,t2a,pk2a,new) - qq*g2(ip,jp)

                      dq2(ip,jp,m1,m2,t2a,pk2a,new)                     &
                          = dq2(ip,jp,m1,m2,t2a,pk2a,new) - qq*g2(ip,jp+6)

                      dq2(ip,jp,m2,m1,t2a,pk2a,new)                     &
                          = dq2(ip,jp,m2,m1,t2a,pk2a,new) - qq*g2(ip+6,jp)

                      dq2(ip,jp,m2,m2,t2a,pk2a,new)                     &
                          = dq2(ip,jp,m2,m2,t2a,pk2a,new) - qq*g2(ip+6,jp+6)

                      !  Extra terms involving derivatives of induced moments at k1
                      do m = 1,mols
                        !  ip belongs to m1 or m2, jp belongs to any m
                        dq2(ip,jp,m1,m,t2a,pk2a,new)                    &
                            = dq2(ip,jp,m1,m,t2a,pk2a,new)              &
                            - aa*g1(ip)*dq1(jp,m,t1,pk1,old)
                        dq2(ip,jp,m2,m,t2a,pk2a,new)                    &
                            = dq2(ip,jp,m2,m,t2a,pk2a,new)              &
                            - aa*g1(ip+6)*dq1(jp,m,t1,pk1,old)
                        !  jp belongs to m1 or m2, ip belongs to any m
                        dq2(ip,jp,m,m1,t2a,pk2a,new)                    &
                            = dq2(ip,jp,m,m1,t2a,pk2a,new)              &
                            - aa*g1(jp)*dq1(ip,m,t1,pk1,old)
                        dq2(ip,jp,m,m2,t2a,pk2a,new)                    &
                            = dq2(ip,jp,m,m2,t2a,pk2a,new)              &
                            - aa*g1(jp+6)*dq1(ip,m,t1,pk1,old)
                        do mm = 1,mols
                          dq2(ip,jp,m,mm,t2a,pk2a,new)                  &
                              = dq2(ip,jp,m,mm,t2a,pk2a,new)            &
                              - aa*g0*dq2(ip,jp,m,mm,t1,pk1,old)
                        end do
                      end do
                    end do
                  endif         !  order >= 2
                end do
              endif             !  order >= 1
            endif
          end do
        endif
        k2a = nextp(k2a)
      end do
    endif
  endif
  n = n + 1
end do    ! end of loop over electrostatic functions

END SUBROUTINE deltaq

