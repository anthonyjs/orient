      SUBROUTINE lattinfo(m1,k1,k2,order,ka,kb,kc,          &
          mxlatt,mylatt,mzlatt,start)

      USE global
      USE interact
      !  headx points to the first lattice site.
      !  (a,b,c)vec are the lattice cell vectors.
      !  (d,e,f)vec are the cell vectors (a,b,c) for the nanocrystal.
      USE lattice, ONLY: headx, avec, bvec, cvec, dvec, evec, fvec
      USE molecules
      USE sites
      IMPLICIT NONE

!  Calculate the scalar products of the local axis vectors and the
!  inter-site vector (magnitude R and unit vector ER) for a pair of
!  sites, one in molecule m1 and the other in the lattice.
!  If order>0, the vector products are also calculated for use in the
!  derivative formulae.

!  Input:
!  m1, k1: The molecule being considered and the site pointer within it.
!  k2:     Site pointer for lattice.
!  start:  First site of lattice (optional; default is headx, which
!          points to the first lattice site)
!  order:  Order of derivatives needed.
!  ka,kb,kc: displacement of lattice site from reference unit
!          cell, as multiples of cell vectors.
!  mxlatt,mylatt,mzlatt: displacement of nanocrystal site from reference
!          cell.
      INTEGER, INTENT(IN) :: k1, k2, m1, order, ka, kb, kc, &
          mxlatt, mylatt, mzlatt
      INTEGER, INTENT(IN), OPTIONAL :: start

!  Output:
!  The following arrays are stored in the common block "interact".
!  E1R(3)    E1R(i) is the scalar product of the unit vector e_i
!  for site k1 with the unit inter-site vector ER
!  (components of ER in the axis system of site k1)
!  E2R(3)    Scalar product of the unit vector e_i for site k2
!  with the unit inter-site vector -ER (components of -ER in
!  the axis system of site k2). NOTE SIGN!
!  xx(3,3)   Scalar products of unit vectors for site k1 and site k2
!  R         distance between sites
!  The above comprise the 16 intermediate variables q(i). Note that they
!  involve the unit intermolecular vector in this implementation.
!  D1(16,12) First derivatives of the 16 intermediate variables w.r.t. the
!  molecular position and torque variables
!  D2(16,12,12) Second derivatives.

      DOUBLE PRECISION ::                                               &
          rab(3),            & ! site-site vector R
          er(3),             & ! normalised site-site vector
          a(3), & ! b(3),    & ! site vectors relative to molecular origin (c. of m.)
          ra(3), rb(3),      & ! R+a and R-b respectively
          rxa(3), & !        & ! rxb(3),    & ! R x a and R x b
          raxw1(3,3),        & ! (R+a) x w1     Note: w1(i,j) = se(i,j,p1)
          rbxw2(3,3),        & ! (R-b) x w2           w2(i,j) = se(i,j,p2)
          w2xa(3,3),         & ! w2 x a& ! w1xb(3,3),     & ! w1 x b
          w1xw2(3,3,3),      & ! w1 x w2
          raa,               & ! (R+a) . a,
          aw1(3), & !        & ! bw1(3),    & ! a . w1, b . w1
          aw2(3), & !        & ! bw2(3),    & ! a . w2, b . w2
          w1ra(3), w2rb(3)     ! w1 . (R+a), w1 . (R-b)
          ! ab, raa, rbb,  & ! a . b, (R+a) . a, (R-b) . b
!     DOUBLE PRECISION :: d11j,d12j,d13j,d14j,d15j,d16j,d116j,d116i,d216ij
!  In the vector products, the first index labels the vector component.
!  w1xw2(i,j1,j2) is the cross product of local unit vector w1(j1)
!  for site k1 with local unit vector w2(j2) for site k2.

      INTEGER :: i, j, j1, j2, k, m, p1, p2,                           &
          i1(3)=(/2,3,1/), i2(3)=(/3,1,2/)
      INTEGER :: ix,iy
      DOUBLE PRECISION :: x,xtmp,y,ytmp,dvec1,dvec2,evec1,evec2,fvec1,fvec2
      DOUBLE PRECISION :: xx(3,3)
      DOUBLE PRECISION :: tmpx,tmpy

      p1=head(m1)
      if (present(start)) then
        p2=start
      else
        p2=headx
      endif

      if (nanocrystal) then
        dvec1=dvec(1)*se(1,1,p1)+dvec(2)*se(1,2,p1)
        dvec2=-dvec(1)*se(2,1,p1)+dvec(2)*se(2,2,p1)
        evec1=evec(1)*se(1,1,p1)+evec(2)*se(1,2,p1)
        evec2=-evec(1)*se(2,1,p1)+evec(2)*se(2,2,p1)
        fvec1=fvec(1)*se(1,1,p1)+fvec(2)*se(1,2,p1)
        fvec2=-fvec(1)*se(2,1,p1)+fvec(2)*se(2,2,p1)
        xtmp=sx(1,p1)-sx(1,p2)+mxlatt*dvec1+mylatt*evec1+mzlatt*fvec1
        ytmp=sx(2,p1)-sx(2,p2)+mxlatt*dvec2+mylatt*evec2+mzlatt*fvec2
        if (dabs(bvec(1)).gt.0.0001d0) then
          y=(ytmp-xtmp*avec(1))/bvec(1)
          if (dabs(bvec(2)).gt.0.0001d0) then
            x=(bvec(2)*xtmp-bvec(1)*ytmp)/                      &
                (avec(1)*bvec(2)-avec(2)*bvec(1))
          else
            x=xtmp/avec(2)
          endif
        else
          x=xtmp/avec(1)
          y=(ytmp-x*avec(2))/bvec(2)
        endif
        ix=nint(x)
        iy=nint(y)
        tmpx=avec(1)*dble(ka+ix)                               &
            +bvec(1)*dble(kb+iy)
        tmpy=avec(2)*dble(ka+ix)                               &
            +bvec(2)*dble(kb+iy)
        rab(1)=sx(1,k2)-sx(1,k1)+tmpx+kc*cvec(1)                        &
            -mzlatt*fvec1-mxlatt*dvec1-mylatt*evec1
        rab(2)=sx(2,k2)-sx(2,k1)+tmpy+kc*cvec(2)                        &
            -mzlatt*fvec2-mxlatt*dvec2-mylatt*evec2
        rab(3)=sx(3,k2)-sx(3,k1)+kc*cvec(3)-mzlatt*fvec(3)
      else
        rab(:)=sx(:,k2)-sx(:,k1)+kc*cvec(:)
        rab(1:2)=rab(1:2)+ka*avec(:)+kb*bvec(:)
      endif
      r=0d0
      do i=1,3
        r=r+rab(i)**2
      end do
      r=sqrt(r)

      !  Pointers to orientation matrices
      p1=sm(k1)
      p2=sm(k2)
      do j=1,3
        do i=1,3
          xx(i,j)=se(1,i,p1)*se(1,j,p2)                                 &
              +se(2,i,p1)*se(2,j,p2)                                    &
              +se(3,i,p1)*se(3,j,p2)
        end do
      end do
      cosine(7:15)=reshape(xx,(/9/))
!     cosine(7:15)=reshape(matmul(transpose(se(:,:,p1)),se(:,:,p2)),(/9/))

!       write(*,*)'sk1 ',k1,sx(1,k1),sx(2,k1),sx(3,k1)
!       write(*,*)'sk2 ',k2,sx(1,k2),sx(2,k2),sx(3,k2), r
!      write(*,*)'o',r,rab(1),rab(2),rab(3),tmpx,tmpy,dble(kc)*cvec(3)
!  .....removed overhead
!  if (r .lt. 0.1d0) then  ! sites are too close : error report
!  WRITE (6,'(/1X,2(A,I3,A,I2),A,1P,E12.5,A)')
!  &     'Sites', K1, ' in molecule', M1, ' and', K2, ' in molecule',
!  &        M2, ' are only', r, ' bohr apart.'
!  c       CALL AXES(M1,.TRUE.)
!  c       CALL AXES(M2,.TRUE.)
!  WRITE (6,'(/1X,A)') 'TASK ABANDONED.'
!  STOP
!  ENDIF

      do i=1,3
        er(i)=rab(i)/r   ! normalized intersite vector
      end do

      do i=1,3
        e1r(i)=0d0
        e2r(i)=0d0
        do j=1,3
          e1r(i)=e1r(i)+er(j)*se(j,i,p1)
          e2r(i)=e2r(i)-er(j)*se(j,i,p2)  ! Note sign
        end do
      end do

!  If order=0 this is all we need
      if (order .lt. 1) return


!  Site k2 is in the lattice, so forces and torques on it are not needed.
!  Consequently we can take the origin at the site rather than at the
!  centre of mass.
!  b is left here in case later formulae inadvertently include it.
      a(:)=sx(:,k1)-sx(:,cm(m1))     !  site vectors wrt GLOBAL AXES
!     b(:)=0d0                       !  relative to molecular origin
      ra(:) = rab(:) + a(:)
      rb(:) = rab(:)

!  Calculate cross products needed in first derivatives
      rxa(1) = rab(2)*a(3) - rab(3)*a(2)
      rxa(2) = rab(3)*a(1) - rab(1)*a(3)
      rxa(3) = rab(1)*a(2) - rab(2)*a(1)

!     rxb(:) = 0d0

      do j1=1,3
        raxw1(1,j1) = ra(2)*se(3,j1,p1) - ra(3)*se(2,j1,p1)
        raxw1(2,j1) = ra(3)*se(1,j1,p1) - ra(1)*se(3,j1,p1)
        raxw1(3,j1) = ra(1)*se(2,j1,p1) - ra(2)*se(1,j1,p1)

        rbxw2(1,j1) = rb(2)*se(3,j1,p2) - rb(3)*se(2,j1,p2)
        rbxw2(2,j1) = rb(3)*se(1,j1,p2) - rb(1)*se(3,j1,p2)
        rbxw2(3,j1) = rb(1)*se(2,j1,p2) - rb(2)*se(1,j1,p2)

        w2xa(1,j1)  = se(2,j1,p2)*a(3) - se(3,j1,p2)*a(2)
        w2xa(2,j1)  = se(3,j1,p2)*a(1) - se(1,j1,p2)*a(3)
        w2xa(3,j1)  = se(1,j1,p2)*a(2) - se(2,j1,p2)*a(1)

        do j2=1,3   !  nine cross products for w1xw2
          w1xw2(1,j1,j2)=se(2,j1,p1)*se(3,j2,p2)-se(3,j1,p1)*se(2,j2,p2)
          w1xw2(2,j1,j2)=se(3,j1,p1)*se(1,j2,p2)-se(1,j1,p1)*se(3,j2,p2)
          w1xw2(3,j1,j2)=se(1,j1,p1)*se(2,j2,p2)-se(2,j1,p1)*se(1,j2,p2)
        end do
      end do

!     w1xb(:,:)  = 0d0

!  Now compute first derivatives of |R|, w1.R, w2.R and w1.w2. The scalar
!  products involve the vector R, not the unit vector er.
      do i=1,3
!  R
        D1(16,i)=-rab(i)/R
        D1(16,3+i)=rxa(i)/R
        D1(16,6+i)=rab(i)/R
        D1(16,9+i)=0d0
        do j=1,3
!  w1.R
          D1(j,i)=-se(i,j,p1)
          D1(j,3+i)=-raxw1(i,j)
          D1(j,6+i)=se(i,j,p1)
          D1(j,9+i)=0d0
!  w2.R
          D1(3+j,i)=se(i,j,p2)
          D1(3+j,3+i)=-w2xa(i,j)
          D1(3+j,6+i)=-se(i,j,p2)
          D1(3+j,9+i)=rbxw2(i,j)
          do j1=1,3
!  w1.w2
            D1(3+j1+3*j,i)=0d0
            D1(3+j1+3*j,3+i)=w1xw2(i,j1,j)
            D1(3+j1+3*j,6+i)=0d0
            D1(3+j1+3*j,9+i)=-w1xw2(i,j1,j)
          end do
        end do
      end do

!  Now we have to manipulate these to get derivatives of the unit-vector
!  scalar products.
!  Not optimised
!     do j=1,3
!       do i=1,12
!         D1(j,i)=(D1(j,i)-e1r(j)*D1(16,i))/R
!         D1(3+j,i)=(D1(3+j,i)-e2r(j)*D1(16,i))/R
!       end do
!     end do
!  Optimised
      do i=1,12
        D1(1,i)=(D1(1,i)-e1r(1)*D1(16,i))/R
        D1(2,i)=(D1(2,i)-e1r(2)*D1(16,i))/R
        D1(3,i)=(D1(3,i)-e1r(3)*D1(16,i))/R
        D1(4,i)=(D1(4,i)-e2r(1)*D1(16,i))/R
        D1(5,i)=(D1(5,i)-e2r(2)*D1(16,i))/R
        D1(6,i)=(D1(6,i)-e2r(3)*D1(16,i))/R
      end do
!  End optimised

      if (order .lt. 2) return

!  Scalar products occurring in second derivatives
      aw1(:)=0.d0
      aw2(:)=0.d0
!     bw1(:)=0.d0
!     bw2(:)=0.d0
      w1ra(:)=0.d0
      w2rb(:)=0.d0

!     ab=0d0
      raa=ra(1)*a(1)+ra(2)*a(2)+ra(3)*a(3)
!     rbb=0d0
      do i=1,3
        do j=1,3
          aw1(i)=aw1(i)+se(j,i,p1)*a(j)
          aw2(i)=aw2(i)+se(j,i,p2)*a(j)
          w1ra(i)=w1ra(i)+se(j,i,p1)*ra(j)
          w2rb(i)=w2rb(i)+se(j,i,p2)*rb(j)
        end do
      end do

!  Second derivatives. Clear matrix.
      d2=0d0

!  Second derivatives of R.R (factor of 2 omitted from all of these)
      do i=1,3
        D2(16,i,i)=1d0
        D2(16,6+i,i)=-1d0
        D2(16,i,6+i)=-1d0
        D2(16,6+i,6+i)=1d0
        do j=1,3
          D2(16,3+i,3+j)=-ra(i)*a(j)
        end do
        D2(16,3+i,3+i)=D2(16,3+i,3+i)+raa
        j=i1(i)
        k=i2(i)
        D2(16,j,3+k)=a(i)
        D2(16,k,3+j)=-a(i)
        D2(16,3+j,k)=-a(i)
        D2(16,3+k,j)=a(i)
        D2(16,3+j,6+k)=a(i)
        D2(16,3+k,6+j)=-a(i)
        D2(16,6+j,3+k)=-a(i)
        D2(16,6+k,3+j)=a(i)
      end do
!  Construct second derivatives of |R| from these
      do i=1,12
        do j=1,12
          D2(16,i,j)=(D2(16,i,j)-D1(16,i)*D1(16,j))/R
        end do
      end do

!  Second derivatives of w1.R and w2.R. Again, the scalar
!  products initially involve the vector R, not the unit vector er.
      do k=1,3
        do i=1,3
          j1=i1(i)
          j2=i2(i)
!  w1(k).R
          do j=1,3
            D2(k,3+i,3+j)=ra(i)*se(j,k,p1)
          end do
          D2(k,3+i,3+i)=D2(k,3+i,3+i)-w1ra(k)
          D2(k,j1,3+j2)=-se(i,k,p1)
          D2(k,j2,3+j1)=se(i,k,p1)
          D2(k,3+j1,j2)=se(i,k,p1)
          D2(k,3+j2,j1)=-se(i,k,p1)
          D2(k,3+j1,6+j2)=-se(i,k,p1)
          D2(k,3+j2,6+j1)=se(i,k,p1)
          D2(k,6+j1,3+j2)=se(i,k,p1)
          D2(k,6+j2,3+j1)=-se(i,k,p1)
!  w2(k).R
          do j=1,3
            D2(3+k,3+i,3+j)=se(i,k,p2)*a(j)
            D2(3+k,3+i,9+j)=-se(i,k,p2)*a(j)
            D2(3+k,9+i,3+j)=-a(i)*se(j,k,p2)
            D2(3+k,9+i,9+j)=-rb(i)*se(j,k,p2)
          end do
          D2(3+k,3+i,3+i)=D2(3+k,3+i,3+i)-aw2(k)
          D2(3+k,3+i,9+i)=D2(3+k,3+i,9+i)+aw2(k)
          D2(3+k,9+i,3+i)=D2(3+k,9+i,3+i)+aw2(k)
          D2(3+k,9+i,9+i)=D2(3+k,9+i,9+i)+w2rb(k)
          D2(3+k,j1,9+j2)=se(i,k,p2)
          D2(3+k,j2,9+j1)=-se(i,k,p2)
          D2(3+k,6+j1,9+j2)=-se(i,k,p2)
          D2(3+k,6+j2,9+j1)=se(i,k,p2)
          D2(3+k,9+j1,j2)=-se(i,k,p2)
          D2(3+k,9+j2,j1)=se(i,k,p2)
          D2(3+k,9+j1,6+j2)=se(i,k,p2)
          D2(3+k,9+j2,6+j1)=-se(i,k,p2)
!  w1(k).w2(m)
          do m=1,3
            do j=1,3
              D2(3+k+3*m,3+i,3+j)=se(j,k,p1)*se(i,m,p2)
              D2(3+k+3*m,3+i,9+j)=-se(j,k,p1)*se(i,m,p2)
              D2(3+k+3*m,9+i,3+j)=-se(i,k,p1)*se(j,m,p2)
              D2(3+k+3*m,9+i,9+j)=se(i,k,p1)*se(j,m,p2)
            end do
            D2(3+k+3*m,3+i,3+i)=D2(3+k+3*m,3+i,3+i)-xx(k,m)
            D2(3+k+3*m,3+i,9+i)=D2(3+k+3*m,3+i,9+i)+xx(k,m)
            D2(3+k+3*m,9+i,3+i)=D2(3+k+3*m,9+i,3+i)+xx(k,m)
            D2(3+k+3*m,9+i,9+i)=D2(3+k+3*m,9+i,9+i)-xx(k,m)
          end do ! m
        end do ! i
      end do ! k

!  Convert derivatives of w1.R and w2.R into derivatives of w1.R/R and w2.R/R
!  Not optimised
!     do k=1,3
!       do i=1,12
!         do j=1,12
!           D2(k,i,j)=(D2(k,i,j)-e1r(k)*D2(16,i,j)
!    &                  -D1(16,i)*D1(k,j)-D1(16,j)*D1(k,i))/R
!           D2(3+k,i,j)=(D2(3+k,i,j)-e2r(k)*D2(16,i,j)
!    &                  -D1(16,i)*D1(3+k,j)-D1(16,j)*D1(3+k,i))/R
!         end do ! j
!       end do ! i
!     end do ! k
!  Optimised
      do j=1,12
            D2(1,1,j)=(D2(1,1,j)-e1r(1)*D2(16,1,j)                       &
                        -D1(16,1)*D1(1,j)-D1(16,j)*D1(1,1))/R
            D2(2,1,j)=(D2(2,1,j)-e1r(2)*D2(16,1,j)                       &
                        -D1(16,1)*D1(2,j)-D1(16,j)*D1(2,1))/R
            D2(3,1,j)=(D2(3,1,j)-e1r(3)*D2(16,1,j)                       &
                        -D1(16,1)*D1(3,j)-D1(16,j)*D1(3,1))/R
            D2(4,1,j)=(D2(4,1,j)-e2r(1)*D2(16,1,j)                       &
                        -D1(16,1)*D1(4,j)-D1(16,j)*D1(4,1))/R
            D2(5,1,j)=(D2(5,1,j)-e2r(2)*D2(16,1,j)                       &
                        -D1(16,1)*D1(5,j)-D1(16,j)*D1(5,1))/R
            D2(6,1,j)=(D2(6,1,j)-e2r(3)*D2(16,1,j)                       &
                        -D1(16,1)*D1(6,j)-D1(16,j)*D1(6,1))/R

            D2(1,2,j)=(D2(1,2,j)-e1r(1)*D2(16,2,j)                       &
                        -D1(16,2)*D1(1,j)-D1(16,j)*D1(1,2))/R
            D2(2,2,j)=(D2(2,2,j)-e1r(2)*D2(16,2,j)                       &
                        -D1(16,2)*D1(2,j)-D1(16,j)*D1(2,2))/R
            D2(3,2,j)=(D2(3,2,j)-e1r(3)*D2(16,2,j)                       &
                        -D1(16,2)*D1(3,j)-D1(16,j)*D1(3,2))/R
            D2(4,2,j)=(D2(4,2,j)-e2r(1)*D2(16,2,j)                       &
                        -D1(16,2)*D1(4,j)-D1(16,j)*D1(4,2))/R
            D2(5,2,j)=(D2(5,2,j)-e2r(2)*D2(16,2,j)                       &
                        -D1(16,2)*D1(5,j)-D1(16,j)*D1(5,2))/R
            D2(6,2,j)=(D2(6,2,j)-e2r(3)*D2(16,2,j)                       &
                        -D1(16,2)*D1(6,j)-D1(16,j)*D1(6,2))/R

            D2(1,3,j)=(D2(1,3,j)-e1r(1)*D2(16,3,j)                       &
                        -D1(16,3)*D1(1,j)-D1(16,j)*D1(1,3))/R
            D2(2,3,j)=(D2(2,3,j)-e1r(2)*D2(16,3,j)                       &
                        -D1(16,3)*D1(2,j)-D1(16,j)*D1(2,3))/R
            D2(3,3,j)=(D2(3,3,j)-e1r(3)*D2(16,3,j)                       &
                        -D1(16,3)*D1(3,j)-D1(16,j)*D1(3,3))/R
            D2(4,3,j)=(D2(4,3,j)-e2r(1)*D2(16,3,j)                       &
                        -D1(16,3)*D1(4,j)-D1(16,j)*D1(4,3))/R
            D2(5,3,j)=(D2(5,3,j)-e2r(2)*D2(16,3,j)                       &
                        -D1(16,3)*D1(5,j)-D1(16,j)*D1(5,3))/R
            D2(6,3,j)=(D2(6,3,j)-e2r(3)*D2(16,3,j)                       &
                        -D1(16,3)*D1(6,j)-D1(16,j)*D1(6,3))/R

            D2(1,4,j)=(D2(1,4,j)-e1r(1)*D2(16,4,j)                       &
                        -D1(16,4)*D1(1,j)-D1(16,j)*D1(1,4))/R
            D2(2,4,j)=(D2(2,4,j)-e1r(2)*D2(16,4,j)                       &
                        -D1(16,4)*D1(2,j)-D1(16,j)*D1(2,4))/R
            D2(3,4,j)=(D2(3,4,j)-e1r(3)*D2(16,4,j)                       &
                        -D1(16,4)*D1(3,j)-D1(16,j)*D1(3,4))/R
            D2(4,4,j)=(D2(4,4,j)-e2r(1)*D2(16,4,j)                       &
                        -D1(16,4)*D1(4,j)-D1(16,j)*D1(4,4))/R
            D2(5,4,j)=(D2(5,4,j)-e2r(2)*D2(16,4,j)                       &
                        -D1(16,4)*D1(5,j)-D1(16,j)*D1(5,4))/R
            D2(6,4,j)=(D2(6,4,j)-e2r(3)*D2(16,4,j)                       &
                        -D1(16,4)*D1(6,j)-D1(16,j)*D1(6,4))/R

            D2(1,5,j)=(D2(1,5,j)-e1r(1)*D2(16,5,j)                       &
                        -D1(16,5)*D1(1,j)-D1(16,j)*D1(1,5))/R
            D2(2,5,j)=(D2(2,5,j)-e1r(2)*D2(16,5,j)                       &
                        -D1(16,5)*D1(2,j)-D1(16,j)*D1(2,5))/R
            D2(3,5,j)=(D2(3,5,j)-e1r(3)*D2(16,5,j)                       &
                        -D1(16,5)*D1(3,j)-D1(16,j)*D1(3,5))/R
            D2(4,5,j)=(D2(4,5,j)-e2r(1)*D2(16,5,j)                       &
                        -D1(16,5)*D1(4,j)-D1(16,j)*D1(4,5))/R
            D2(5,5,j)=(D2(5,5,j)-e2r(2)*D2(16,5,j)                       &
                        -D1(16,5)*D1(5,j)-D1(16,j)*D1(5,5))/R
            D2(6,5,j)=(D2(6,5,j)-e2r(3)*D2(16,5,j)                       &
                        -D1(16,5)*D1(6,j)-D1(16,j)*D1(6,5))/R

            D2(1,6,j)=(D2(1,6,j)-e1r(1)*D2(16,6,j)                       &
                        -D1(16,6)*D1(1,j)-D1(16,j)*D1(1,6))/R
            D2(2,6,j)=(D2(2,6,j)-e1r(2)*D2(16,6,j)                       &
                        -D1(16,6)*D1(2,j)-D1(16,j)*D1(2,6))/R
            D2(3,6,j)=(D2(3,6,j)-e1r(3)*D2(16,6,j)                       &
                        -D1(16,6)*D1(3,j)-D1(16,j)*D1(3,6))/R
            D2(4,6,j)=(D2(4,6,j)-e2r(1)*D2(16,6,j)                       &
                        -D1(16,6)*D1(4,j)-D1(16,j)*D1(4,6))/R
            D2(5,6,j)=(D2(5,6,j)-e2r(2)*D2(16,6,j)                       &
                        -D1(16,6)*D1(5,j)-D1(16,j)*D1(5,6))/R
            D2(6,6,j)=(D2(6,6,j)-e2r(3)*D2(16,6,j)                       &
                        -D1(16,6)*D1(6,j)-D1(16,j)*D1(6,6))/R

            D2(1,7,j)=(D2(1,7,j)-e1r(1)*D2(16,7,j)                       &
                        -D1(16,7)*D1(1,j)-D1(16,j)*D1(1,7))/R
            D2(2,7,j)=(D2(2,7,j)-e1r(2)*D2(16,7,j)                       &
                        -D1(16,7)*D1(2,j)-D1(16,j)*D1(2,7))/R
            D2(3,7,j)=(D2(3,7,j)-e1r(3)*D2(16,7,j)                       &
                        -D1(16,7)*D1(3,j)-D1(16,j)*D1(3,7))/R
            D2(4,7,j)=(D2(4,7,j)-e2r(1)*D2(16,7,j)                       &
                        -D1(16,7)*D1(4,j)-D1(16,j)*D1(4,7))/R
            D2(5,7,j)=(D2(5,7,j)-e2r(2)*D2(16,7,j)                       &
                        -D1(16,7)*D1(5,j)-D1(16,j)*D1(5,7))/R
            D2(6,7,j)=(D2(6,7,j)-e2r(3)*D2(16,7,j)                       &
                        -D1(16,7)*D1(6,j)-D1(16,j)*D1(6,7))/R

            D2(1,8,j)=(D2(1,8,j)-e1r(1)*D2(16,8,j)                       &
                        -D1(16,8)*D1(1,j)-D1(16,j)*D1(1,8))/R
            D2(2,8,j)=(D2(2,8,j)-e1r(2)*D2(16,8,j)                       &
                        -D1(16,8)*D1(2,j)-D1(16,j)*D1(2,8))/R
            D2(3,8,j)=(D2(3,8,j)-e1r(3)*D2(16,8,j)                       &
                        -D1(16,8)*D1(3,j)-D1(16,j)*D1(3,8))/R
            D2(4,8,j)=(D2(4,8,j)-e2r(1)*D2(16,8,j)                       &
                        -D1(16,8)*D1(4,j)-D1(16,j)*D1(4,8))/R
            D2(5,8,j)=(D2(5,8,j)-e2r(2)*D2(16,8,j)                       &
                        -D1(16,8)*D1(5,j)-D1(16,j)*D1(5,8))/R
            D2(6,8,j)=(D2(6,8,j)-e2r(3)*D2(16,8,j)                       &
                        -D1(16,8)*D1(6,j)-D1(16,j)*D1(6,8))/R

            D2(1,9,j)=(D2(1,9,j)-e1r(1)*D2(16,9,j)                       &
                        -D1(16,9)*D1(1,j)-D1(16,j)*D1(1,9))/R
            D2(2,9,j)=(D2(2,9,j)-e1r(2)*D2(16,9,j)                       &
                        -D1(16,9)*D1(2,j)-D1(16,j)*D1(2,9))/R
            D2(3,9,j)=(D2(3,9,j)-e1r(3)*D2(16,9,j)                       &
                        -D1(16,9)*D1(3,j)-D1(16,j)*D1(3,9))/R
            D2(4,9,j)=(D2(4,9,j)-e2r(1)*D2(16,9,j)                       &
                        -D1(16,9)*D1(4,j)-D1(16,j)*D1(4,9))/R
            D2(5,9,j)=(D2(5,9,j)-e2r(2)*D2(16,9,j)                       &
                        -D1(16,9)*D1(5,j)-D1(16,j)*D1(5,9))/R
            D2(6,9,j)=(D2(6,9,j)-e2r(3)*D2(16,9,j)                       &
                        -D1(16,9)*D1(6,j)-D1(16,j)*D1(6,9))/R

            D2(1,10,j)=(D2(1,10,j)-e1r(1)*D2(16,10,j)                    &
                        -D1(16,10)*D1(1,j)-D1(16,j)*D1(1,10))/R
            D2(2,10,j)=(D2(2,10,j)-e1r(2)*D2(16,10,j)                    &
                        -D1(16,10)*D1(2,j)-D1(16,j)*D1(2,10))/R
            D2(3,10,j)=(D2(3,10,j)-e1r(3)*D2(16,10,j)                    &
                        -D1(16,10)*D1(3,j)-D1(16,j)*D1(3,10))/R
            D2(4,10,j)=(D2(4,10,j)-e2r(1)*D2(16,10,j)                    &
                        -D1(16,10)*D1(4,j)-D1(16,j)*D1(4,10))/R
            D2(5,10,j)=(D2(5,10,j)-e2r(2)*D2(16,10,j)                    &
                        -D1(16,10)*D1(5,j)-D1(16,j)*D1(5,10))/R
            D2(6,10,j)=(D2(6,10,j)-e2r(3)*D2(16,10,j)                    &
                        -D1(16,10)*D1(6,j)-D1(16,j)*D1(6,10))/R

            D2(1,11,j)=(D2(1,11,j)-e1r(1)*D2(16,11,j)                    &
                        -D1(16,11)*D1(1,j)-D1(16,j)*D1(1,11))/R
            D2(2,11,j)=(D2(2,11,j)-e1r(2)*D2(16,11,j)                    &
                        -D1(16,11)*D1(2,j)-D1(16,j)*D1(2,11))/R
            D2(3,11,j)=(D2(3,11,j)-e1r(3)*D2(16,11,j)                    &
                        -D1(16,11)*D1(3,j)-D1(16,j)*D1(3,11))/R
            D2(4,11,j)=(D2(4,11,j)-e2r(1)*D2(16,11,j)                    &
                        -D1(16,11)*D1(4,j)-D1(16,j)*D1(4,11))/R
            D2(5,11,j)=(D2(5,11,j)-e2r(2)*D2(16,11,j)                    &
                        -D1(16,11)*D1(5,j)-D1(16,j)*D1(5,11))/R
            D2(6,11,j)=(D2(6,11,j)-e2r(3)*D2(16,11,j)                    &
                        -D1(16,11)*D1(6,j)-D1(16,j)*D1(6,11))/R

            D2(1,12,j)=(D2(1,12,j)-e1r(1)*D2(16,12,j)                    &
                        -D1(16,12)*D1(1,j)-D1(16,j)*D1(1,12))/R
            D2(2,12,j)=(D2(2,12,j)-e1r(2)*D2(16,12,j)                    &
                        -D1(16,12)*D1(2,j)-D1(16,j)*D1(2,12))/R
            D2(3,12,j)=(D2(3,12,j)-e1r(3)*D2(16,12,j)                    &
                        -D1(16,12)*D1(3,j)-D1(16,j)*D1(3,12))/R
            D2(4,12,j)=(D2(4,12,j)-e2r(1)*D2(16,12,j)                    &
                        -D1(16,12)*D1(4,j)-D1(16,j)*D1(4,12))/R
            D2(5,12,j)=(D2(5,12,j)-e2r(2)*D2(16,12,j)                    &
                        -D1(16,12)*D1(5,j)-D1(16,j)*D1(5,12))/R
            D2(6,12,j)=(D2(6,12,j)-e2r(3)*D2(16,12,j)                    &
                        -D1(16,12)*D1(6,j)-D1(16,j)*D1(6,12))/R
      end do ! j

!      do j=1,12
!        d11j=D1(1,j)
!        d12j=D1(2,j)
!        d13j=D1(3,j)
!        d14j=D1(4,j)
!        d15j=D1(5,j)
!        d16j=D1(6,j)
!        d116j=D1(16,j)
!        do i=1,12
!            d116i=D1(16,i)
!            d216ij=D2(16,i,j)
!            D2(1,i,j)=(D2(1,i,j)-e1r(1)*d216ij
!     &                  -d116i*d11j-d116j*D1(1,i))/R
!            D2(2,i,j)=(D2(2,i,j)-e1r(2)*d216ij
!     &                  -d116i*d12j-d116j*D1(2,i))/R
!            D2(3,i,j)=(D2(3,i,j)-e1r(3)*d216ij
!     &                  -d116i*d13j-d116j*D1(3,i))/R
!            D2(4,i,j)=(D2(4,i,j)-e2r(1)*d216ij
!     &                  -d116i*d14j-d116j*D1(4,i))/R
!            D2(5,i,j)=(D2(5,i,j)-e2r(2)*d216ij
!     &                  -d116i*d15j-d116j*D1(5,i))/R
!            D2(6,i,j)=(D2(6,i,j)-e2r(3)*d216ij
!     &                  -d116i*d16j-d116j*D1(6,i))/R
!        end do ! i
!      end do ! j
!  End optimised

      END SUBROUTINE lattinfo
