#!/usr/bin/perl -w

$file="out";
$ok=1;

open(NEW,"$file") or die "No file $file. Stopped";
open(CHECK,"check/$file") or die "No file check/$file. Stopped";

print "\nChecking output file $file\n";

&check("Initial minimum");
&diff("Frequencies of the intermolecular modes",12);

%minima=("-116.590619" +> 1,
  "-95.029567" => 1,
  "-93.063992" => 1,
  "-93.677782" => 1,
  "-85.452734" => 1,
  "-100.505135" => 1.
);
$quenches=0;
while (<NEW>) {
  if ( /new minimum \d+, energy = +(-?\d+\.\d+)/ ) {
    $e=$1;
    chomp;
    print;
    $note="    unexpected -- may be incorrect";
    for ($i=0;$i<@minima;$i++) {
      if ( $min eq $minima[$i] ) {
	$note="    -- O.K.";
	$qok=1;
      }
    }
    s/\r?$/$note/;
    $ok=$ok && $qok;
    print;
  }
}

if ( $ok && $quenches==10 ) {
  print "File $file checked.\n";
}
else {
  print "Differences found in file $file.\n";
  exit 1;
}

sub find_energy {
  local (*H) = @_;
  local $n=0;
  local $e="";
  while (<H>) {
    die "End of file" if eof;
    $n++ if /^$s *\r?$/;
    last if $n==2;
  }
  while (<H>) {
    last if /^Total energy/;
  }
  if (/^Total energy: *(-?[0-9]+\.[0-9]+)/) {
    $e=$1
  }
  $e
}

sub find {
  local (*H,$s) = @_;
  while (<H>) {
    die "End of file" if eof;
    last if /$s/;
  }
}

sub check {
  local ($s)=@_;
  print "$s: ";
  $e_check=&find_energy(*CHECK);
  $e_new=&find_energy(*NEW);
  if ( $e_new eq "") {
    print "No match in new file\n";
  }
  else {
    if (abs(($e_check-$e_new)/($e_check+$e_new)) < 1e-6 ) {
      printf "%-10.5f -- O.K.\n", $e_new;
    }
    else {
      printf "Energies differ -- new %-10.5f, check %-10.5f\n", $e_new, $e_check;
      $ok=0;
    }
  }
}

sub diff {
  local ($s,$m)=@_;
  my $ok=1;
  my $n;
  &find(*CHECK,$s);
  &find(*NEW,$s);
  $n=0;
  while (<CHECK>) {
    $n++;
    $new=<NEW>;
    $new=~s/\r$//;
    last if /^ *$/ && $n>$m;
    if ( $new ne $_ ) {
      print "check file: $_";
      print "new file:   $new\n";
      $ok=0;
    }
  }
  if ( ! $ok ) {
    printf "New file differs from check file\n";
  }
  $ok
}
