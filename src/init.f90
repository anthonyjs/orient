SUBROUTINE init

USE alphas
USE binomials
USE connect, ONLY : indexb
USE consts
USE global
USE indexes
USE indparams
USE induction
USE input, ONLY: error_file
USE interact
USE molecules
USE options, ONLY: default_options
USE Sfns, ONLY : sindex
USE sites
USE switches
USE types
USE variables

IMPLICIT NONE

INTEGER :: i, ok


!  Initialize index arrays
call reset(indexb,nsites,3000,.true.)
call reset(indexa,nsites,max(nsites+1000,nsites*nsites),.false.)

!  Initialize binomials and square roots
call init_binomials

!  Initialize variables
call alloc_variables

!  Initialize sites
se(:,:,0)=0d0
do i=1,3
  se(i,i,0)=1d0
end do
sx(:,0)=0d0
head(0)=0
sm(0)=0
level(0)=0
link(0)=0
sf(0)=0
name(0)='System'
q(:,:)=0d0
name(:)=' '
allocate (slink(3,nsites),srotlink(3,3,nsites), stat=ok)
if (ok>0) call die("Can't allocate link arrays")
slink=0d0
srotlink=0d0

!  Initialize alphas
call init_alphas

!  Allocate and initialize types
call init_types

!  Initialize S function index
call sindex

!  Initialize induction
!  call init_induction

!  Initialize interact
call init_interact

!  Set default optimization options
call default_options

!  Set switch pointer
recalc_sfns=>switch(12)

!  Specify error message file
error_file="orient_error"

END SUBROUTINE
