#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Compare Orient frequency file with reference.
"""

from functions import *
import argparse
import re
from math import sqrt
# import os.path
# import string
# import subprocess
import sys

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare orient frequency file with reference.
""",epilog=f"""
Usage: {this} file1 [file2]
Compare frequency files.
If one file argument is given, file2 is taken to be check/file1.
""")

parser.add_argument("file", help="Files to compare", nargs="+")

args = parser.parse_args()

file1 = args.file[0]
f1 = ofile(file1)
if len(args.file) > 1:
    file2 = args.file[1]
else:
    file2 = f"check/{file1}"
f2 = ofile(file2)
print(f"\nChecking {file1} against {file2}")

eps = 1.0e-5
p = 0
ok = True
while p < len(f1.lines) and p < len(f2.lines):
    m1 = re.match(r' +(-?\d+\.\d+)([i ])?$', f1.lines[p])
    m2 = re.match(r' +(-?\d+\.\d+)([i ])?$', f2.lines[p])
    if m1 and m2:
        x1 = float(m1.group(1)); i1 = m1.group(2)
        x2 = float(m2.group(1)); i2 = m2.group(2)
        if abs(x1-x2) > eps and i1 == i2 or sqrt(x1**2+x2**2) > eps and i1 != i2:
            print(f"Mismatch at line {p:1d}:\n{f1.lines[p]}{f2.lines[p]}")
            ok = False
    else:
        if not m1:
            print(f"Unexpected line {p:1d} in file {file1}\n{f1.lines[p]}")
        if not m2:
            print(f"Unexpected line {p:1d} in file {file2}\n{f2.lines[p]}")
        exit(2)
    p += 1

if ok:
    print(f"Files {file1} and {file2} match to within {eps:6.1e}")
    sys.exit(0)
else:
    sys.exit(1)
