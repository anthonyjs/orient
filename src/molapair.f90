      SUBROUTINE molapair(m1,k1,m2,k2,order,es,er,disp)

!  Calculate the electrostatic, repulsion and induction terms in the
!  interaction energy between a pair of sites.

!  Based on sitepair, but allowing for the possibility that site k2 is
!  in the lattice (in which case m2=0) or that m2 is a periodic image 
!  of m1 in a genuine periodic layer.

      USE consts
      USE damping
      USE derivs
      USE interact
      USE monolayer
      USE pairderiv
      USE switches
      USE types, ONLY : dform, dampf, BMunit

      IMPLICIT NONE


      INTEGER, INTENT(IN) ::  k1, k2, m1, m2, order
      DOUBLE PRECISION, INTENT(OUT) ::  es, er, disp

      INTEGER :: jp, k, kmax, m, n
      DOUBLE PRECISION :: vr(12), gn, gn1, gn2, pn,g,g1,g2,pk, qq
      LOGICAL :: newd

!  The entries in u0(n), n=1..6 will be the R^{-n} terms in the
!  electrostatic energy. Those in c0(n), n=6..10 are
!  the dispersion terms. alpha0 and rho0 are the parameters of the exponential
!  repulsion; er is the complete repulsion energy for this pair.
!  u1 and u2 hold the first and second derivatives of the electrostatic
!  terms, and the other arrays similarly.

      vr(1)=1d0/r
      do k=1,11
        vr(k+1)=vr(k)*vr(1)
      end do
      u0(1:eslimit)=0d0
      u1=0d0
      u2=0d0
      c0(6:12)=0d0
      do k=6,12
        if (isdisp(k)) then
          c1(:,k)=0d0
          c2(:,:,k)=0d0
        end if !isdisp(k)
      end do
      alpha0=0d0
      rho0=0d0
      es0=0d0
      er0=0d0
      disp0=0d0
      if (order .ge. 1) then
        es1(:)=0d0
        er1(:)=0d0
        alpha1(:)=0d0
        rho1(:)=0d0
        disp1(:)=0d0
        if (order .ge. 2) then
          es2(:,:)=0d0
          er2(:,:)=0d0
          alpha2(:,:)=0d0
          rho2(:,:)=0d0
          disp2(:,:)=0d0
        endif
      endif

!  We don't need derivatives involving site k2 if it is in the lattice,
!  i.e. if m2=0.
      if (m2 .ge. 1) then
        m=12
      else
        m=6
      endif
!  If m1=m2, we are dealing with a genuine periodic layer. In this case
!  the derivatives for m2,k2 need to be added to those for m1,k2, but we
!  need a factor of 1/2 to avoid double-counting. 

      k=1
      do n=1,nesmax
        do while (power(n) .gt. k)
          k=k+1
        end do
        qq=q1(n)*q2(n)*fac(n)
!  print '(I4,I3,3F12.8)', list(n), k, qq,
!  &      s0(n), qq*s0(n)*vr(k)
        u0(k)=u0(k)+qq*s0(n)
        if (order .ge. 1) then
          u1(1:m,k)=u1(1:m,k)+qq*d1s(1:m,n)
          if (order .ge. 2) then
            u2(1:m,1:m,k)=u2(1:m,1:m,k)+qq*d2s(1:m,1:m,n)
          endif
        endif
      end do
      kmax=k

!  We need to put in the r-dependence
      pk=0d0
      do k=1,kmax
        pk=pk+1d0
        if (debug(10)) print '(A,I1,F15.8)', 'R^-', k, u0(k)*vr(k)
        es0=es0+u0(k)*vr(k)
        if (order .ge. 1) then
          es1(1:m)=es1(1:m)+vr(k)*(u1(1:m,k)-vr(1)*u0(k)*pk*d1(16,1:m))
          if (order .ge. 2) then
            do jp=1,m
               es2(1:m,jp)=es2(1:m,jp)+vr(k)*(u2(1:m,jp,k)                &
                   -pk*vr(1)*(u1(1:m,k)*d1(16,jp)+u1(jp,k)*d1(16,1:m)     &
                   +u0(k)*d2(16,1:m,jp))                                  &
                   +pk*(pk+1d0)*vr(2)*u0(k)*d1(16,1:m)*d1(16,jp))
            end do
          endif
        endif
      end do

!  Dispersion terms are similar, but remember that dispersion
!  coefficients have the minus sign omitted.
      do n=1,nmax
        do k=6,12
          if (isdisp(k)) then
            if (cx(n,k) .ne. 0d0) then
              c0(k)=c0(k)-cx(n,k)*s0(n)
              if (order .ge. 1) then
                c1(1:m,k)=c1(1:m,k)-cx(n,k)*d1s(1:m,n)
                if (order .ge. 2) then
                  c2(1:m,1:m,k)=c2(1:m,1:m,k)-cx(n,k)*d2s(1:m,1:m,n)
                endif
              endif
            endif
          end if !isdisp(k)
        end do
!  Repulsion coefficients (alpha and rho) can be done in the same loop
        if (alphax(n) .ne. 0d0) then
          alpha0=alpha0+alphax(n)*s0(n)
          if (order .ge. 1) then
            alpha1(1:m)=alpha1(1:m)+alphax(n)*d1s(1:m,n)
            if (order .ge. 2) then
              alpha2(1:m,1:m)=alpha2(1:m,1:m)+alphax(n)*d2s(1:m,1:m,n)
            endif
          endif
        endif
        if (rhox(n) .ne. 0d0) then
          rho0=rho0+rhox(n)*s0(n)
          if (order .ge. 1) then
!  Optimisation seems unnecessary
            rho1(1:m)=rho1(1:m)+rhox(n)*d1s(1:m,n)
            if (order .ge. 2) then
              rho2(1:m,1:m)=rho2(1:m,1:m)+rhox(n)*d2s(1:m,1:m,n)
            endif
          endif
        endif
      end do


!  Include damped or undamped r dependence for dispersion terms.
!  gn is R^-n f_n(R), gn1 and gn2 are its derivatives w.r.t. R.
!  f_n here is a Tang-Toennies damping function.
      pn=6d0
      newd=.true.
      do n=6,12
        if (isdisp(n)) then
          call damp(dform(pair),dampf(0:,pair),n,alpha0,newd, r, g,g1,g2)
          gn=g*vr(n)
          disp0=disp0+c0(n)*gn
          if (order .ge. 1) then
            gn1=g1*vr(n)-pn*vr(1)*gn
            disp1(1:m)=disp1(1:m)                                       &
                +gn*c1(1:m,n)+gn1*c0(n)*d1(16,1:m)
            if (order .ge. 2) then
              gn2=vr(n)*(g2-2d0*pn*g1*vr(1))+pn*(pn+1d0)*vr(2)*gn
              do jp=1,m
                disp2(1:m,jp)=disp2(1:m,jp)+gn*c2(1:m,jp,n)             &
                    +gn1*(c1(1:m,n)*d1(16,jp)+c1(jp,n)*d1(16,1:m))      &
                    +c0(n)*(gn1*d2(16,1:m,jp)+gn2*d1(16,1:m)*d1(16,jp))
              end do
            endif
          endif
        endif
        pn=pn+1d0
        newd=.false.
      end do

!  Complete repulsion expression
      if (alpha0 .gt. 0d0) then
        prefac0=(1d0+R*(BMunit(1,pair)+R*BMunit(2,pair)))
        er0=BMunit(0,pair)*exp(alpha0*(rho0-R))
        if (order .ge. 1) then
          prefac1(1:m)=(BMunit(1,pair)+2d0*R*BMunit(2,pair))*d1(16,1:m)
          er1(1:m)=er0*(alpha1(1:m)*(rho0-R)                           &
              +alpha0*(rho1(1:m)-d1(16,1:m)))
          if (order .ge. 2) then
            do jp=1,m
              prefac2(1:m,jp)=(BMunit(1,pair)+2d0*R*BMunit(2,pair))     &
                  *d2(16,1:m,jp) + 2d0*BMunit(2,pair)*(d1(16,1:m)*d1(16,jp))
              er2(1:m,jp)=er0*(                                         &
                  (alpha1(1:m)*(rho0-R)+alpha0*(rho1(1:m)-d1(16,1:m)))  &
                  *(alpha1(jp)*(rho0-R)+alpha0*(rho1(jp)-d1(16,jp)))    &
                  +alpha2(1:m,jp)*(rho0-R)                              &
                  +alpha1(1:m)*(rho1(jp)-d1(16,jp))                     &
                  +alpha1(jp)*(rho1(1:m)-d1(16,1:m))                    &
                  +alpha0*(rho2(1:m,jp)-d2(16,1:m,jp)))
              er2(1:m,jp)=er2(1:m,jp)*prefac0+er1(1:m)*prefac1(jp)      &
                  +er1(jp)*prefac1(1:m)+er0*prefac2(1:m,jp)
            end do
          endif
          er1(1:m)=er1(1:m)*prefac0+er0*prefac1(1:m)
        endif
        er0=er0*prefac0
      endif

      if (print .gt. 2) print '(2I4,F10.3,5'//EFMT//')',                &
          k1, k2, r*rfact, es0*efact, er0*efact,                        &
          disp0*efact, (es0+er0+disp0)*efact

      es=es0
      er=er0
      disp=disp0

      if (order .ge. 1) then
        if (m2 .eq. m1) then
          !  Layer: no translational contribution to derivatives
          deriv1(4:6,m1)=deriv1(4:6,m1)+0.5d0*                          &
              (es1(4:6)+disp1(4:6)+er1(4:6)                             &
              +es1(10:12)+disp1(10:12)+er1(10:12))
        else if (m2 .gt. 0) then
          deriv1(1:6,m1)=deriv1(1:6,m1)+es1(1:6)+disp1(1:6)+er1(1:6)
          deriv1(1:6,m2)=deriv1(1:6,m2)+es1(7:12)+disp1(7:12)+er1(7:12)
        else
          deriv1(1:6,m1)=deriv1(1:6,m1)+es1(1:6)+disp1(1:6)+er1(1:6)
        endif
        if (order .ge. 2) then
          if (m2 .eq. m1) then
            !  Layer: no translational contribution to derivatives
            deriv2(4:6,4:6,m1,m1)=deriv2(4:6,4:6,m1,m1)+0.5d0*          &
                (es2(4:6,4:6)+disp2(4:6,4:6)+er2(4:6,4:6)               &
                +es2(4:6,10:12)+disp2(4:6,10:12)+er2(4:6,10:12)         &
                +es2(10:12,4:6)+disp2(10:12,4:6)+er2(10:12,4:6)         &
                +es2(10:12,10:12)+disp2(10:12,10:12)+er2(10:12,10:12))
          else if (m2 .gt. 0) then
            deriv2(1:6,1:6,m1,m1)=deriv2(1:6,1:6,m1,m1)                 &
                +es2(1:6,1:6)+disp2(1:6,1:6)+er2(1:6,1:6)
            deriv2(1:6,1:6,m1,m2)=deriv2(1:6,1:6,m1,m2)                 &
                +es2(1:6,7:12)+disp2(1:6,7:12)+er2(1:6,7:12)
            deriv2(1:6,1:6,m2,m1)=deriv2(1:6,1:6,m2,m1)                 &
                +es2(7:12,1:6)+disp2(7:12,1:6)+er2(7:12,1:6)
            deriv2(1:6,1:6,m2,m2)=deriv2(1:6,1:6,m2,m2)                 &
                +es2(7:12,7:12)+disp2(7:12,7:12)+er2(7:12,7:12)
          else
            deriv2(1:6,1:6,m1,m1)=deriv2(1:6,1:6,m1,m1)                 &
                +es2(1:6,1:6)+disp2(1:6,1:6)+er2(1:6,1:6)
          endif
        endif
      endif

!  Calculate fields and their derivatives if necessary
      if (switch(7)) call fieldsml(m1,k1,m2,k2,order)

      END SUBROUTINE molapair
