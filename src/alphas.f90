MODULE alphas

USE consts, ONLY: dp
USE labels
USE switches, print_level => print
USE indexes
! USE connect, only : indexb
USE global, ONLY: tracing
USE input
USE output
USE parameters, ONLY: sq
USE indparams, ONLY: maxla, sa
USE sites
USE sizes, ONLY: lq, qsize
USE types, ONLY: gettype
USE molecules, molcl => mol  !  Avoid name clash
! USE induction, ONLY: psites
USE utility
!  USE f90_unix_io, ONLY: flush

IMPLICIT NONE

INTEGER, SAVE :: psites, n_alpha, npol
!  N_ALPHA = maximum number of polarizability matrices

!  PSITES is the maximum number of polarizable sites
!  PR(k), if non-negative, indicates that this site is polarizable, and
!  specifies the rank of polarizabilities on this site.
!  PZ(K) is the number of polarizable components to be included for
!  site K -- initially qsize(pr(k)) for a polarizable site but can be
!  reduced by a LIMIT command.
!  PS(k) if nonzero, points to the entry in the arrays of fields and
!  induced moments for site k.
!  PS, PR and PZ  must be in module SITES because they are
!  used in module properties which is used by this module.
!  NEXTP(k), if nonzero, points to the next polarizable site in this
!  molecule. The first site in this list for molecule MOL is FIRSTP(MOL).

!  LASTA is the last polarizability matrix defined so far.
!  FREEA pointer to first free polarizability matrix (only if matrices
!       have been used and then released).
INTEGER :: mol, lasta, freea, freq_index=-1
!  LA(ix) is the maximum rank of polarizabilities in polarizability
!  matrix ix
!  IXA(k1,k2) is the index ix of the polarizability matrix between
!  sites k1 and k2
!  LIST is a temporary list of sites.
INTEGER, ALLOCATABLE, SAVE :: list(:), la(:), ixa(:,:), nextp(:)
TYPE(index), SAVE :: indexa
DOUBLE PRECISION, ALLOCATABLE, SAVE :: alpha(:,:,:)
DOUBLE PRECISION :: test=1d-7, test0=1d-7, ignore, freq_sq=1d0
LOGICAL :: verbose=.false.
! CHARACTER(LEN=3) :: tag(-1:10)=["???","000",                          &
!     "001","002","003","004","005","006","007","008","009","010"]
CHARACTER(LEN=80) :: msg
PRIVATE
PUBLIC init_alphas, psites, pdata, alpha, indexa, ixa, npol, nextp, &
    n_alpha

CONTAINS

!-----------------------------------------------------------------------

SUBROUTINE init_alphas

INTEGER ok
lasta=0
freea=0
allocate(ixa(0:nsites,0:nsites),  nextp(nsites), stat=ok)
if (ok > 0) call die                                                    &
    ("Couldn't allocate arrays in ALPHAS module",.true.)
ixa = 0
nextp = 0
if (psites > 0) then
  allocate(list(psites),  stat=ok)
  if (ok > 0) call die                                                    &
      ("Couldn't allocate list array in ALPHAS module",.true.)
  list = 0
  allocate(la(0:n_alpha),  stat=ok)
  if (ok > 0) call die                                                    &
      ("Couldn't allocate la array in ALPHAS module",.true.)
  la = -1
end if

END SUBROUTINE init_alphas

!-----------------------------------------------------------------------

SUBROUTINE pdata

IMPLICIT NONE

!  Syntax:

!  POLARIZABILITIES [FOR] molecule
!     commands
!  END

!  In the commands, the sites are specified by name, so every site
!  should have a unique name. (If however there is more than one site
!  with the same name, the last one mentioned in the molecule definition
!  is used.) Every site mentioned must belong to the same molecule; a
!  separate POLARIZABILITIES command is needed for each molecule.

!  VERBOSE
!    Print additional information

!  QUIET
!    (default) Don't print extra information

!  READ PAIRS [[FILE] file | HERE]
!    Read polarizabilities from a new-format polarizability file, with
!    site and rank details incorporated. The polarizabilities are read from
!    the file if specified, otherwise they are expected inline.
!  or
!  READ [PAIRS] [FILE file] [RANK rank] [TRIANGLE] [CARTESIAN]
!         [AXES {X|Y|Z} FROM site TO site {X|Y|Z} FROM site TO site]
!    Read polarizabilities for the sites specified. If axes are specified,
!    they define the local axis system in which the polarizabilities were
!    calculated. Otherwise the axes are assumed to be global.
!    If no rank is specified it is assumed to be 4. If TRIANGLE
!    is specified, the polarizabilities are arranged in blocks, in the
!    order (s1,s1), (s2,s1), (s2,s2), (s3,s1), ... Otherwise they are in
!    the order ((((alpha(s,k,s',k'), k'=1,max), s'=1,n), k=1,max), s=1,n).
!    If CARTESIAN is specified, the polarizabilities are given as Cartesian
!    components q, mu_x, mu_y, mu_z, theta_xx, etc.

!  s1  s2  [l_ab]
!      ((alpha(s1,k,s2,k'), k'=1,max),k=1,max)
!    Read polarizabilities for a pair of sites s1 and s2
!    where the first line gives the two site names and the polarizabilities
!    follow on subsequent lines. l_ab, if present, is the rank of
!    polarizabilities for this pair of sites. If omitted, the rank is
!    assumed to be 4.

!    All polarizabilities are expected in atomic units.

!  LIMIT [ALL] [RANK] limit [FOR [SITES [site-list]|TYPES [type-list]]]
!  
!    Limit the rank of polarizabilities for the sites or types specified
!    to the value given. The higher polarizabilities are not deleted, and
!    can be restored by a subsequent LIMIT command. If sites are listed,
!    all the cross-polarizabilities involving the named sites are limited.
!    If types are listed, all the cross-polarizabilities involving sites
!    of that type are limited. If ALL is specified, all sites are limited
!    and no site-list or type-list should be specified.

!  SUM-RULE [CORRECT] [TEST eps]
!    Check that the polarizabilities satisfy the charge-flow sum rules.
!    If TEST eps is given, eps is used as the criterion; otherwise a
!    default value is used. If CORRECT is specified, the values are adjusted
!    so that they satisfy the sum-rule.

!  LOCALIZE LS [TEST test] [[ EXCEPT | ONLY ] CHARGEFLOW] +++
!     [ ALL | SITES a b ... | PAIR a b TO c ]
!    Localize the polarizabilities for the current molecule.
!    Nonlocal polarizabilities are zeroed by transferring their effects to
!    local polarizabilities. If EXCEPT CHARGEFLOW is specified, the charge-
!    flow polarizabilities are unaffected; if ONLY CHARGEFLOW is specified,
!    only the charge-charge and charge-multipole components are localised.
!    The localisation is carried out in such a way that the sum rules are
!    maintained. If a list of site pairs is given, non-local polarizabilities
!    involving those pairs only are localized. If a list of sites is given,
!    all non-local polarizabilities involving those sites are localized.
!    Otherwise all non-local polarizabilities are localized.
!    If TEST is specified, the program checks whether non-local
!    polarizabilities have been reduced below the value given; otherwise
!    a default value is used.
!  LOCALIZE [LW] [[ONLY|EXCEPT] CHARGEFLOW] [LIMIT rank]
!    Localize using the Lillestolen-Wheatley algorithm. This is the default
!    method. Normally, all polarizabilities, for all polarizable sites, up
!    to the specified limiting rank are localized. If ONLY CHARGEFLOW is
!    specified, only the chargeflow components are removed. Bonds, for the
!    polarizable sites only, must be specified before using this option.

!  MOVE [FROM] site1 [site2] [CROSS] TO site3 [RANK [MIN lmin] [KEEP lmax]]
!    If CROSS is specified, only the non-local polarizabilities are moved.
!    Otherwise the local polarizabilities are also moved. If site2 is
!    omitted, it is set equal to site1 and the CROSS option is faulted.
!    If a minimum rank lmin is specified, only the polarizabilities for which
!    one or both indices have this rank or more are moved. If a maximum rank
!    is specified, polarizabilities on the new site are restricted to this
!    rank and below.

!  COPY SITES site [site ...] FROM [MOLECULE mol] SITES site [site ...]
!    Copy the specified polarizability matrices from the source given. All
!    polarizability matrices involving the specified sites are copied. If
!    a molecule name is specified, the polarizabilities are copied from 
!    the specified sites in that molecule; otherwise they are copied from
!    sites within the same molecule. If polarizability matrices have not
!    been defined for any site or pair of sites in the source list, the
!    corresponding target matrix is left unchanged (which will probably
!    mean that it is left undefined).

!    This is more efficient than specifying the polarizabilities explicitly,
!    because only pointers are involved, not the polarizabilities themselves.

!  DELETE site1 [site2]
!    Delete local and non-local polarizabilities for site1 and site2.
!    If site2 is omitted, only the local polarizabilities for site1 are
!    deleted.

!  PRINT [LIMIT r | RANKS r1 [to] r2 ] [ ALL | [SITES] site1 site2 ...]
!    Print the polarizabilities for the sites listed. If rank limits are
!    specified, only the polarizabilities for those ranks are printed.
!    LIMIT r is equivalent to RANK 0 to r.

!  WRITE [FILE] filename [LIMIT r | RANKS r1 [to] r2 ] 
!       [BINARY|ASCII|FORMAT fmt] [LOCAL] site1 site2 ...
!    Write the polarizabilities for the sites listed to the specified
!    formatted or unformatted file. The default format for a formatted
!    file is "(9g14.6)". If rank limits are specified, only the
!    polarizabilities for those ranks are written. Each polarizability
!    matrix is preceded by a record giving the two site indices, the
!    rank and the size of the matrix. The matrix itself is written one
!    row to a record. If LOCAL is specifed, only local polarizabilities
!    are written out.

!  TOTAL [RANK limit]
!    Print total molecular dipole-dipole polarizability. Default limit=1.
!    limit=0 includes only charge-flow polarizabilities. Limit>1 is
!    treated as limit=1.

!  DRUDE  [version  [parameter]]
!    site  q  k
!    ...
!    END
!    Provide site charges and spring constants for Drude model. Site names
!    must be unique. The available versions are currently "none", "Thole-2",
!    "YuMS11". The parameter, if given, needs to be a value appropriate for
!    the model. Defaults are 0.572 for Thole-2, 2.0 for YuMS11.

!  COMMENT "text"
!    Print the text given.


CHARACTER(LEN=12) :: key
CHARACTER(LEN=20) :: ww, w1
CHARACTER(LEN=80) :: string

LOGICAL :: eof, posn, flow, only_flow, fix, cross, none, prune=.false., &
    lw=.false.
INTEGER :: flag
INTEGER :: i, i1, i2, ix, ixx, j, k, k1, k2, kk1, kk2, m, n, nc, n1,    &
    laa, lmin=0, lmax=4, limit, pk
CHARACTER(LEN=*), PARAMETER :: this_routine="pdata"

if (tracing) print "(2a)", "Entering ", this_routine

if ( .not. allocated(alpha)) then
  allocate(alpha(sa,sa,n_alpha), stat=i)
  if (i > 0) call die                                                   &
      ("Can't allocate polarizability array alpha",.false.)
  alpha=0d0
endif
call readu(ww)
if (ww .ne. 'FOR') call reread(-1)
call reada(w1)
mol=locate(w1)
posn=.false.
print "(/2a)", "Polarizabilities for molecule ", trim(name(head(mol)))
if (psites <= 0) then 
  call die("Can't define polarizabilities -- no polarizable sites have been allocated.")
end if

do
  call read_line(eof)
  call readu(key)
  if (eof .or. key == "END") then
    !  Set up linked list of polarizable sites for molecule mol
    !  Initially empty if no polarizabilities have been read for this
    !  molecule
    k = head(mol)
    firstp(mol) = 0
    if (verbose) print "(/a)", "Summary of polarizable sites:"
    do while (k > 0)
      if (ps(k) > 0) then
        if (firstp(mol) == 0) then
          firstp(mol) = k
        else
          nextp(pk) = k
        endif
        pk = k
        nextp(k) = 0
        if (verbose) print "(i3,1x,2a, i1, a,i1, a, i0)", k, name(k),   &
            " rank ", pr(k), "  limit ", lq(pz(k)), "  ps ", ps(k)
      endif
      k = next(k)
    end do
    return
  endif

  select case(key)
  case(" ","!","NOTE")
    cycle
  case("VERBOSE")
    verbose=.true.
  case("QUIET")
    verbose=.false.

  case("READ")
    !  READ [PAIRS | NEW [FORMAT]] [HERE | FILE file]
    !  Read polarizabilities from data or from a file in the new format
    !  READ [OLD FORMAT] 
    !  Read polarizabilities in the old format. Deprecated.
    !  See subroutine read_alphas for details.
    call reada(ww)
    select case(upcase(ww))
    case("FORMAT")
      call reada(ww)
      select case(upcase(ww))
      case("NEW")
        call read_site_pairs(lmax)
      case("OLD")
        call read_alphas(lmax)
      case default
        call die ("Unrecognized format "//trim(ww)//                    &
            " in polarizability read command",.true.)
      end select
    case("NEW")
      call readu(ww)
      if (ww .ne. "FORMAT") call reread(-1)
      call read_site_pairs(lmax)
    case("PAIR","PAIRS","HERE")
      call read_site_pairs(lmax)
    case("OLD")
      call readu(ww)
      if (ww .ne. "FORMAT") call reread(-1)
      call read_alphas(lmax)
    case default
      call reread(-1)
      call read_alphas(lmax)
    end select

  case("LIMIT")
    call limit_rank

  case("LOCALISE","LOCALIZE")
    !  Localize polarizabilities. First it is necessary to invoke the AXES
    !  routine on the un-rotated molecule, so that the site coordinates in
    !  local axes are in the array SX.
    if (.not. posn) then
      flag=sf(mol)
      sf(mol)=iand(sf(mol),255-8)
      calc(mol)=.false.
      call axes(mol,.false.)
      sf(mol)=flag
      posn=.true.
    endif

    !  If FLOW is .TRUE., then charge-flow polarizabilities are also localized.
    flow=.true.
    only_flow=.false.
    lw=.true.
    test=test0
    ignore=0d0
    k1=0
    k2=0
    n=0
    do while (item < nitems)
      call reada(ww)
      select case(upcase(ww))
      case("TEST")
        call readf(test)
      case("IGNORE")
        call readf(ignore)
      case("RANK","LIMIT")
        call readi(lmax)
      case("PRUNE")
        call reada(ww)
        select case(upcase(ww))
        case("OFF","NO")
          prune=.false.
        case("ON","YES","ZEROS","")
          prune=.true.
        case default
          prune=.true.
          call reread(-1)
        end select
      case("ALL")
        k1 = head(mol)
        do while (k1 .ne. 0)
          if (pr(k1) > 0) then
            n = n+1
            list(n) = k1
          end if
          k1 = next(k1)
        end do
      case("EXCEPT","ONLY")
        call readu(ww)
        if (ww .ne. "CHARGEFLOW" .and. ww .ne. "CHARGE-FLOW")        &
            call die('Syntax error in LOCALIZE',.true.)
        if (key .eq. "EXCEPT") then
          flow=.false.
        else
          only_flow=.true.
        end if
      case("SITES","FOR")
        do while (item < nitems)
          n=n+1
          call read_site(list(n),mol)
        end do
      case("NEIGHBOURS","NEIGHBORS","BONDS","LW")
        lw=.true.
      case("LESUEUR","LS")
        lw=.false.
      case("PAIRS","PAIR")
        call read_site(k1,mol)
        call read_site(k2,mol)
        if (k1==k2) call die("Sites must differ in LOCALIZE PAIR",.true.)
        if (item<nitems) then
          call readu(ww)
          nc=0
          if (ww .ne. "TO") call die ("Syntax error in LOCALIZE",.true.)
          do while (item < nitems)
            nc=nc+1
            call read_site(list(nc),mol)
          end do
          call localize(k1,k2,list(1:nc),flow,only_flow,test)
        else
          call localize(k1,k2,list(1:0),flow,only_flow,test)
        end if
      case default
        !  Assume that it's a site name
        call reread(-1)
        n=n+1
        call read_site(list(n),mol,.true.)  ! return zero if not found
        if (list(n) == 0) then
          if (n > 1) then
            call die("Site not found",.true.)
          else
            call die("Syntax error in LOCALIZE",.true.)
          end if
        end if
      end select
    end do
    if (ignore == 0d0) ignore = 3d0*test
    if (lw) then
      !  Lillestolen-Wheatley localization
      print "(/a)", "Localising polarizabilities using the Lillestolen-Wheatley method"
      call localize_lw(mol,lmax,only_flow,.not.flow)
    else if (n > 0) then
      !  There is a list of sites to localise
      print "(/a)", "Localising polarizabilities using the Le Sueur-Stone method"
      do i=1,n
        do j=i+1,n
          call localize(list(i),list(j),list(1:0),flow,only_flow,test)
        end do
      end do
    end if
    if (prune) then
      !  Remove all-zero polarizability matrices
      k1 = firstp(mol)
      do while (k1 > 0)
        k2 = firstp(mol)
        do while (k2 > 0)
          ix = lookup(indexa,k1,k2)
          if (ix > 0) then
            if (all(abs(alpha(1:qsize(pr(k1)),1:qsize(pr(k2)),ix))      &
                < ignore)) then
              call remove(indexa,k1,k2)
              la(ix) = -1
            end if
          end if
          k2 = nextp(k2)
        end do
        k1 = nextp(k1)
      end do
    end if

  case("MOVE")
    cross=.false.
    lmin=0
    lmax=lq(sa)
    k1=0
    k2=0
    do while (item<nitems)
      call readu(ww)
      do
        select case(ww)
        case("FROM")
          call read_site(k1,mol)
          call read_site(k2,mol,.true.)
          if (k2 == 0) then
            k2 = k1
            call reread(-1)
          end if
          ! print "(a,3x,a)", trim(name(k1)), trim(name(k2))
        case("CROSS")
          cross=.true.
        case("TO")
          call read_site(k,mol)
          if (lookup(indexa,k,k) .eq. 0) then
            lasta=lasta+1
            call insert(indexa,k,k,lasta,'polarizability index')
            la(lasta)=maxla
            alpha(1:sa,1:sa,lasta)=0d0
            call register(k,maxla)
          endif
        case("KEEP","MAX")
          call readi(lmax)
        case("MIN")
          call readi(lmin)
        case("RANK","RANKS")
          call readu(ww)
          select case(ww)
          case("MIN")
            call readi(lmin)
          case("KEEP","MAX")
            call readi(lmax)
          end select
        case default
          call reread(-1)
          if (k1==0) then
            ww="FROM"
            cycle
          else
            call die("Syntax error in MOVE",.true.)
          end if
        end select
        ! exit
      end do
    end do
    if (cross .and. k1 == k2)                                            &
        call die ("Invalid use of CROSS option in MOVE",.true.)

!  Before moving the polarizabilities it is necessary to invoke the AXES
!  routine on the un-rotated molecule, so that the site coordinates in
!  local axes are in the array SX.
    if (.not. posn) then
      flag = sf(mol)
      sf(mol) = iand(sf(mol),255-8)
      calc(mol) = .false.
      call axes(mol,.false.)
      sf(mol) = flag
      posn = .true.
    endif

!  Move polarizabilities between K1 and K2 to site K.
    ! print "(a,3x,3a)", trim(name(k1)), trim(name(k2)), " to ", trim(name(k))
    if (link(k1) .ne. link(k) .or. link(k2) .ne. link(k)) then
      call die("Attempt to move polarizabilities between sites with different axes",.true.)
    end if
    if (.not.cross) then
      if (k1 .ne. k) call movep(k1,k1, k, lmin,lmax)
      if (k2 .ne. k1 .and. k2 .ne. k) call movep(k2,k2, k, lmin,lmax)
    endif
    if (k1 .ne. k2) then
      call movep(k1,k2, k, lmin,lmax)
      call movep(k2,k1, k, lmin,lmax)
    endif

  case("DELETE")
    !  Redundant
    call read_site(k1,mol)
    call read_site(k2,mol)
    if (k2 == 0) k2 = k1
    laa = lookup(indexa,k1,k1)
    alpha(:,:,laa) = 0d0
    if (k2 .ne. k1) then
      alpha(:,:,lookup(indexa,k1,k2)) = 0d0
      alpha(:,:,lookup(indexa,k2,k1)) = 0d0
      alpha(:,:,lookup(indexa,k2,k2)) = 0d0
    endif
    if (k2 .eq. k1) then
      if (firstp(mol)==k1) then
        firstp(mol)=next(k1)
        la(laa)=freea
        freea=laa
      end if
    end if

  case("COPY")
    !  COPY SITES site [site ...] FROM [MOLECULE mol] SITES site [site ...]
    !   Copy the specified polarizability matrices from the source
    !   given. In fact the index entry ixa(k1,k2) for a target site
    !   pair k1,k2 just points to the same polarizability matrix as
    !   the entry isa(kk1,kk2) for the source pair.
   
    !   All polarizability matrices involving the specified sites are
    !   copied. If a molecule name is specified, the polarizabilities
    !   are copied from the specified sites in that molecule;
    !   otherwise they are copied from sites within the same molecule.
    !   If polarizability matrices have not been defined for any site
    !   or pair of sites in the source list, the corresponding target
    !   matrix is left unchanged (which will probably mean that it is
    !   left undefined).

    n = 0
    n1 = 0
    m = mol
    do while (item<nitems)
      call readu(ww)
      ! print "(a)", ww
      select case(ww)
      case("SITES","SITE")
        !  Get list of polarizable sites
        do while (item < nitems)
          call readu(ww)
          ! print "(a)", ww
          select case(ww)
          case("FROM")
            n1 = n
            call reread(-1)
            write (6,"(a,20(1x,i0:))",advance="no")             &
                "Copying sites", list(1:n)
            exit
          case default
            n = n+1
            call reread(-1)
            call read_site(k,m)
            list(n) = k
            ! call register(k,lmax)
          end select
        end do
      case("FROM")
        call readu(ww)
        if (ww == "MOLECULE") then
          call reada(ww)
          m = locate(ww)
        else
          m = mol
          call reread(-1)
        endif
      end select
    end do
    write (6,"(a,20(1x,i0:))",advance="no") " from sites", list(n1+1:n)
    if (m .ne. mol) then
      print "(a,a)", " in ", trim(name(head(m)))
    else
      print "(1x)"
    end if
    call assert(n > 0,"No sites specified",.true.)
    call assert(n == 2*n1,"Number of source sites and number of target sites differ",.true.)
    !  Loop over target sites
    do i1 = 1,n1
      k1 = list(i1)
      do i2 = 1,n1
        k2 = list(i2)
        !  Source sites
        kk1 = list(n1+i1)
        kk2 = list(n1+i2)
        !  Does the source matrix exist?
        ixx = lookup(indexa,kk1,kk2)
        if (ixx > 0) then
          call register(k1,pr(kk1))
          call register(k2,pr(kk2))
          ixa(k1,k2) = ixx
          if (print_level>1) then
            write (6,"(a,2i4,a,2i4, a,i0)",advance="no")                     &
                "Polarizabilities for", k1, k2, " copied from", kk1, kk2, &
                " index ", ixx
            if (m .ne. mol) then
              print "(a,a)", " in ", trim(name(head(m)))
            else
              print "(1x)"
            end if
          end if
        end if
      end do
    end do

  case("PRINT")
    call print_alphas

  case("WRITE")
    call write_alphas

  case("TOTAL")
    !  Calculate total molecular dipole-dipole polarizability
    !  TOTAL [RANK limit]
    !  limit = 0 to include only charge-flow polarizabilities.
    limit = 1
    do while (item < nitems)
      call readu(ww)
      select case(ww)
      case("RANK","LIMIT")
        call readi(limit)
      case default
        call die("Syntax error in TOTAL command",.true.)
      end select
    end do
    call total_dd(limit)

    !  Old version -- not reliable
    !  Move all polarizabilities to the head site and print the result.
    !  Note that this currently destroys all the distributed polarizabilities.
    !  limit = 4
    !  do while (item<nitems)
    !    call readu(ww)
    !    select case(ww)
    !    case("LIMIT","RANK")
    !      call readi(limit)
    !    end select
    !  end do
    !  call total(limit)
    !  k = head(mol)
    !  ix = lookup(indexa,k,k)
    !  call print_polarizabilities(ix,limit)

  case("SUM-RULE","SUM-RULES")
    !  Check to make sure that the sum rule is obeyed.
    test = test0
    fix = .false.
    do while (item < nitems)
      call readu(ww)
      select case(upcase(ww))
      case ("FIX","CORRECT")
        fix = .true.
      case ("TEST")
        call readf(test)
      end select
    end do
    print "(/a,1pe11.3)", "Checking sum-rules, threshold", test
    if (fix) print "(a)", "Forcing compliance"
    print_level = print_level + 1
    call sum_rule(fix,test)
    print_level = print_level - 1

  case("DRUDE")
    !  Set up Drude model for polarizabilities
    call drude_alpha(mol)
    lmax = 1

  case("SUMMARY")
    print "(2a)", "Summary of polarizable sites on molecule ",         &
        trim(name(head(mol)))
    none = .true.
    k1 = firstp(mol)
    do while (k1 > 0)
      print "(4x,2a,i0)", trim(name(k1)), " is polarizable"
      print "(4x,a)", "Polarizability matrices"
      k2 = k1
      do while (k2 > 0)
        ix=lookup(indexa,k1,k2)
        if (ix > 0) then
          none=.false.
          print "(8x,a,1x,a,a,i0,a,i0,a,i0)", trim(name(k1)),       &
              trim(name(k2)), ": matrix ", ix,                      &
              ", rank ", pr(k1), " by ", pr(k2)
        end if
        k2 = nextp(k2)
      end do
      k1 = nextp(k1)
    end do
    if (none) print "(4x,a)", "None"

  case("COMMENT")
    call reada(string)
    print "(a)", string

  case default
    call die("Unrecognised keyword "//trim(KEY)                        &
        //" in polarizability directive",.true.)

  end select
end do

END SUBROUTINE pdata

!-----------------------------------------------------------------------

SUBROUTINE register(k,lmax)

!  Register k as a polarizable site with maximum rank lmax of
!  polarizabilities, unless already registered. If necessary
!  increase the maximum rank to lmax.

INTEGER, INTENT(IN) :: k, lmax
INTEGER :: k1, pk

if (pr(k) < 0) then
  !  New polarizable site
  npol = npol+1
  if (npol > psites) then
    write(msg,"(a,i0)") 'Too many polarizable sites: maximum is ', psites
    call die(msg,.true.)
  end if
  ps(k) = npol
  pr(k) = lmax
  pz(k) = qsize(lmax)
  l(k) = max(l(k),lmax)
  !  Set up or update linked list of polarizable sites for this molecule
  if (firstp(mol) == 0) then
    firstp(mol) = k
    nextp(k) = 0
  else
    pk = firstp(k)
    k1 = head(mol)
    do while (k1 > 0)
      if (k1 == k) then
        !  Insert k into the list of polarizable sites for this molecule
        nextp(k) = nextp(pk)
        nextp(pk) = k
        exit
      else
        ! Advance the polarizable-site list pointer if this site is
        ! polarizable.
        if (pr(k1) > 0) pk = k1
      end if
      k1 = next(k1)
    end do
  end if
end if
if (pr(k) < lmax) then
  pr(k) = lmax
  pz(k) = qsize(lmax)
end if

END SUBROUTINE register

!-----------------------------------------------------------------------

SUBROUTINE read_alphas(lmax)

!  Read polarizabilities in the original camcasp/orient format

!  READ [FILE file] [RANK rank] [TRIANGLE] [CARTESIAN]
!         [AXES {X|Y|Z} FROM site TO site {X|Y|Z} FROM site TO site]
!         [SITES s1 s2 ...]
!    Read polarizabilities for the sites specified. If axes are specified,
!    they define the local axis system in which the polarizabilities were
!    calculated. Otherwise the axes are assumed to be global.
!    If no rank is specified it is assumed to be 4. If TRIANGLE
!    is specified, the polarizabilities are arranged in blocks, in the
!    order (s1,s1), (s2,s1), (s2,s2), (s3,s1), ... Otherwise they are in
!    the order ((((alpha(s,k,s',k'), k'=1,max), s'=1,n), k=1,max), s=1,n).
!    If CARTESIAN is specified, the polarizabilities are given as Cartesian
!    components q, mu_x, mu_y, mu_z, theta_xx, etc.

INTEGER, INTENT(OUT) :: lmax

CHARACTER(LEN=20) :: ww
CHARACTER(LEN=80) :: datafile

DOUBLE PRECISION :: d(9,10), temp(sq,sq)

LOGICAL :: triangle, cartesian, binary
INTEGER :: flag, unit
INTEGER :: i, i1, i2, ix, ixt, j, k, k1, k2, n, last, lastc, ok
INTEGER :: sizec(0:4) = (/1,4,10,20,35/)
CHARACTER(LEN=*), PARAMETER :: this_routine="read_alphas"

if (tracing) print "(2a)", "Entering ", this_routine
datafile=""
binary=.false.
cartesian=.false.
lmax=maxla
last=sa
triangle=.false.
unit=5
n=0
do while (item < nitems)
  call reada(ww)
  select case(upcase(ww))
  case("RANK","LIMIT")
    call readi(lmax)
    if (lmax > maxla) then
      call die('Polarizabilities limited to rank'              &
          //trim(stri(maxla)),.true.)
    endif
    last = qsize(lmax)
  case("TRIANGLE")
    triangle=.true.
  case("CARTESIAN")
    cartesian=.true.
  case("BINARY")
    binary=.true.
  case("SITES","SITE")
    !  Get list of polarizable sites
    do while (item < nitems)
      n = n+1
      call read_site(k,mol)
      list(n) = k
      call register(k,lmax)
    end do
    print "(a/(10a8))", "Sites ", (name(list(i)), i=1,n)
  case("FILE")
    call reada(datafile)
    print "(2a)", "Polarizabilities from file ", datafile
    unit=find_io(17)
    open(unit,file=datafile,status="old",iostat=ok)
    if (ok>0) call die ("Can't open file "//trim(datafile),.true.)
    call stream(unit)
  case default
    call die                                                       &
        ('Syntax error in POLARIZABILITY READ directive',.true.)
  end select
end do

if (binary) then
  if (datafile == "") then
    call die("No datafile specified for binary polarizabilities",.true.)
  end if
  do
    read (unit,iostat=flag) i1,i2,lmax,last
    ! print "(a,2i2,a,i2,a,i3)",                                     &
    !     "Sites ", i1, i2, "  Rank ", lmax, "  Size ", last
    if (flag<0) exit  !  End of file
    k1=list(i1)
    k2=list(i2)
    call getix(k1,k2,lmax,ix)
    do i=1,last
      read(unit) alpha(i,1:last,ix)
    end do
  end do
else if (n == 0) then
  !  No site list specified.
  call die("No sites specified in READ subcommand of POLARIZABILITIES",.true.)
else
  print "(a,20(1x,a)/(34x,20(1x,a)))",                             &
      "Reading polarizabilities for sites", (trim(name(list(i))), i=1,n)
  !  Read polarizabilities
  !  Create index of polarizability matrices
  do i1=1,n
    k1=list(i1)
    do i2=1,n
      k2=list(i2)
      call getix(k1,k2,lmax,ix)
      alpha(1:sa,1:sa,ix)=0d0
      print '(2i4,i5)', list(i1), list(i2), ix
    end do
  end do
  !  Read values
  if (triangle) then
    if (cartesian) then
      lastc=sizec(lmax)
      if (lmax .gt. 2) call die                                 &
          ('rank too high for cartesian polarizabilities',.true.)
      do i=1,last
        do j=1,lastc
          d(i,j)=0d0
        end do
      end do
      d(1,1)=1d0
      d(2,4)=1d0
      d(3,2)=1d0
      d(4,3)=1d0
      d(5,10)=1d0
      d(5,5)=-0.5d0
      d(5,8)=-0.5d0
      d(6,7)=sqrt(3d0)
      d(7,9)=sqrt(3d0)
      d(8,5)=0.5*sqrt(3d0)
      d(8,8)=0.5*sqrt(3d0)
      d(9,6)=sqrt(3d0)
    endif
    do i1=1,n
      k1=list(i1)
      pr(k1) = lmax
      pz(k1) = qsize(lmax)
      do i2=1,i1
        k2=list(i2)
        ix=lookup(indexa,k1,k2)
        ixt=lookup(indexa,k2,k1)
        ixa(k1,k2)=ix
        ixa(k2,k1)=ixt
        if (cartesian) then
          do i=1,lastc
            do j=1,lastc
              call getf(alpha(i,j,ix))
            end do
          end do
          !  Transform from cartesian to spherical format
          temp(1:last,1:lastc) =                                        &
              matmul(d(1:last,1:lastc),alpha(1:lastc,1:lastc,ix))
          alpha(1:last,1:last,ix) =                                     &
              matmul(temp(1:last,1:lastc),transpose(d(1:last,1:lastc)))
          alpha(:,:,ixt) = transpose(alpha(:,:,ix))
        else
          do i=1,last
            do j=1,last
              call getf(alpha(i,j,ix))
              alpha(j,i,ixt)=alpha(i,j,ix)
            end do
          end do
        endif
        !  print '(a, a6,a6)', 'Sites ', name(k1), name(k2)
        !  do i=1,last
        !    print '(5f15.8)', (alpha(i,j,ix), j=1,last)
        !  end do
      end do
    end do
  else
    do i1 = 1,n
      k1 = list(i1)
      pr(k1) = lmax
      pz(k1) = qsize(lmax)
      do i = 1,last
        do i2 = 1,n
          k2 = list(i2)
          ix = lookup(indexa,k1,k2)
          ixa(k1,k2) = ix
          if (binary) then
            read (unit) alpha(i,1:last,ix)
          else
            do j=1,last
              call getf(alpha(i,j,ix))
            end do
            print "(9f9.5)", alpha(i,1:last,ix)
          end if
        end do
      end do
    end do
  end if
end if
if (datafile .ne. "") then
  close(unit)
  datafile=""
  call stream(5)
end if
switch(3)=.true.
print "(a)", "Done"

END SUBROUTINE read_alphas

!-----------------------------------------------------------------------

SUBROUTINE read_site_pairs(lmax)

!  Read polarizabilities in new format

INTEGER, INTENT(OUT) :: lmax

CHARACTER(LEN=20) :: ww
CHARACTER(LEN=80) :: datafile=""
CHARACTER(LEN=40) :: dummy, molecule
LOGICAL :: eof
INTEGER :: i, ix, ixt, j, k1, k2, l_ab, last, s1, s2, min1=0, max1=4,   &
    min2=0, max2=4, limit, limsize,unit, ok
CHARACTER(LEN=*), PARAMETER :: this_routine="read_site_pairs"
DOUBLE PRECISION :: fsq=0d0

if (tracing) print "(2a)", "Entering ", this_routine
molecule = ""
datafile = ""
limit = maxla
limsize = qsize(limit)
do while (item < nitems)
  call reada(ww)
  select case(upcase(ww))
  case("FILE")
    if (datafile == "") then
      call reada(datafile)
    else
      print "(a)", "Syntax error in READ PAIRS"
    end if
  case("LIMIT")
    call readi(limit)
    if (limit > maxla) then
      call die('Maximum rank for polarizabilities is'//trim(stri(maxla)),.true.)
    else
      print "(/a)", "LIMIT option here is DEPRECATED. Use separate LIMIT subcommand."
      print "(a)", "All polarizabilities given will be read in."
      print "(a,i0,a/)", "Rank will be limited to ", limit," here but may be changed subsequently."
    endif
    limsize = qsize(limit)
  case("RANK")
    call readi(i)
    !  Ignore
  case("!", "SITES", "HERE")
    !  Ignore and exit
    exit
  case default
    if (datafile == "") then
      call reread(-1)
      call reada(datafile)
    else
      print "(3a)", "WARNING: ", trim(ww), " inappropriate for READ PAIRS"
    end if
  end select
end do

if (datafile .ne. "") then
  print "(2a)", "Polarizabilities from file ", datafile
  unit=find_io(17)
  open(unit,file=datafile,status="old",iostat=ok)
  if (ok>0) call die ("Can't open file "//trim(datafile),.true.)
  call stream(unit)
end if

!  Read polarizability matrices individually
do
  call read_line(eof)
  if (eof) then
    if (unit == 5) then
      call die ("Unexpected end of data file")
    else
      !  End of polarizability file
      exit
    end if
  end if
  call reada(ww)
  select case(upcase(ww))
  case("","NOTE","END", "!")
    cycle
  case("ENDFILE")
    !  End of polarizability matrix list
    exit
  case("POL","ALPHA")
    !  New format polarizability file
    s1 = 0; s2 = 0
    min2 = -1; max2 = -1
    do while (item < nitems)
      call reada(ww)
      select case(upcase(ww))
      case("SITE-LABELS","SITE-NAMES","SITES")
        call read_site(k1,mol)
        call read_site(k2,mol)
        ! print "(2a)", name(k1), name(k2)
      case("SITE-INDICES")
        call readi(s1)
        call readi(s2)
      case("RANK")
        call readi(min1)
        call readu(dummy)
        if (dummy .ne. "TO" .and. dummy .ne. ":") call reread(-1)
        call readi(max1)
        !  Assume square matrix unless there's a "BY" keyword
        if (min2 < 0) min2 = min1
        if (max2 < 0) max2 = max1
      case("BY")
        call readi(min2)
        call reada(dummy)
        if (dummy .ne. "TO" .and. dummy .ne. ":") call reread(-1)
        call readi(max2)
      case("INDEX")
        call readi(freq_index)
      case("FREQ2","FREQSQ")
        call readf(fsq)
        if (freq_sq == 1d0) then
          freq_sq = fsq
        else if (fsq .ne. freq_sq) then
          call die("Polarizabilities must all have the same imaginary frequency",.true.)
        end if
      case("CARTSPHER")
        call readu(ww)
        if (ww .ne. "S") call die("Can't handle cartesian polarizabilities",.true.)
      case default
        if (molecule == "") then
          molecule = ww
        else if (ww .ne. molecule) then
          call die("Keyword "//trim(ww)//" in site-pair header not recognized",.true.)
        end if
      end select
    end do
    if (freq_sq == 1d0) then
      !  Frequency unset. Assume static.
      freq_sq = 0d0
    end if
    ! print "(2a,1x,a)", "Header read, sites ", trim(name(k1)), trim(name(k2)) 
    if (s1 .ne. 0 .and. k1 .ne. head(mol)+s1                            &
        .or. s2 .ne. 0 .and. k2 .ne. head(mol)+s2) then
      print "(a/i3,a/i3,a)", "Mismatch between site names and numbers", &
          s1, name(k1), s2, name(k2)
      call die("Likely program error",.true.)
    end if
    call register(k1,max1)
    call register(k2,max2)
  case default
    !  Assume old site-pair format, i.e.
    !  s1  s2  [rank]
    call reread(-1)
    call read_site(k1,mol)
    call read_site(k2,mol)
    if (item > nitems) then
      call readi(l_ab)
    else
      l_ab = 4
    end if
    min1 = 0; min2 = 0
    max1 = l_ab; max2 = l_ab
    call register(k1,max1)
    call register(k2,max2)
    !  Treat frequency as unset
    freq_sq = 1d0
  end select

  !  Read the matrix
  lmax = max(max1,max2)
  last = (lmax+1)**2
  call getix(k1,k2,lmax,ix)
  ! if (verbose) print "(i3,2x,a3,2x,i3,2x,a3, i5)",                      &
  !     k1, trim(name(k1)), k2, trim(name(k2)), ix
  alpha(:,:,ix) = 0d0
  ! call read_line(eof)
  do i = qsize(min1-1)+1,qsize(max1)
    do j = qsize(min2-1)+1,qsize(max2)
      call getf(alpha(i,j,ix))
    end do
  end do
  if (k1 .ne. k2) then
    call getix(k2,k1,max(max1,max2),ixt)
    alpha(1:last,1:last,ixt)=transpose(alpha(1:last,1:last,ix))
  end if
  ! la(ix) = min(lmax,limit)
  ! la(ixt) = min(lmax,limit)
  pr(k1) = max(max1,pr(k1))
  pz(k1) = min(max(pz(k1),qsize(pr(k1))),limsize)
  if (k2 .ne. k1) then
    pr(k2) = max(max2,pr(k2))
    pz(k2) = min(max(pz(k2),qsize(pr(k2))),limsize)
  end if
  !  End of this site-site polarizability matrix
  ! if (verbose) then
  !   do i = qsize(min1-1)+1,qsize(min(2,max1))
  !     print "(9f8.3)", (alpha(i,j,ix), j=qsize(min2-1)+1,qsize(min(2,max2)))
  !   end do
  ! end if
  
end do

if (datafile .ne. "") then
  close(unit)
  datafile=""
  call stream(5)
end if
switch(3)=.true.

if (verbose) then
  print "(a)", "Summary of polarizable sites as read:"
  k1 = head(mol)
  do while (k1 > 0)
    if (ps(k1) > 0) then
      print "(i3,1x,2a, i1, a,i1, a, i0)", k1, name(k1),                &
          " rank ", pr(k1), "  limit ", lq(pz(k1)), "  ps ", ps(k1)
    end if
    k1 = next(k1)
  end do
end if

END SUBROUTINE read_site_pairs

!-----------------------------------------------------------------------

SUBROUTINE getix(k1,k2,lmax,ix)

!  Find the index ix for the polarizability matrix for sites k1 and k2.
!  If no matrix has been assigned, assign one and return its index.

INTEGER, INTENT(IN) :: k1, k2, lmax
INTEGER, INTENT(OUT) :: ix

ix = lookup(indexa,k1,k2)
if (ix == 0) then
  lasta = lasta+1
  if (lasta > n_alpha)                                           &
      call die ('too many polarizabilities',.true.)
  call register(k1,lmax)
  call register(k2,lmax)
  call insert(indexa,k1,k2,lasta,'polarizability index')
  ! la(lasta) = lmax
  alpha(:,:,lasta) = 0d0
  ix = lookup(indexa,k1,k2)
  ixa(k1,k2) = ix
end if

END SUBROUTINE getix

!-----------------------------------------------------------------------

SUBROUTINE limit_rank

!  LIMIT [ALL] [RANK] limit [FOR [SITES [site-list]|TYPES [type-list]]]
!  
!    Limit the rank of polarizabilities for the sites or types specified
!    to the value given. The higher polarizabilities are not deleted, and
!    can be restored by a subsequent LIMIT command. If sites are listed,
!    all the cross-polarizabilities involving the named sites are limited.
!    If types are listed, all the cross-polarizabilities involving sites
!    of that type are limited. If ALL is specified, all sites are limited
!    and no site-list or type-list should be specified.


LOGICAL :: limit_sites=.false., limit_types=.false., limit_all=.false.
INTEGER :: k, t, lmax
CHARACTER(LEN=20) :: ww
CHARACTER(LEN=*), PARAMETER :: this_routine="limit_rank"

if (tracing) print "(2a)", "Entering ", this_routine
limit_sites = .false.
limit_types = .false.
limit_all = .false.
lmax = -1

do while (item<nitems)
  call reada(ww)
  select case(upcase(ww))
  case("RANK")
    call readi(lmax)
  case("ALL")
    limit_all = .true.
  case("FOR")
    if (lmax<0) call die ("Specify rank before site list in LIMIT",.true.)
    call readu(ww)
    select case(ww)
    case("SITE","SITES")
      if (limit_all .or. limit_types) call die                          &
          ("Contradictory options in POLARIZABILITY LIMIT command",.true.)
      limit_sites = .true.
      exit
    case("TYPE","TYPES")
      if (limit_all .or. limit_sites) call die                          &
          ("Contradictory options in POLARIZABILITY LIMIT command",.true.)
      limit_types = .true.
      exit
    case default
      call reread(-1)
      limit_sites = .true.
      exit
    end select
  case default
    if (lmax<0) then
      call reread(-1)
      call readi(lmax)
    else
      call die("Unexpected item "//trim(ww)//" in POLARIZABILITY LIMIT command",.true.)
    end if
  end select
end do

!  At this point we should either have limit_all set or there should
!  still be a list of sites or types in the input buffer
if (limit_all) then
  k = head(mol)
  do while (k > 0)
    if (pr(k) >= 0) then
      pz(k) = min(qsize(pr(k)),qsize(lmax))
    end if
  k = next(k)
  end do
else if (limit_sites) then
  do while (item < nitems)
    call reada(ww)
    k = head(mol)
    do while (k > 0)
      if (name(k) == ww .and. pr(k) >= 0) then
        pz(k) = min(qsize(pr(k)),qsize(lmax))
      end if
      k = next(k)
    end do
  end do
else if (limit_types) then
  do while (item<nitems)
    call reada(ww)
    !  If the type hasn't been defined, this is probably an error.
    t = gettype(ww,.true.)
    k = head(mol)
    do while (k>0)
      if (type(k) == t .and. pr(k) >= 0) then
        pz(k) = min(qsize(pr(k)),qsize(lmax))
      end if
      k = next(k)
    end do
  end do
endif

END SUBROUTINE limit_rank

!-----------------------------------------------------------------------

SUBROUTINE write_alphas

CHARACTER(LEN=80) :: datafile=""
CHARACTER(LEN=40) :: format="(25f20.12)"
CHARACTER(LEN=20) :: ww
CHARACTER(LEN=3) :: indx="---"
INTEGER :: i, i1, i2, ix, iw, k, k1, k2, n, flag, length, lmin, lmax,   &
    lmax1, lmax2, start, last, last1, last2
LOGICAL :: all, local, binary=.false.
DOUBLE PRECISION :: ignore=1d-8
CHARACTER(LEN=*), PARAMETER :: this_routine="write_alphas"

if (tracing) print "(2a)", "Entering ", this_routine
lmin = 0
lmax = maxla
local = .false.
binary = .false.
all = .true.
datafile = ""
! format="(25g16.8)"
do while (item < nitems)
  call reada(ww)
  ! print "(a)", trim(ww)
  select case(upcase(ww))
  case("FILE")
    call reada(datafile)
    ! if (string .ne. datafile) then
    !   datafile=string
    !   if (iw>0) close(iw)
    !   iw=0
    ! end if
  case("LIMIT")
    call readi(lmax)
    lmin = 0
  case("RANK","RANKS")
    call readi(lmin)
    call reada(ww)
    if (upcase(ww) .ne. "TO") call reread(-1)
    call readi(lmax)
  case("BINARY")
    binary=.true.
  case("ASCII","TXT","TEXT")
    binary=.false.
  case("FORMAT")
    binary=.false.
    call reada(format)
  case("LOCAL")
    local=.true.
  case("TEST","IGNORE")
    call readf(ignore)
  case("INDEX")
    call reada(indx)
  case("ALL")
    !  Redundant
    all = .true.
  case ("SITES")
    n=0
    do while (item < nitems)
      call read_site(k,mol)
      if (ps(k) > 0) then
        n = n+1
        list(n) = k
      else
        print "(a)", "Warning: site ", trim(name(k)), " is not polarizable."
      end if
    end do
    all = .false.
  case default
    if (datafile == "") then
      datafile = ww
    else
      call reread(-1)
      exit
    end if
  end select
end do

if (all) then
  k = head(mol)
  n = 0
  do while (k > 0)
    if (ps(k) > 0) then
      n = n+1
      list(n) = k
    end if
    k = next(k)
  end do
end if
if (n == 0) then
  print "(a)", "WARNING: no polarizable sites specified for POLARIZABILITY WRITE"
  return
end if
!  print '(20i4)', (list(k), k=1,n)

start = qsize(lmin-1) + 1
last = qsize(lmax)
!  Open output file for polarizabilities
iw = find_io(51)
if (binary) then
  ix = lookup(indexa,list(1),list(1))
  inquire(iolength=length) alpha(1,start:last,ix)
  length=max(length,32)
  open(iw,file=datafile,form="unformatted",recl=length,status="replace",iostat=flag)
  if (flag>0) then
    call die("Can't open file "//trim(datafile)//" for unformatted output",.true.)
  else
    print "(2a)", "Writing binary file ", trim(datafile)
  end if
else
  open(iw,file=datafile,form="formatted",status="replace",iostat=flag)
  if (flag>0) then
    call die("Can't open file "//trim(datafile)//" for output",.true.)
  else
    print "(2a)", "Writing ASCII file ", trim(datafile)
  end if
end if

do i1 = 1,n
  k1 = list(i1)
  last1 = min(last,pz(k1))
  lmax1 = ceiling(sqrt(float(pz(k1)))) - 1
  do i2 = 1,n
    k2 = list(i2)
    if (local .and. k2 .ne. k1) cycle
    ix = lookup(indexa,k1,k2)
    if (ix > 0) then
      last2 = min(last,pz(k2))
      lmax2 = ceiling(sqrt(float(pz(k2)))) - 1
      if (any(abs(alpha(start:last1,start:last2,ix)) > ignore)) then
        if (verbose) print "(4a)", "Sites ", trim(name(k1)), " and ", trim(name(k2))
        if (binary) then
          write (iw) i1, i2, lmin, lmax1, start, last2
          do i = start,last1
            write (iw) alpha(i,start:last2,ix)
          end do
        else
          write (iw,"(5(a,2x),2(a,1x,i0,1x))",advance="no") "ALPHA",    &
              trim(name(mol)), "SITE-NAMES", trim(name(k1)), trim(name(k2)), &
              "RANK", lmin, "TO", lmax1
          if (lmax2 .ne. lmax1) write(iw,"(2(a,1x,i0,1x))",advance="no") &
              "BY", lmin, "TO", lmax2 
          if (freq_index > -1) then
            write (iw,"(a,1x,i3,1x,a,f16.7)") "INDEX", freq_index, "FREQSQ", freq_sq
          else
            write (iw,"(1x)")
          end if
          do i = start,last1
            write (iw,fmt=format) alpha(i,start:last2,ix)
          end do
        end if
      end if
    end if
  end do
end do
if (.not. binary) write (iw,"(a)") "ENDFILE"
close(iw)
print "(a)", "Done"

END SUBROUTINE write_alphas

!-----------------------------------------------------------------------

SUBROUTINE print_alphas

!  (PRINT) [LIMIT r | RANKS r1 [to] r2 ] [ ALL | [SITES] site1 site2 ...]
!  Print polarizability matrices on standard output. The rank limit r1 is
!  0 or 1: that is, it specifies whether to include charge-flow
!  polarizabilities or not. The upper limit r2 is a maximum: polarizabilities
!  of higher ranks will not be printed, but if any site has a lower limit,
!  only polarizabilities up to that limit will be printed.

LOGICAL :: cross, local
DOUBLE PRECISION :: ignore=1d-8
INTEGER :: i1, i2, ix, j, k, k1, k2, lmin, lmax, last, n, start, size,  &
    size1, size2
CHARACTER(LEN=16) :: ww

lmin = 0
lmax = maxla
cross = .false.
local = .false.
ignore = 1d-8
n = 0
do while (item < nitems)
  call readu(ww)
  select case(ww)
  case("LIMIT")
    lmin = 0
    call readi(lmax)
  case("CROSS","NON-LOCAL")
    cross = .true.
  case ("LOCAL")
    local = .true.
  case("RANK","RANKS")
    call readi(lmin)
    call readu(ww)
    if (ww .ne. "TO") call reread(-1)
    call readi(lmax)
  case("TEST","IGNORE")
    call readf(ignore)
  case("ALL")
    k = head(mol)
    n = 0
    do while (k > 0)
      if (ps(k) > 0) then
        n = n+1
        list(n) = k
      end if
      k = next(k)
    end do
  case("SITE","SITES")
    exit
  case default
    !  Assume explicit list of sites
    call reread(-1)
    exit
  end select
end do
do while (item < nitems)
  call read_site(k,mol)
  n = n+1
  list(n) = k
end do
if (n == 0) then
  print "(a)", "WARNING: no sites specified for POLARIZABILITY PRINT"
  return
end if

start = qsize(lmin-1) + 1
size = qsize(lmax)
!  print '(20i4)', (list(k), k=1,n)
do i1 = 1,n
  k1 = list(i1)
  if (local .and. cross .or. (.not. local .and. .not. cross)) then
    j = i1
    last = n
  else if (local) then
    j = i1
    last = i1
  else if (cross) then
    j = i1+1
    last = n
  end if
  do i2 = j,last
    k2 = list(i2)
    if (k1 == k2) then
      print "(2a)", "Polarizabilities for site ", trim(name(k1))
    else
      print "(4a)", "Polarizabilities for sites ", trim(name(k1)), &
          " and ", trim(name(k2))
    endif
    ix = ixa(k1,k2)
    ! print "(a,i0,1x,i0,a,i0)", "Sites ", k1, k2, ", index ", ix
    if (ix == 0) then
      print "(8x,a)", "None"
    else
      size1 = min(pz(k1),size)
      size2 = min(pz(k2),size)
      if (all(abs(alpha(start:size1,start:size2,ix)) < ignore)) then
        print "(8x,a)", "None"
      else
        call print_polarizabilities(ix,start,size1,size2)
      end if
    end if
  end do
end do

END SUBROUTINE print_alphas

!-----------------------------------------------------------------------

SUBROUTINE localize(a,b,target,flow,only_flow,test)

!  Localize the cross-polarizabilities alpha(a,b) and alpha(b,a)
!  for molecule mol using the Le Sueur algorithm.
!  The localized polarizabilities are shared equally between the sites
!  listed in the array target. If this array is empty (size zero), the
!  multipoles are transferred between sites a and b as in Ruth's original
!  scheme.
!  If FLOW is .TRUE., charge-flow polarizabilities are also localized;
!  otherwise not. If ONLY_FLOW is true, only the charge-charge and
!  charge-multipole components are localised.
!  TEST is the value used to check that non-local polarizabilities have
!  been removed. Useful to change it from the default if rounding errors
!  might be a problem; e.g. for polarizabilities given to 4 decimal places,
!  use a value of say 3E-4.

!  NOTE particularly that all polarizabilities must be referred to the
!  same set of axes. If local polarizabilities referred to local axes
!  are required, localize first in global axes and then redefine the
!  axes using the EDIT routine.

USE parameters
USE indparams
USE global
USE indexes
USE labels
USE properties, ONLY: shiftq
USE sites
USE sizes
USE molecules
USE binomials

IMPLICIT NONE

INTEGER, INTENT(IN) :: a, b, target(:)
LOGICAL, INTENT(IN) :: flow, only_flow
DOUBLE PRECISION, INTENT(IN) :: test

DOUBLE PRECISION :: sab(3), sac(3), sba(3), sbc(3), sca(3), scb(3), w
INTEGER :: c, iaa, iab, iac, iba, ibb, ibc, ica, icb, i, last, n, t, u
LOGICAL:: ok
CHARACTER(LEN=*), PARAMETER :: this_routine="localize"

if (tracing) print "(2a)", "Entering ", this_routine
if (b .eq. a) return
iab=lookup(indexa,a,b)
if (iab .le. 0) return

iba=lookup(indexa,b,a)
iaa=lookup(indexa,a,a)
ibb=lookup(indexa,b,b)
last=max(pz(a),pz(b))

if (size(target) == 0) then
  if (print_level>0) print "(/2a/a/4a)",                               &
      "Cross-polarizabilities alpha(a,b) and alpha(b,a) ",             &
      "to be transferred", "to alpha(a,a) and alpha(b,b)",             &
      "a = ", trim(name(a)), ", b = ", trim(name(b))
  sab=sx(:,b)-sx(:,a)
  sba=-sab

  !  Charge-flows
  if (flow) then
    !  print "(/a)", "Transferring charge flows"
    !  Transfer w from alpha(1,1,ba) to alpha(aa)
    !  Transfer -w from alpha(1,1,bb) to alpha(ab)
    !  These transfers taken together preserve the sum rules.
    w=0.25d0*alpha(1,1,iab)
    call move_col(w,iba,1,1,iaa,sab)
    call move_col(-w,ibb,1,1,iab,sab)
    !  Similarly with each following pair.
    call move_row(w,iab,1,1,iaa,sab)
    call move_row(-w,ibb,1,1,iba,sab)
    call move_col(w,iab,1,1,ibb,sba)
    call move_col(-w,iaa,1,1,iba,sba)
    call move_row(w,iba,1,1,ibb,sba)
    call move_row(-w,iaa,1,1,iab,sba)
  end if

  !  Charge-multipole
  do t=2,last
    !  In this case the sum rules are preserved as long as we move e.g. w from
    !  alpha(1,t,ba) to alpha(aa) and -w from alpha(1,t,aa) to alpha(ba).
    w=0.5d0*alpha(1,t,iba)
    call move_col(w,iba,1,t,iaa,sab)
    call move_col(-w,iaa,1,t,iba,sba)
    call move_row(w,iab,t,1,iaa,sab)
    call move_row(-w,iaa,t,1,iab,sba)

    w=0.5d0*alpha(1,t,iab)
    call move_col(w,iab,1,t,ibb,sba)
    call move_col(-w,ibb,1,t,iab,sab)
    call move_row(w,iba,t,1,ibb,sba)
    call move_row(-w,ibb,t,1,iba,sab)

  end do

    !  Multipole-multipole
  if (.not. only_flow) then
    do t=2,last
      do u=2,last
        !  Transfer alpha(t,u,ab)/4 to from alpha(ab) to alpha(aa)
        !  Transfer with opposite sign from alpha(bb) to alpha(ba)
        w=0.25d0*alpha(t,u,iab)
        call move_row(w,iab,t,u,iaa,sab)
        call move_row(-w,iaa,t,u,iab,sba)
        call move_row(w,iba,u,t,ibb,sba)
        call move_row(-w,ibb,u,t,iba,sab)
        
        call move_col(w,iba,u,t,iaa,sab)
        call move_col(-w,iaa,u,t,iba,sba)
        call move_col(w,iab,t,u,ibb,sba)
        call move_col(-w,ibb,t,u,iab,sab)
      end do
    end do
  end if

else

!  Transfer alpha(a,b) and alpha(b,a), equally between the sites listed
!  in the array target.
  if (print_level>0) print "(/a/5a/a,6(1x,a))",                        &
      "Cross-polarizabilities alpha(a,b) and (alpha(b,a) ",            &
      "(a = ", trim(name(a)), ", b = ", trim(name(b)), ")",            &
      "to be transferred equally to sites",                            &
      (trim(name(target(i))), i=1,size(target))
  ! call die ("This option not yet implemented")
  n=size(target)
  do i=1,n
    c=target(i)
    iac=lookup(indexa,a,c)
    ibc=lookup(indexa,b,c)
    ica=lookup(indexa,c,a)
    icb=lookup(indexa,c,b)
    ! icc=lookup(indexa,c,c)
    sac=sx(:,c)-sx(:,a)
    sca=-sac
    sbc=sx(:,c)-sx(:,b)
    scb=-sbc

    !  Charge-flows
    if (flow) then
      !  Transfer w from alpha(1,1,ba) to alpha(ca)
      !  Transfer -w from alpha(1,1,bb) to alpha(cb)
      !  These transfers taken together preserve the sum rules.
      w=0.5d0*alpha(1,1,iab)/n
      call move_col(w,iba,1,1,ica,scb)
      call move_col(-w,ibb,1,1,icb,scb)
      !  Similarly with each following pair.
      call move_row(w,iab,1,1,iac,scb)
      call move_row(-w,ibb,1,1,ibc,scb)

      call move_col(w,iab,1,1,icb,sca)
      call move_col(-w,iaa,1,1,ica,sca)
      call move_row(w,iba,1,1,ibc,sca)
      call move_row(-w,iaa,1,1,iac,sca)
    end if

    !  Charge-multipole
    do t=2,last
      w=alpha(1,t,iba)/n
      call move_col(w,iba,1,t,ica,scb)
      call move_col(-w,ibb,1,t,icb,scb)
      call move_row(w,iab,t,1,iac,scb)
      call move_row(-w,ibb,t,1,ibc,scb)

      w=alpha(1,t,iab)/n
      call move_col(w,iab,1,t,icb,sca)
      call move_col(-w,iaa,1,t,ica,sca)
      call move_row(w,iba,t,1,ibc,sca)
      call move_row(-w,iaa,t,1,iac,sca)
    end do

    !  Multipole-multipole
    if (.not. only_flow) then
      do t=2,last
        do u=2,last
          !  Transfer alpha(t,u,ab)/2 to from alpha(ab) to alpha(cb)
          !  and from alpha(ab) to alpha(ac)
          w=0.5d0*alpha(t,u,iab)/n
          call move_col(w,iab,t,u,icb,sca)
          call move_row(w,iab,t,u,iac,scb)
          call move_row(w,iba,u,t,ibc,sca)
          call move_col(w,iba,u,t,ica,scb)
        end do
      end do
    end if

  end do

endif

!  Check that all elements of alpha(ab) and alpha(ba) are below
!  acceptable magnitude.
ok=.true.
if (flow) then
  do t=1,last
    call check(a,b,t,1,iab)
    call check(a,b,1,t,iab)
    call check(b,a,t,1,iba)
    call check(b,a,1,t,iba)
  end do
end if
if (.not. only_flow) then
  do t=2,last
    do u=2,last
      call check(a,b,t,u,iab)
      call check(b,a,u,t,iba)
    end do
  end do
end if

!  Also check to make sure that the sum rule is still obeyed after each
!  transfer between a pair of sites has been performed.
call sum_rule(.false.,test)

if (.not. only_flow) then
  if (flow) then
    !  Delete cross-polarizability entry and return tensor to free list
    !  (Note that the array LA is used for this list, as well as for the
    !  rank of polarizabilities.)
    call insert(indexa,a,b,0,'polarizability index')
    call insert(indexa,b,a,0,'polarizability index')
    la(iab)=freea
    la(iba)=iab
    freea=iba
  else
    !  Set rank of cross-polarizabilities to zero
    la(iab)=0
    la(iba)=0
  end if
end if
if (print_level>0) print "(a)", 'Completed.'

CONTAINS

SUBROUTINE move_col(w,iac,row,col,ibc,s)

INTEGER, INTENT(IN) :: iac, row, col, ibc
DOUBLE PRECISION, INTENT(IN) :: s(3), w

DOUBLE PRECISION :: work(sa)

!  Move w out of the specified element of the polarizability matrix
!  alpha(ac) into alpha(bc). s is the position of site a (the source) 
!  relative to site b (the target).

alpha(row,col,iac)=alpha(row,col,iac)-w
work=0d0
work(row)=w
call shiftq(work,0,pz(a),s(1),s(2),s(3),alpha(:,col,ibc),pz(b))

END SUBROUTINE move_col

SUBROUTINE move_row(w,ica,row,col,icb,s)

INTEGER, INTENT(IN) :: ica, row, col, icb
DOUBLE PRECISION, INTENT(IN) :: s(3), w

DOUBLE PRECISION :: work(sa)

!  Move w out of the specified element of the polarizability matrix
!  alpha(ca) into alpha(cb). s is the position of site a relative to site b.

alpha(row,col,ica)=alpha(row,col,ica)-w
work=0d0
work(col)=w
call shiftq(work,0,pz(a),s(1),s(2),s(3),alpha(row,:,icb),pz(b))

END SUBROUTINE move_row

SUBROUTINE check(a,b,t,u,iab)

INTEGER, INTENT(IN) :: a, b, t, u, iab

if (abs(alpha(t,u,iab)) .ge. test) then
  if (ok) then 
    print "(a)", "Failed to zero the following elements: "
    ok=.false.
  end if
  print "(4x,4(a,1x),2x,f14.6)",                                    &
      trim(name(a)),label(t),trim(name(b)),label(u),alpha(t,u,iab)
end if

END SUBROUTINE check

END SUBROUTINE localize

!-----------------------------------------------------------------------

SUBROUTINE print_polarizabilities(ix,start,size1,size2)

!  Print polarizability matrix ix

USE labels
USE output
USE sizes
IMPLICIT NONE
INTEGER, INTENT(IN) :: ix, start, size1, size2
CHARACTER(LEN=128) :: buffer

INTEGER :: i, j, m1, m2, p

if (all(abs(alpha(start:size1,start:size2,ix)) < 1d-6)) then
  print "(a)", "All smaller than 1e-6"
else
  m2 = start - 1
  do while (m2 < size2)
    m1=m2+1
    m2=min(m2+9,size2)
    print "(4x,9(8x,a))", label(m1:m2)
    p = 1
    do i = start,size1
      buffer = ""
      write (buffer,"(a)") label(i)
      p = 6
      do j=m1,m2
        if (abs(alpha(i,j,ix)) < 1d-6) then
          buffer(p:) = "     0"
        else if (abs(alpha(i,j,ix)) < 1000d0) then
          write (buffer(p:),"(f12.5)") alpha(i,j,ix)
        else
          write (buffer(p:),"(1pg12.4)") alpha(i,j,ix)
        end if
        p = p + 12
      end do
      print "(a)", trim(buffer)
    end do
  end do
end if

END SUBROUTINE print_polarizabilities

!-----------------------------------------------------------------------

SUBROUTINE sum_rule(fix,test)

!  Check the charge-flow polarizability sum rules for the current molecule
!  If fix is true, adjust the polarizabilities so that the sum-rules are
!  obeyed.
!  test is the criterion for the sums to be treated as acceptable.

!  The requirement is that the charge-flow distributed polarizabilities
!  for a molecule should satisfy
!    \sum_b \alpha^{ab}_{t0} = 0 and
!    \sum_b \alpha^{ba}_{0t} = 0 for all a and t.

IMPLICIT NONE

LOGICAL, INTENT(IN) :: fix
DOUBLE PRECISION, INTENT(IN) :: test

LOGICAL :: allok, ok
INTEGER :: ix, ixaa, kb, ka, t
DOUBLE PRECISION :: sum1(sa), sum2(sa)

allok = .true.
ka = head(mol)
do while (ka > 0)
  sum1(1:sa) = 0d0
  sum2(1:sa) = 0d0
  ixaa = lookup(indexa,ka,ka)
  kb = head(mol)
  do while (kb > 0)
    ix = lookup(indexa,ka,kb)
    if (ix > 0) then
      do t = 1,pz(ka)
        sum1(t) = sum1(t)+alpha(t,1,ix)
      end do
    endif
    ix = lookup(indexa,kb,ka)
    if (ix > 0) then
      do t = 1,pz(ka)
        sum2(t) = sum2(t)+alpha(1,t,ix)
      end do
    endif
    kb = next(kb)
  end do
  ok = .true.
  do t = 1,sa
    if (abs(sum1(t)) .ge. test .or. abs(sum2(t)) .ge. test) then
      if (ok) print "(/a,a)", "Sum rule failures for ", name(ka)
      ok = .false.
      allok = .false.
      print "(4x,a,3x,2f14.8)", label(t), sum1(t), sum2(t)
      if (fix) then
        if (t == 1) then
          alpha(t,1,ixaa) = alpha(t,1,ixaa)-0.5*(sum1(t)+sum2(t))
        else
          alpha(t,1,ixaa) = alpha(t,1,ixaa)-sum1(t)
          alpha(1,t,ixaa) = alpha(1,t,ixaa)-sum2(t)
        end if
      end if
    end if
  end do
  if (fix .and. .not. ok .and. abs(sum1(1)-sum2(1)) > test)         &
      print "(a)", "Sum rule failure could not be corrected"
  ka = next(ka)
end do
if (allok .and. print_level>0) print "(a,es9.2)",                      &
    "Sum rules all satisfied to within tolerance", test

END SUBROUTINE sum_rule

!-----------------------------------------------------------------------

SUBROUTINE total(n)

!  Evaluate the total polarizabilities for the molecule, keeping terms
!  up to rank n.

!  Uses movep so may not be correct.

INTEGER, INTENT(IN) :: n

INTEGER :: h, i, j, k, ix, ixh, nc

k=head(mol)
nc=0
do while (k>0)
  if (ps(k)>0) then
    nc=nc+1
    list(nc)=k
  end if
  k=next(k)
end do
do i=1,nc
  do j=i+1,nc
    call localize(list(i),list(j),list(1:0),.true.,.false.,test)
  end do
end do
h=head(mol)
ixh=lookup(indexa,h,h)
if (ixh .eq. 0) call getix(h,h,n,ixh)
k=next(h)
test=1d-4
do while (k>0)
  ix=lookup(indexa,k,k)
  if (ix .ne. 0) then
    call movep(k,k, h, 0,n)
  end if
  k=next(k)
end do

END SUBROUTINE total

!-----------------------------------------------------------------------

SUBROUTINE movep(a,b, c, m,n)

!  Not fully checked. Doesn't work correctly if charge-flows are present.

!  Move polarizabilities for sites A,B to site C.
!  Polarizabilities of ranks below M are left on the original site.
!  Polarizabilities above rank N on the new site are discarded.

USE properties

IMPLICIT NONE

INTEGER, INTENT(IN) :: a, b, c, m, n

DOUBLE PRECISION :: temp(sa), w(sa,sa), sca(3), scb(3)
INTEGER :: iab, icc, lab, t, t1, u, top, first(0:5)=(/1,2,5,10,17,26/)

iab = ixa(a,b)
if (iab == 0) call die('Invalid attempt to move polarizabilities', .true.)

icc = ixa(c,c)
sca = sx(:,a)-sx(:,c)
scb = sx(:,b)-sx(:,c)
lab = max(pr(a),pr(b))
top = qsize(lab)

!  Move the polarizabilities alpha(ab) to site c

if (m == 0 .and. (any(abs(alpha(1,1:top,iab)) > test)                    &
    .or. any(abs(alpha(1:top,1,iab)) > test))) then
  print "(6a)", "Attempting move from sites ", trim(name(a)), " and ", &
      trim(name(b)), " to site ", trim(name(c))
  do t = 1,top
    if (abs(alpha(t,1,iab)) > test) print "(a,i0,a,f12.7)",            &
        "alpha(ab)[",t,",1]  =  ", alpha(t,1,iab)
    if (abs(alpha(1,t,iab)) > test) print "(a,i0,a,f12.7)",            &
        "alpha(ab)[1,",t,"]  =  ", alpha(1,t,iab)
  end do
  call die("Can't move charge-flows -- use LOCALIZE",.true.)
end if

!  Clear temporary workspace
w(:,:) = 0d0
temp(:) = 0d0

!  Transform columns of source polarizability and store temporarily
t1 = first(m)
do u = 1,top
  if (u == first(m)) t1 = 1
  temp(t1:top) = alpha(t1:top,u,iab)
  alpha(t1:top,u,iab) = 0d0
  call shiftq(temp, 0,lab, sca(1), sca(2), sca(3), w(:,u),n)
end do

!  Transform rows and add to target
do t = 1,qsize(n)
  temp(1:top) = w(t,1:top)
  call shiftq(temp, 0,lab, scb(1), scb(2), scb(3), alpha(t,:,icc),n)
end do

! la(icc) = min(n,la(iab))
print "(5a,i0,5a)", "Polarizabilities alpha(",trim(name(a)),",",trim(name(b)),&
    ") of rank ", m, " and above moved to alpha(",                     &
    trim(name(c)),",",trim(name(c)),")"

if (all(abs(alpha(:,:,iab))<test)) then
  !  Delete polarizability entry and return tensor to free list.
  !  (Note that the array LA is used for this list, as well as for the
  !  rank of polarizabilities.)
  call insert(indexa,a,b,0,'polarizability index')
  la(iab) = freea
  freea = iab
endif

END SUBROUTINE movep

!-----------------------------------------------------------------------

SUBROUTINE total_dd(rank)

USE rotations, ONLY: wigner
!  Total dipole-dipole polarizability for the molecule
!  Should now be correct for polarizabilities (local or non-local)
!  referred to local axes.

INTEGER, INTENT(IN) :: rank
!  rank=1 to include polarizabilities up to dipole
!  rank=0 to include charge-flows only.

DOUBLE PRECISION :: a(3,3), w(1:4,1:4), d(1:4,1:4), alpha_mean, gamma
INTEGER :: i, j, ix, ka, kb

!  Polarizability components are in multipole order (0,z,x,y) while
!  coordinates are in order (x,y,z)
INTEGER :: map(3)=[3,4,2]

call axes(mol,.false.)
a = 0d0
ka = firstp(mol)
do while (ka > 0)
  kb = firstp(mol)
  do while (kb > 0)
    ix = lookup(indexa,ka,kb)
    !  print "(2a/(4f8.3))", name(ka), name(kb), alpha(1:4,1:4,ix)
    if (ix > 0) then
      call wigner(se(:,:,sm(ka)),d,1)
      w = matmul(d,alpha(1:4,1:4,ix))
      if (sm(kb).ne.sm(ka)) call wigner(se(:,:,sm(kb)),d,1)
      w = matmul(w,transpose(d))
      do i = 1,3
        do j = 1,3
          a(i,j) = a(i,j) + sx(i,ka)*sx(j,kb)*w(1,1)
          if (rank > 0) then
            a(i,j) = a(i,j)                                 &
                + sx(i,ka)*w(1,map(j)) + w(map(i),1)*sx(j,kb)           &
                + w(map(i),map(j))
          end if
        end do
      end do
    end if
    kb = nextp(kb)
  end do
  ka = nextp(ka)
end do

print "(/2a)", "Total dipole-dipole polarizability for molecule ",     &
    trim(name(head(mol)))
if (rank == 0) print "(a)", "including charge-flow polarizabilities only"
print "(a/(a,3f15.8))",                                                &
    "          x              y              z",                       &
    " x ", a(1,:), " y ", a(2,:), " z ", a(3,:)

alpha_mean = (a(1,1) + a(2,2) + a(3,3))/3d0

if (a(1,2) == 0 .and. a(2,3) == 0 .and. a(3,1) == 0   &
    .and. (abs(a(1,1) - a(2,2)) < 1d-6                          &
    .or. abs(a(2,2) - a(3,3)) < 1d-6                            & 
    .or. abs(a(3,3) - a(1,1)) < 1d-6)) then
  !  Linear or symmetric top
  if (abs(a(1,1) - a(2,2)) < 1d-6 .and. abs(a(2,2) - a(3,3)) < 1d-6) then
    gamma = 0d0
  else if (abs(a(1,1) - a(2,2)) < 1d-6) then
    gamma = a(3,3) - (a(1,1) + a(2,2))/2d0
  else if (abs(a(2,2) - a(3,3)) < 1d-6) then
    gamma = a(1,1) - (a(2,2) + a(3,3))/2d0
  else
    gamma = a(2,2) - (a(3,3) + a(1,1))/2d0
  end if
else
  gamma = sqrt(3d0*(a(1,2)**2 + a(2,3)**2 + a(3,1)**2)            &
      + 0.5d0 * ((a(1,1)-a(2,2))**2 + (a(2,2)-a(3,3))**2 + (a(3,3)-a(1,1))**2))
end if

print "(a,f10.5,a,f10.5,a)", "Mean polarizability = ", alpha_mean,      &
    "  anisotropy = ", gamma, " a.u."

END SUBROUTINE total_dd

!-----------------------------------------------------------------------

SUBROUTINE applequist

DOUBLE PRECISION,ALLOCATABLE :: A(:,:), B(:,:), alpha_inv(:,:)
INTEGER :: d, ix, lmin, lmax, m, n, p, q, ok

lmin=5
lmax=0
n=0
p=firstp(mol)
do while (p>0)
  n=n+1
  lmax=max(lmax,pr(p))
  lmin=min(lmin,pr(p))
  p=nextp(p)
end do
call assert (lmax == lmin,                                             &
    "APPLEQUIST: All polarizable sites must have the same rank.")
call assert(n>0,"APPLEQUIST: There are no polarizable sites.")

d=qsize(lmax)
m=n*d
allocate(a(m,m),b(m,m),alpha_inv(d,d),stat=ok)
call assert(ok==0,"APPLEQUIST: Can't allocate arrays")

m=0
p=firstp(mol)
do while (p>0)
  n=0
  q=firstp(mol)
  do while (q>0)
    ix=lookup(indexa,p,q)
    if (ix>0) then
      a(m+1:m+d,n+1:n+d)=alpha(1:d,1:d,ix)
    else
      call assert(p .ne. q, "APPLEQUIST: Zero block on diagonal")
      a(m+1:m+d,n+1:n+d)=0
    end if
    n=n+d
    q=nextp(q)
  end do
  m=m+d
  p=nextp(p)
end do

!  Construct B = matrix inverse of A

!  Loop through polarizable site pairs as above
!  For each pair,
!    If ix=lookup(indexa,p,q)=0, check to see that the p,q block of B
!        is (close to) zero.
!    If p=q, invert submatrix a(m+1:m+d,n+1,n+d), which will be the new
!        alpha(1:d,1:d,ix)
!    Otherwise, calculate T^pq_tu elements and compare with the p,q
!        block of B.

END SUBROUTINE applequist

!-----------------------------------------------------------------------

SUBROUTINE localize_lw(mol,max_rank,only_flow,no_flow)

!  Localize non-local polarizabilities using the Lillestolen-Wheatley
!  algorithm.
!  Arguments:
!  mol       The molecule to be treated.
!  max_rank  The maximum rank of polarizability to be handled
!  only_flow If true, just remove charge-flow polarizabilities and stop.
!  no_flow   If true, don't touch charge-flow polarizabilities

USE sites, ONLY : next, tops, sx
USE molecules, ONLY : head
USE utility, ONLY : matwrt
USE connect, ONLY : bond_matrix
USE labels
USE properties, ONLY : shiftq
USE sizes, ONLY : lq, qsize

IMPLICIT NONE
INTEGER, INTENT(IN) :: mol, max_rank
LOGICAL, INTENT(IN) :: only_flow, no_flow

INTERFACE
  SUBROUTINE hdiag (iegen,order,a,eivr,n)
  LOGICAL, INTENT(IN) :: order, iegen
  DOUBLE PRECISION :: a(:,:)
  DOUBLE PRECISION, INTENT(OUT) :: eivr(:,:)
  INTEGER, INTENT(IN) :: n
  END SUBROUTINE hdiag
END INTERFACE

INTEGER :: i, j, k, l, ka, kb, kc, kd, lt, last, n, nb, pab,            &
     pcb, pdb, pbc, pbd, t, u, map(tops), pam(tops), size
REAL(dp), ALLOCATABLE :: lambda(:), w(:,:), v(:,:), z(:,:), temp(:), umove(:)
INTEGER, ALLOCATABLE :: bm(:,:) 
DOUBLE PRECISION :: g, q, scd(3)
CHARACTER(LEN=8) :: fmt
LOGICAL :: ok
CHARACTER(LEN=*), PARAMETER :: this_routine="localize_lw"

if (tracing) print "(2a)", "Entering ", this_routine
call axes(mol,.false.)

!  Note that only polarizable sites are included in this process.

!  Construct the bond matrix bm for the polarizable sites. Zero except:
!  bm(a,b) = 1 if polarizable sites a and b are directly bonded
!  bm(a,a) = minus the number of bonds to a
call bond_matrix(mol,bm,n,nb,map,pam)
!  Mapping between sites and matrix rows: map(i) contains
!  the site number corresponding to row i of the matrix, and n contains
!  the dimension of the matrix (the number of polarizable sites).
!  pam(k) contains the row of the matrix associated with site k.
if (nb == 0) then
  !  No bonds found
  print "(a)", "No localization carried out -- polarizabilities unchanged"
  return
end if
if (verbose) then
  print "(/a)", "Bond matrix"
  write(fmt,"(a,i0,a)") "(", n, "i5)"
  print fmt, bm
end if

allocate (w(n,n),v(n,n),lambda(n))
!  Diagonalize
w = bm
call hdiag(.true.,.false.,w,v,n)

do i = 1,n
  lambda(i) = w(i,i)
end do

!  Check
!  do i = 1,n
!    w(1:n,i) = lambda(i)*v(1:n,i)
!  end do
!  w(1:n,1:n) = matmul(w(1:n,1:n),transpose(v(1:n,1:n)))
!  call matwrt(u(1:n,1:n),fmt="6f12.6")
if (verbose) then
  print "(/a)", "Eigenvalues and eigenvectors of bond matrix"
  call matwrtv(lambda,v,1,n,1,n,fmte="9f9.5",fmtc="9f9.5",ncols=8)
end if

allocate(z(n,n),temp(qsize(max_rank)),umove(n))
z = 0d0
do j = 1,n
  do k = 1,j
    do i = 1,n
      if (abs(lambda(i)) < 1d-4) cycle
      z(j,k) = z(j,k)+v(j,i)*v(k,i)/lambda(i)
    end do
    z(k,j) = z(j,k)
  end do
end do

if (verbose) then
  print "(/a)", "Z matrix"
  call matwrt(z,ncols=8,fmt="8f10.5")
end if

if (only_flow) then
  last = 1
else
  last = qsize(max_rank)
end if
if (only_flow .and. no_flow) then
  print "(a)", "Localise LW: contradictory options for charge-flows"
  stop
end if
do t = 1,last
  lt = lq(t)
  do u = t,qsize(max_rank)
    if (no_flow .and. u == 1) then
      !  t == 1 also. Omit this case.
      cycle
    end if
    !  Find nonlocal polarizabilities alpha^ab_tu to be removed. These are
    !  for fixed t, u, with u >= t. We want to remove the non-local tu
    !  components by shifting alpha^at 'multipoles' to neighbouring sites.

    v = 0d0
    do i = 1,n
      ka = map(i)
      do j = 1,n
        if (i == j) cycle
        kb = map(j)
        pab = lookup(indexa,ka,kb)
        if (pab > 0) then
          v(i,j) = alpha(t,u,pab)
        end if
      end do
    end do

    !  v now contains the array of unwanted tu components.
    if (verbose) print "(/a,2a4)", "Polarizabilities to remove for ", label(t), label(u)
    if (all(abs(v(:,:)) < test)) then
      if (verbose) print "(a)", "None"
      cycle  ! u
    end if
    if (verbose) call matwrt(v(1:n,1:n),fmt="8f10.5",ncols=8)

    if (t == u) then
      v(1:n,1:n) = 0.5d0*v(1:n,1:n)
    end if

    !  For each column (fixed b,tu in alpha^ab_tu), we move "at" components
    !  between sites. We transform to get the quantities to shift:
    temp = 0d0
    do i = 1,n        !  i <-> b
      kb = map(i)
      !  Adjust v(i,i) to ensure that the column sum is zero
      v(i,i) = v(i,i)-sum(v(1:n,i))
      umove(1:n) = matmul(z(1:n,1:n),v(1:n,i))
      if (verbose) then
        print "(/i2,8f9.4:/(2x,8f9.4))", i, umove(1:n)
        if (abs(sum(umove(1:n))).gt.ignore) print "(a,f9.4)",          &
            "Sum is non-zero: ", sum(umove(1:n))
      end if
      do k = 1,n
        kc = map(k)
        !  Transfers to neighbours of kc
        do l = k+1,n
          if (bm(k,l).ne.1) cycle  !  Nearest neighbours only
          kd = map(l)
          q = 0.5d0*(umove(l)-umove(k))
          if (abs(q) > test) then
            !  Shift q from alpha^cb_tu to alpha^db_tu
            call getix(kc,kb,1,pcb)
            call getix(kd,kb,1,pdb)
            scd = sx(:,kc)-sx(:,kd)
            temp = 0d0
            temp(t) = q
            if (verbose) print "(f9.4,10(1x,a))", q, "from ",          &
                trim(name(kc)), trim(name(kb)), trim(label(t)), trim(label(u)), "to ", &
                trim(name(kd)), trim(name(kb)), trim(label(t)), trim(label(u))
            call shiftq(temp,lt,lt, scd(1),scd(2),scd(3), alpha(:,u,pdb),max_rank)
            alpha(t,u,pcb) = alpha(t,u,pcb)-q
            !  Shift -q from alpha^cb_tu to alpha^db_tu
            if (verbose) print "(f9.4,10(1x,a))", -q, "from ",          &
                trim(name(kd)), trim(name(kb)), trim(label(t)), trim(label(u)), "to ", &
                trim(name(kc)), trim(name(kb)), trim(label(t)), trim(label(u))
            scd = -scd
            temp(t) = -q
            call shiftq(temp,lt,lt, scd(1),scd(2),scd(3), alpha(:,u,pcb),max_rank)
            alpha(t,u,pdb) = alpha(t,u,pdb)+q
            temp(t) = 0d0
          end if
        end do
      end do
    end do
    !  Similarly for each row (fixed b,ut in alpha^ba_ut). We can use the
    !  _columns_ of the v matrix again because of the symmetry
    !  alpha^ab_tu = alpha^ba_ut.
    !  tt = u
    !  uu = t
    temp = 0d0
    do i = 1,n
      kb = map(i)
      umove(1:n) = matmul(z(1:n,1:n),v(1:n,i))
      if (verbose) then
        print "(/i2,8f9.4:/(2x,8f9.4))", i, umove(1:n)
        if (abs(sum(umove(1:n))).gt.ignore) print "(a,f9.4)",          &
            "Sum is non-zero: ", sum(umove(1:n))
      end if
      do k = 1,n
        kc = map(k)
        !  Transfers to neighbours of kc
        do l = k+1,n
          if (bm(k,l).ne.1d0) cycle  !  Nearest neighbours only
          kd = map(l)
          q = 0.5d0*(umove(l)-umove(k))
          if (abs(q) > test) then
            !  Shift q from alpha^bc_tu to alpha^bd_tu
            call getix(kb,kc,1,pbc)
            call getix(kb,kd,1,pbd)
            scd = sx(:,kc)-sx(:,kd)
            temp = 0d0
            temp(t) = q
            if (verbose) print "(f9.4,10(1x,a))", q, "from ",         &
                trim(name(kb)), trim(name(kc)), trim(label(t)), trim(label(u)), "to ", &
                trim(name(kb)), trim(name(kd)), trim(label(t)), trim(label(u))
            call shiftq(temp,lt,lt, scd(1),scd(2),scd(3), alpha(u,:,pbd),max_rank)
            alpha(u,t,pbc) = alpha(u,t,pbc)-q
            !  Shift -q from alpha^bd_tu to alpha^bc_tu
            temp(t) = -q
            scd = -scd
            if (verbose) print "(f9.4,10(1x,a))", -q, "from ",         &
                trim(name(kb)), trim(name(kd)), trim(label(t)), trim(label(u)), "to ", &
                trim(name(kb)), trim(name(kc)), trim(label(t)), trim(label(u))
            call shiftq(temp,lt,lt, scd(1),scd(2),scd(3), alpha(u,:,pbc),max_rank)
            alpha(u,t,pbd) = alpha(u,t,pbd)+q
            temp(t) = 0d0
          end if
        end do
      end do
    end do
    if (verbose) then
      print "(/a,2a4)", "Polarizabilities remaining for ", label(t), label(u)
      v = 0d0
      do i = 1,n
        ka = map(i)
        do j = 1,n
          kb = map(j)
          pab = lookup(indexa,ka,kb)
          if (pab > 0) then
            v(i,j) = alpha(t,u,pab)
          end if
        end do
      end do
      call matwrt(v(1:n,1:n),fmt="8f10.5",ncols=8)
    end if
  end do
  if (verbose) then
    call total_dd(1)
  end if
end do

if (verbose) print "(a)", "Localization completed"

!  Check that no unacceptably large elements of the non-local polarizabilities
!  remain.
ok = .true.
size = qsize(max_rank)
ka = head(mol)
do while (ka > 0)
  kb = head(mol)
  do while (kb > 0)
    if (kb == ka) then
      kb = next(kb)
      cycle
    end if
    pab = lookup(indexa,ka,kb)
    if (pab .ne. 0) then
      g = maxval(abs(alpha(1:last,1:size,pab)))
      if (g > ignore) then
        ok = .false.
        print "(5a,1p,e7.1)", "alpha(", trim(name(ka)), ",",           &
            trim(name(kb)), ") still has elements larger than ", ignore
        do i = 1,size
          do j = 1,size
            if (abs(alpha(i,j,pab)) > ignore) then
              print "(5a,1p,e12.5)",                                   &
                  "alpha(", trim(label(i)), ",", trim(label(j)),       &
                  ") = ", alpha(i,j,pab)
            end if
          end do
        end do
      end if
    end if
    kb = next(kb)
  end do
  ka = next(ka)
end do
if (ok .and. verbose) then
  if (only_flow) then
    print "(a,i0,a,1p,e9.2)",                                          &
        "All charge-flow polarizabilities up to rank ", max_rank,      &
        " are smaller than ", ignore
  else
    print "(a,i0,a,1p,e9.2)",                                          &
        "All non-local polarizabilities up to rank ", max_rank,        &
        " are smaller than ", ignore
  end if
end if

deallocate(w,v,lambda,z,temp,umove,bm)

END SUBROUTINE localize_lw

!-----------------------------------------------------------------------

SUBROUTINE drude_alpha(m)

!  Generate a non-local dipole-dipole polarizability model from Drude
!  oscillator data.

!  NOTE that this routine generates polarizability tensors in spherical
!  (10,11c,11s) order, not in Cartesian (x,y,z) order.

!  (DRUDE already read) [version [parameter]]
!  site  q  [s]  (q is the Drude charge, s the spring constant, default 0.1)
!  ...
!  END

USE molecules, ONLY: mol
USE sites, ONLY: next, find_site, name, nsites
USE input, ONLY: read_line, item, nitems, reada, readf, upcase, assert, die

!  Molecule index
INTEGER, INTENT(IN) :: m
!  Thole damping version
!  CHARACTER(LEN=*), INTENT(IN) :: version

DOUBLE PRECISION, ALLOCATABLE :: B(:,:), A(:,:), alpha_drude(:),        &
    work(:), mu(:,:)
INTEGER, ALLOCATABLE :: ipiv(:)
DOUBLE PRECISION :: chg, spring, spring_default=0.1d0, damp_param
INTEGER :: i, j, k, k1, k2, p, p1, p2, nb, ns, ix, info, lw
LOGICAL :: eof
CHARACTER(LEN=12) :: w, version, inverter="DPOSV"
CHARACTER(LEN=6), ALLOCATABLE :: site_name(:)

version = ""
do while (item < nitems)
  !  (Note: DRUDE already read)
  call reada(w)
  print "(a)", w
  select case(upcase(w))
  case("SPRING","FORCE")
    call readf(spring_default)
  case("CONSTANT","CONST")
    !  Ignore
  case("INVERTER","INVERT","INVERSION")
    call readu(inverter)
  case default
    if (version == "") then
      version = w
      select case(upcase(version))
      case("THOLE-1")
        damp_param = 2.089d0
      case("THOLE-2")
        damp_param = 0.572d0
      case("YUMS11")
        damp_param = 2d0
      end select
    else
      call reread(-1)
      call readf(damp_param)
    end if
  end select
end do

allocate(alpha_drude(nsites), stat=info)
if (info > 0) call die("Can't allocate alpha_drude array",.true.)
alpha_drude = 0d0

ns = 0
data: do
  call read_line(eof)
  if (eof) call die("Unexpected end of input in drude_parser", .true.)
    call reada(w)
    select case(upcase(w))
    case("END")
      exit data
    case("", "!", "NOTE")
      cycle data
    case default
      !  Site drude parameters
      k = find_site(m,w)
      ns = ns + 1
      call readf(chg)
      call readf(spring)
      if (spring == 0d0) spring = spring_default
      alpha_drude(k) = chg**2 / spring
      print "(a,i0,a, f12.5)", "Site ", k, "  alpha ", alpha_drude(k)
    end select
end do data


allocate(B(3*ns,3*ns), A(3*ns,3*ns), ipiv(3*ns), work(6*ns),            &
    site_name(ns), mu(3,0:ns), stat=info)
if (info > 0) call die("Can't allocate B and A arrays in drude_parser",.true.)
B = 0d0

k1 = mol(m)%head
p1 = 0
ns = 0
do while (k1 > 0)
  if (alpha_drude(k1) > 0d0) then
    ns = ns + 1
    site_name(ns) = name(k1)
    call register(k1,1)
    k2 = mol(m)%head
    p2 = 0
    do while (k2 > 0)
      if (alpha_drude(k2) > 0d0) then
        if (k1 == k2) then
          do i = 1,3
            B(p1+i,p1+i) = 1d0/alpha_drude(k1)
          end do
        else
          call thole_T(version, damp_param, k1,k2,                                  &
              (alpha_drude(k1)*alpha_drude(k2))**(1d0/6d0),             &
              B(p1+1:p1+3,p2+1:p2+3))
          B(p1+1:p1+3,p2+1:p2+3) = -B(p1+1:p1+3,p2+1:p2+3)
        end if
        p2 = p2+3
      end if
      k2 = next(k2)
    end do
    p1 = p1 + 3
  end if
  k1 = next(k1)
end do

nb = p1
call assert(nb==3*ns,"Dimension mismatch in drude_parser")

if (verbose) then
  !  Print matrix B
  print "(/3a,f6.4)", "B matrix, damping version ", trim(version),      &
      ", parameter ", damp_param
  p1 = 0
  do i=1,ns
    p2 = 0
    do j=1,ns
      print "(/2a)", site_name(i), site_name(j)
      print "(3f12.5)", (B(p1+p,p2+1:p2+3), p=1,3)
      p2 = p2+3
    end do
    p1 = p1+3
  end do
end if

!  Invert matrix B
A = 0d0
do i = 1,nb
  A(i,i) = 1d0
end do

select case(inverter)
case("DPOSV")
  !  Version for real symmetric positive definite matrices
  call dposv("U",nb,nb, B,nb, A,nb, info)
  select case(info)
  case(0)
    !  Successful inversion
  case(-8:-1)
    print "(a,i0,a)", "Argument ", -info, " has an illegal value"
    call die("Stopping")
  case(1:)
    print "(a)", "Matrix is not positive definite"
    call die("Stopping")
  end select
case("DSYSV")
  !  Version for real symmetric matrices
  !  Find optimum size for work array
  lw = -1
  call dsysv("U", nb,nb, B,nb, ipiv, A,nb, work,lw, info)
  lw = work(1)
  if (verbose) print "(a,i0)", "Workspace size needed for DSYSV = ", lw
  if (lw > size(work)) then
    deallocate(work)
    allocate(work(lw))
  end if
  call dsysv("U", nb,nb, B,nb, ipiv, A,nb, work,lw, info)
  select case(info)
  case(0)
    !  Successful inversion
  case(-8:-1)
    print "(a,i0,a)", "Argument ", -info, " has an illegal value"
    call die("Stopping")
  case(1:)
    print "(a)", "Matrix is singular"
    call die("Stopping")
  end select
case default
  print "(2a)", "Unknown inversion routine ", trim(inverter)
  call die("Stopping")
end select

k1 = mol(m)%head
p1 = 0
i = 0
if (verbose) print "(/a)", "Non-local polarizabilities"
do while (k1 > 0)
  if (alpha_drude(k1) > 0d0) then
    i = i + 1
    k2 = mol(m)%head
    p2 = 0
    j = 0
    do while (k2 > 0)
      if (alpha_drude(k2) > 0d0) then
        j = j + 1
        call getix(k1,k2,1,ix)
        if (verbose) then
          print "(/a6,i3,i3,2x,1x,a6,i3,i3,2x, i7)",                     &
              name(k1), k1, ps(k1), name(k2), k2, ps(k2), ix
          print "(3f12.5)", (A(p1+p,p2+1:p2+3), p=1,3)
        end if
        alpha(:,:,ix) = 0d0
        alpha(2:4,2:4,ix) = A(p1+1:p1+3,p2+1:p2+3)
        p2 = p2 + 3
      end if
      k2 = next(k2)
    end do
    p1 = p1 + 3
  end if
  k1 = next(k1)
end do

!  Response to uniform field
print "(/a/a)", "Responses to uniform fields",                          &
    "             z           x           y"
mu = 0d0
ns = 0
k = mol(m)%head
do while (k > 0)
  if (alpha_drude(k) > 0d0) then
    ns = ns + 1
    do i = 1,3
      do p = i,nb,3
        mu(i,ns) = mu(i,ns) + A(3*(ns-1)+i,p)  !  i.e. A(3*ns+i,p)*1d0
      end do
    end do
    print "(a, 3f12.5)", site_name(ns), mu(:,ns)
    mu(:,0) = mu(:,0) + mu(:,ns)
  end if
  k = next(k)
end do
print "(/a, 3f12.5)", "Total ", mu(:,0)

deallocate(A, B, site_name, alpha_drude, mu)

END SUBROUTINE drude_alpha

!-----------------------------------------------------------------------

SUBROUTINE thole_T(version, damp_param, k1,k2,s,T)

  !  Set up Thole  dipole-dipole interaction tensor for sites k1 and k2.

USE sites, ONLY: sx

CHARACTER(LEN=*), INTENT(IN) :: version
INTEGER, INTENT(IN) :: k1, k2
DOUBLE PRECISION, INTENT(IN) :: damp_param, s
DOUBLE PRECISION, INTENT(OUT) :: T(3,3)

INTEGER :: i, j
DOUBLE PRECISION :: x(4), r, u, fe, ft, d1T, d2T, a, p
!  DOUBLE PRECISION, PARAMETER ::  a=0.572d0, p=2d0

!  Inter-site coordinates in order z,x,y
x(2:4) = sx(:,k2)-sx(:,k1)
x(1) = x(4)
r = sqrt(x(1)**2+x(2)**2+x(3)**2)
! print "(2i4, f12.5)", k1, k2, r
u = r/s

select case(upcase(version))
case("NONE")

  !  Undamped
  do i=1,3
    do j=1,3
      T(i,j) = 3d0*x(i)*x(j)/r**5
    end do
    T(i,i) = T(i,i) - 1d0/r**3
  end do

case("THOLE-1")

  !  Thole model 1
  a = damp_param
  ft = 1d0 - exp(-a*u)*(1 + a*u + 0.5*(a*u)**2 + (a*u)**3/6d0)
  fe = 1d0 - exp(-a*u)*(1 + a*u + 0.5d0*(a*u)**2)
  do i=1,3
    do j=1,3
      T(i,j) = 3d0*ft*x(i)*x(j)/r**5
    end do
    T(i,i) = T(i,i) - fe/r**3
  end do

case("THOLE-2")

  !  Thole model 2
  a = damp_param
  fe = 1d0 - exp(-a*u**3)
  ft = 1d0 - (1d0 + a*u**3)*exp(-a*u**3)
  ! print "(2i4, 3f12.5)", k1, k2, r, ft, fe
  do i=1,3
    do j=1,3
      T(i,j) = 3d0*ft*x(i)*x(j)/r**5
    end do
    T(i,i) = T(i,i) - fe/r**3
  end do

case("YUMS11")

  !  YuMS11 model
  p = damp_param
  d1T = - (1d0/r**2) * ( 1d0 - exp(-p*u)*(1d0 + p*u - 0.5d0*(p*u)**2) )
  d2T = (2d0/r**3) *                                                    &
      (1d0 - exp(-p*u) * (1d0 + p*u + 0.5d0*(p*u)**2 - 0.25d0*(p*u)**3) )
  do i = 1,3
    do j = 1,3
      T(i,j) = d2T*x(i)*x(j)/r**2 - d1T*x(i)*x(j)/r**3
    end do
    T(i,i) = T(i,i) - d1T/r
  end do

end select

END SUBROUTINE thole_T

END MODULE alphas
