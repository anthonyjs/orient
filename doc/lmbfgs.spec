!  This file contains the LBFGS algorithm and supporting routines

!  Converted to Fortran 90: AJS, July 2000.
!  Common block and block data replaced by initialized module variables.
!  Comment and continuation lines changed to F90 format.
!  Info argument added to LBFGS call so that it can be used by calling
!  routine.
!  IMPLICIT NONE added to all routines, and variables declared
!  explicitly as necessary.


      MODULE LMBFGS
      IMPLICIT NONE

!     COMMON /LB3/MP,LP,GTOL,STPMIN,STPMAX
      INTEGER :: MP=6, LP=0
      DOUBLE PRECISION :: GTOL=0.9, STPMIN=1d-20, STPMAX=1d+20
!  MP is used as the unit number for the printing of the monitoring
!  information controlled by IPRINT.

!  LP is used as the unit number for the printing of error messages.
!  This printing may be suppressed by setting LP to a non-positive value.

!  GTOL is a DOUBLE PRECISION variable with default value 0.9, which
!  controls the accuracy of the line search routine MCSRCH. If the
!  function and gradient evaluations are inexpensive with respect
!  to the cost of the iteration (which is sometimes the case when
!  solving very large problems) it may be advantageous to set GTOL
!  to a small value. A typical small value is 0.1.  Restriction:
!  GTOL should be greater than 1.D-04.

!  STPMIN and STPMAX are non-negative DOUBLE PRECISION variables which
!  specify lower and uper bounds for the step in the line search.
!  Their default values are 1.D-20 and 1.D+20, respectively. These
!  values need not be modified unless the exponents are too large
!  for the machine being used, or unless the problem is extremely
!  badly scaled (in which case the exponents should be increased).

      PRIVATE
      PUBLIC :: lbfgs, mp, lp, gtol

      CONTAINS

!  ****************
!  LBFGS SUBROUTINE
!  ****************

      SUBROUTINE LBFGS(N,M,X,F,G,DIAGCO,DIAG,IPRINT,EPS,XTOL,W,IFLAG,   &
          info,DGUESS)
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: N,M,IPRINT(2)
      INTEGER, INTENT(INOUT) :: IFLAG, info
      DOUBLE PRECISION, INTENT(INOUT) :: X(N),G(N),DIAG(N),W(:)
      DOUBLE PRECISION, INTENT(IN) :: F,EPS,XTOL,DGUESS
      LOGICAL, INTENT(IN) ::  DIAGCO

!  LIMITED MEMORY BFGS METHOD FOR LARGE SCALE OPTIMIZATION
!  JORGE NOCEDAL
!  *** July 1990 ***


!  This subroutine solves the unconstrained minimization problem

!  min F(x),    x= (x1,x2,...,xN),

!  using the limited memory BFGS method. The routine is especially
!  effective on problems involving a large number of variables. In
!  a typical iteration of this method an approximation Hk to the
!  inverse of the Hessian is obtained by applying M BFGS updates to
!  a diagonal matrix Hk0, using information from the previous M steps.
!  The user specifies the number M, which determines the amount of
!  storage required by the routine. The user may also provide the
!  diagonal matrices Hk0 if not satisfied with the default choice.
!  The algorithm is described in "On the limited memory BFGS method
!  for large scale optimization", by D. Liu and J. Nocedal,
!  Mathematical Programming B 45 (1989) 503-528.

!  The user is required to calculate the function value F and its
!  gradient G. In order to allow the user complete control over
!  these computations, reverse  communication is used. The routine
!  must be called repeatedly under the control of the parameter
!  IFLAG.

!  The steplength is determined at each iteration by means of the
!  line search routine MCVSRCH, which is a slight modification of
!  the routine CSRCH written by More' and Thuente.

!  The calling statement is

!  CALL LBFGS(N,M,X,F,G,DIAGCO,DIAG,IPRINT,EPS,XTOL,W,IFLAG)

!  where

!  N       is an INTEGER variable that must be set by the user to the
!          number of variables. It is not altered by the routine.
!          Restriction: N>0.

!  M       is an INTEGER variable that must be set by the user to
!          the number of corrections used in the BFGS update. It
!          is not altered by the routine. Values of M less than 3 are
!          not recommended; large values of M will result in excessive
!          computing time. 3<= M <=7 is recommended. Restriction: M>0.

!  X       is a DOUBLE PRECISION array of length N. On initial entry
!          it must be set by the user to the values of the initial
!          estimate of the solution vector. On exit with IFLAG=0, it
!          contains the values of the variables at the best point
!          found (usually a solution).

!  F       is a DOUBLE PRECISION variable. Before initial entry and on
!          a re-entry with IFLAG=1, it must be set by the user to
!          contain the value of the function F at the point X.

!  G       is a DOUBLE PRECISION array of length N. Before initial
!          entry and on a re-entry with IFLAG=1, it must be set by
!          the user to contain the components of the gradient G at
!          the point X.

!  DIAGCO  is a LOGICAL variable that must be set to .TRUE. if the
!          user  wishes to provide the diagonal matrix Hk0 at each
!          iteration. Otherwise it should be set to .FALSE., in which
!          case  LBFGS will use a default value described below. If
!          DIAGCO is set to .TRUE. the routine will return at each
!          iteration of the algorithm with IFLAG=2, and the diagonal
!          matrix Hk0  must be provided in the array DIAG.


!  DIAG    is a DOUBLE PRECISION array of length N. If DIAGCO=.TRUE.,
!          then on initial entry or on re-entry with IFLAG=2, DIAG
!          it must be set by the user to contain the values of the
!          diagonal matrix Hk0.  Restriction: all elements of DIAG
!          must be positive.

!  DGUESS  is used for all diagonal elements of Hk0 if DIAGCO is .TRUE.

!  IPRINT  is an INTEGER array of length two which must be set by the
!          user.

!  IPRINT(1) specifies the frequency of the output:
!  IPRINT(1) < 0 : no output is generated,
!  IPRINT(1) = 0 : output only at first and last iteration,
!  IPRINT(1) > 0 : output every IPRINT(1) iterations.

!  IPRINT(2) specifies the type of output generated:
!  IPRINT(2) = 0 : iteration count, number of function
!                  evaluations, function value, norm of the
!                  gradient, and steplength,
!  IPRINT(2) = 1 : same as IPRINT(2)=0, plus vector of
!                  variables and  gradient vector at the
!                  initial point,
!  IPRINT(2) = 2 : same as IPRINT(2)=1, plus vector of
!                  variables,
!  IPRINT(2) = 3 : same as IPRINT(2)=2, plus gradient vector.


!  EPS     is a positive DOUBLE PRECISION variable that must be set by
!          the user, and determines the accuracy with which the solution
!          is to be found. The subroutine terminates when

!  ||G|| < EPS max(1,||X||),
!  where ||.|| denotes the Euclidean norm.

!  Changed to RMS < EPS by DJW.

!  XTOL    is a  positive DOUBLE PRECISION variable that must be set by
!          the user to an estimate of the machine precision (e.g.
!          10**(-16) on a SUN station 3/60). The line search routine will
!          terminate if the relative width of the interval of uncertainty
!          is less than XTOL.

!  W       is a DOUBLE PRECISION array of length N(2M+1)+2M used as
!          workspace for LBFGS. This array must not be altered by the
!          user.

!  IFLAG   is an INTEGER variable that must be set to 0 on initial entry
!          to the subroutine. A return with IFLAG<0 indicates an error,
!          and IFLAG=0 indicates that the routine has terminated without
!          detecting errors. On a return with IFLAG=1, the user must
!          evaluate the function F and gradient G. On a return with
!          IFLAG=2, the user must provide the diagonal matrix Hk0.

!          The following negative values of IFLAG, detecting an error,
!          are possible:

!  IFLAG=-1  The line search routine MCSRCH failed. The
!            parameter INFO provides more detailed information
!            (see also the documentation of MCSRCH):

!  INFO = 0  Improper input parameters.

!  INFO = 2  Relative width of the interval of
!            uncertainty is at most XTOL.

!  INFO = 3  More than 20 function evaluations were
!            required at the present iteration.

!  INFO = 4  The step is too small.

!  INFO = 5  The step is too large.

!  INFO = 6  Rounding errors prevent further progress.
!            There may not be a step which satisfies
!            the sufficient decrease and curvature
!            conditions. Tolerances may be too small.

!  INFO = 7  The search direction is not a descent direction.

!  IFLAG=-2  The i-th diagonal element of the diagonal inverse
!            Hessian approximation, given in DIAG, is not
!            positive.

!  IFLAG=-3  Improper input parameters for LBFGS (N or M are
!            not positive).



!  ON THE DRIVER:

!  The program that calls LBFGS must contain the declaration:

!     USE lmbgfs

!  MACHINE DEPENDENCIES

!  The only variables that are machine-dependent are XTOL,
!  STPMIN and STPMAX.


!  GENERAL INFORMATION

!  Other routines called directly:  DAXPY, DDOT, LB1, MCSRCH

!  Input/Output  :  No input; diagnostic messages on unit MP and
!  error messages on unit LP.

