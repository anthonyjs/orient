!  $Id: slice.F90,v 1.2 2005/06/13 15:36:47 anthony Exp $	
!  Matt's SLICE routine for examining a slice through a potential surface
SUBROUTINE slice

USE consts
USE force
USE input
USE molecules
USE switches
USE variables
#ifdef NAGF95
USE f90_unix_io, ONLY: flush
#endif

IMPLICIT NONE 

CHARACTER (len=20) :: word,slice_file,slice_grid
CHARACTER (len=12), DIMENSION(2) :: val_name,val_typen
!     CHARACTER (len=2) ,DIMENSION(2) :: val_unit

DOUBLE PRECISION, DIMENSION(2) :: val_min,val_max,val_step,parm,val_fact

DOUBLE PRECISION :: es,er,eind,edisp,etotal

INTEGER :: i
INTEGER, DIMENSION(2) :: val_ind
INTEGER :: novals=0,noindit

LOGICAL :: eof

WRITE(6,'(/a)') 'Examining slice through the PES:'

! Preset default values

if(switch(7)) then  ! switch induce on
  if(switch(8)) then ! switch iterate on
    noindit=100       ! iterate until convergence
  else
    noindit=1
  end if
else
  noindit=0
end if

slice_file=""
slice_grid=""
novals=0

! Read input

input_loop: do
  call read_line(eof)
  if (eof) call die ("Unexpected end of data file",.false.)
  do while (item .lt. nitems)
    call readu(word)
    select case (word)
    case("END")
      exit input_loop
    case("FILE")
      call reada(slice_file)
    case("GRID")
      call reada(slice_grid)
    case("FROM","TO","STEP")
      if (novals .eq. 0) then
        call die ("Specify variable name before range",.true.)
      else
        select case (word)
        case("FROM")
          call readf(val_min(novals))
        case("TO")
          call readf(val_max(novals))
        case("STEP")
          call readf(val_step(novals))
        end select
      endif
    case default
      novals=novals+1
      call reread(-1)
      call reada(val_name(novals))
      val_ind(novals)=0
      do i=1,nvar
        if(val_name(novals).eq.vname(i)) then
          val_ind(novals)=i
          exit
        end if
      end do
      if(val_ind(novals) .eq. 0) then
        call die("Variable "//trim(val_name(novals))//" not found",.true.)
      end if
    end select
  end do
end do input_loop

if(novals.eq.0) then
  call die("No variables specified",.false.)
end if

! Check whether variables are angles or distances, and convert to
! internal units

do i=1,novals
  if(vtype(val_ind(i)).eq.1) then ! value is a distance
    val_fact(i)=rfact
    val_typen(i)='    Distance'
  else if(vtype(val_ind(i)).eq.2) then ! value is an angle
    val_fact(i)=afact
    val_typen(i)='    Angle'
  else
    val_fact(i)=1d0
    val_typen(i)=' Unspecified'
  end if
  val_min(i)=val_min(i)/val_fact(i)
  val_max(i)=val_max(i)/val_fact(i)
  val_step(i)=val_step(i)/val_fact(i)
end do


write(6,'(/a/)') &
    'Index   Parameter          From         To         Step      Type'

do i=1,novals
  write(6,'(3x,i2,3x,a12,3f12.3,a/)')                             &
      val_ind(i),val_name(i),val_min(i)*val_fact(i),              &
      val_max(i)*val_fact(i),val_step(i)*val_fact(i),val_typen(i)
end do

! Sanity checks:-

if(novals.eq.2) then
  if(val_name(1).eq.val_name(2)) then
    call die("Choose _different_ variables",.false.)
  end if
end if


do i=1,novals

  if(val_step(i).eq.0.0) then
    write(6,'(/a,i2,a/)') "Index ",val_ind(i),":"
    call die("choose _non-zero_ step sizes",.false.)
  end if

  if(((val_max(i)-val_min(i))/val_step(i)).lt.0.0) then
    val_step(i)=-val_step(i)
    write(6,'(/a,i2)') "Index ",val_ind(i),": sign of step changed"
  end if

  if(val_max(i).eq.val_min(i)) then
    write(6,'(/a,i2)') "index ",val_ind(i),":"
    call die("Choose different values for max and min",.false.)
  end if

  val_max(i)=val_max(i)+0.01*val_step(i)

end do



if (slice_file .ne. "") open(23,file=slice_file,status='unknown')
if (slice_grid .ne. "") open(24,file=slice_grid,status='unknown')

parm(1)=val_min(1)

select case(novals)

case(1)

  write(6,'(2x,a14,2a/)') val_name(1),'  Energy / ',eunit

  !! Loop over first parameter

  do

    if (parm(1) .gt. val_max(1) .and. val_step(1) .gt. 0d0 .or.  &
        parm(1) .lt. val_max(1) .and. val_step(1) .lt. 0d0) exit

    value(val_ind(1))=parm(1)

    do i=1,mols
      calc(i)=.false.
      call axes(i,.false.)
    end do

    call forces(0,noindit,es,er,eind,edisp,etotal)

    write(6,'(f12.3,f20.8)') parm(1)*val_fact(1),etotal*efact
    if (slice_file .ne. "") write(23,'(f12.3,f20.8)')                 &
        parm(1)*val_fact(1),etotal*efact
    if (slice_grid .ne. "") write(24,'(f14.8)') etotal*efact

    call flush(6)

    parm(1)=parm(1)+val_step(1)

  end do

case(2)

  write(6,'(2x,2a16,2a/)') val_name(1),val_name(2),'  Energy / ',eunit

  !! Loop over first parameter

  do

    if (parm(1) .gt. val_max(1) .and. val_step(1) .gt. 0d0 .or.  &
        parm(1) .lt. val_max(1) .and. val_step(1) .lt. 0d0) exit

    value(val_ind(1))=parm(1)

    parm(2)=val_min(2)

    !! loop over second parameter

    do

      if (parm(2) .gt. val_max(2) .and. val_max(2) .gt. 0d0 .or. &
          parm(2) .lt. val_max(2) .and. val_max(2) .lt. 0d0) exit

      value(val_ind(2))=parm(2)

      do i=1,mols
        calc(i)=.false.
        call axes(i,.false.)
      end do

      call forces(0,noindit,es,er,eind,edisp,etotal)

      write(6,'(3f16.3)')                                        &
          parm(1)*val_fact(1), parm(2)*val_fact(2), etotal*efact
      if (slice_file .ne. "") write(23,'(3f16.3)')               &
          parm(1)*val_fact(1),parm(2)*val_fact(2),etotal*efact
      if (slice_grid .ne. "") write(24,'(f16.3)',advance="no")   &
          etotal*efact

      parm(2)=parm(2)+val_step(2)

    end do

    if (slice_grid .ne. "") write(24,'()',advance='yes')                  ! advance the output

    call flush(6)

    parm(1)=parm(1)+val_step(1)

  end do

end select

if (slice_grid .ne. "") close(23)
if (slice_grid .ne. "") close(24)

END SUBROUTINE slice
