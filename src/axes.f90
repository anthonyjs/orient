SUBROUTINE axes (m,prt)

!  Recalculate the position and orientation of the sites in molecule m.
!  Print details if prt=.true.

USE consts
USE switches
USE sites
USE molecules
USE rotations, ONLY : rotmat, angleaxis
USE variables, ONLY : vvalue
IMPLICIT NONE

INTEGER, INTENT(IN) :: m
LOGICAL, INTENT(IN) :: prt
DOUBLE PRECISION :: x(3), e(3,3), f(3,3), r, theta, phi,         &
    cosa, sina, cosb, sinb, cosc, sinc
INTEGER :: flag, i, j, lk, k

!  Have we done this already?
if (fixed(m) .and. calc(m)) return

k=head(m)
do while (k > 0)
  if (prt) then
    if (k == head(m)) then
      print '(/a,i2,3x,a)', 'Molecule ', m, name(k)
    else
      print '(a,i4,3x,a)', 'Site', k, name(k)
    end if
  end if
  lk=link(k)
  ! print "(a,i0,a,a,a,i0)", "Site ", k, " (", trim(name(k)), ") linked to ", lk
  if (.not. calc(m) .or. iand(sf(k),64+4) .gt. 0) then
    if (iand(sf(k),16) .gt. 0) then
      !  Position is in polar coordinates
      r=vvalue(sp(1,k))
      theta=vvalue(sp(2,k))
      phi=vvalue(sp(3,k))
      x(1)=r*sin(theta)*cos(phi)
      x(2)=r*sin(theta)*sin(phi)
      x(3)=r*cos(theta)
    else
      !  Position is in cartesian coordinates
      x(1)=vvalue(sp(1,k))
      x(2)=vvalue(sp(2,k))
      x(3)=vvalue(sp(3,k))
    endif
    !  Position is relative to site lk
    do i=1,3
      sx(i,k)=sx(i,lk)
      do j=1,3
        sx(i,k)=sx(i,k)+se(i,j,sm(lk))*x(j)
      end do
    end do
    ! print "(2i3, 3(3x,3f8.4))", k, lk, sx(:,lk), x, sx(:,k)
  endif

  if (prt) print '(3(a,1x,f10.5,1x))',                            &
      'position:    x =', rfact*sx(1,k), '    y =', rfact*sx(2,k),&
      '    z =', rfact*sx(3,k)
  if (debug(7)) print '(i3,3x,a,3(1x,f10.5))',                    &
      k, name(k), (rfact*sx(i,k), i=1,3)

  if (.not. calc(m) .or. iand(sf(k),8) .gt. 0) then
    !  Rotation is variable, or it's fixed but we haven't calculated it yet.
    flag=iand(sf(k),7)
    if (flag .eq. 0) then
      !  Same orientation as parent site lk.
      if (lk==0) then
        !  Use global axes
        se(:,:,k)=se(:,:,0)
        sm(k)=k
      else
        sm(k)=sm(lk)
        if (prt) print '(a,i4)', 'Local axes as for site', lk
      end if
    else
      !  Evaluate twist matrix
      if (iand(flag,4) .gt. 0) then
        !  Twist matrix is set up in local coordinate system of parent,
        !  so the axis of rotation is the vector X already evaluated.
        call rotmat(vvalue(st(k)),x, f)
        e=matmul(se(:,:,sm(lk)),f)
      else
        e(:,:)=se(:,:,sm(lk))
      endif

      flag=iand(flag,3)
      if (flag .eq. 1) then
        !  Orientation described by Euler angles
        !  if (prt) print "(a, 3f9.3)", "Euler angles",                   &
        !      (vvalue(sr(i,k))*afact, i=1,3)
        cosa=cos(vvalue(sr(1,k)))
        sina=sin(vvalue(sr(1,k)))
        cosb=cos(vvalue(sr(2,k)))
        sinb=sin(vvalue(sr(2,k)))
        cosc=cos(vvalue(sr(3,k)))
        sinc=sin(vvalue(sr(3,k)))
        f(1,1)=cosa*cosb*cosc-sina*sinc
        f(2,1)=sina*cosb*cosc+cosa*sinc
        f(3,1)=-sinb*cosc
        f(1,2)=-cosa*cosb*sinc-sina*cosc
        f(2,2)=-sina*cosb*sinc+cosa*cosc
        f(3,2)=sinb*sinc
        f(1,3)=cosa*sinb
        f(2,3)=sina*sinb
        f(3,3)=cosb
      else if (flag .eq. 2) then
        !  Orientation described as angle-axis
        if (prt) print "(a,1x,f10.5, a, 3(1x,f10.5))", "Rotated by",   &
            vvalue(sr(1,k))*afact, " about ", (vvalue(sr(i,k)), i=2,4)
        do i=1,3
          x(i)=vvalue(sr(i+1,k))
        end do
        call rotmat(vvalue(sr(1,k)),x, f)
      endif
      if (flag .gt. 0) then
        se(:,:,k)=matmul(e,f)
      else
        se(:,:,k)=e(:,:)
      endif
      sm(k)=k
      if (prt) write (6,1002) ((se(j,i,k), i=1,3), j=1,3)
1002  format ('    Local axes:     x         y         z'/             &
              'Global axes:   X', 3F10.5/                              &
              '               Y', 3F10.5/                              &
              '               Z', 3F10.5)
    endif
  endif
  if (k==head(m)) then
    call angleaxis(se(:,:,k),mol(m)%p)
  end if
  k=next(k)
end do
calc(m)=.true.

END SUBROUTINE axes
