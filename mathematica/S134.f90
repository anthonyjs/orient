
  case(80)
    !  80 (80)   10  30  4
    S0(ix) = (3d0*czz*(-1d0 + 5d0*rbz**2) + 5d0*raz*rbz*(-3d0 +         &
        7d0*rbz**2))/8.d0
    if (level > 1) then
      S1(3,ix) = (5d0*rbz*(-3d0 + 7d0*rbz**2))/8.d0
      S1(6,ix) = (30d0*czz*rbz + 70d0*raz*rbz**2 + 5d0*raz*(-3d0 +      &
          7d0*rbz**2))/8.d0
      S1(15,ix) = (3d0*(-1d0 + 5d0*rbz**2))/8.d0
      if (level > 2) then
        S2(6,3,ix) = (70d0*rbz**2 + 5d0*(-3d0 + 7d0*rbz**2))/8.d0
        S2(6,6,ix) = (30d0*czz + 210d0*raz*rbz)/8.d0
        S2(15,6,ix) = (15d0*rbz)/4.d0
      end if
    end if

  case(81)
    !  81 (81)   10  31c 4
    S0(ix) = (Sqrt(1.5d0)*(czx*(-1d0 + 5d0*rbz**2) +                    &
        5d0*rbx*(2d0*czz*rbz + raz*(-1d0 + 7d0*rbz**2))))/8.d0
    if (level > 1) then
      S1(3,ix) = (5d0*Sqrt(1.5d0)*rbx*(-1d0 + 7d0*rbz**2))/8.d0
      S1(4,ix) = (5d0*Sqrt(1.5d0)*(2d0*czz*rbz + raz*(-1d0 +            &
          7d0*rbz**2)))/8.d0
      S1(6,ix) = (Sqrt(1.5d0)*(10d0*czx*rbz + 5d0*rbx*(2d0*czz +        &
          14d0*raz*rbz)))/8.d0
      S1(13,ix) = (Sqrt(1.5d0)*(-1d0 + 5d0*rbz**2))/8.d0
      S1(15,ix) = (5d0*Sqrt(1.5d0)*rbx*rbz)/4.d0
      if (level > 2) then
        S2(4,3,ix) = (5d0*Sqrt(1.5d0)*(-1d0 + 7d0*rbz**2))/8.d0
        S2(6,3,ix) = (35d0*Sqrt(1.5d0)*rbx*rbz)/4.d0
        S2(6,4,ix) = (5d0*Sqrt(1.5d0)*(2d0*czz + 14d0*raz*rbz))/8.d0
        S2(6,6,ix) = (Sqrt(1.5d0)*(10d0*czx + 70d0*raz*rbx))/8.d0
        S2(13,6,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(15,4,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(15,6,ix) = (5d0*Sqrt(1.5d0)*rbx)/4.d0
      end if
    end if

  case(82)
    !  82 (82)   10  31s 4
    S0(ix) = (Sqrt(1.5d0)*(czy*(-1d0 + 5d0*rbz**2) +                    &
        5d0*rby*(2d0*czz*rbz + raz*(-1d0 + 7d0*rbz**2))))/8.d0
    if (level > 1) then
      S1(3,ix) = (5d0*Sqrt(1.5d0)*rby*(-1d0 + 7d0*rbz**2))/8.d0
      S1(5,ix) = (5d0*Sqrt(1.5d0)*(2d0*czz*rbz + raz*(-1d0 +            &
          7d0*rbz**2)))/8.d0
      S1(6,ix) = (Sqrt(1.5d0)*(10d0*czy*rbz + 5d0*rby*(2d0*czz +        &
          14d0*raz*rbz)))/8.d0
      S1(14,ix) = (Sqrt(1.5d0)*(-1d0 + 5d0*rbz**2))/8.d0
      S1(15,ix) = (5d0*Sqrt(1.5d0)*rby*rbz)/4.d0
      if (level > 2) then
        S2(5,3,ix) = (5d0*Sqrt(1.5d0)*(-1d0 + 7d0*rbz**2))/8.d0
        S2(6,3,ix) = (35d0*Sqrt(1.5d0)*rby*rbz)/4.d0
        S2(6,5,ix) = (5d0*Sqrt(1.5d0)*(2d0*czz + 14d0*raz*rbz))/8.d0
        S2(6,6,ix) = (Sqrt(1.5d0)*(10d0*czy + 70d0*raz*rby))/8.d0
        S2(14,6,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(15,5,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(15,6,ix) = (5d0*Sqrt(1.5d0)*rby)/4.d0
      end if
    end if

  case(83)
    !  83 (83)   10  32c 4
    S0(ix) = (rt15*(czz*(rbx**2 - rby**2) + (2d0*czx*rbx - 2d0*czy*rby  &
        + 7d0*raz*(rbx**2 - rby**2))*rbz))/8.d0
    if (level > 1) then
      S1(3,ix) = (7d0*rt15*(rbx**2 - rby**2)*rbz)/8.d0
      S1(4,ix) = (rt15*(2d0*czz*rbx + (2d0*czx +                        &
          14d0*raz*rbx)*rbz))/8.d0
      S1(5,ix) = (rt15*(-2d0*czz*rby + (-2d0*czy -                      &
          14d0*raz*rby)*rbz))/8.d0
      S1(6,ix) = (rt15*(2d0*czx*rbx - 2d0*czy*rby + 7d0*raz*(rbx**2 -   &
          rby**2)))/8.d0
      S1(13,ix) = (rt15*rbx*rbz)/4.d0
      S1(14,ix) = -(rt15*rby*rbz)/4.d0
      S1(15,ix) = (rt15*(rbx**2 - rby**2))/8.d0
      if (level > 2) then
        S2(4,3,ix) = (7d0*rt15*rbx*rbz)/4.d0
        S2(4,4,ix) = (rt15*(2d0*czz + 14d0*raz*rbz))/8.d0
        S2(5,3,ix) = (-7d0*rt15*rby*rbz)/4.d0
        S2(5,5,ix) = (rt15*(-2d0*czz - 14d0*raz*rbz))/8.d0
        S2(6,3,ix) = (7d0*rt15*(rbx**2 - rby**2))/8.d0
        S2(6,4,ix) = (rt15*(2d0*czx + 14d0*raz*rbx))/8.d0
        S2(6,5,ix) = (rt15*(-2d0*czy - 14d0*raz*rby))/8.d0
        S2(13,4,ix) = (rt15*rbz)/4.d0
        S2(13,6,ix) = (rt15*rbx)/4.d0
        S2(14,5,ix) = -(rt15*rbz)/4.d0
        S2(14,6,ix) = -(rt15*rby)/4.d0
        S2(15,4,ix) = (rt15*rbx)/4.d0
        S2(15,5,ix) = -(rt15*rby)/4.d0
      end if
    end if

  case(84)
    !  84 (84)   10  32s 4
    S0(ix) = (rt15*(czz*rbx*rby + (czy*rbx + (czx +                     &
        7d0*raz*rbx)*rby)*rbz))/4.d0
    if (level > 1) then
      S1(3,ix) = (7d0*rt15*rbx*rby*rbz)/4.d0
      S1(4,ix) = (rt15*(czz*rby + (czy + 7d0*raz*rby)*rbz))/4.d0
      S1(5,ix) = (rt15*(czz*rbx + (czx + 7d0*raz*rbx)*rbz))/4.d0
      S1(6,ix) = (rt15*(czy*rbx + (czx + 7d0*raz*rbx)*rby))/4.d0
      S1(13,ix) = (rt15*rby*rbz)/4.d0
      S1(14,ix) = (rt15*rbx*rbz)/4.d0
      S1(15,ix) = (rt15*rbx*rby)/4.d0
      if (level > 2) then
        S2(4,3,ix) = (7d0*rt15*rby*rbz)/4.d0
        S2(5,3,ix) = (7d0*rt15*rbx*rbz)/4.d0
        S2(5,4,ix) = (rt15*(czz + 7d0*raz*rbz))/4.d0
        S2(6,3,ix) = (7d0*rt15*rbx*rby)/4.d0
        S2(6,4,ix) = (rt15*(czy + 7d0*raz*rby))/4.d0
        S2(6,5,ix) = (rt15*(czx + 7d0*raz*rbx))/4.d0
        S2(13,5,ix) = (rt15*rbz)/4.d0
        S2(13,6,ix) = (rt15*rby)/4.d0
        S2(14,4,ix) = (rt15*rbz)/4.d0
        S2(14,6,ix) = (rt15*rbx)/4.d0
        S2(15,4,ix) = (rt15*rby)/4.d0
        S2(15,5,ix) = (rt15*rbx)/4.d0
      end if
    end if

  case(85)
    !  85 (85)   10  33c 4
    S0(ix) = (Sqrt(2.5d0)*(3d0*czx*(rbx**2 - rby**2) +                  &
        rbx*(-6d0*czy*rby + 7d0*raz*(rbx**2 - 3d0*rby**2))))/8.d0
    if (level > 1) then
      S1(3,ix) = (7d0*Sqrt(2.5d0)*rbx*(rbx**2 - 3d0*rby**2))/8.d0
      S1(4,ix) = (Sqrt(2.5d0)*(6d0*czx*rbx + 14d0*raz*rbx**2 -          &
          6d0*czy*rby + 7d0*raz*(rbx**2 - 3d0*rby**2)))/8.d0
      S1(5,ix) = (Sqrt(2.5d0)*(-6d0*czx*rby + rbx*(-6d0*czy -           &
          42d0*raz*rby)))/8.d0
      S1(13,ix) = (3d0*Sqrt(2.5d0)*(rbx**2 - rby**2))/8.d0
      S1(14,ix) = (-3d0*Sqrt(2.5d0)*rbx*rby)/4.d0
      if (level > 2) then
        S2(4,3,ix) = (Sqrt(2.5d0)*(14d0*rbx**2 + 7d0*(rbx**2 -          &
            3d0*rby**2)))/8.d0
        S2(4,4,ix) = (Sqrt(2.5d0)*(6d0*czx + 42d0*raz*rbx))/8.d0
        S2(5,3,ix) = (-21d0*Sqrt(2.5d0)*rbx*rby)/4.d0
        S2(5,4,ix) = (Sqrt(2.5d0)*(-6d0*czy - 42d0*raz*rby))/8.d0
        S2(5,5,ix) = (Sqrt(2.5d0)*(-6d0*czx - 42d0*raz*rbx))/8.d0
        S2(13,4,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(13,5,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(14,4,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(14,5,ix) = (-3d0*Sqrt(2.5d0)*rbx)/4.d0
      end if
    end if

  case(86)
    !  86 (86)   10  33s 4
    S0(ix) = (Sqrt(2.5d0)*(3d0*czy*(rbx**2 - rby**2) + rby*(6d0*czx*rbx &
        + 7d0*raz*(3d0*rbx**2 - rby**2))))/8.d0
    if (level > 1) then
      S1(3,ix) = (7d0*Sqrt(2.5d0)*rby*(3d0*rbx**2 - rby**2))/8.d0
      S1(4,ix) = (Sqrt(2.5d0)*(6d0*czy*rbx + (6d0*czx +                 &
          42d0*raz*rbx)*rby))/8.d0
      S1(5,ix) = (Sqrt(2.5d0)*(6d0*czx*rbx - 6d0*czy*rby -              &
          14d0*raz*rby**2 + 7d0*raz*(3d0*rbx**2 - rby**2)))/8.d0
      S1(13,ix) = (3d0*Sqrt(2.5d0)*rbx*rby)/4.d0
      S1(14,ix) = (3d0*Sqrt(2.5d0)*(rbx**2 - rby**2))/8.d0
      if (level > 2) then
        S2(4,3,ix) = (21d0*Sqrt(2.5d0)*rbx*rby)/4.d0
        S2(4,4,ix) = (Sqrt(2.5d0)*(6d0*czy + 42d0*raz*rby))/8.d0
        S2(5,3,ix) = (Sqrt(2.5d0)*(-14d0*rby**2 + 7d0*(3d0*rbx**2 -     &
            rby**2)))/8.d0
        S2(5,4,ix) = (Sqrt(2.5d0)*(6d0*czx + 42d0*raz*rbx))/8.d0
        S2(5,5,ix) = (Sqrt(2.5d0)*(-6d0*czy - 42d0*raz*rby))/8.d0
        S2(13,4,ix) = (3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(13,5,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(14,4,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(14,5,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
      end if
    end if

  case(87)
    !  87 (87)   11c 30  4
    S0(ix) = (3d0*cxz*(-1d0 + 5d0*rbz**2) + 5d0*rax*rbz*(-3d0 +         &
        7d0*rbz**2))/8.d0
    if (level > 1) then
      S1(1,ix) = (5d0*rbz*(-3d0 + 7d0*rbz**2))/8.d0
      S1(6,ix) = (30d0*cxz*rbz + 70d0*rax*rbz**2 + 5d0*rax*(-3d0 +      &
          7d0*rbz**2))/8.d0
      S1(9,ix) = (3d0*(-1d0 + 5d0*rbz**2))/8.d0
      if (level > 2) then
        S2(6,1,ix) = (70d0*rbz**2 + 5d0*(-3d0 + 7d0*rbz**2))/8.d0
        S2(6,6,ix) = (30d0*cxz + 210d0*rax*rbz)/8.d0
        S2(9,6,ix) = (15d0*rbz)/4.d0
      end if
    end if

  case(88)
    !  88 (88)   11c 31c 4
    S0(ix) = (Sqrt(1.5d0)*(cxx*(-1d0 + 5d0*rbz**2) +                    &
        5d0*rbx*(2d0*cxz*rbz + rax*(-1d0 + 7d0*rbz**2))))/8.d0
    if (level > 1) then
      S1(1,ix) = (5d0*Sqrt(1.5d0)*rbx*(-1d0 + 7d0*rbz**2))/8.d0
      S1(4,ix) = (5d0*Sqrt(1.5d0)*(2d0*cxz*rbz + rax*(-1d0 +            &
          7d0*rbz**2)))/8.d0
      S1(6,ix) = (Sqrt(1.5d0)*(10d0*cxx*rbz + 5d0*rbx*(2d0*cxz +        &
          14d0*rax*rbz)))/8.d0
      S1(7,ix) = (Sqrt(1.5d0)*(-1d0 + 5d0*rbz**2))/8.d0
      S1(9,ix) = (5d0*Sqrt(1.5d0)*rbx*rbz)/4.d0
      if (level > 2) then
        S2(4,1,ix) = (5d0*Sqrt(1.5d0)*(-1d0 + 7d0*rbz**2))/8.d0
        S2(6,1,ix) = (35d0*Sqrt(1.5d0)*rbx*rbz)/4.d0
        S2(6,4,ix) = (5d0*Sqrt(1.5d0)*(2d0*cxz + 14d0*rax*rbz))/8.d0
        S2(6,6,ix) = (Sqrt(1.5d0)*(10d0*cxx + 70d0*rax*rbx))/8.d0
        S2(7,6,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(9,4,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(9,6,ix) = (5d0*Sqrt(1.5d0)*rbx)/4.d0
      end if
    end if

  case(89)
    !  89 (89)   11c 31s 4
    S0(ix) = (Sqrt(1.5d0)*(cxy*(-1d0 + 5d0*rbz**2) +                    &
        5d0*rby*(2d0*cxz*rbz + rax*(-1d0 + 7d0*rbz**2))))/8.d0
    if (level > 1) then
      S1(1,ix) = (5d0*Sqrt(1.5d0)*rby*(-1d0 + 7d0*rbz**2))/8.d0
      S1(5,ix) = (5d0*Sqrt(1.5d0)*(2d0*cxz*rbz + rax*(-1d0 +            &
          7d0*rbz**2)))/8.d0
      S1(6,ix) = (Sqrt(1.5d0)*(10d0*cxy*rbz + 5d0*rby*(2d0*cxz +        &
          14d0*rax*rbz)))/8.d0
      S1(8,ix) = (Sqrt(1.5d0)*(-1d0 + 5d0*rbz**2))/8.d0
      S1(9,ix) = (5d0*Sqrt(1.5d0)*rby*rbz)/4.d0
      if (level > 2) then
        S2(5,1,ix) = (5d0*Sqrt(1.5d0)*(-1d0 + 7d0*rbz**2))/8.d0
        S2(6,1,ix) = (35d0*Sqrt(1.5d0)*rby*rbz)/4.d0
        S2(6,5,ix) = (5d0*Sqrt(1.5d0)*(2d0*cxz + 14d0*rax*rbz))/8.d0
        S2(6,6,ix) = (Sqrt(1.5d0)*(10d0*cxy + 70d0*rax*rby))/8.d0
        S2(8,6,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(9,5,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(9,6,ix) = (5d0*Sqrt(1.5d0)*rby)/4.d0
      end if
    end if

  case(90)
    !  90 (90)   11c 32c 4
    S0(ix) = (rt15*(cxz*(rbx**2 - rby**2) + (2d0*cxx*rbx - 2d0*cxy*rby  &
        + 7d0*rax*(rbx**2 - rby**2))*rbz))/8.d0
    if (level > 1) then
      S1(1,ix) = (7d0*rt15*(rbx**2 - rby**2)*rbz)/8.d0
      S1(4,ix) = (rt15*(2d0*cxz*rbx + (2d0*cxx +                        &
          14d0*rax*rbx)*rbz))/8.d0
      S1(5,ix) = (rt15*(-2d0*cxz*rby + (-2d0*cxy -                      &
          14d0*rax*rby)*rbz))/8.d0
      S1(6,ix) = (rt15*(2d0*cxx*rbx - 2d0*cxy*rby + 7d0*rax*(rbx**2 -   &
          rby**2)))/8.d0
      S1(7,ix) = (rt15*rbx*rbz)/4.d0
      S1(8,ix) = -(rt15*rby*rbz)/4.d0
      S1(9,ix) = (rt15*(rbx**2 - rby**2))/8.d0
      if (level > 2) then
        S2(4,1,ix) = (7d0*rt15*rbx*rbz)/4.d0
        S2(4,4,ix) = (rt15*(2d0*cxz + 14d0*rax*rbz))/8.d0
        S2(5,1,ix) = (-7d0*rt15*rby*rbz)/4.d0
        S2(5,5,ix) = (rt15*(-2d0*cxz - 14d0*rax*rbz))/8.d0
        S2(6,1,ix) = (7d0*rt15*(rbx**2 - rby**2))/8.d0
        S2(6,4,ix) = (rt15*(2d0*cxx + 14d0*rax*rbx))/8.d0
        S2(6,5,ix) = (rt15*(-2d0*cxy - 14d0*rax*rby))/8.d0
        S2(7,4,ix) = (rt15*rbz)/4.d0
        S2(7,6,ix) = (rt15*rbx)/4.d0
        S2(8,5,ix) = -(rt15*rbz)/4.d0
        S2(8,6,ix) = -(rt15*rby)/4.d0
        S2(9,4,ix) = (rt15*rbx)/4.d0
        S2(9,5,ix) = -(rt15*rby)/4.d0
      end if
    end if

  case(91)
    !  91 (91)   11c 32s 4
    S0(ix) = (rt15*(cxz*rbx*rby + (cxy*rbx + (cxx +                     &
        7d0*rax*rbx)*rby)*rbz))/4.d0
    if (level > 1) then
      S1(1,ix) = (7d0*rt15*rbx*rby*rbz)/4.d0
      S1(4,ix) = (rt15*(cxz*rby + (cxy + 7d0*rax*rby)*rbz))/4.d0
      S1(5,ix) = (rt15*(cxz*rbx + (cxx + 7d0*rax*rbx)*rbz))/4.d0
      S1(6,ix) = (rt15*(cxy*rbx + (cxx + 7d0*rax*rbx)*rby))/4.d0
      S1(7,ix) = (rt15*rby*rbz)/4.d0
      S1(8,ix) = (rt15*rbx*rbz)/4.d0
      S1(9,ix) = (rt15*rbx*rby)/4.d0
      if (level > 2) then
        S2(4,1,ix) = (7d0*rt15*rby*rbz)/4.d0
        S2(5,1,ix) = (7d0*rt15*rbx*rbz)/4.d0
        S2(5,4,ix) = (rt15*(cxz + 7d0*rax*rbz))/4.d0
        S2(6,1,ix) = (7d0*rt15*rbx*rby)/4.d0
        S2(6,4,ix) = (rt15*(cxy + 7d0*rax*rby))/4.d0
        S2(6,5,ix) = (rt15*(cxx + 7d0*rax*rbx))/4.d0
        S2(7,5,ix) = (rt15*rbz)/4.d0
        S2(7,6,ix) = (rt15*rby)/4.d0
        S2(8,4,ix) = (rt15*rbz)/4.d0
        S2(8,6,ix) = (rt15*rbx)/4.d0
        S2(9,4,ix) = (rt15*rby)/4.d0
        S2(9,5,ix) = (rt15*rbx)/4.d0
      end if
    end if

  case(92)
    !  92 (92)   11c 33c 4
    S0(ix) = (Sqrt(2.5d0)*(3d0*cxx*(rbx**2 - rby**2) +                  &
        rbx*(-6d0*cxy*rby + 7d0*rax*(rbx**2 - 3d0*rby**2))))/8.d0
    if (level > 1) then
      S1(1,ix) = (7d0*Sqrt(2.5d0)*rbx*(rbx**2 - 3d0*rby**2))/8.d0
      S1(4,ix) = (Sqrt(2.5d0)*(6d0*cxx*rbx + 14d0*rax*rbx**2 -          &
          6d0*cxy*rby + 7d0*rax*(rbx**2 - 3d0*rby**2)))/8.d0
      S1(5,ix) = (Sqrt(2.5d0)*(-6d0*cxx*rby + rbx*(-6d0*cxy -           &
          42d0*rax*rby)))/8.d0
      S1(7,ix) = (3d0*Sqrt(2.5d0)*(rbx**2 - rby**2))/8.d0
      S1(8,ix) = (-3d0*Sqrt(2.5d0)*rbx*rby)/4.d0
      if (level > 2) then
        S2(4,1,ix) = (Sqrt(2.5d0)*(14d0*rbx**2 + 7d0*(rbx**2 -          &
            3d0*rby**2)))/8.d0
        S2(4,4,ix) = (Sqrt(2.5d0)*(6d0*cxx + 42d0*rax*rbx))/8.d0
        S2(5,1,ix) = (-21d0*Sqrt(2.5d0)*rbx*rby)/4.d0
        S2(5,4,ix) = (Sqrt(2.5d0)*(-6d0*cxy - 42d0*rax*rby))/8.d0
        S2(5,5,ix) = (Sqrt(2.5d0)*(-6d0*cxx - 42d0*rax*rbx))/8.d0
        S2(7,4,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(7,5,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(8,4,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(8,5,ix) = (-3d0*Sqrt(2.5d0)*rbx)/4.d0
      end if
    end if

  case(93)
    !  93 (93)   11c 33s 4
    S0(ix) = (Sqrt(2.5d0)*(3d0*cxy*(rbx**2 - rby**2) + rby*(6d0*cxx*rbx &
        + 7d0*rax*(3d0*rbx**2 - rby**2))))/8.d0
    if (level > 1) then
      S1(1,ix) = (7d0*Sqrt(2.5d0)*rby*(3d0*rbx**2 - rby**2))/8.d0
      S1(4,ix) = (Sqrt(2.5d0)*(6d0*cxy*rbx + (6d0*cxx +                 &
          42d0*rax*rbx)*rby))/8.d0
      S1(5,ix) = (Sqrt(2.5d0)*(6d0*cxx*rbx - 6d0*cxy*rby -              &
          14d0*rax*rby**2 + 7d0*rax*(3d0*rbx**2 - rby**2)))/8.d0
      S1(7,ix) = (3d0*Sqrt(2.5d0)*rbx*rby)/4.d0
      S1(8,ix) = (3d0*Sqrt(2.5d0)*(rbx**2 - rby**2))/8.d0
      if (level > 2) then
        S2(4,1,ix) = (21d0*Sqrt(2.5d0)*rbx*rby)/4.d0
        S2(4,4,ix) = (Sqrt(2.5d0)*(6d0*cxy + 42d0*rax*rby))/8.d0
        S2(5,1,ix) = (Sqrt(2.5d0)*(-14d0*rby**2 + 7d0*(3d0*rbx**2 -     &
            rby**2)))/8.d0
        S2(5,4,ix) = (Sqrt(2.5d0)*(6d0*cxx + 42d0*rax*rbx))/8.d0
        S2(5,5,ix) = (Sqrt(2.5d0)*(-6d0*cxy - 42d0*rax*rby))/8.d0
        S2(7,4,ix) = (3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(7,5,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(8,4,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(8,5,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
      end if
    end if

  case(94)
    !  94 (94)   11s 30  4
    S0(ix) = (3d0*cyz*(-1d0 + 5d0*rbz**2) + 5d0*ray*rbz*(-3d0 +         &
        7d0*rbz**2))/8.d0
    if (level > 1) then
      S1(2,ix) = (5d0*rbz*(-3d0 + 7d0*rbz**2))/8.d0
      S1(6,ix) = (30d0*cyz*rbz + 70d0*ray*rbz**2 + 5d0*ray*(-3d0 +      &
          7d0*rbz**2))/8.d0
      S1(12,ix) = (3d0*(-1d0 + 5d0*rbz**2))/8.d0
      if (level > 2) then
        S2(6,2,ix) = (70d0*rbz**2 + 5d0*(-3d0 + 7d0*rbz**2))/8.d0
        S2(6,6,ix) = (30d0*cyz + 210d0*ray*rbz)/8.d0
        S2(12,6,ix) = (15d0*rbz)/4.d0
      end if
    end if

  case(95)
    !  95 (95)   11s 31c 4
    S0(ix) = (Sqrt(1.5d0)*(cyx*(-1d0 + 5d0*rbz**2) +                    &
        5d0*rbx*(2d0*cyz*rbz + ray*(-1d0 + 7d0*rbz**2))))/8.d0
    if (level > 1) then
      S1(2,ix) = (5d0*Sqrt(1.5d0)*rbx*(-1d0 + 7d0*rbz**2))/8.d0
      S1(4,ix) = (5d0*Sqrt(1.5d0)*(2d0*cyz*rbz + ray*(-1d0 +            &
          7d0*rbz**2)))/8.d0
      S1(6,ix) = (Sqrt(1.5d0)*(10d0*cyx*rbz + 5d0*rbx*(2d0*cyz +        &
          14d0*ray*rbz)))/8.d0
      S1(10,ix) = (Sqrt(1.5d0)*(-1d0 + 5d0*rbz**2))/8.d0
      S1(12,ix) = (5d0*Sqrt(1.5d0)*rbx*rbz)/4.d0
      if (level > 2) then
        S2(4,2,ix) = (5d0*Sqrt(1.5d0)*(-1d0 + 7d0*rbz**2))/8.d0
        S2(6,2,ix) = (35d0*Sqrt(1.5d0)*rbx*rbz)/4.d0
        S2(6,4,ix) = (5d0*Sqrt(1.5d0)*(2d0*cyz + 14d0*ray*rbz))/8.d0
        S2(6,6,ix) = (Sqrt(1.5d0)*(10d0*cyx + 70d0*ray*rbx))/8.d0
        S2(10,6,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(12,4,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(12,6,ix) = (5d0*Sqrt(1.5d0)*rbx)/4.d0
      end if
    end if

  case(96)
    !  96 (96)   11s 31s 4
    S0(ix) = (Sqrt(1.5d0)*(cyy*(-1d0 + 5d0*rbz**2) +                    &
        5d0*rby*(2d0*cyz*rbz + ray*(-1d0 + 7d0*rbz**2))))/8.d0
    if (level > 1) then
      S1(2,ix) = (5d0*Sqrt(1.5d0)*rby*(-1d0 + 7d0*rbz**2))/8.d0
      S1(5,ix) = (5d0*Sqrt(1.5d0)*(2d0*cyz*rbz + ray*(-1d0 +            &
          7d0*rbz**2)))/8.d0
      S1(6,ix) = (Sqrt(1.5d0)*(10d0*cyy*rbz + 5d0*rby*(2d0*cyz +        &
          14d0*ray*rbz)))/8.d0
      S1(11,ix) = (Sqrt(1.5d0)*(-1d0 + 5d0*rbz**2))/8.d0
      S1(12,ix) = (5d0*Sqrt(1.5d0)*rby*rbz)/4.d0
      if (level > 2) then
        S2(5,2,ix) = (5d0*Sqrt(1.5d0)*(-1d0 + 7d0*rbz**2))/8.d0
        S2(6,2,ix) = (35d0*Sqrt(1.5d0)*rby*rbz)/4.d0
        S2(6,5,ix) = (5d0*Sqrt(1.5d0)*(2d0*cyz + 14d0*ray*rbz))/8.d0
        S2(6,6,ix) = (Sqrt(1.5d0)*(10d0*cyy + 70d0*ray*rby))/8.d0
        S2(11,6,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(12,5,ix) = (5d0*Sqrt(1.5d0)*rbz)/4.d0
        S2(12,6,ix) = (5d0*Sqrt(1.5d0)*rby)/4.d0
      end if
    end if

  case(97)
    !  97 (97)   11s 32c 4
    S0(ix) = (rt15*(cyz*(rbx**2 - rby**2) + (2d0*cyx*rbx - 2d0*cyy*rby  &
        + 7d0*ray*(rbx**2 - rby**2))*rbz))/8.d0
    if (level > 1) then
      S1(2,ix) = (7d0*rt15*(rbx**2 - rby**2)*rbz)/8.d0
      S1(4,ix) = (rt15*(2d0*cyz*rbx + (2d0*cyx +                        &
          14d0*ray*rbx)*rbz))/8.d0
      S1(5,ix) = (rt15*(-2d0*cyz*rby + (-2d0*cyy -                      &
          14d0*ray*rby)*rbz))/8.d0
      S1(6,ix) = (rt15*(2d0*cyx*rbx - 2d0*cyy*rby + 7d0*ray*(rbx**2 -   &
          rby**2)))/8.d0
      S1(10,ix) = (rt15*rbx*rbz)/4.d0
      S1(11,ix) = -(rt15*rby*rbz)/4.d0
      S1(12,ix) = (rt15*(rbx**2 - rby**2))/8.d0
      if (level > 2) then
        S2(4,2,ix) = (7d0*rt15*rbx*rbz)/4.d0
        S2(4,4,ix) = (rt15*(2d0*cyz + 14d0*ray*rbz))/8.d0
        S2(5,2,ix) = (-7d0*rt15*rby*rbz)/4.d0
        S2(5,5,ix) = (rt15*(-2d0*cyz - 14d0*ray*rbz))/8.d0
        S2(6,2,ix) = (7d0*rt15*(rbx**2 - rby**2))/8.d0
        S2(6,4,ix) = (rt15*(2d0*cyx + 14d0*ray*rbx))/8.d0
        S2(6,5,ix) = (rt15*(-2d0*cyy - 14d0*ray*rby))/8.d0
        S2(10,4,ix) = (rt15*rbz)/4.d0
        S2(10,6,ix) = (rt15*rbx)/4.d0
        S2(11,5,ix) = -(rt15*rbz)/4.d0
        S2(11,6,ix) = -(rt15*rby)/4.d0
        S2(12,4,ix) = (rt15*rbx)/4.d0
        S2(12,5,ix) = -(rt15*rby)/4.d0
      end if
    end if

  case(98)
    !  98 (98)   11s 32s 4
    S0(ix) = (rt15*(cyz*rbx*rby + (cyy*rbx + (cyx +                     &
        7d0*ray*rbx)*rby)*rbz))/4.d0
    if (level > 1) then
      S1(2,ix) = (7d0*rt15*rbx*rby*rbz)/4.d0
      S1(4,ix) = (rt15*(cyz*rby + (cyy + 7d0*ray*rby)*rbz))/4.d0
      S1(5,ix) = (rt15*(cyz*rbx + (cyx + 7d0*ray*rbx)*rbz))/4.d0
      S1(6,ix) = (rt15*(cyy*rbx + (cyx + 7d0*ray*rbx)*rby))/4.d0
      S1(10,ix) = (rt15*rby*rbz)/4.d0
      S1(11,ix) = (rt15*rbx*rbz)/4.d0
      S1(12,ix) = (rt15*rbx*rby)/4.d0
      if (level > 2) then
        S2(4,2,ix) = (7d0*rt15*rby*rbz)/4.d0
        S2(5,2,ix) = (7d0*rt15*rbx*rbz)/4.d0
        S2(5,4,ix) = (rt15*(cyz + 7d0*ray*rbz))/4.d0
        S2(6,2,ix) = (7d0*rt15*rbx*rby)/4.d0
        S2(6,4,ix) = (rt15*(cyy + 7d0*ray*rby))/4.d0
        S2(6,5,ix) = (rt15*(cyx + 7d0*ray*rbx))/4.d0
        S2(10,5,ix) = (rt15*rbz)/4.d0
        S2(10,6,ix) = (rt15*rby)/4.d0
        S2(11,4,ix) = (rt15*rbz)/4.d0
        S2(11,6,ix) = (rt15*rbx)/4.d0
        S2(12,4,ix) = (rt15*rby)/4.d0
        S2(12,5,ix) = (rt15*rbx)/4.d0
      end if
    end if

  case(99)
    !  99 (99)   11s 33c 4
    S0(ix) = (Sqrt(2.5d0)*(3d0*cyx*(rbx**2 - rby**2) +                  &
        rbx*(-6d0*cyy*rby + 7d0*ray*(rbx**2 - 3d0*rby**2))))/8.d0
    if (level > 1) then
      S1(2,ix) = (7d0*Sqrt(2.5d0)*rbx*(rbx**2 - 3d0*rby**2))/8.d0
      S1(4,ix) = (Sqrt(2.5d0)*(6d0*cyx*rbx + 14d0*ray*rbx**2 -          &
          6d0*cyy*rby + 7d0*ray*(rbx**2 - 3d0*rby**2)))/8.d0
      S1(5,ix) = (Sqrt(2.5d0)*(-6d0*cyx*rby + rbx*(-6d0*cyy -           &
          42d0*ray*rby)))/8.d0
      S1(10,ix) = (3d0*Sqrt(2.5d0)*(rbx**2 - rby**2))/8.d0
      S1(11,ix) = (-3d0*Sqrt(2.5d0)*rbx*rby)/4.d0
      if (level > 2) then
        S2(4,2,ix) = (Sqrt(2.5d0)*(14d0*rbx**2 + 7d0*(rbx**2 -          &
            3d0*rby**2)))/8.d0
        S2(4,4,ix) = (Sqrt(2.5d0)*(6d0*cyx + 42d0*ray*rbx))/8.d0
        S2(5,2,ix) = (-21d0*Sqrt(2.5d0)*rbx*rby)/4.d0
        S2(5,4,ix) = (Sqrt(2.5d0)*(-6d0*cyy - 42d0*ray*rby))/8.d0
        S2(5,5,ix) = (Sqrt(2.5d0)*(-6d0*cyx - 42d0*ray*rbx))/8.d0
        S2(10,4,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(10,5,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(11,4,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(11,5,ix) = (-3d0*Sqrt(2.5d0)*rbx)/4.d0
      end if
    end if

  case(100)
    !  100 (100)   11s 33s 4
    S0(ix) = (Sqrt(2.5d0)*(3d0*cyy*(rbx**2 - rby**2) + rby*(6d0*cyx*rbx &
        + 7d0*ray*(3d0*rbx**2 - rby**2))))/8.d0
    if (level > 1) then
      S1(2,ix) = (7d0*Sqrt(2.5d0)*rby*(3d0*rbx**2 - rby**2))/8.d0
      S1(4,ix) = (Sqrt(2.5d0)*(6d0*cyy*rbx + (6d0*cyx +                 &
          42d0*ray*rbx)*rby))/8.d0
      S1(5,ix) = (Sqrt(2.5d0)*(6d0*cyx*rbx - 6d0*cyy*rby -              &
          14d0*ray*rby**2 + 7d0*ray*(3d0*rbx**2 - rby**2)))/8.d0
      S1(10,ix) = (3d0*Sqrt(2.5d0)*rbx*rby)/4.d0
      S1(11,ix) = (3d0*Sqrt(2.5d0)*(rbx**2 - rby**2))/8.d0
      if (level > 2) then
        S2(4,2,ix) = (21d0*Sqrt(2.5d0)*rbx*rby)/4.d0
        S2(4,4,ix) = (Sqrt(2.5d0)*(6d0*cyy + 42d0*ray*rby))/8.d0
        S2(5,2,ix) = (Sqrt(2.5d0)*(-14d0*rby**2 + 7d0*(3d0*rbx**2 -     &
            rby**2)))/8.d0
        S2(5,4,ix) = (Sqrt(2.5d0)*(6d0*cyx + 42d0*ray*rbx))/8.d0
        S2(5,5,ix) = (Sqrt(2.5d0)*(-6d0*cyy - 42d0*ray*rby))/8.d0
        S2(10,4,ix) = (3d0*Sqrt(2.5d0)*rby)/4.d0
        S2(10,5,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(11,4,ix) = (3d0*Sqrt(2.5d0)*rbx)/4.d0
        S2(11,5,ix) = (-3d0*Sqrt(2.5d0)*rby)/4.d0
      end if
    end if
