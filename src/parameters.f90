MODULE parameters
!  Parameters:    MAXLQ  maximum rank of multipole moments
!  Derived value: SQ     size of multipole moment arrays
INTEGER, PARAMETER :: MAXLQ=5
INTEGER, PARAMETER :: SQ=(MAXLQ+1)**2
END MODULE parameters
