#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

import subprocess
import sys

rc = 0
rc += subprocess.call(["../cmp_dump.py", "dump.min"])
rc += subprocess.call(["../cmp_freq.py", "min.freq"])
rc += subprocess.call(["../cmp_freq.py", "ts.freq"])
rc += subprocess.call(["../cmp_xyz.py", "min.xyz"])
rc += subprocess.call(["../cmp_xyz.py", "ts.xyz"])
rc += subprocess.call(["./cmp_out.py", "out"])

# print "rc = ", rc
sys.exit(rc)

